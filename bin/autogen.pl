#!/usr/bin/perl
# 
$PI=3.14159265358979323846;
$SLICES32=32;
$SLICES36=36;
$SLICES24=24;
$LENGTH=0xffff;
$AUTOGEN_CPP="../stoolkit/SAutogen.cpp";
$AUTOGEN_H="../stoolkit/SAutogen.h";

die "can not open open $AUTOGEN_CPP" unless (open (C, "> $AUTOGEN_CPP"));
die "can not open open $AUTOGEN_H" unless (open (H, "> $AUTOGEN_H"));

print C <<EOD;
/**
 * Autogenerated by $0. 
 * UNIT=$LENGTH
 */

/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2005  Gaspar Sinai <gsinai\@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stoolkit/SAutogen.h>
EOD

print H <<EOD;
/**
 * Autogenerated by $0. Rotated unit vector around the clock. 
 */

/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2005  Gaspar Sinai <gsinai\@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef SAutogen_h
#define SAutogen_h
#include <stoolkit/SLineCurve.h>

EOD

$SLICES=24;
&printFile();
$SLICES=32;
&printFile();
$SLICES=36;
&printFile();

print H<<EOD;
#endif /*SAutogen_h*/
EOD
close (H);
close (C);

exit (0);

sub
printFile
{

print C <<EOD;

/**
 * Rotated unit vector around the clock, from 12:00 clockwise. 
 * The coordinate system follows the screen coordinates.
 * SLICES=$SLICES 
 * \@return a full set of vectors, to make a closed curve.
 */
SLineCurve
getCurveCircle$SLICES (void)
{
  SLineCurve lc;
EOD
for ($i=0; $i<$SLICES +1; $i++)
{
   $slice = $i * 2 * $PI / $SLICES;
   # Make it clockwise
   # and follow screen coords.
   $s = int (sin ($slice) * $LENGTH);
   $c = -int (cos ($slice) * $LENGTH);
   print C "  lc.append (SLocation ($s,$c)); // $i/$SLICES\n";
}
print C <<EOD;
  return SLineCurve(lc);
}
EOD

print H <<EOD;
/**
 * Rotated unit vector around the clock, from 12:00 clockwise. 
 * The coordinate system follows the screen coordinates.
 * \@return a full set of vectors, to make a closed curve.
 */
SLineCurve getCurveCircle$SLICES (void);

EOD
}
