#!/usr/bin/perl

#  Yudit Unicode Editor Source File
#
#  Copyright (C) 2000  Gaspar Sinai <gsinai@yudit.org>  
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
# 
#  This script makes a compressed precompose map.
#  Compressed means: for a range only the first one will be 
#  encoded.
#  In order to run the script you need to get
#   ftp://ftp.unicode.org/Public/UNIDATA/UnicodeData.txt
#  as and input.
#  encode composing chars -> precomposed char
#  decode precomposed char -> composing chars

%GeneralCategory = (
"Lu", 1,
"Ll", 2,
"Lt", 3,
"Mn", 4,
"Mc", 5,
"Me", 6,
"Nd", 7,
"Nl", 8,
"No", 9,
"Zs", 10,
"Zl", 11,
"Zp", 12,
"Cc", 13,
"Cf", 14,
"Cs", 15,
"Co", 16,
"Cn", 17,

# Informative Categories

"Lm", 18,
"Lo", 19,
"Pc", 20,
"Pd", 21,
"Ps", 22,
"Pe", 23,
"Pi", 24,
"Pf", 25,
"Po", 26,
"Sm", 27,
"Sc", 28,
"Sk", 29,
"So", 30
);

@encodes = ();
@decodes = ();

@full_date = localtime(time);
$year = $full_date[5] + 1900;
$mon = $full_date[4] + 1;
$mday = $full_date[3];
$date = sprintf ("%4d-%02d-%02d", $year, $mon, $mday);

die "No version specified." if ($#ARGV == -1);
$VERSION=shift;

print <<EOD;
NAME=charclass.my
COMM=
COMM= generated by $0 $date
COMM= 
COMM= from ftp://ftp.unicode.org/Public/$VERSION/uc/UnicodeData.txt 
COMM=

#  01 Lu Letter, Uppercase
#  02 Ll Letter, Lowercase
#  03 Lt Letter, Titlecase
#  04 Mn Mark, Non-Spacing
#  05 Mc Mark, Spacing Combining
#  06 Me Mark, Enclosing
#  07 Nd Number, Decimal Digit
#  08 Nl Number, Letter
#  09 No Number, Other
#  0A Zs Separator, Space
#  0B Zl Separator, Line
#  0C Zp Separator, Paragraph
#  0D Cc Other, Control
#  0E Cf Other, Format
#  0F Cs Other, Surrogate
#  10 Co Other, Private Use
#  11 Cn Other, Not Assigned (no characters in the file have this property)

# Informative Categories

#  12 Lm Letter, Modifier
#  13 Lo Letter, Other
#  14 Pc Punctuation, Connector
#  15 Pd Punctuation, Dash
#  16 Ps Punctuation, Open
#  17 Pe Punctuation, Close
#  18 Pi Punctuation, Initial quote(may behave like Ps or Pe depending on usage)
#  19 Pf Punctuation, Final quote (may behave like Ps or Pe depending on usage)
#  1A Po Punctuation, Other
#  1B Sm Symbol, Math
#  1C Sc Symbol, Currency
#  1D Sk Symbol, Modifier
#  1E So Symbol, Other
#
# This ia a compressed map: that means all characters in the range are
# represented by the first element only. Holes are represented with 0.
#
COMM=Informative Categories
COMM=Compressed Range Format
TYPE=0
SECTION=encode
ENCODE=1
#
# key 2 for 32 bit (32-bitunicode)
# value 0 for 8 bit (charclass) values 
#
KEY_WIDTH=2
VALUE_WIDTH=0
KEY_LENGTH=0
VALUE_LENGTH=0
EOD


$last = -1;
$lastvle = -2;

while (<>)
{
  @_ = split(';', $_);
  next if ($#_ < 6);

  $plain = hex $_[0];
  $general = $_[2];
  if (!defined ($GeneralCategory {$general}))
  {
    $general = 0;
  }
  else
  {
    $general = $GeneralCategory {$general};
  }
  if ($last+1 == $plain && $lastvle == $general)
  {
    $last++;
    next;
  }  
  # hole
  if ($last+1 < $plain && $_[1]!~/Last>/)
  {
    printf ("%08X -> %02X$/", $last+1, 0);
  }
  printf ("%08X -> %02X$/", $plain, $general);
  $last = $plain;
  $lastvle = $general;
}
printf ("%08X -> %02X$/", $last+1, 0);

print "$/#END$/";
