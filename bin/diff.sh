#!/bin/sh
ORIG=yudit-2.9.1.beta4
NEW=yudit-2.9.1.beta5
diff --exclude=*.kmap --exclude=*.my --exclude=*.mo \
  --exclude=config.status --exclude=config.cache \
  --context=2 --new-file --recursive \
  $ORIG $NEW
