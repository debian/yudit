#!/usr/bin/perl
$SCALE_X = 1.0;
$SCALE_Y = 1.0;
#1. $TRANS_X =  10;
#$TRANS_X = 240;
$TRANS_X = 0;

$TRANS_Y = 0;

while (<>)
{
 if (/\/\S \{ \%/)
 {
  print "SD_BEGIN_GLYPH, /*" . $_ . "*/";
 }
 elsif (/closepath (\S+) (\S+) moveto/)
 {
   $x = $1;
   $y = $2;
   print " SD_CLOSE_PATH,\n";
   print "  SD_MOVE_TO, $x, $y,\n";
 }
 elsif (/(\S+) (\S+) moveto/)
 {
   $x = $1/$SCALE_X+$TRANS_X;
   $y = $2/$SCALE_Y+$TRANS_Y;
   print "  SD_MOVE_TO, $x, $y,\n";
 }
 elsif (/closepath fill/)
 {
   print " SD_CLOSE_PATH,\n";
   print "SD_END_GLYPH,\n";
 }
 elsif (/closepath/)
 {
   print "SD_CLOSE_PATH,\n";
 }
 elsif (/fill/)
 {
   print "  w->stroke ();\n";
 }
 elsif (/(\S+) (\S+) lineto/)
 {
   $x = $1/$SCALE_X+$TRANS_X;
   $y = $2/$SCALE_Y+$TRANS_Y;
   print "  SD_LINE_TO, $x, $y,\n";
 }
 elsif (/(\S+) (\S+) (\S+) (\S+) (\S+) (\S+) curveto/)
 {
   $x1 = $1/$SCALE_X+$TRANS_X;
   $y1 = $2/$SCALE_Y+$TRANS_Y;
   $x2 = $3/$SCALE_X+$TRANS_X;
   $y2 = $4/$SCALE_Y+$TRANS_Y;
   $x3 = $5/$SCALE_X+$TRANS_X;
   $y3 = $6/$SCALE_Y+$TRANS_Y;
   print "  SD_CURVE_TO, $x1, $y1, $x2, $y2, $x3, $y3,\n";
 }
 
}
