#!/usr/bin/perl
$line = 16;
for ($i=0; $i<0x10ffff/$line; $i++)
{
  $char = sprintf ("U+%08X", $i*$line);
  $utf8 = "";
  for ($j=0; $j<$line; $j++)
  {
    $utf8 .= sprintf ("U+%08X", $i*$line+$j);
  }
  $utf8 =~ s/U\+([0-9A-Fa-f]{4,8})/pack "U", hex $1/ge;
  die "can not open uniconv" unless (open (OUT, 
   "| ./uniconv -encode gb-18030 -out /tmp/aa.txt"));
  print OUT $utf8;
  close (OUT);
  die "can not open uniconv" unless (open (IN, 
   "./uniconv -decode gb-18030 -in /tmp/aa.txt |"));
  $check = <IN>;
  close (IN);
  if ($i >= 0xd800 && $i <= 0xdfff)
  {
    if ($check eq $utf8)
    {
      print STDERR "Problem at $char not expected $utf8 got $check\n";
    }
  }
  elsif ($check ne $utf8)
  {
    print STDERR "Problem at $char expected $utf8 got $check\n";
  }
  print "$char\n" if (($i % 0x10)==0);
}
