#!/usr/bin/perl -w 

%consonants = (
	"k" => "0x0a15",
	"kh" => "0x0a16",
	"g" => "0x0a17",
	"gh" => "0x0a18",
	"N^" => "0x0a19",
	"~N" => "0x0a19",
	"c" => "0x0a1a",
	"ch" => "0x0a1b",
	"Ch" => "0x0a1b",
	"j" => "0x0a1c",
	"jh" => "0x0a1d",
	"~n" => "0x0a1e",
	"JN" => "0x0a1e",
	"T" => "0x0a1f",
	"Th" => "0x0a20",
	"D" => "0x0a21",
	"Dh" => "0x0a22",
	"N" => "0x0a23",
	"t" => "0x0a24",
	"th" => "0x0a25",
	"d" => "0x0a26",
	"dh" => "0x0a27",
	"n" => "0x0a28",
	"p" => "0x0a2a",
	"ph" => "0x0a2b",
	"b" => "0x0a2c",
	"bh" => "0x0a2d",
	"m" => "0x0a2e",
	"y" => "0x0a2f",
	"r" => "0x0a30",
	"l" => "0x0a32",
	"L" => "0x0a33",
	"ld" => "0x0a33",
	"v" => "0x0a35",
	"w" => "0x0a35",
	"sh" => "0x0a36",
	"s" => "0x0a38",
	"h" => "0x0a39",
	"K" => "0x0a59",
	"G" => "0x0a5a",
	"z" => "0x0a5b",
	"J" => "0x0a5b",
	".D" => "0x0a5c",
	"f" => "0x0a5e",
	"GY" => "0x0a1c 0x0a4d 0x0a1e",
	"dny" => "0x0a1c 0x0a4d 0x0a1e",
);

%doubles = (
	"k" => "k",
	"kh" => "k",
	"g" => "g",
	"gh" => "g",
	"c" => "c",
	"ch" => "c",
	"Ch" => "c",
	"j" => "j",
	"jh" => "j",
	"T" => "T",
	"Th" => "T",
	"D" => "D",
	"Dh" => "D",
	"t" => "t",
	"th" => "t",
	"d" => "d",
	"dh" => "d",
	"p" => "p",
	"ph" => "p",
	"b" => "b",
	"bh" => "b",
	"y" => "y",
	"r" => "r",
	"l" => "l",
	"v" => "v",
	"w" => "w",
	"s" => "s",
);

%nasals = (
	"k" => "N^",
	"kh" => "N^",
	"g" => "N^",
	"gh" => "N^",
	"N^" => "N^",
	"~N" => "N^",
	"c" => "~n",
	"ch" => "~n",
	"Ch" => "~n",
	"j" => "~n",
	"jh" => "~n",
	"~n" => "~n",
	"JN" => "~n",
	"T" => "N",
	"Th" => "N",
	"D" => "N",
	"Dh" => "N",
	"N" => "N",
	"t" => "n",
	"th" => "n",
	"d" => "n",
	"dh" => "n",
	"n" => "n",
	"p" => "m",
	"ph" => "m",
	"b" => "m",
	"bh" => "m",
	"m" => "m",
	"GY" => "~n",
	"dny" => "~n",
);

%alt = (
	"N^" => "~N",
	"~n" => "JN"
);

%vowels = (
	"" => " 0x0a4d",
	"a" => "",
	"A" => " 0x0a3e",
	"aa" => " 0x0a3e",
	"i" => " 0x0a3f",
	"I" => " 0x0a40",
	"ii" => " 0x0a40",
	"u" => " 0x0a41",
	"U" => " 0x0a42",
	"uu" => " 0x0a42",
	"e" => " 0x0a47",
	"ai" => " 0x0a48",
	"o" => " 0x0a4b",
	"au" => " 0x0a4c",
	".h" => " 0x0a4d 0x200c",
);

print "// Gurmukhi kmap according to Itrans transliteration\n";
print "// by Miikka-Markus Alhonen 2002-01-26\n";
print "// 2003-06-04 upgraded to Unicode 4.0\n\n";
print "// Consonant + vowel/virama\n";

foreach $cons (sort keys %consonants) {
  foreach $vowel (sort keys %vowels) {
    printf ("\"%s%s=%s%s\",\n",$cons,$vowel,$consonants{$cons},$vowels{$vowel});
  }
}

print "\n// Double consonant + vowel/virama\n";

$mod = "0x0a71 ";

foreach $cons (sort keys %doubles) {
  foreach $vowel (sort keys %vowels) {
    printf ("\"%s%s%s=%s%s%s\",\n",$doubles{$cons},$cons,$vowel,$mod,$consonants{$cons},$vowels{$vowel});
  }
}

print "\n// Nasal + consonant + vowel/virama\n";

$mod = "0x0a70 ";

foreach $cons (sort keys %nasals) {
  foreach $vowel (sort keys %vowels) {
    printf ("\"%s%s%s=%s%s%s\",\n",$nasals{$cons},$cons,$vowel,$mod,$consonants{$cons},$vowels{$vowel});
  }
  if ($alt{$nasals{$cons}}) {
    foreach $vowel (sort keys %vowels) {
      printf ("\"%s%s%s=%s%s%s\",\n",$alt{$nasals{$cons}},$cons,$vowel,$mod,$consonants{$cons},$vowels{$vowel});
    }
  }
}

print <<EOF;

// Independent vowels

"a=0x0a05",
"A=0x0a06",
"aa=0x0a06",
"i=0x0a07",
"I=0x0a08",
"ii=0x0a08",
"u=0x0a09",
"U=0x0a0a",
"uu=0x0a0a",
"e=0x0a0f",
"ai=0x0a10",
"o=0x0a13",
"au=0x0a14",

// Numbers

"0x30=0x0a66",
"0x31=0x0a67",
"0x32=0x0a68",
"0x33=0x0a69",
"0x34=0x0a6a",
"0x35=0x0a6b",
"0x36=0x0a6c",
"0x37=0x0a6d",
"0x38=0x0a6e",
"0x39=0x0a6f",

// Other marks

"khNDa=0x262c",
"KND=0x262c",
".N=0x0a02",
".n=0x0a70",
"M=0x0a70",
"H=0x0a03",
".c=0x0a71",
".h=0x0a4d 0x200c",
".=0x0964",
"..=0x0965",
"|=0x0964",
"||=0x0965",
";=0x0965",
"{\\\\rm 0x20 .}=0x002e",
"{}=0x200d",
EOF
