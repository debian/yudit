#!/usr/bin/perl -w 

%consonants = (
	"k" => "0x0d15",
	"kh" => "0x0d16",
	"g" => "0x0d17",
	"gh" => "0x0d18",
	"\\\"n" => "0x0d19",
	"c" => "0x0d1a",
	"ch" => "0x0d1b",
	"j" => "0x0d1c",
	"jh" => "0x0d1d",
	"~n" => "0x0d1e",
	".t" => "0x0d1f",
	".th" => "0x0d20",
	".d" => "0x0d21",
	".dh" => "0x0d22",
	".n" => "0x0d23",
	"t" => "0x0d24",
	"th" => "0x0d25",
	"d" => "0x0d26",
	"dh" => "0x0d27",
	"n" => "0x0d28",
	"..n" => "0x0d28",
	"p" => "0x0d2a",
	"ph" => "0x0d2b",
	"f" => "0x0d2b",
	"b" => "0x0d2c",
	"bh" => "0x0d2d",
	"m" => "0x0d2e",
	"y" => "0x0d2f",
	"r" => "0x0d30",
	".r" => "0x0d31",
	"..t" => "0x0d31",
	"l" => "0x0d32",
	".l" => "0x0d33",
	"zh" => "0x0d34",
	"v" => "0x0d35",
	"sh" => "0x0d36",
	".s" => "0x0d37",
	"s" => "0x0d38",
	"h" => "0x0d39",
);

%vowels = (
	"" => " 0x0d4d",
	"a" => "",
	"aa" => " 0x0d3e",
	"i" => " 0x0d3f",
	"ii" => " 0x0d40",
	"u" => " 0x0d41",
	"uu" => " 0x0d42",
	"R" => " 0x0d43",
#	"RR" => " 0x0d44", # not encoded in Unicode 3.1
	"e" => " 0x0d46",
	"ee" => " 0x0d47",
	"ai" => " 0x0d48",
	"o" => " 0x0d4a",
	"oo" => " 0x0d4b",
	"au" => " 0x0d4c",
	"+" => " 0x0d4d 0x200c",
#	"L" => " 0x0d62", # not encoded in Unicode 3.1
#	"LL" => " 0x0d63", # not encoded in Unicode 3.1
);

print "// Malayalam kmap according to Malayalam-TeX transliteration\n";
print "// by Miikka-Markus Alhonen 2002-02-12\n\n";
print "// Consonant + vowel/virama\n";

foreach $cons (sort keys %consonants) {
  foreach $vowel (sort keys %vowels) {
    printf ("\"%s%s=%s%s\",\n",$cons,$vowel,$consonants{$cons},$vowels{$vowel});
  }
}

print <<EOF;

// Independent vowels

"a=0x0d05",
"aa=0x0d06",
"i=0x0d07",
"ii=0x0d08",
"u=0x0d09",
"uu=0x0d0a",
"R=0x0d0b",
"L=0x0d0c",
"e=0x0d0e",
"ee=0x0d0f",
"ai=0x0d10",
"o=0x0d12",
"oo=0x0d13",
"au=0x0d14",
"RR=0x0d60",
"LL=0x0d61",

// Numbers

"0x30=0x0d66",
"0x31=0x0d67",
"0x32=0x0d68",
"0x33=0x0d69",
"0x34=0x0d6a",
"0x35=0x0d6b",
"0x36=0x0d6c",
"0x37=0x0d6d",
"0x38=0x0d6e",
"0x39=0x0d6f",

// Other marks

".m=0x0d02",
".h=0x0d03",
"+=0xd4d 0x200c",
"[]=0x25cc",
">>=0x200c",
"<<=0x200d",
EOF
