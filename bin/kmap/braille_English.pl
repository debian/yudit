#!/usr/bin/perl
# Braille Codes Reference:
# https://www.pharmabraille.com/braille-codes/unified-english-braille-ueb-code/
# https://en.wikipedia.org/wiki/English_Braille
# Added some capitals from:
# https://en.wikipedia.org/wiki/Gardner%E2%80%93Salinas_braille_codes
# Gaspar Sinai 2023-01-03 Tokyo

use utf8;

use open IN => ":utf8";
binmode(STDOUT, ":utf8");
binmode(STDIN, ":utf8");
binmode(STDERR, ":utf8");

%rkmap = ();
($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
$year += 1900;
$mon += 1;

$date = sprintf ("%04s-%02s-%02s", $year, $mon, $mday);
print "// Generated file: BrailleEnglish.kmap\n";
print "// This is a best effort kmap with limitation.\n";
print "// Yudit: cd bin; make braille\n";
print "// Yudit: bin/kmap/braille_English.pl $date\n";
print "// !!!!!! Modify the perl script to change this file.!!!!!!\n\n";

print "\"0x20 =  0x2800\", // Space |⠀|\n";

while (<DATA>) {
    s/^#.*$//;
    my @rec = split (/\|/, $_);
    next if ($#rec < 2);
    my $lead = $rec[0];
    my $unicode = $rec[1];
    my $roma = $rec[2];
    if (defined $rkmap{$unicode}) {
        if ($rec[3] ne "!") {
            die "Duplicate key $unicode $roma / $rkmap{$unicode}" 
        }
    }
    $rkmap{$unicode} = $roma;
    my $value = $unicode;
    $value =~ s/(.)/sprintf (" 0x%x",ord($1));/ge;
    print ("\"$roma = $value\", // $lead |$unicode|\n");
}
print "// END\n";

exit (0);
__DATA__
a|⠁|a|
b|⠃|b|
c|⠉|c|
d|⠙|d|
e|⠑|e|
f|⠋|f|
g|⠛|g|
h|⠓|h|
i|⠊|i|
j|⠚|j|
k|⠅|k|
l|⠇|l|
m|⠍|m|
n|⠝|n|
o|⠕|o|
p|⠏|p|
q|⠟|q|
r|⠗|r|
s|⠎|s|
t|⠞|t|
u|⠥|u|
v|⠧|v|
w|⠺|w|
x|⠭|x|
y|⠽|y|
z|⠵|z|
number indicator|⠼|0x23|
1|⠼⠁|0x31|
2|⠼⠃|0x32|
3|⠼⠉|0x33|
4|⠼⠙|0x34|
5|⠼⠑|0x35|
6|⠼⠋|0x36|
7|⠼⠛|0x37|
8|⠼⠓|0x38|
9|⠼⠊|0x39|
0|⠼⠚|0x30|
# ,|⠂|,|
# ;|⠆|;|
# :|⠒|:|
# .|⠲|.|
# ?|⠦|?|
# !|⠖|!|
# apostrophe|⠄|'|
# open quote|⠦|``|!|
# close quote|⠴|''|
(|⠐⠣|(|
)|⠐⠜|)|
/|⠸⠌|/|
dash/hyphen|⠤|-|
long dash or hyphen|⠠⠤|--|
&amp;|⠈⠯|&|
*|⠐⠔|*|
@|⠈⠁|@|
©|⠘⠉|(C)|
®|⠘⠗|(R)|
™|⠘⠞|(TM)|
°|⠘⠚|DEG|
%|⠨⠴|0x25|
+|⠐⠖|+|
− (minus)|⠐⠤|-|
=|⠐⠶|0x3d|
×|⠐⠦|×|
÷|⠐⠌|DIV|
letter sign|⠰|LS-|
capital letter indicator|⠠|CAP-|
# Capital Leters
A|⠠⠁|A|
B|⠠⠃|B|
C|⠠⠉|C|
D|⠠⠙|D|
E|⠠⠑|E|
F|⠠⠋|F|
G|⠠⠛|G|
H|⠠⠓|H|
I|⠠⠊|I|
J|⠠⠚|J|
K|⠠⠅|K|
L|⠠⠇|L|
M|⠠⠍|M|
N|⠠⠝|N|
O|⠠⠕|O|
P|⠠⠏|P|
Q|⠠⠟|Q|
R|⠠⠗|R|
S|⠠⠎|S|
T|⠠⠞|T|
U|⠠⠥|U|
V|⠠⠧|V|
W|⠠⠺|W|
X|⠠⠭|X|
Y|⠠⠽|Y|
Z|⠠⠵|Z|
# Extended from https://en.wikipedia.org/wiki/English_Braille
and|⠯|and| 
And|⠠⠯|And| 
for|⠿|for| 
For|⠠⠿|For| 
of|⠷|of| 
Of|⠠⠷|Of| 
the|⠮|the| 
The|⠠⠮|The| 
with|⠾|with|
With|⠠⠾|With|
ch|⠡|ch|
Ch|⠠⠡|Ch|
gh|⠣|gh|
Gh|⠠⠣|Gh|
sh|⠩|sh|
Sh|⠠⠩|Sh|
th|⠹|th|
Th|⠠⠹|Th|
wh|⠱|wh|
Wh|⠠⠱|Wh|
ed|⠫|ed|
Ed|⠠⠫|Ed|
er|⠻|er|
Er|⠠⠻|Er|
ou|⠳|ou|
Ou|⠠⠳|Ou|
ow|⠪|ow|
Ow|⠠⠪|Ow|
# w|⠺|w| - defined

# ea is more common than ","
-ea-|⠂|-ea-|
,|⠂|,|!|

-bb-|⠆|-bb-|
;|⠆|;|!|

-cc-|⠒|-cc-|
:|⠒|:|!|

# "." is more common than  -dd-
.|⠲|.|
-dd-|⠲|-dd-|!|

en|⠢|en|

-ff-|⠖|-ff-|
!|⠖|!|!|

-gg-|⠶|-gg-|
?|⠦|?|

open-quote|⠦|``|!|
in|⠔|in|
by|⠴|by|
close-quote|⠴|''|!|
apostrophe|⠄|'|
# dash/hyphen|⠤|-| - defined
accent-|⠈|acc-|
abbrev-|⠘|abbr2-|
st|⠌|st|
ar|⠜|ar|!|
-ing|⠬|ing|
# number indicator|⠼|0x23| - defined
disp-|⠨|disp-|
emph-|⠨|emph-|!|
abbrev-|⠸|abbr3-|
abbrev-|⠐|abbr1-|
# lette sign|⠰|LS-|
# capital letter indicator|⠠|CAP-|

# Initial abbreviations

# with ⟨⠐⟩ 
here|⠐⠓|here|
there|⠐⠮|there|
where|⠐⠱|where|
ever|⠐⠑|ever|
ought|⠐⠳|ought|
father|⠐⠋|father|
mother|⠐⠍|mother|
name|⠐⠝|name|
character|⠐⠡|character|
question|⠐⠟|question|
know|⠐⠅|know|
lord|⠐⠇|lord|
one|⠐⠕|one|
day|⠐⠙|day|
some|⠐⠎|some|
part|⠐⠏|part|
time|⠐⠞|time|
right|⠐⠗|right|
through|⠐⠹|through|
under|⠐⠥|under|
work|⠐⠺|work|
young|⠐⠽|young|

# with ⟨⠘⟩ 
these|⠘⠮|these|
those|⠘⠹|those|
upon|⠘⠥|upon|
whose|⠘⠱|whose|
word|⠘⠺|word|

# with ⟨⠸⟩
cannot|⠸⠉|cannot|
many|⠸⠍|many|
had|⠸⠓|had|
their|⠸⠮|their|
spirit|⠸⠎|spirit|
world|⠸⠺|world|

# Final abbreviations
# disp-
-ound|⠨⠙|-ound|
-ount|⠨⠞|-ount|
-ance|⠨⠑|-ance|
-less|⠨⠎|-less|
-sion|⠨⠝|-sion|
# LS-
-ong|⠰⠛|-ong|
-ful|⠰⠇|-ful|
-ment|⠰⠞|-ment|
-ence|⠰⠑|-ence|
-ness|⠰⠎|-ness|
-tion|⠰⠝|-ion|
-ity|⠰⠽|-ity|
# CAP-
-ally|⠠⠽|-ally|!| # also Y
-ation|⠠⠝|-ation|!| # also N
#
# https://en.wikipedia.org/wiki/Gardner%E2%80%93Salinas_braille_codes
# Capitals
A|⡁|_A|
B|⡃|_B|
C|⡉|_C|
D|⡙|_D|
E|⡑|_E|
F|⡋|_F|
G|⡛|_G|
H|⡓|_H|
I|⡊|_I|
J|⡚|_J|
K|⡅|_K|
L|⡇|_L|
M|⡍|_M|
N|⡝|_N|
O|⡕|_O|
P|⡏|_P|
Q|⡟|_Q|
R|⡗|_R|
S|⡎|_S|
T|⡞|_T|
U|⡥|_U|
V|⡧|_V|
W|⡺|_W|
X|⡭|_X|
Y|⡽|_Y|
Z|⡵|_Z|
And|⡯|_And| 
For|⡿|_For| 
Of|⡷|_Of| 
The|⡮|_The| 
With|⡾|_With|
Ch|⡡|_Ch|
Gh|⡣|_Gh|
Sh|⡩|_Sh|
Th|⡹|_Th|
Wh|⡱|_Wh|
Ed|⡫|_Ed|
Er|⡻|_Er|
Ou|⡳|_Ou|
Ow|⡪|_Ow|
# skipped:
#-ea-|⠂|-ea-|
#-bb-|⠆|-bb-|
#-cc-|⠒|-cc-|
#-dd-|⠲|-dd-|!|
En|⡢|_En|
#-ff-|⠖|-ff-|
#-gg-|⠶|-gg-|
In|⡔|_In|
By|⡴|_By|
__END__
