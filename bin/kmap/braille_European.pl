#!/usr/bin/perl
# Braille Codes Reference:
# https://www.pharmabraille.com/braille-codes/ebu-baille-code/
# Gaspar Sinai 2023-01-03 Tokyo

use utf8;

use open IN => ":utf8";
binmode(STDOUT, ":utf8");
binmode(STDIN, ":utf8");
binmode(STDERR, ":utf8");

%rkmap = ();
($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
$year += 1900;
$mon += 1;

$date = sprintf ("%04s-%02s-%02s", $year, $mon, $mday);
print "// Generated file: BrailleEuropean.kmap\n";
print "// Yudit: cd bin; make braille\n";
print "// Yudit: bin/kmap/braille_European.pl $date\n";
print "// !!!!!! Modify the perl script to change this file.!!!!!!\n\n";

print "\"0x20 =  0x2800\", // Space |⠀|\n";

while (<DATA>) {
    s/^#.*$//;
    my @rec = split (/\|/, $_);
    next if ($#rec < 2);
    my $lead = $rec[0];
    my $unicode = $rec[1];
    my $roma = $rec[2];
    if (defined $rkmap{$unicode}) {
        if ($rec[3] ne "!") {
            die "Duplicate key $unicode $roma / $rkmap{$unicode}" 
        }
    }
    $rkmap{$unicode} = $roma;
    my $value = $unicode;
    $value =~ s/(.)/sprintf (" 0x%x",ord($1));/ge;
    print ("\"$roma = $value\", // $lead |$unicode|\n");
}

print "// END\n";

exit (0);
__DATA__
a|⠁|a|
b|⠃|b|
c|⠉|c|
d|⠙|d|
e|⠑|e|
f|⠋|f|
g|⠛|g|
h|⠓|h|
i|⠊|i|
j|⠚|j|
k|⠅|k|
l|⠇|l|
m|⠍|m|
n|⠝|n|
o|⠕|o|
p|⠏|p|
q|⠟|q|
r|⠗|r|
s|⠎|s|
t|⠞|t|
u|⠥|u|
v|⠧|v|
w|⠺|w|
x|⠭|x|
y|⠽|y|
z|⠵|z|
comma, thousands separator, decimal point|⠂|,|
semi-colon|⠆|;|
colon|⠒|:|
full stop/period, thousands separator|⠄|.|
(|⠶|(|
)|⠶|)|!|
forward slash or solidus|⠌|/|
dash/hyphen|⠤|-|
long dash or hyphen|⠤⠤|--|
per cent|⠏⠉|0x25|
per mille|⠏⠍|0x25 0x25|
grams|⠛|grams|!|
milligrams|⠍⠛|milligrams|
micrograms|⠍⠉⠛|micrograms|
litres|⠇|litres|!|
decilitres|⠙⠇|decilitres|
millilitres|⠍⠇|millilitres|
Internationale Einheiten|⠊⠑|IE|
intra muscular|⠊⠍|IM|
intravenous|⠊⠧|IV|
million international units|⠍⠊⠥|MIU|
subcutaneous|⠎⠉|SC|
sustained relief|⠎⠗|SR|
Unites Internationales|⠥⠊|UI|
а́ A With Acute|⠈⠁|а́|
â A With Circumflex|⠈⠁|a ^|!|
ä A With Diaeresis|⠈⠁|a :|!|
å A With Ring Above|⠈⠁|a 0|!|
ą A With Ogonek|⠈⠁|a ,|!|
ǎ A With Caron|⠈⠁|a <|!|
č C With Caron|⠈⠉|c <|
ç C With Cedilla|⠈⠉|c ,|!|
é E With Acute|⠈⠑|e '|
ė E With Dot Above|⠈⠑|e .|!|
ę E With Ogonek|⠈⠑|e ,|!|
î I With Circumflex|⠈⠊|i ^|
í I With Acute|⠈⠊|i '|!|
į I With Ogonek|⠈⠊|i ,|!|
ñ N With Tilde|⠈⠝|n ~|
ó O With Acute|⠈⠕|o '|
ö O With Diaeresis|⠈⠕|o :|!|
õ O With Tilde|⠈⠕|o ~|!|
ő O With Double Acute|⠈⠕|o 0x22|!|
ş S With Cedilla|⠈⠎|s ,|
š S With Caron|⠈⠎|š|!|
ß Sharp S|⠈⠎|ss|!|
ţ T With Cedilla|⠈⠞|t ,|
ú U With Acute|⠈⠥|u '|
ü U With Diaeresis|⠈⠥|u :|!|
ū U With Macron|⠈⠥|u -|!|
ų U With Ogonek|⠈⠥|u ,|!|
ű U With Double Acute|⠈⠥|u 0x22|!|
ž Z With Caron|⠈⠵|z <|
number indicator|⠼|0x23|
1|⠼⠁|0x31|
2|⠼⠃|0x32|
3|⠼⠉|0x33|
4|⠼⠙|0x34|
5|⠼⠑|0x35|
6|⠼⠋|0x36|
7|⠼⠛|0x37|
8|⠼⠓|0x38|
9|⠼⠊|0x39|
0|⠼⠚|0x30|
__END__
