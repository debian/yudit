#!/usr/bin/perl
# Braille Code References:
# https://www.pharmabraille.com/braille-codes/hungary-braille-codes/
# http://mek.oszk.hu/00000/00056/html/158.htm
# Gaspar Sinai 2023-01-03 Tokyo

use utf8;

use open IN => ":utf8";
binmode(STDOUT, ":utf8");
binmode(STDIN, ":utf8");
binmode(STDERR, ":utf8");

%rkmap = ();
($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
$year += 1900;
$mon += 1;

$date = sprintf ("%04s-%02s-%02s", $year, $mon, $mday);
print "// Generated file: BrailleHungarian.kmap\n";
print "// Yudit: cd bin; make braille\n";
print "// Yudit: bin/kmap/braille_Hugaria.pl $date\n";
print "// !!!!!! Modify the perl script to change this file.!!!!!!\n\n";

print "\"0x20 =  0x2800\", // Space |⠀|\n";

while (<DATA>) {
    s/^#.*$//;
    my @rec = split (/\|/, $_);
    next if ($#rec < 2);
    my $lead = $rec[0];
    my $unicode = $rec[1];
    my $roma = $rec[2];
    if (defined $rkmap{$unicode}) {
        if ($rec[3] ne "!") {
            die "Duplicate key $unicode $roma / $rkmap{$unicode}" 
        }
    }
    $rkmap{$unicode} = $roma;
    my $value = $unicode;
    $value =~ s/(.)/sprintf (" 0x%x",ord($1));/ge;
    print ("\"$roma = $value\", // $lead |$unicode|\n");
}

print "// END\n";

exit (0);
__DATA__
a|⠁|a|
b|⠃|b|
c|⠉|c|
d|⠙|d|
e|⠑|e|
f|⠋|f|
g|⠛|g|
h|⠓|h|
i|⠊|i|
j|⠚|j|
k|⠅|k|
l|⠇|l|
m|⠍|m|
n|⠝|n|
o|⠕|o|
p|⠏|p|
q|⠯|q|
r|⠗|r|
s|⠎|s|
t|⠞|t|
u|⠥|u|
v|⠧|v|
w|⠺|w|
x|⠭|x|
y|⠽|y|
z|⠣|z|
,|⠂|,|
;|⠆|;|
(|⠮|(|
)|⠵|)| # Usually z.
%|⠼⠚⠴|0x25|
‰|⠼⠚⠴⠴|0x25 0x25|
ä A With Diaeresis|⠘|a :|
á A With Acute|⠈|a '|
é E With Acute|⠡|e '|
í I With Acute|⠌|i '|
ó O With Acute|⠪|o '|
ö O With Diaeresis|⠟|o :|
ő O With Double Acute|⠻|o 0x22|
ú U With Acute|⠬|u '|
ü U With Diaeresis|⠷|u :|
ű U With Double Acute|⠾|u 0x22|
number indicator|⠼|0x23|
1|⠼⠁|0x31|
2|⠼⠃|0x32|
3|⠼⠉|0x33|
4|⠼⠙|0x34|
5|⠼⠑|0x35|
6|⠼⠋|0x36|
7|⠼⠛|0x37|
8|⠼⠓|0x38|
9|⠼⠊|0x39|
0|⠼⠚|0x30|
Caps indicator|⠨|NB-|
# Capital Leters
A|⠨⠁|A|
B|⠨⠃|B|
C|⠨⠉|C|
D|⠨⠙|D|
E|⠨⠑|E|
F|⠨⠋|F|
G|⠨⠛|G|
H|⠨⠓|H|
I|⠨⠊|I|
J|⠨⠚|J|
K|⠨⠅|K|
L|⠨⠇|L|
M|⠨⠍|M|
N|⠨⠝|N|
O|⠨⠕|O|
P|⠨⠏|P|
Q|⠨⠯|Q|
R|⠨⠗|R|
S|⠨⠎|S|
T|⠨⠞|T|
U|⠨⠥|U|
V|⠨⠧|V|
W|⠨⠺|W|
X|⠨⠭|X|
Y|⠨⠽|Y|
Z|⠨⠣|Z|
Á|⠨⠈|A '|
Ä|⠨⠘|A :|
É|⠨⠡|E '|
Í|⠨⠌|I '|
Ó|⠨⠪|O '|
Ö|⠨⠟|O :|
Ú|⠨⠬|U '|
Ü|⠨⠷|U :|
Ő|⠨⠻|O 0x22|
Ű|⠨⠾|U 0x22|
cs|⠩|cs|
gy|⠹|gy|
ly|⠸|ly|
ny|⠫|ny|
sz|⠱|sz|
ty|⠳|ty|
zs|⠜|zs|
# http://mek.oszk.hu/00000/00056/html/158.htm
-/gondolatjel|⠤|--|
* or quotation mark|⠦|*|
quoation mark or * |⠦|0x22|!|
=|⠶|0x3d|
+ or !|⠖|0x2b|
! or +|⠖|0x21|!|
?|⠢|?|
/|⠲|0x2f|
. dot|⠲|.|!|
# This is from phrama
. number dot|⠄|#.|
: or minus|⠒|:|
- minus or : |⠒|-|!|
# This is from pharma probably wrong.
# /|⠐⠂|/
__END__
