#!/usr/bin/perl
# Braille Code Reference:
# https://en.wikipedia.org/wiki/Japanese_Braille
# Sample text reference:
# http://braille.jpn.org/sonota/naitaaka.html
# Gaspar Sinai 2023-01-03 Tokyo

use utf8;

use open IN => ":utf8";
binmode(STDOUT, ":utf8");
binmode(STDIN, ":utf8");
binmode(STDERR, ":utf8");

%rkmap = ();

($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
$year += 1900;
$mon += 1;

$date = sprintf ("%04s-%02s-%02s", $year, $mon, $mday);
print "// Generated file: BrailleJapanese.kmap\n";
print "// Yudit: cd bin; make braille\n";
print "// Yudit: bin/kmap/braille_Japanese.pl $date\n";
print "// !!!!!! Modify the perl script to change this file.!!!!!!\n\n";

print "\"0x20 =  0x2800\", // Space |⠀|\n";
print "\"0x20 0x20 =  0x3000\", // Japanese Space |⠀|\n";

while (<DATA>) {
    s/^#.*$//;
    my @rec = split (/\|/, $_);
    next if ($#rec < 2);
    my $lead = $rec[0];
    my $unicode = $rec[1];
    my $roma = $rec[2];
    if (defined $rkmap{$unicode}) {
        if ($rec[3] ne "!") {
            die "Duplicate key $unicode $roma / $rkmap{$unicode}" 
        }
    }
    $rkmap{$unicode} = $roma;
    my $value = $unicode;
    $value =~ s/(.)/sprintf (" 0x%x",ord($1));/ge;
    print ("\"$roma = $value\", // $lead |$unicode|\n");
}

print "// END\n";

exit (0);
__DATA__
あ|⠁|a|
い|⠃|i|
う|⠉|u|
え|⠋|e|
お|⠊|o|

か|⠡|ka|
き|⠣|ki|
く|⠩|ku|
け|⠫|ke|
こ|⠪|ko|

さ|⠱|sa|
し|⠳|si|
す|⠹|su|
せ|⠻|se|
そ|⠺|so|

た|⠕|ta|
ち|⠗|ti|
つ|⠝|tu|
て|⠟|te|
と|⠞|to|

な|⠅|na|
に|⠇|ni|
ぬ|⠍|nu|　
ね|⠏|ne|
の|⠎|no|

は|⠥|ha|
ひ|⠧|hi|
ふ|⠭|hu|
へ|⠯|he|
ほ|⠮|ho|

ま|⠵|ma|
み|⠷|mi|
む|⠽|mu|
め|⠿|me|
も|⠾|mo|

ら|⠑|ra|
り|⠓|ri|
る|⠙|ru|
れ|⠛|re|
ろ|⠚|ro|

や|⠌|ya|
ゆ|⠬|yu|
よ|⠜|yo|

わ|⠄|wa|
ゐ|⠆|vi|
を|⠔|wo|
# -w-|⠔|-w-|
ん|⠴|n'|
ん|⠴|n|!|

# sokuon usual placement
っ|⠂|t-|

# chōon usual placement
ー|⠒|-|

dakuten-prefix|⠐|g-|
handakuten-prefix or CAPITAL|⠠|p-|
yōon-prefix|⠈|y-|
yōon-dakuten-prefix|⠘|gy-|
yōon-handakuten-prefix|⠨|py-|

# goyouon sacrificed for ?
# w-|⠢|w-|

# goyouon-dakuten
# wg-|⠲|wg-|

。or gōyōon-dakuten|⠲|.|
LATIN or 、|⠰|,|
？or gōyōon|⠢|?|
！|⠖|!|
-|⠤|-|
DIGIT|⠼|#|
# LATIN or 、|⠰|,|
# Sacrificed for |p-|⠠|p-|
# CAPITAL|⠠|CAP-|
1|⠼⠁|0x31|
2|⠼⠃|0x32|
3|⠼⠉|0x33|
4|⠼⠙|0x34|
5|⠼⠑|0x35|
6|⠼⠋|0x36|
7|⠼⠛|0x37|
8|⠼⠓|0x38|
9|⠼⠊|0x39|
0|⠼⠚|0x30|

が/dakuten-ka|⠐⠡|ga|
ぎ/dakuten-ki|⠐⠣|gi|
ぐ/dakuten-ku|⠐⠩|gu|
げ/dakuten-ke|⠐⠫|ge|
ご/dakuten-ko|⠐⠪|go|

ざ/dakuten-sa|⠐⠱|za|
じ/dakuten-si|⠐⠳|zi|
ず/dakuten-su|⠐⠹|zu|
ぜ/dakuten-se|⠐⠻|ze|
ぞ/dakuten-so|⠐⠺|zo|

だ/dakuten-ta|⠐⠕|da|
ぢ/dakuten-ti|⠐⠗|di|
づ/dakuten-tu|⠐⠝|du|
で/dakuten-te|⠐⠟|de|
ど/dakuten-to|⠐⠞|do|

ば/dakuten-ha|⠐⠥|ba|
び/dakuten-hi|⠐⠧|bi|
ぶ/dakuten-hu|⠐⠭|bu|
べ/dakuten-he|⠐⠯|be|
ぼ/dakuten-ho|⠐⠮|bo|

ぱ/handakuten-ha|⠠⠥|pa|
ぴ/handakuten-hi|⠠⠧|pi|
ぷ/handakuten-hu|⠠⠭|pu|
ぺ/handakuten-he|⠠⠯|pe|
ぽ/handakuten-ho|⠠⠮|po|

きゃ/yōon-ka|⠈⠡|kya|
きゅ/yōon-ku|⠈⠩|kyu|
きょ/yōon-ko|⠈⠪|kyo|

しゃ/yōon-sa|⠈⠱|sya|
しゅ/yōon-su|⠈⠹|syu|
しょ/yōon-so|⠈⠺|syo|

ちゃ/yōon-ta|⠈⠕|tya|
ちゅ/yōon-tu|⠈⠝|tyu|
ちょ/yōon-to|⠈⠞|tyo|

にゃ/yōon-na|⠈⠅|nya|
にゅ/yōon-nu|⠈⠍|nyu|　
にょ/yōon-no|⠈⠎|nyo|

ひゃ/yōon-ha|⠈⠥|hya|
ひゅ/yōon-ha|⠈⠭|hyu|
ひょ/yōon-ha|⠈⠮|hyo|

みゃ/yōon-ma|⠈⠵|mya|
みゅ/yōon-mu|⠈⠽|myu|
みょ/yōon-mo|⠈⠾|myo|

りゃ/yōon-ra|⠈⠑|rya|
りゅ/yōon-ru|⠈⠙|ryu|
りょ/yōon-ro|⠈⠚|ryo|

ぎゃ/yōon-dakuten-ka|⠘⠡|gya|
ぎゅ/yōon-dakuten-ku|⠘⠩|gyu|
ぎょ/yōon-dakuten-ko|⠘⠪|gyo|

じゃ/yōon-dakuten-sa|⠘⠱|zya|
じゅ/yōon-dakuten-su|⠘⠹|zyu|
じょ/yōon-dakuten-so|⠘⠺|zyo|

びゃ/yōon-dakuten-ha|⠘⠥|bya|
びゅ/yōon-dakuten-hu|⠘⠭|byu|
びょ/yōon-dakuten-ho|⠘⠮|byo|

ぴゃ/yōon-hanbdakuten-ha|⠨⠥|pya|
ぴゅ/yōon-hanbdakuten-hu|⠨⠭|pyu|
ぴょ/yōon-hanbdakuten-ho|⠨⠮|pyo|

# duplicates
しゃ/yōon-sa/sya|⠈⠱|sha|!|
しゅ/yōon-su/sya|⠈⠹|shu|!|
しょ/yōon-so/sya|⠈⠺|sho|!|

ちゃ/tya|⠈⠕|cha|!|
ち/ti|⠗|chi|!|
ちゅ/tyu|⠈⠝|chu|!|
ちょ/tyo|⠈⠞|cho|!|

つ/tu|⠝|tsu|!|
ふ/fu|⠭|fu|!|

じゃ/zya|⠘⠱|ja|!|
じゅ/zyu|⠘⠹|ju|!|
じょ/zyo|⠘⠺|jo|!|

じ/zi|⠐⠳|ji|!|

ぢゃ/yōon-dakuten-ta|⠘⠕|dya|
ぢゅ/yōon-dakuten-tu|⠘⠝|dyu|
ぢょ/yōon-dakuten-to|⠘⠞|dyo|

# Preceded by a small tu (sokuon)

っか/sokuon-ka|⠂⠡|kka|
っき/sokuon-ki|⠂⠣|kki|
っく/sokuon-ku|⠂⠩|kku|
っけ/sokuon-ke|⠂⠫|kke|
っこ/sokuon-ko|⠂⠪|kko|

っさ/sokuon-sa|⠂⠱|ssa|
っし/sokuon-si|⠂⠳|ssi|
っす/sokuon-su|⠂⠹|ssu|
っせ/sokuon-se|⠂⠻|sse|
っそ/sokuon-so|⠂⠺|sso|

った/sokuon-ta|⠂⠕|tta|
っち/sokuon-ti|⠂⠗|tti|
っつ/sokuon-tu|⠂⠝|ttu|
って/sokuon-te|⠂⠟|tte|
っと/sokuon-to|⠂⠞|tto|

っは/sokuon-ta|⠂⠥|hha|
っひ/sokuon-ta|⠂⠧|hhi|
っふ/sokuon-ta|⠂⠭|hhu|
っへ/sokuon-ta|⠂⠯|hhe|
っほ/sokuon-ta|⠂⠮|hho|

っま/sokuon-ma|⠂⠵|mma|
っみ/sokuon-mi|⠂⠷|mmi|
っむ/sokuon-mu|⠂⠽|mmu|
っめ/sokuon-me|⠂⠿|mme|
っも/sokuon-mo|⠂⠾|mmo|

っや/sokuon-ya|⠂⠌|yya|
っゆ/sokuon-ya|⠂⠬|yyu|
っよ/sokuon-ya|⠂⠜|yyo|

っら/sokuon-ra|⠂⠑|rra|
っり/sokuon-ri|⠂⠓|rri|
っる/sokuon-ru|⠂⠙|rru|
っれ/sokuon-re|⠂⠛|rre|
っろ/sokuon-ro|⠂⠚|rro|

っが/sokuon-dakuten-ka|⠂⠐⠡|gga|
っぎ/sokuon-dakuten-ki|⠂⠐⠣|ggi|
っぐ/sokuon-dakuten-ku|⠂⠐⠩|ggu|
っげ/sokuon-dakuten-ke|⠂⠐⠫|gge|
っご/sokuon-dakuten-ko|⠂⠐⠪|ggo|

っざ/sokuon-dakuten-sa|⠂⠐⠱|zza|
っじ/sokuon-dakuten-si|⠂⠐⠳|zzi|
っず/sokuon-dakuten-su|⠂⠐⠹|zzu|
っぜ/sokuon-dakuten-se|⠂⠐⠻|zze|
っぞ/sokuon-dakuten-so|⠂⠐⠺|zzo|

っだ/sokuon-dakuten-ta|⠂⠐⠕|dda|
っぢ/sokuon-dakuten-ti|⠂⠐⠗|ddi|
っづ/sokuon-dakuten-tu|⠂⠐⠝|ddu|
っで/sokuon-dakuten-te|⠂⠐⠟|dde|
っど/sokuon-dakuten-to|⠂⠐⠞|ddo|

っば/sokuon-dakuten-ha|⠂⠐⠥|bba|
っび/sokuon-dakuten-hi|⠂⠐⠧|bbi|
っぶ/sokuon-dakuten-hu|⠂⠐⠭|bbu|
っべ/sokuon-dakuten-he|⠂⠐⠯|bbe|
っぼ/sokuon-dakuten-ho|⠂⠐⠮|bbo|

っぱ/sokuon-handakuten-ha|⠂⠠⠥|ppa|
っぴ/sokuon-handakuten-hi|⠂⠠⠧|ppi|
っぷ/sokuon-handakuten-hu|⠂⠠⠭|ppu|
っぺ/sokuon-handakuten-he|⠂⠠⠯|ppe|
っぽ/sokuon-handakuten-ho|⠂⠠⠮|ppo|

っきゃ/sokuon-yōon-ka|⠂⠈⠡|kkya|
っきゅ/sokuon-yōon-ku|⠂⠈⠩|kkyu|
っきょ/sokuon-yōon-ko|⠂⠈⠪|kkyo|

っしゃ/sokuon-yōon-sa|⠂⠈⠱|ssya|
っしゅ/sokuon-yōon-su|⠂⠈⠹|ssyu|
っしょ/sokuon-yōon-so|⠂⠈⠺|ssyo|

っちゃ/sokuon-yōon-ta|⠂⠈⠕|ttya|
っちゅ/sokuon-yōon-tu|⠂⠈⠝|ttyu|
っちょ/sokuon-yōon-to|⠂⠈⠞|ttyo|

っにゃ/sokuon-yōon-na|⠂⠈⠅|nnya|
っにゅ/sokuon-yōon-nu|⠂⠈⠍|nnyu|　
っにょ/sokuon-yōon-no|⠂⠈⠎|nnyo|

っひゃ/sokuon-yōon-ha|⠂⠈⠥|hhya|
っひゅ/sokuon-yōon-ha|⠂⠈⠭|hhyu|
っひょ/sokuon-yōon-ha|⠂⠈⠮|hhyo|

っみゃ/sokuon-yōon-ma|⠂⠈⠵|mmya|
っみゅ/sokuon-yōon-mu|⠂⠈⠽|mmyu|
っみょ/sokuon-yōon-mo|⠂⠈⠾|mmyo|

っりゃ/sokuon-yōon-ra|⠂⠈⠑|rrya|
っりゅ/sokuon-yōon-ru|⠂⠈⠙|rryu|
っりょ/sokuon-yōon-ro|⠂⠈⠚|rryo|

っぎゃ/sokuon-yōon-dakuten-ka|⠂⠘⠡|ggya|
っぎゅ/sokuon-yōon-dakuten-ku|⠂⠘⠩|ggyu|
っぎょ/sokuon-yōon-dakuten-ko|⠂⠘⠪|ggyo|

っじゃ/sokuon-yōon-dakuten-sa|⠂⠘⠱|zzya|
っじゅ/sokuon-yōon-dakuten-su|⠂⠘⠹|zzyu|
っじょ/sokuon-yōon-dakuten-so|⠂⠘⠺|zzyo|

っびゃ/sokuon-yōon-dakuten-ha|⠂⠘⠥|bbya|
っびゅ/sokuon-yōon-dakuten-hu|⠂⠘⠭|bbyu|
っびょ/sokuon-yōon-dakuten-ho|⠂⠘⠮|bbyo|

っぴゃ/sokuon-yōon-hanbdakuten-ha|⠂⠨⠥|ppya|
っぴゅ/sokuon-yōon-hanbdakuten-hu|⠂⠨⠭|ppyu|
っぴょ/sokuon-yōon-hanbdakuten-ho|⠂⠨⠮|ppyo|

っわ/sokuon-wa|⠂⠄|wwa|

し/si|⠳|shi|!|
しい/sii|⠳⠃|shii|
っし/sokuon-si|⠂⠳|sshi|!|
っち/sokuon-thi|⠂⠗|cchi|!|

#ゑ|⠖|we|!| # Duplicate of ！
__END__
