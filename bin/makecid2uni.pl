#!/usr/bin/perl
# This script is a modified version of
# makecid2uni that can be found in:
#  http://kappa.allnet.ne.jp/Kochi-CID/index-e.html -> CIDKit-20020407.tar.gz
# This will make a conversion map from:
# http://www.aihara.co.jp/~taiji/tops/index-j.html -> cjkvttcidmap-0.94.tar.gz
# Adobe-Japan1-Unicode

$cid = 0;
while (<>) {
  last if (/DuplicateCodeMap/);
  next unless /^16#/;

  foreach $v (split) {
    $v =~ s/^16#//;
    $cid2uni[$cid] = hex($v) if $v ne '0000';
    $cid++;
  }
}
$cidsup = $cid;

while (<>) {
  last if (/def/);
  next unless s/\[\s*(.*?)\s*\]/$1/;
  s/16#//g;
  ($cid, @unis) = split;
  foreach $u (@unis) {
    $u = hex($u);
  }
  $dupcode[$cid] = [@unis];
}

$cid2uni[0] = 0;
for ($cid = 0; $cid < $cidsup; $cid++) {
  if (defined $dupcode[$cid]) {
    @all = @{$dupcode[$cid]};
    printf ("%04X ->", $cid); 
    for (@all)
    {
      printf (" %04X", $_);
    }
    printf ("\n");
  } elsif (defined $cid2uni[$cid]) {
    printf ("%04X -> %04X\n", $cid, $cid2uni[$cid]); 
  }
}
