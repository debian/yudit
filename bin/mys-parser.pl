#!/usr/bin/perl
# Print a map of characters from kmap.
# Gaspar Sinai <gaspar yudit org> 2020-05-17, Tokyo
$uniconv = "/home/gsinai/Test/bin/uniconv";
while (<>) {
    chop;
    $line=$_;    
    next unless (/(.*) -> (.*) #(.*)$/); 
    $input = $1;
    $chars = $2;
    $comment = $3;
    @ch = split (" ", $chars);
    foreach (@ch) {
      s/^[0]+//;
      printf ("U+%s ", $_);
    }
    $input =~ s/''/MAKKA/g;
    $input =~ s/'//g;
    $input =~ s/ //g;
    $input =~ s/MAKKA/'/g;
    $minput = $input;
    $minput =~ s/"/\\"/g;
    $output = `echo "$minput " | uniconv -decode OldHungarian`;
    print ("$comment ($input) $output");
} 
