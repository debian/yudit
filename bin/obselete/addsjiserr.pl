#!/usr/bin/perl
while (<>)
{
  chomp;
  $l = $_;
  if (/^\d+-\d+-\d+\s+([0-9A-F]{2})([0-9A-F]{2}).*/)
  {
    $l .= sprintf ("%c%c", hex($1), hex($2)); 
  }
  print "$l\n";
}
