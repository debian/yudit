#!/usr/bin/perl
#
# EE00-E650 = 1968
# E650 E6FF EE00
if ($#ARGV != 2)
{
  print STDERR "usage: shifthex rangefrom rangeto new_from < filein > fileout$/";
  exit (1);
}

$range_from = hex (shift);
$range_to = hex(shift);
$new_from = hex(shift);

while (<>)
{
  # read chars 
  s/([0-9a-fA-F])([0-9a-fA-F])([0-9a-fA-F])([0-9a-fA-F])/&shifthex($1,$2,$3,$4)/ge;
  s/#SGC#//go;
  print;
}

exit (0);

sub shifthex {
  $in = hex ($_[0]) * 16*16*16 +  hex ($_[1]) *16*16 +  hex ($_[2])*16 +  hex ($_[3]);
  if ($in >= $range_from && $in <= $range_to)
  {
    $in =  $in + $new_from-$range_from;
  }
  return sprintf ("#SGC#%02X#SGC#%02X", $in /256, $in % 256);
}
