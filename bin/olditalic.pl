#!/usr/bin/perl
# Create OldItalic16 or OldItalic
# OldItalic16 is 16 bit (Using surrogates)

die "No version specified." if ($#ARGV == -1);
$VERSION=shift;

if ($#ARGV < 0 || ($ARGV[0] ne "16" && $ARGV[0] ne "u"))
{
  print STDERR "usage: $0 u|16 UnicodeData.txt > OldItalic.kmap\n";
  exit (0);
}


$surrogates = ($ARGV[0] eq "16") ? 1 : 0;

$suffix=($surrogates==1)?"16":"";

shift;


if ($surrogates==1)
{
  print "// Experimental OldItalic$suffix.kmap\n";
  print "// Generated from http://www.unicode.org/Public/$VERSION/uc/UnicodeData.txt\n";
  print "// Please give me a better map if you have any.\n";
  print "// Generated by $0 Gaspar Sinai <gsinai\@yudit.org>\n";
  print "// GNU (C) 2002-04-03\n";
  print "// kmap can handle only utf-16 values. Use mys for a ucs-4 map!\n";
  print "// This map will produce a surrogate pair - bad boy!.\n";
  print "// \n";
  print "// Mytool option -type clkmap must be specified when compiling.\n";
 
}
else
{
  print "# YUDIT-NtoN  1.0\n";
  print "NAME=OldItalic$suffix.kmap\n";
  print "COMM= Generated from http://www.unicode.org/Public/$VERSION/uc/UnicodeData.txt\n";
  print "COMM= Experimental OldItalic$suffix.mys\n";
  print "COMM= Please give me a better map if you have any.\n";
  print "COMM= Generated by $0 Gaspar Sinai <gsinai\@yudit.org>\n";
  print "COMM= GNU (C) 2002-04-03\n";
  print "COMM= This map will produce UCS4 characters.\n";
  print "TYPE=00000001\n";
  print "SECTION=Section1\n";
  print "ENCODE=00\n";
  print "KEY_WIDTH=00\n";
  print "VALUE_WIDTH=02\n";
  print "KEY_LENGTH=00\n";
  print "VALUE_LENGTH=00\n";
}

while (<>)
{
  if (/(103[0-9a-fA-F]{2});(OLD ITALIC \S+) ([^;]+);/)
  {
    $num = hex ($1);
    $name = $2 . " " . $3;
    $cname = $3;
    $cname =~ y/A-Z/a-z/;
    #print "num=" . $num . " " . $cname . "\n";
    $cname =~ s/ku/q/;
    $cname =~ s/ke/c/;
    $cname =~ s/ka/j/;
    $cname =~ s/khe/k/;
    $cname =~ s/the/g/;
    $cname =~ s/one/0x31/;
    $cname =~ s/five/0x35/;
    $cname =~ s/ten/0x31 0x30/;
    $cname =~ s/fifty/0x35 0x30/;
    $cname =~ s/ks/x/;
    $cname =~ s/(.)e/$1/;
    $cname =~ s/e(.)/$1/ if ($cname ne "esh");
    if (defined ($names{$cname}))
    {
      print STDERR "Already defined $name in $names{$cname}\n";
      exit (1);
    }
    $names{$cname} = $name;
    if ($surrogates == 1)
    {
      $num = $num - 0x10000;
      $to = sprintf ("0x%X 0x%X", ($num >> 10) + 0xd800, ($num & 0x3ff) + 0xdc00);
      printf ("\"%s = %s\", // %s\n", $cname, $to, $name);
    }
    else
    {
      $to = sprintf ("%08X", $num);
      $cname =~ s/0x3(.) /$1/g;
      $cname =~ s/0x3(.)/$1/g;
      $cname =~ s/(.) ?/'$1/g;
      printf ("%s -> %s # %s\n", $cname, $to, $name);
    }
  }
}
exit(0);
