#!/usr/bin/perl
# Parse JIS0213-X.TXT
# Gaspar Sinai <gsinai@yudit.org>
# Tokyo 2002-04-09

if ($#ARGV <1)
{
  die "usage: $0 {1,2} name [input-file like:JIS0213-1.TXT]";
}

$FIELD=shift;
$NAME=shift;

print <<HEADER;
NAME=$NAME
TYPE=0
SECTION=decode
ENCODE=0
KEY_WIDTH=1
VALUE_WIDTH=2
KEY_LENGTH=0
VALUE_LENGTH=0
HEADER

if ($NAME ne "jis-0213-2.mys")
{
print <<EOD;
# The following are not round trip compatible, but they are
# needed for font mapping. This will not cause any problems
# in conversion because converters are aware of this and
# they call expandYuditLigatures routine in stoolkit/SCluster.cpp.
# Y+0001 MODIFIER LETTER EXTRA-HIGH TONE BAR
# Y+0002 MODIFIER LETTER HIGH TONE BAR
# Y+0003 MODIFIER LETTER MID TONE BAR
# Y+0004 MODIFIER LETTER LOW TONE BAR
# Y+0005 MODIFIER LETTER EXTRA-LOW TONE BAR
# Y+0010 RISING LETTER
# Y+0011 FALLING LETTER
EOD
}

#
# JIS
#
if ($FIELD == 2 && $NAME ne "jis-0213-2.mys")
{
print <<EOD;
2B60 -> 80000001
2B61 -> 80000002
2B62 -> 80000003
2B63 -> 80000004
2B64 -> 80000005
2B65 -> 80000010
2B66 -> 80000011
EOD
}
#
# SJIS
#
if ($FIELD == 1)
{
print <<EOD;
8680 -> 80000001
8681 -> 80000002
8682 -> 80000003
8683 -> 80000004
8684 -> 80000005
8685 -> 80000010
8686 -> 80000011
EOD
}

while (<>)
{
  if (/^0x([0-9A-F]+)[ \t]+0x([0-9A-F]+)[ \t]+0x([0-9A-F]+)[ \t]+0x([0-9A-F]+)[ \t]+0x([0-9A-F]+)[ \t]+(.*)/)
  {
    $first = $1 if ($FIELD == 1);
    $first = $2 if ($FIELD == 2);
    printf ("%04X -> %08X %08X %08X %s\n", hex ($first), hex ($3), hex ($4), hex($5), $6);
    next;
  }
  if (/^0x([0-9A-F]+)[ \t]+0x([0-9A-F]+)[ \t]+0x([0-9A-F]+)[ \t]+0x([0-9A-F]+)[ \t]+(.*)/)
  {
    $first = $1 if ($FIELD == 1);
    $first = $2 if ($FIELD == 2);
    printf ("%04X -> %08X %08X %s\n", hex ($first), hex ($3), hex ($4), $5);
    next;
  }
  if (/^0x([0-9A-F]+)[ \t]+0x([0-9A-F]+)[ \t]+0x([0-9A-F]+)[ \t]+(.*)/)
  {
    $first = $1 if ($FIELD == 1);
    $first = $2 if ($FIELD == 2);
    printf ("%04X -> %08X %s\n", hex ($first), hex ($3), $4);
    next;
  }
}
exit (0);
