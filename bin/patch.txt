2,73d1
< #	Name:             JIS X 0208 (1990) to Unicode
< #	Unicode version:  1.1
< #	Table version:    0.9
< #	Table format:     Format A
< #	Date:             8 March 1994
< #
< #	Copyright (c) 1991-1994 Unicode, Inc.  All Rights reserved.
< #
< #	This file is provided as-is by Unicode, Inc. (The Unicode Consortium).
< #	No claims are made as to fitness for any particular purpose.  No
< #	warranties of any kind are expressed or implied.  The recipient
< #	agrees to determine applicability of information provided.  If this
< #	file has been provided on magnetic media by Unicode, Inc., the sole
< #	remedy for any claim will be exchange of defective media within 90
< #	days of receipt.
< #
< #	Recipient is granted the right to make copies in any form for
< #	internal distribution and to freely use the information supplied
< #	in the creation of products supporting Unicode.  Unicode, Inc.
< #	specifically excludes the right to re-distribute this file directly
< #	to third parties or other organizations whether for profit or not.
< #
< #	General notes:
< #
< #
< # This table contains one set of mappings from JIS X 0208 (1990) into Unicode.
< # Note that these data are *possible* mappings only and may not be the
< # same as those used by actual products, nor may they be the best suited
< # for all uses.  For more information on the mappings between various code
< # pages incorporating the repertoire of JIS X 0208 (1990) and Unicode, consult the
< # VENDORS mapping data.  Normative information on the mapping between
< # JIS X 0208 (1990) and Unicode may be found in the Unihan.txt file in the
< # latest Unicode Character Database.
< #
< # If you have carefully considered the fact that the mappings in
< # this table are only one possible set of mappings between JIS X 0208 (1990)
< # and Unicode and have no normative status, but still feel that you
< # have located an error in the table that requires fixing, you may
< # report any such error to errata@unicode.org.
< #
< #
< #	Format:  Four tab-separated columns
< #		 Column #1 is the shift-JIS code (in hex)
< #		 Column #2 is the JIS X 0208 code (in hex as 0xXXXX)
< #		 Column #3 is the Unicode (in hex as 0xXXXX)
< #		 Column #4 the Unicode name (follows a comment sign, '#')
< #			The official names for Unicode characters U+4E00
< #			to U+9FA5, inclusive, is "CJK UNIFIED IDEOGRAPH-XXXX",
< #			where XXXX is the code point.  Including all these
< #			names in this file increases its size substantially
< #			and needlessly.  The token "<CJK>" is used for the
< #			name of these characters.  If necessary, it can be
< #			expanded algorithmically by a parser or editor.
< #
< #	The entries are in JIS X 0208 order
< #
< #	The following algorithms can be used to change the hex form
< #		of JIS 0208 to other standard forms:
< #
< #		To change hex to EUC form, add 0x8080
< #		To change hex to kuten form, first subtract 0x2020.  Then
< #			the high and low bytes correspond to the ku and ten of
< #			the kuten form.  For example, 0x2121 -> 0x0101 -> 0101;
< #			0x7426 -> 0x5406 -> 8406
< #
< #   The kanji mappings are a normative part of ISO/IEC 10646.  The
< #       non-kanji mappings are provisional, pending definition of
< #       official mappings by Japanese standards bodies
< #
< #	Any comments or problems, contact <John_Jenkins@taligent.com>
< #
< #
6952a6881
> # SJIS JIS UNICODE
