#!/usr/bin/perl
# Make diff of 3 files
# GNU (C) Gaspar Sinai  <gsinai@yudit.org>
# Tokyo 2002-04-10

$PLANE1="JISX0213-1.TXT";
$PLANE2="JISX0213-2.TXT";
$VERSION="Version 1.1";

#
# SJIS is KEY
#
%JISMAP=();
%KUTENMAP=();
%UMAP=();

%ALLJ=();
%ALLU=();

&makeMap ($PLANE1);
&makeMap ($PLANE2);

#
# NAME URL DELIMITER JIS SJIS UNICODE
#
@FILE1=("WAKABA", "jisx0213code-csv32.txt",
  "http://www.ksky.ne.jp/~smile4me/charcode/jisx0213code-csv_u32.lzh",
  ",", 4, 6, 7);
# No JIS MAP.
@FILE2=("IBM_GLY", "IBM1394toUCS4-GLY.txt",
  "http://www.cse.cuhk.edu.hk/~irg/irg/N807_TablesX0123-UCS.zip",
  " ", -1, 0, 1); 
# No JIS MAP.
@FILE3=("IBM_IRV", "IBM1394toUCS4-IRV.txt",
  "http://www.cse.cuhk.edu.hk/~irg/irg/N807_TablesX0123-UCS.zip",
  " ", -1, 0, 1,); 

&makeCMP (@FILE1);
&makeCMP (@FILE2);

# Turned out to be the same.
# &makeCMP (@FILE3);

@KEYS = sort (keys (JISMAP));
$name1 = $FILE1[0];
$name2 = $FILE2[0];
$name3 = $FILE3[0];
print <<EOD;
All Unicode Maps ftp://ftp.unicode.org/Public/MAPPINGS/EASTASIA 
became obsolete from Unicode 3.2.

To be able to continue working on our Unicode Projects we 
needed to create JISX0213-1.TXT for men-1 (Plane 1) and 
JISX0213-2.TXT men-2 (Plane 2). These two maps are reconciled 
between Bruno Haible <haible\@ilog.fr> and Gaspar Sinai 
<gsinai\@yudit.org>.

These two maps are totally unofficial, and they will not solve
any problems mentioned by Tomohiro KUBOTA <tkubota\@riken.go.jp>  
http://www.debian.or.jp/~kubota/unicode-symbols.html. 

It should also be noted that tone letters that are used in this
mapping are mentioned mentioned in Section 7.8 (Modifier Letters).
The rendering engine must render at least the following glyphs:

U+02E9 U+02E5 # RISING (声調記号上昇調) in JIS X 0213
U+02E5 U+02E9 # FALLING (声調記号下降調) in JIS X 0213

------------------------------------------------------------------
Further Unresolved Issues
------------------------------------------------------------------

1)  0x83F6 0x2678 0x31F7 0x309A # 1-6-88
    This character is a 'small' variant of 0x30D7.  It will be the 
    task of the display engine to position the small circle at the 
    right position.

Differences between unofficial mappings:

EOD

printf ("%-7.7s %-4.4s %-8.8s %-17.17s %-26.26s\n",
"m-k-t", "SJIS", $name1, $name2,"LINUX");
print <<EOD;
------------------------------------------------------------------
EOD
#-----------------------------------------------------------------------------------
for (@KEYS)
{
   if ($ALLU{$name1}{$_} ne $UMAP{$_} || $ALLU{$name2}{$_} ne $UMAP{$_})
   {
     $uni = $UMAP{$_};
     $uni =~ s/\+//g;
     $uni =~ s/([0-9A-Fa-f]{8})/pack "U", hex $1/ge;
     $sj = sprintf ("%04X", $_);
     printf ("%-7.7s %-4.4s %-8.8s %-17.17s %-24.24s%s\n", 
            $KUTENMAP{$_}, $sj,
            $ALLU{$name1}{$_},
            $ALLU{$name2}{$_},
            $UMAP{$_},
            $uni);
   }
}
$date = `date '+%Y-%m-%d'`;
print <<EOD;

Resources
---------
Input file LINUX: $VERSION JIS0213-1.TXT JIS0213-2.TXT
URL: http://www.yudit.org/MAPPINGS/

Input file $name1: $FILE1[1]
URL: $FILE1[2]

Input file $name2: $FILE2[1]
URL: $FILE2[2]

Script: $0
URL: http://www.yudit.org/MAPPINGS/recmat.pl.gz

Shift_JISX0213 Encoding:
URL[euc-jp]: http://www.asahi-net.or.jp/~wq6k-yn/code/enc-x0213.html

Note:
  $name2 provides Shift_JISX0213 and Unicode codepoints only.
  This is why Shift_JISX0213 was used to synchronize the files.

Gaspar Sinai <gsinai\@yudit.org>
Tokyo, $date 
EOD

exit (0);

sub makeCMP
{
  ($name, $file, $url, $delim, $jis, $sjis, $uni) = @_; 
  open (P1, "< $file") || die "Can not open $file"; 
  while (<P1>)
  {
    chomp; split ($delim);
    $sj = hex ($_[$sjis]); 
    next if ($sj==0);
    $j = hex ($_[$jis]); 
    $u = $_[$uni]; 
    $ALLJ{$name}{$sj}=sprintf ("%04X", $j);
    if ($u =~ /........+........+......../)
    {
      $ALLU{$name}{$sj}=$u;
    }
    elsif ($u =~ /........+......../)
    {
      $ALLU{$name}{$sj}=$u;
    }
    else
    {
      $ALLU{$name}{$sj}=sprintf ("%08X", hex($u));
    }
  }
  close (P1);
}

sub makeMap 
{
  open (P1, "<$_[0]") || die "Can not open $_[0]"; 
  while (<P1>)
  {
    chomp;
    next if (/UNASSIGNED .* JIS /);
    
    if (/^0x([0-9A-F]+)[ \t]+0x([0-9A-F]+)[ \t]+0x([0-9A-F]+)[ \t]+0x([0-9A-F]+)[ \t]+0x([0-9A-F]+)[ \t]+(.*)/)
    {
      $SJ=hex($1);
      $JISMAP{($SJ)}=sprintf ("%04X", hex($2));
      $UMAP{($SJ)}=sprintf ("%08X+%08X+%08X", hex($3), hex($4), hex($5));
      $KT=$6;
      $KT=$1 if ($KT=~/(\d+-\d+-\d+)/);
      $KUTENMAP{$SJ}=$KT;
    }
    elsif (/^0x([0-9A-F]+)[ \t]+0x([0-9A-F]+)[ \t]+0x([0-9A-F]+)[ \t]+0x([0-9A-F]+)[ \t]+(.*)/)
    {
      $SJ=hex($1);
      $JISMAP{($SJ)}=sprintf ("%04X", hex($2));
      $UMAP{($SJ)}=sprintf ("%08X+%08X", hex($3), hex($4));
      $KT=$5;
      $KT=$1 if ($KT=~/(\d+-\d+-\d+)/);
      $KUTENMAP{$SJ}=$KT;
    }
    elsif (/^0x([0-9A-F]+)[ \t]+0x([0-9A-F]+)[ \t]+0x([0-9A-F]+)[ \t]+(.*)/)
    {
      $SJ=hex($1);
      $JISMAP{($SJ)}=sprintf ("%04X", hex($2));
      $UMAP{($SJ)}=sprintf ("%08X", hex($3));
      $KT=$4;
      $KT=$1 if ($KT=~/(\d+-\d+-\d+)/);
      $KUTENMAP{$SJ}=$KT;
    }
    elsif (/^0x([0-9A-F]+)[ \t]+0x([0-9A-F]+)[ \t]+(.*)/)
    {
      $SJ=hex($1);
      $JISMAP{($SJ)}=sprintf ("%04X", hex($2));
      $UMAP{($SJ)}="";
      $KT=$3;
      $KT=$1 if ($KT=~/(\d+-\d+-\d+)/);
      $KUTENMAP{$SJ}=$KT;
    }
  }
  close (P1);
}
