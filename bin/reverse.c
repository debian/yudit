#include <stdio.h>

int
main()
{
  char a[256];
  char b[20];
  char quotel;
  char quoter;
  char pcomma[256];
  int scanned;
  int line = 0;
  while (fgets  (a, 256, stdin))
  {
    line++;
    if ((scanned = sscanf (a, "%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c",
       &quotel,
       &b[0],&b[1],&b[2],&b[3],&b[4],&b[5],&b[6],&b[7],&b[8],&b[9],
       &b[10],&b[11],&b[12],&b[13],&b[14],&b[15],&b[16],&b[17],&b[18],&b[19],
       &quoter,  pcomma)) >= 22 && line > 2)
    {
      fprintf (stdout, "\"%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c\"%s\n",
       b[19],b[18],b[17],b[16],b[15],b[14],b[13],b[12],b[11],b[10],
       b[9],b[8],b[7],b[6],b[5],b[4],b[3],b[2],b[1],b[0],
       pcomma);
    }
    else
    {
      fprintf (stderr, "scanned=%d\n", scanned);
      fprintf (stdout, "%s", a);
    }
     
  }
}
