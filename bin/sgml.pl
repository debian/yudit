#!/usr/bin/perl
die unless open (UNI, "< SGML.txt");


while (<UNI>)
{
  chop;
  split /\t/;
  next if ($#_ < 3);
  $comm = $_[3];
  $comm =~ s/^#/\/\//;
  $ALL{$_[0]} = 1;
  print "\"&". $_[0] . ";" . "=" . $_[2] . "\", " . $comm;
  print $/;
}

print <<EOD;
// The following are not in SGML.TXT
"&sextile;=0x2736"
"&euro;=0x20AC",    // EURO SIGN
"&#8364;=0x20AC",   // EURO SIGN
EOD

