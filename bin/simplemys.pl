#!/usr/bin/perl
# This script creates a mys source file from a simple
# keymap file.

print <<EOD;
# YUDIT-NtoN  1.0
NAME=Kmap for $ARGV[0]
COMM=Made from UnicodeData.txt version 13.0.0
COMM=Script: simplemys.pl
COMM=GNU (C) 2020-12-12
COMM=This map will produce UCS4 characters.
TYPE=00000001
SECTION=Section1
ENCODE=00
KEY_WIDTH=00
VALUE_WIDTH=02
KEY_LENGTH=00
VALUE_LENGTH=00
EOD

while (<>) {
    chop;
    next if (/^#/);
    my @kvle = split (',', $_, 2);
    next unless ($#kvle == 1);
    my @vles = split (/ /, $kvle[0]);
    my $vle = "";
    for (@vles) {
       $vle .= sprintf (" %08X", hex($_));
    } 
    my $raw = $kvle[1];
    $raw =~ s/\([^\)]+\)//go;
    $raw =~ s/“//go;
    $raw =~ s/”//go;
    $raw =~ s/’/'/go;
    $raw =~ s/: /:/go;
    $raw =~ s/^ //go;
    $raw =~ s/ $//go;
    $raw =~ s/ /-/go;
    my @chars = split (//, $raw);
    my $key = join ("'", @chars);
    print "'$key ->$vle\n"; 
}
