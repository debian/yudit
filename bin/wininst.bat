echo off

REM XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
set TOPLEVEL="C:\Build\Yudit\yudit-3.1.0"
REM XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

REM 
REM You can create a package on windows:
REM o Install  Microsoft Windows SDK for Windows 7 and .NET  (ISO)
REM http://www.microsoft.com/downloads/details.aspx?FamilyID=71deb800-c591-4f97-a900-bea146e4fae1&displaylang=en
REM o install UnxTools
REM o http://sourceforge.net/projects/unxutils/
REM o set the path to <INSTALL-ROOT->/usr/local/wbin/ so that
REM   cp make rm 
REM o mkdir C:\Build\Yudit
REM o cd C:\Build\Yudit
REM o gunzip yudit-3.1.0.tar.gz
REM o tar xf yudit-3.1.0.tar
REM o cd yudit-3.1.0
REM o cp Makefile.in Makefile
REM o make
REM o bin\wininst
REM o build package with iss using yudit32.iss
REM

cd "%TOPLEVEL%\bin"
REM set INSTALLDIR="D:\usr\share\yudit"
set INSTALLDIR="C:\Build\Yudit\Install"
rm -rf %INSTALLDIR%
mkdir "%INSTALLDIR%"
mkdir "%INSTALLDIR%\bin"
mkdir "%INSTALLDIR%\data"
mkdir "%INSTALLDIR%\man"
mkdir "%INSTALLDIR%\man\man1"
mkdir "%INSTALLDIR%\fonts"
mkdir "%INSTALLDIR%\syntax"
mkdir "%INSTALLDIR%\config"
mkdir "%INSTALLDIR%\src"

copy /b "%TOPLEVEL%\mytool\mytool.exe"  "%INSTALLDIR%\bin"
copy /b "%TOPLEVEL%\mytool\mytool.1"  "%INSTALLDIR%\man\man1"
copy /b "%TOPLEVEL%\uniprint\uniprint.exe"  "%INSTALLDIR%\bin"
copy /b "%TOPLEVEL%\uniprint\uniprint.1"  "%INSTALLDIR%\man\man1"
copy /b "%TOPLEVEL%\uniconv\uniconv.exe"  "%INSTALLDIR%\bin"
copy /b "%TOPLEVEL%\uniconv\uniconv.1"  "%INSTALLDIR%\man\man1"
copy /b "%TOPLEVEL%\gui\yudit.exe"  "%INSTALLDIR%\bin"
copy /b "%TOPLEVEL%\fonts\README.TXT"  "%INSTALLDIR%\fonts"
copy /b "%TOPLEVEL%\fonts\yudit.hex"  "%INSTALLDIR%\fonts"
REM copy /b "%TOPLEVEL%\fonts\unifont.hex"  "%INSTALLDIR%\fonts"
REM copy /b "%TOPLEVEL%\fonts\markus9x18.bdf"  "%INSTALLDIR%\fonts"
REM copy /b "%TOPLEVEL%\fonts\markus18x18ja.bdf"  "%INSTALLDIR%\fonts"

cd "%TOPLEVEL%\gui"
REM cp -Pr locale  "%INSTALLDIR%"
mkdir "%INSTALLDIR%/locale" 

REM Renaming files in DOS is hard.
FOR %%x IN (am ar az bg bn cs de el en es fi fr ga gu hi hu ja ko mn mr pa pl ru sl sr ta uk ur vi yi zh zh_CN) DO (
md "%INSTALLDIR%\locale\%%x\LC_MESSAGES"
copy /b "locale\%%x\LC_MESSAGES\messages.mo" "%INSTALLDIR%\locale\%%x\LC_MESSAGES\yudit.mo"
)
REM xcopy /b /s /e /y locale  "%INSTALLDIR%\locale"

cd "%TOPLEVEL%"

REM cp -Pr doc  "%INSTALLDIR%"
mkdir "%INSTALLDIR%/doc" 
xcopy /b /s /e /y doc  "%INSTALLDIR%\doc"
copy /b "README.TXT"  "%INSTALLDIR%\doc"
copy /b "COPYING.TXT"  "%INSTALLDIR%\doc"
copy /b "yuditw.properties"  "%INSTALLDIR%\config\yudit.properties"

cd "%TOPLEVEL%\syntax"
copy /b  *.*  "%INSTALLDIR%\syntax"

cd "%TOPLEVEL%\mytool\kmap"
copy /b  *.my  "%INSTALLDIR%\data"
copy /b  *.kmap  "%INSTALLDIR%\src"

cd "%TOPLEVEL%\mytool\mys"
copy /b  *.my  "%INSTALLDIR%\data"
copy /b  *.mys  "%INSTALLDIR%\src"

cd "%TOPLEVEL%\mytool\uni"
copy /b  *.my  "%INSTALLDIR%\data"

cd "%TOPLEVEL%\mytool\hwd"
copy /b  *.hwd  "%INSTALLDIR%\data"

cd "%TOPLEVEL%\addon"

make -k wininst_fonts INSTALLDIR=%INSTALLDIR%
make -k wininst_syntax INSTALLDIR=%INSTALLDIR%

cd "%TOPLEVEL%\bin"

REM These should take precedence over winist_fonts.
copy /b "%TOPLEVEL%\fonts\yudit.ttf"  "%INSTALLDIR%\fonts"
copy /b "%TOPLEVEL%\fonts\OldHungarian_Full.ttf"  "%INSTALLDIR%\fonts"

cd "%TOPLEVEL%\bin"


REM cd %TOPLEVEL%
REM dir

REM
REM This is a replacement for YUDIT_DATA. On unix this is
REM /usr/share/yudit
REM -installdir is only for windows. for unix, yudit should be
REM recompiled.
REM copy /b  \Yudit\Medium\raghu.ttf  "%INSTALLDIR%\fonts"

REM %TOPLEVEL%\mytool\mytool.exe -installdir "%INSTALLDIR%"
