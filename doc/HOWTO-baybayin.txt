Baybayin input in Yudit with Baybayin.kmap

Introduction
------------

Babayin in the ancient writing system of the Tagalog people and other
tribes in the Philippines.  It is a syllabary with 3 vowels, 14
consonants and 2 punctuation marks.  Later in time, a virama or vowel
killer was added.

Installation
------------

It is assumed in this document that you have already installed yudit in
your machine.  

1.  Download a unicode baybayin font preferably the opentype font
"bbyiOT.ttf" and the keymap "Baybayin.my" from http://tagalog.ws. 

2.  Copy "bbyiOT.ttf" or any unicode baybayin font to 
/usr/share/yudit/fonts/  or ~/.yudit/fonts/

3.  After that modify /usr/share/config/yudit.properties or 
~/.yudit/yudit.properties with a text editor.

	Add a your new font by inserting the following line:

		yudit.font.MyFont=bbyiOT.ttf,...

	You can use this font in the editor window if you add it to:

		yudit.editor.fonts=.../MyFont,...

4.  Copy  "Baybayin.my" /usr/share/yudit/data/  or ~/.yudit/data/

5.  Start Yudit and select Myfont for fonts and Baybayin for keymap.

Now you can start typing in baybayin.  Have fun!

Keymap
------

The following are the keys that are not the same as latin keys.

A/a = Tagalog A
capital O/U = Tagalog O/U
capital E/I = Tagalog E/I
small letter o/u = Tagalog vowel mark o/u below the letter
small letter e/i = Tagalog vowel mark e/i above the letter
J/j = Tagalog NGA
C/c = Tagalog period
F/f = Tagalog comma
V/v = Tagalog virama

The rest are intuitive for latin typist.

h/H = Tagalog HA
p/P = Tagalog PA
k/K = Tagalog KA
s/S = Tagalog SA
l/L = Tagalog LA
t/T = Tagalog TA
n/N = Tagalog NA
b/B = Tagalog BA
m/M = Tagalog MA
g/G = Tagalog GA
r/R = Tagalog RA
d/D = Tagalog DA
y/Y = Tagalog YA
w/W = Tagalog WA

If you wish to create a phonetic transliteration from latin to baybayin, 
read the HOWTO-keymap.txt of yudit.


Sample text
-----------


ᜀᜅ᜔ ᜑᜒᜈ᜔ᜇᜒ ᜋᜄ᜔ᜋᜑᜎ᜔ ᜐ ᜐᜇᜒᜎᜒᜅ᜔ ᜏᜒᜃ
ᜀᜌ᜔ ᜋᜐᜑᜓᜎ᜔ ᜉ ᜐ ᜋᜎᜈ᜔ᜐᜅ᜔ ᜁᜐ᜔ᜇ







by Roel P. Cantada
<rcantada2002@yahoo.com>
Philippines, 4-11-2005





