How to use Yudit to edit Malayalam text.

Written by: Rajkumar S <raj2569@yahoo.com>
Date: 2002-03-09

Yudit (http://www.yudit.org/) is a free Unicode Editor, it can be
used for editing text in Indic scripts in Unicode.

To input and edit Malayalam you need a Unicode Malayalam font.
A Unicode Malayalam font is the font which has correct Malayalam
glyph in correct Unicode Codepoints. code2000.ttf True Type font
from
  http://home.att.net/~jameskass/
is one such example. 

Yudit can also use any Opentype rendering that is present in the font
if it is corresponding to the Indic spec at

http://microsoft.com/typography/OTSPEC/indicot/default.htm,

In order to render Malayalam properly a good set of glyph for 
consonant conjuncts as well as corresponding Opentype tables
are a must.

A sample font, dc-font.ttf, has been made for Yudit which has a
fairly decent collection of glyphs as well as Opentype layout
tables to render Malayalam. It will be available for download
separately at:
 
   http://www.yudit.org/download/fonts

After downloading the font you need to install it:
1. Locate font directory: 

  Windows: C:\Program Files\yudit\fonts or ~/.yudit/fonts
  Linux: /usr/share/yudit/fonts or ~/.yudit/fonts

2. Copy dc-font.ttf  or any other Malayalam Open Type/True Type
  font you need to install to the fonts directory.
3. Invoke and exit Yudit. This will create a

    ~/.yudit/yudit.properties 

  file. You need to edit a font property in
  this file, for instance:

  yudit.font.TrueType=arial.ttf,raghu.ttf...

  Add the name of your font to this line, like this:
  yudit.font.TrueType=arial.ttf,raghu.ttf,dc-font.ttf

Two key maps are provided with Yudit, one following the CDAC Inscript
format (Mal-Inscript) and another (Malayalam) following a phonetic
scheme. See the end of this document for the key assignment.

If you are not familiar with Inscript then the Malayalam keymap will 
be more intuitive. 

Select one that is convenient for you.

you are all set to use Malayalam. Please contact me if you are having
any further queries with Malayalam


Malayalam keymap

 a അ      ai ഐ    ka ക             .ta ട       pa പ          sha ശ 
aa ആ     o ഒ       kha ഖ           .tha ഠ       pha ഫ       .sa ഷ 
i ഇ        oo ഓ    ga ഗ             .da ഡ        ba ബ        sa സ 
ii ഈ     au ഔ    gha ഘ           .dha ഢ      bha ഭ        ha ഹ 
u ഉ        .m    ം    "na ങ           .na ണ        ma മ         .la ള 
uu ഊ      .h  ഃ      ca ച              ta ത          ya യ        zha ഴ 
R ഋ                   cha ഛ           tha ഥ        ra ര         + ്‌
L ഌ                   ja ജ                da ദ          .ra റ         
e എ                   jha ഝ            dha ധ       la ല 
ee ഏ                 ~na ഞ              na ന        va വ      
