How to use Tibetan fonts and keyboard map in Yudit

Original: Gregory Mokhin <mokhin__at___bog.msu.ru> 2002-10-10
Last modified: 2003-08-03


Tibetan support in Yudit is still at beta stage, however it can be useful
both for testing purposes and for typing many Tibetan texts using Unicode.
You can type Tibetan using Wylie transliteration and you will have proper
conversion to Unicode symbols as you type.  In case you have an OpenType font,
you will have proper display of the majority of Tibetan glyphs, including
very complex Tibetan-Sanskrit stacks.  But you may still see something
unexpected.


Introduction

Yudit ( http://www.yudit.org ) is a free Unicode editor which can be applied
to editing text in Tibetan script (including Tibetan proper and Dzongkha,
used mainly in Bhutan).


Pre-requisites

1. Download the latest version of Yudit from http://www.yudit.org
   and follow the installation instructions.
2. Download utibetan.ttf font from
      ***
   and copy it into the /usr/share/yudit/fonts or ~/.yudit/fonts

UTibetan.ttf font is available under GPL.  It was developed by Gregory
Mokhin, based on the glyph outlines of the LTibetan.ttf
font by Pierre Robillard, who had designed LTibetan and had made it available
under GPL (see http://www.interlog.com/~pierrer).
3. There are OpenType Tibetan fonts available elsewhere, but they are not
yet supplied with this distribution due to unclear licensing issues.


Configuration

After installing Yudit and the fonts you need to configure the fonts.  First
invoke and exit Yudit.  This will create a ~/.yudit/yudit.properties file.

Edit the font property in this file to add your Tibetan font, for instance:

yudit.font.TrueType=arial.ttf,utibetan.ttf ...

Add OpenType font to this line if you have one.

Select Tibetan for input in the Yudit GUI.  This is a clustering kmap, which
means that a whole cluster needs to be entered to get proper shaping.


Tibetan text rendering

It is important to keep in mind the difference between proper encoding of a
text (i.e., correct Unicode values for the typed letters and syllables) and
proper rendering and display of the text by a word processor.  Yudit is
capable of using glyph substitution and positioning required for proper
display of Tibetan, but only when the font itself has OpenType tables
(technically speaking, GSUB and GPOS) and the other OpenType features
necessary for the rendering engine to work properly (see the Appendix).
Utibetan.ttf is an incomplete version of the font.  It contains the glyphs
for the basic Unicode points for Tibetan but lacks the OpenType features
necessary for proper Tibetan support.  These features are currently under
development.  There exist other Tibetan fonts that do support OpenType
features, and Tibetan texts typed in Yudit will be displayed properly
with these fonts (when/if these fonts become available for public download
by their authors).


Typing Tibetan

Use Wylie transliteration to type Tibetan.  For example, try typing

      ye shes mig gcig dri ma med

to see what manifests.

To learn the Wylie transliteration system for Tibetan, one may consult an
excellent document prepared by Prof. David Germano and THDL team,
http://www.thdl.org, which presents both the original Wylie scheme and
their extensions to classic Wylie, called "Extended Wylie."  See
http://iris.lib.virginia.edu/tibet/tools/ewts.pdf
(but note the errata at
http://sourceforge.net/tracker/index.php?func=detail&aid=646583&group_id=61934&atid=502515).
Thanks to David Chandler for informing me about this.)

Note that there is still no consensus regarding a standardized set of
extensions to classic Wylie transliteration.  However, the differences
mainly concern Tibetanized Sanskrit stacks and mantras and punctuation
details.


Modifying Tibetan kmap

If you modify the Tibetan-Wylie.kmap to your needs, you should compile it
as follows:

$ mytool -convert my -write Tibetan-Wylie.my -kmap Tibetan-Wylie.kmap

and substitute the original Tibetan-Wylie.my file coming with Yudit
distribution by the compiled Tibetan-Wylie.my file.

Please send your comments to the Yudit developers and to me.

Gregory Mokhin, <mokhin__at___bog.msu.ru>
Moscow,
03 August 2003

=================================================
Appendix: Technical notes

In addition to the general flow of text from left to right, a number of
Tibetan letters may also be combined from top to bottom, and the Unicode
chart for Tibetan does attempt to take into account this property of Tibetan
writing.  Unfortunately, the existing Unicode chart for Tibetan is not
sufficient for proper display of all Tibetan syllables, and, similar to
Indic scripts, requires OpenType glyph substitution and positioning tables
to be implemented and applied for proper font rendering.

Different variants of subjoined letters should be used for different initial
syllables.  For example, the "r" in "kr", "khr", "tr", "thr" should
represent different variants of subjoined "r", because the vertical position
in the syllable with respect to the baseline depends on the initial (upper)
part of the syllable.  The same is true for subjoined "ya", "wa", and "u".
One should also take into account the variations in vertical positioning of
the elements mantric syllable stacks, which were originally borrowed from
Sanskrit and are quite common in Tibetan texts.

In fact, OpenType support for Tibetan is simpler than for Indic scripts.
The main registered OpenType layout features needed to support Tibetan
script are:

'ccmp', 'blws', 'abvs' - all belonging to GSUB (glyph substitution)
'blwm', 'abvm', 'kern' - all GPOS (glyph positioning)

