   O souborech kmap

   kmap soubory poskytují vstupní mapování mezi napsanými
   znaky a požadovanými unikódovými znakovými řetězci.
   Tyto zdrojové soubory by měly být převedeny pomocí
   programu mytool do binárního mapového souboru "jméno.my".
   Obecné zdrojové soubory .mys poskytují více vlastností,
   Tento formát je doporučeným zdrojem pro nové soubory kmap.

   Syntaxe je velmi jednoduchá a je navržena tak, aby bylo možné
   soubor zahrnout do progamu v c++.

   Poznámky
        poznámky by měly začínat //

   mapa
        Jedna řádka může obsahovat pouze jednu mapu. Mapa
        začíná a končí znakem uvozovek. Znak uvozovek
        může být vložen do řádky s použitím oddělovače \".

        Mezery jsou ignorovány. Část řetězce, která začíná
        číslem je považována za unikódový znakový kód.
        Lze specifikovat osmičková, desítková a šestnáctková
        čísla. Levá strana znaku '=' je napsaný znakový
        řetězec a pravá strana je mapovaný unikódový řetězec.

        Vzorové mapové řádky z Kana.kmap:

        "\033 KE=0x30F5",       // SMALL KE
        "\"R=0x201D",           // RIGHT DOUBLE QUOTATION MARK
        "kke=0x3063 0x3051",
        "0x20 = 0x201D",    // INPUT LITERAL BLANK 
        "0x31= 0x31",         // DIGITS
        "&/ /=0x005C",        // YUDIT NEEDS SPACE BETWEEN / /
        "0x3D=0x003D",    // INPUT '=' SIGN THIS WAY

        Pokud lze překlad rozdělit na (maximálně 5) podřízené
        překlady, pak může první řádka v souboru kmap obsahovat:

        "řetězec1+řetězec2+řetězec...",
     
        Následující řádky obsahují mezi "begin řetězec1" a "end řetězec1"
        řádky podřízených překladů. Výsledné unikódové číslo je
        hodnota sloučených podřízených překladů. Vestavěné unikódové
        řazení a kmapy Hangul používají tuto metodu.

        Podřízené překlady mohou obsahovat prázdné řetězce
        "=0x0021",
        ale samostatné nemohou.

        Překlad používá lačný algoritmus, avšak je-li první podřízený
        překlad rovný prázdnému řetězci, může být jeden znak vypůjčen
        z předchozího překladu.

Gaspar Sinai <gsinai@yudit.org> Tokyo 2001-01-11
