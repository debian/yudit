Yudit 유니코드 편집기 잦은 질문과 답 
========================
판: 2.9.6
저자:  Gaspar Sinai <gaspar@yudit.org>
최종 수정일:   2003년 6월 25일 (동경, 일본)

질문 1: 이 질문과 답을 다시 보려면 어떻게 해야 합니까?
답 1: 명령어 영역에서 `help'라고 치십시오. 또, 거기에서 `test'라고 치면 
유디트 시험 페이지를 볼 수 있습니다. `howto <topic>' (예를 들어, 
`howto configure')를 치면 howto 문서를 볼 수 있습니다. 



질문 2: 최신판은 어디서 구할 수 있나요? 
답 2: <http://www.yudit.org>에서 내려 받을 수 있습니다.

질문 3: 저장 단추를 클릭했는데도 아무런 일이 안 생깁니다. 왜 그런가요?
답 3: Yudit has a command line. 
  Most propbably you just need to hit Enter Key.

질문 4: 거의 항상 "Unix"라고 설정되어 있는 메뉴바의 맨 오른쪽에 있는 
[줄 바꿈]은 무엇을 위한 것인가요? 
A4: 줄바꿈 문자는 운영 체계나 사용자 필요에 따라 다릅니다. 유디트는
다음을 지원합니다. 
 UNIX='\n'   - 유닉스 방식 줄 바꿈
 DOS='\r\n'  - 도스 방식 줄 바꿈 
 MAC='\r'    - 맥 방식 줄 바꿈 
 LS = U+2028 - 유니코드의 줄 바꿈  문자 
 PS = U+2029 - 유니코드의 단락 바꿈  문자

 유디트에서는 이들 줄 바꿈 문자를 섞어 쓸 수 있습니다. 예를 들어,
 도스 방식 줄 바꿈과 유닉스 방식 줄 바꿈이 섞인 문서를 만들 수
 있습니다. 하지만, 가능하면 둘 이상의 줄 바꿈 문자를 섞는 일은
 피하십시오. 

질문 5: 어떻게 하면 `-e 인코딩' 옵션에 지정할 인코딩 목록을 알 수 있나요?
답 5: `man uniconv' 혹은  `uniconv -h'이라고 명령행 프롬프트에서 치십시오.

질문 6: 어떻게 키보드 설정 파일을 추가할 수 있나요?
답 6: 더하고자 하는 파일이 이미 다음 장소에 있는지 먼저 확인해 보십시오.

    /usr/share/yudit/data

 그런 경우에는 다음 파일에 추가하기만 하면 됩니다.

    /usr/share/yudit/config/yudit.properties

 If not, you need to convert the source kmap file into binary format:
 그렇지 않은 경우엔 컴파일되지 않은 kmap 파일을 이진 형식으로 
 컴파일해야 합니다. 

    mytool -type kmap -kmap My.kmap -rkmap My.kmap  -write My.my

 이렇게 얻은 My.my를 ~/.yudit/data에 복사해 넣으십시오.

    cp My.my ~/.yudit/data

 새로 만든 kmap 파일이 있다면 그 소스 파일을 다음 판에 넣을 수 있도록 
 제게 보내 주십시오. kmap  파일 형식에 대해 더 자세히 알고 싶으면 
 다음 파일을 참고하십시오. 

      /usr/share/yudit/doc/keymap-format.txt

 유디트에 이미 들어 있는 kmap 소스 파일은 다음 장소에 있습니다.

      /usr/share/yudit/src/

 kmap 파일은 인코딩 변환과 글꼴 맵 파일로도 쓰일 수 있답니다.

질문 7: 유디트의 메뉴나 도움말 등을 한국어나 다른 영어 이외의 언어로
     나오게 하려면 어떻게 합니까? 
답 7: 유디트 소스 파일을 다운로드한 후에 README.TXT를 읽어 보면,
  어떻게 해야 하는지 알 수 있습니다. 아직 지원되지 않는 언어로
	번역했다면, 다음 판에 넣을 수 있도록 그 결과를 제게 보내 주시기 
	바랍니다. 

질문 8: 트루타잎 글꼴은 어떻게 추가합니까? 
답 8: font1.ttf와 font2.ttf를 /usr/share/yudit/fonts 혹은  ~/.yudit/fonts
      에 복사하십시오. 그 다음에 /usr/share/yudit/config/yudit.properties
			에 새로운 가상 글꼴 MyFont를 다음과 같이 더해 주십시오.

    yudit.font.MyFont=font-file.ttf,some-other-fontfile.ttf,..

 편집기에서 쓰려면 다음을 더하시고, 
    yudit.editor.fonts=...,MyFont,...
 GUI 메뉴 등에서 쓰려면 다음을 더하십시오.
    yudit.fonts=...,MyFont,...

질문 9: 윈도 버전을 내놓을 계획인가요? 
답 9: 유디트는 플랫폼 독립적인 창 관련 툴킷을 쓰고 있어서 현재
  지원하는 GUI 플랫폼이 아닌 다른 플랫폼 (예를 들어, svgalib나  윈도)에 
	이식하는 일은 매우 손쉽습니다. 2001년 12월에 제 아내 유코를 위해
	윈도에 이식했습니다. 그것은 제가 윈도를 만진 처음이자 마지막이었습니다.

질문 10: 유디트는 얼마나 좋은 유니코드  편집기인가요? 
답 10: 저는 유니코드 표준이 합리적이라고 생각하는 부분에서는 
   이 표준을 구현하려고 노력했습니다. 유니코드 표준에는 하지만
	 몇 가지 저에겐 이상해 보이는 부분이 있습니다. BIDI (아랍어,
	 히브리어 등에서 필요한 양방향) 지원이 유니코드 표준에 얼마나
	 부합하는지에 대해서는   다음 문서를 읽어 보십시오.

     /usr/share/yudit/doc/HOWTO-bidi.txt

  바탕  글자(base character)와 `액센트' (여러 유럽이나 아프리카의 언어나
	베트남어, 수학 기호 등에서 필요한)를  결합해서 새로운 글자를 만들어 
	내는 것은 (A와 U+0302 - circumflex - 를 잇달아 써서 Â를 나타내는 일 
	따위)  지원합니다. 아랍어나 히브리어, 인도의 여러 스크립트에서 필요한
	`shaping'도 지원을 더하는 중입니다.  (역주 : 중세 한국어 지원은 아직
	더해지지 않았습니다.)

질문 11: 명령행에 3개의 문서를 지정했습니다만, 오직 하나만 보입니다. 
답 11: 나머지 두 개는 전에 편집한 문서 목록에 들어 있습니다. 명령
  입력 영역으로 가서 위 화살표 (혹은 Ctrl-k)나 아래 화살표 (혹은
	Ctrl-j)를 누르면 이 목록에 있는 문서 사이를 이동할 수 있습니다.

질문 12: 단축 키는 무엇 무엇이 있나요? 
답 12: 대부분의 단축 키는 노란색 `풍선' 도움말을 통해 알 수 있습니다.
 이들  대부분은 Alt는 물론 Shift와도 같이 작동합니다. 
 
 유디트에 있는 편집기는 다음과 같은 키를 지원합니다. 

 Escape        : 명령 모드와 편집 모드 사이를 전환합니다.
 Home          : 화면 맨 위 왼쪽으로 이동합니다.
 CTRL-b        : 한 쪽 아래 
 CTRL-f        : 한 쪽 아래
 CTRL-k        : 위
 CTRL-n,CTRL-j : 아래 
 CTRL-h        : 왼쪽 
 CTRL-l        : 오른쪽 
 CTRL-m        : 한 줄 지움
 CTRL-x        : 선택한 부분 지움
 CTRL-v        : 마지막으로 선택한 부분 오려 붙이기 

 CTRL 대신 Shift나 Meta(대부분 PC 키보드에서 Alt)를 누른 채로 커서를 
 움직이면 문서의 부분을 선택할 
 수 있습니다.  마우스를 두 번 클릭하면 단어를 선택하고, 세 번 연속 클릭하면 
 줄 전체를 선택합니다.

질문 13: 이진 kmap 파일의 내용을 알고 싶으면 어떻게 해야 합니까? 
답13: 다음과 같이 하면 이진 파일로부터 원본 파일을 얻을 수 있습니다.
 mytool -my /usr/share/yudit/data/GreekBible.my \
      -convert mys -write GreekBible.mys

질문 14: 코드 포인트를 아는 유니코드 글자를 쉽게 넣는 방법이 있나요? 
답 14: 입력 모드를  [unicode]로 바꾼 후에  `uxxxx' 혹은 `Uxxxxxxxx'를
 치면 됩니다. `x'는 16진 숫자입니다. 

질문 15: 치환 기능이 있나요? 
답 15: 명령 영역에서 다음을 넣은 후에 커서가 `old-text'에 가 있을 때 
  [enter]를 누르십시오. 
  replace old-text new-text

질문 16: 마우스로 그려 입력하기를 쓰려면 어떻게 하나요? 
답 16: 파란 화살표(입력 방법)을 클릭한 후에 `freehand'를 기능 키 가운데
 하나에 할당하면 됩니다.

Q17: How can I turn off syntax highlighting?
A17: From the command area by typing: 'syntax none'.
  The available options are printed in error label
  if you just type 'syntax'.  You can set this in
  the config file too.

Q18: How can I turn off word wrapping?
A18: From the command area by typing: 'wordwrap false'.
  Typing 'wordwrap true' turns on word wrapping.
  You can set this in the config file too.

부록 
========
1.1. 키보드 맵 보기 

 다음 보기는 유디트에 들어 있는 몇몇 kmap입니다. 

 러시아어 (Russian)
  라틴 알파벳으로 키릴 알파벳의 러시아어 발음을 입력하면 해당하는
	키릴 알파벳을 입력할 수 있습니다. 그다지 직관적이지 않은 것들에는
	다음이 있습니다. 
     yo ->  ё , c -> ь,  q -> ъ
     x  ->  ы , ee -> э

 헝가리어 (Hungarian)
  헝가리어와 독일어에 쓰이는 액센트 붙은 글자를 넣을 때 쓰입니다.
	액센트 붙은 글자는 액센트 없는 라틴 알파벳을 친 다음에 액센트를
	쳐서 입력할 수 있습니다. 

    보기 : 
      a' ->  á , o: ->  ö ,  o" ->  ő 
      SS ->  독일어의 ß , Ss  -> 문단/절 기호  §.
  카나 
	 일본어 입력에 쓰이는 방식으로 Romaji (로마자)를 히라가나와
	 카타카나로 바꿔 줍니다. 소문자는 히라가나에  대문자는 카타카나에
	 씁니다. 직관적으로 알 수 없는 보기는 아래와 같습니다. 

      PP - 〒   <> - ◇  <>> - ◆     [] - □
      []] - ■  OO - ●  <ESC>* - ☆  <ESC>** - ★  
       ~ - ・    Oo - ◎  o+ - ♀      o- - ♂
       oC - ℃   Y= - ￥

  Mnemonic 
	  IETF RFC 1345에서 정의한 니모닉을 써서 입력할 수 있습니다.
    보기 :  
       &0u - ☺  &0U - ☻ &tel - ☏  &PI - ¶
       &SU - ☼  &cH-   -  ♥  &M2=♫ &sn - ش

  SGML 
	  SGML entity 이름을 Unicode 글자로 대응시킵니다. 
     보기 : 
       &female; - ♀ &spades; - ♠, &boxvR; - ╞
       &block; - █ &blk14; - ░ &frac18; - ⅛

  Hangul 
	 한국어  로마자 표기법대로 로마자를 넣으면 대응하는 한글을
	 입력할 수 있습니다. 한국어 로마자 표기법은 ISO/TC46/SC2/WG4 
	 규정을 따릅니다. 자세한 것은 다음 웹 사이트에서 찾을 수 있습니다. 

      http://www.hansoft.com/hangul/faq.html

   오토마타가 쓰이는 것이 아니고 단순히 키보드 맵만을 쓰고 있으므로
	 때로는 음절 구별을 위해 공백 문자를 쳐야 하는 경우도 있습니다.

    보기 : 
      ulyanghan - 우량한  pyohyeon - 표현
      cinseon - 친선,  jageug - 자극 hwang geum 황금
