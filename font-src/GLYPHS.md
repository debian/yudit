The font package contains support for the following basic glyphs in the Old Hungarian plane:

- `U+10C80` CAPITAL A: 𐲀
- `U+10C81` CAPITAL AA: 𐲁
- `U+10C82` CAPITAL EB: 𐲂
- `U+10C83` CAPITAL AMB: 𐲃
- `U+10C84` CAPITAL EC: 𐲄
- `U+10C85` CAPITAL ENC: 𐲅
- `U+10C86` CAPITAL ECS: 𐲆
- `U+10C87` CAPITAL ED: 𐲇
- `U+10C88` CAPITAL AND: 𐲈
- `U+10C89` CAPITAL E: 𐲉
- `U+10C8A` CAPITAL CLOSE E: 𐲊
- `U+10C8B` CAPITAL EE: 𐲋
- `U+10C8C` CAPITAL EF: 𐲌
- `U+10C8D` CAPITAL EG: 𐲍
- `U+10C8E` CAPITAL EGY: 𐲎
- `U+10C8F` CAPITAL EH: 𐲏
- `U+10C90` CAPITAL I: 𐲐
- `U+10C91` CAPITAL II: 𐲑
- `U+10C92` CAPITAL EJ: 𐲒
- `U+10C93` CAPITAL EK: 𐲓
- `U+10C94` CAPITAL AK: 𐲔
- `U+10C95` CAPITAL UNK: 𐲕
- `U+10C96` CAPITAL EL: 𐲖
- `U+10C97` CAPITAL ELY: 𐲗
- `U+10C98` CAPITAL EM: 𐲘
- `U+10C99` CAPITAL EN: 𐲙
- `U+10C9A` CAPITAL ENY: 𐲚
- `U+10C9B` CAPITAL O: 𐲛
- `U+10C9C` CAPITAL OO: 𐲜
- `U+10C9D` CAPITAL NIKOLSBURG OE: 𐲝
- `U+10C9E` CAPITAL RUDIMENTA OE: 𐲞
- `U+10C9F` CAPITAL OEE: 𐲟
- `U+10CA0` CAPITAL EP: 𐲠
- `U+10CA1` CAPITAL EMP: 𐲡
- `U+10CA2` CAPITAL ER: 𐲢
- `U+10CA3` CAPITAL SHORT ER: 𐲣
- `U+10CA4` CAPITAL ES: 𐲤
- `U+10CA5` CAPITAL ESZ: 𐲥
- `U+10CA6` CAPITAL ET: 𐲦
- `U+10CA7` CAPITAL ENT: 𐲧
- `U+10CA8` CAPITAL ETY: 𐲨
- `U+10CA9` CAPITAL ECH: 𐲩
- `U+10CAA` CAPITAL U: 𐲪
- `U+10CAB` CAPITAL UU: 𐲫
- `U+10CAC` CAPITAL NIKOLSBURG UE: 𐲬
- `U+10CAD` CAPITAL RUDIMENTA UE: 𐲭
- `U+10CAE` CAPITAL EV: 𐲮
- `U+10CAF` CAPITAL EZ: 𐲯
- `U+10CB0` CAPITAL EZS: 𐲰
- `U+10CB1` CAPITAL ENT-SHAPED SIGN: 𐲱
- `U+10CB2` CAPITAL US: 𐲲
- `U+10CC0` SMALL A: 𐳀
- `U+10CC1` SMALL AA: 𐳁
- `U+10CC2` SMALL EB: 𐳂
- `U+10CC3` SMALL AMB: 𐳃
- `U+10CC4` SMALL EC: 𐳄
- `U+10CC5` SMALL ENC: 𐳅
- `U+10CC6` SMALL ECS: 𐳆
- `U+10CC7` SMALL ED: 𐳇
- `U+10CC8` SMALL AND: 𐳈
- `U+10CC9` SMALL E: 𐳉
- `U+10CCA` SMALL CLOSE E: 𐳊
- `U+10CCB` SMALL EE: 𐳋
- `U+10CCC` SMALL EF: 𐳌
- `U+10CCD` SMALL EG: 𐳍
- `U+10CCE` SMALL EGY: 𐳎
- `U+10CCF` SMALL EH: 𐳏
- `U+10CD0` SMALL I: 𐳐
- `U+10CD1` SMALL II: 𐳑
- `U+10CD2` SMALL EJ: 𐳒
- `U+10CD3` SMALL EK: 𐳓
- `U+10CD4` SMALL AK: 𐳔
- `U+10CD5` SMALL UNK: 𐳕
- `U+10CD6` SMALL EL: 𐳖
- `U+10CD7` SMALL ELY: 𐳗
- `U+10CD8` SMALL EM: 𐳘
- `U+10CD9` SMALL EN: 𐳙
- `U+10CDA` SMALL ENY: 𐳚
- `U+10CDB` SMALL O: 𐳛
- `U+10CDC` SMALL OO: 𐳜
- `U+10CDD` SMALL NIKOLSBURG OE: 𐳝
- `U+10CDE` SMALL RUDIMENTA OE: 𐳞
- `U+10CDF` SMALL OEE: 𐳟
- `U+10CE0` SMALL EP: 𐳠
- `U+10CE1` SMALL EMP: 𐳡
- `U+10CE2` SMALL ER: 𐳢
- `U+10CE3` SMALL SHORT ER: 𐳣
- `U+10CE4` SMALL ES: 𐳤
- `U+10CE5` SMALL ESZ: 𐳥
- `U+10CE6` SMALL ET: 𐳦
- `U+10CE7` SMALL ENT: 𐳧
- `U+10CE8` SMALL ETY: 𐳨
- `U+10CE9` SMALL ECH: 𐳩
- `U+10CEA` SMALL U: 𐳪
- `U+10CEB` SMALL UU: 𐳫
- `U+10CEC` SMALL NIKOLSBURG UE: 𐳬
- `U+10CED` SMALL RUDIMENTA UE: 𐳭
- `U+10CEE` SMALL EV: 𐳮
- `U+10CEF` SMALL EZ: 𐳯
- `U+10CF0` SMALL EZS: 𐳰
- `U+10CF1` SMALL ENT-SHAPED SIGN: 𐳱
- `U+10CF2` SMALL US: 𐳲
- `U+10CFA` NUMBER ONE: 𐳺
- `U+10CFB` NUMBER FIVE: 𐳻
- `U+10CFC` NUMBER TEN: 𐳼
- `U+10CFD` NUMBER FIFTY: 𐳽
- `U+10CFE` NUMBER ONE HUNDRED: 𐳾
- `U+10CFF` NUMBER ONE THOUSAND: 𐳿

The font also supports the following commonly used ligatures:

- `U+10C87 + U+200D + U+10CAF` CAPITAL DZ LIGATURE (D+Z): 𐲇‍𐲯
- `U+10C87 + U+200D + U+10CB0` CAPITAL DZS LIGATURE (D+ZS): 𐲇‍𐲰
- `U+10C93 + U+200D + U+10CAE` CAPITAL Q LIGATURE (K+V): 𐲓‍𐲮
- `U+10CAE + U+200D + U+10CAE` CAPITAL W LIGATURE (V+V): 𐲮‍𐲮
- `U+10C93 + U+200D + U+10CA5` CAPITAL X LIGATURE (K+SZ): 𐲓‍𐲥
- `U+10C92 + U+200D + U+10C90` CAPITAL Y LIGATURE (J+I): 𐲒‍𐲐
- `U+10CC7 + U+200D + U+10CEF` SMALL DZ LIGATURE (D+Z): 𐳇‍𐳯
- `U+10CC7 + U+200D + U+10CF0` SMALL DZS LIGATURE (D+ZS): 𐳇‍𐳰
- `U+10CD3 + U+200D + U+10CEE` SMALL Q LIGATURE (K+V): 𐳓‍𐳮
- `U+10CEE + U+200D + U+10CEE` SMALL W LIGATURE (V+V): 𐳮‍𐳮
- `U+10CD3 + U+200D + U+10CE5` SMALL X LIGATURE (K+SZ): 𐳓‍𐳥
- `U+10CD2 + U+200D + U+10CD0` SMALL Y LIGATURE (J+I): 𐳒‍𐳐

The font also supports the number five hundred as a ligature of the 5 and 100 numerals:

- `U+10CFB + U+200D + U+10CFE` NUMBER FIVE HUNDRED LIGATURE (5+100): 𐳻‍𐳾

All ligatures can be written both left to right or right to left, they are both defined, and the same in the font

The font also defines `U+200D` for ligatures to work.

It also defines the following puonctuation marks, as they are used in Old Hungarian but commonly
not found in fonts:

- `U+2E41` REVERSED COMMA: ⹁
- `U-2E42` DOUBLE LOW-REVERSED-9 QUOTATION MARK: ⹂

The full version of the font also contains some glyphs from the Lato package for applications that doesn't support fallbacks
for punctuation and whitespace characters. The slim version of the font doesn't contain these additional characters.
