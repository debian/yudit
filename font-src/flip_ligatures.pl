#!/usr/bin/perl
# Flip the ligatures to LR in OldHungarian.ttf
# Gaspar Sinai <gaspar yudit org>
# 2020-05-17
# After running the script change the ligature meta data to 
# mark these as LR ligatures.

while (<>) {
    chop;
    if (/Ligature2: "OLD_HUNGARIAN_LIGATURES" (.*)$/) {
        print "Ligature2: \"OLD_HUNGARIAN_LIGATURES\"  ". &rev ($1) . "\n";
        next;
    } 
    if (/Ligature2: "ROVAS_SZABVANY_LIGATURES" (.*)$/) {
        print "Ligature2: \"ROVAS_SZABVANY_LIGATURES\"  ". &rev ($1) . "\n";
        next;
    }
    print "$_\n";
}

sub rev () {
    my @arr = split (/ /, $_[0]);
    my $i;
    my @ret = ();
    for ($i=0; $i<=$#arr; $i++) {
        push (@ret, $arr[$#arr - $i]);
    }
    return join (" ", @ret);
}
