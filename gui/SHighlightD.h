/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef SHighlightD_h
#define SHighlightD_h

#include "swidget/SFrame.h"
#include "swidget/SBorder.h"
#include "swidget/STextList.h"
#include "swidget/STextEdit.h"
#include "swidget/SButton.h"
#include "swidget/SListBox.h"
#include "swidget/STextEdit.h"
#include "swidget/STextDialog.h"
#include "swindow/SImage.h"
#include <stoolkit/SIO.h> 

class SHighlightD : public SFrame, 
  public SListListener, public SButtonListener, public SFrameListener
{
public:
  SHighlightD (void);
  virtual ~SHighlightD ();
  bool  getInput(const SString& title);

  void setFont (const SString& font, double fontSize=0.0);
  void setFontSize (double fontSize);

  virtual void setBackground (const SColor& bg);
  virtual void setForeground (const SColor& lrfg, const SColor& rlfg);
  virtual void setTitleForeground (const SColor& fg);
  virtual void setSliderBackground (const SColor& bg);
  virtual void setApplicationImage (const SImage& image);

  void setLabelForeground (const SColor& fg);
  void setHighlightName (const SString& filename);

  SString getHighlightName ();

  protected:
  // SButtonListener
  virtual void buttonPressedAccel (void* source, const SAccelerator* acc);
  // SListListener 
  virtual void itemSelected (void* source, const SAccelerator* acc);
  // SFrameListener
  virtual bool close (SPanel* comp);

private:
  bool     isCancel;

  void     recalc ();
  void     populate ();
  void     populate (const SString& cat);
  void     highlightChanged ();

  SListBox*  categoryListBox;
  SListBox*  highlightListBox;

  SLabel*    remarkTitleLabel;
  SLabel*    remarkNameLabel;

  SLabel*    folderTitleLabel;
  SLabel*    folderNameLabel;
 
  SButton*   yesButton;
  SButton*   cancelButton;

  SString    currentCat;
  SString    currentHi;

};

#endif /* SHighlightD_h */
