/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "gui/SKMapDialog.h"

SKMapDialog::SKMapDialog (void) : SDialog (new SKMapPanel())
{
  panel = (SKMapPanel*) dialogPanel;
  //recalc();
}

SKMapDialog::~SKMapDialog ()
{
}

bool
SKMapDialog::getInput (const SString& title, 
  const SStringVector& kmapList, int selected)
{
  panel->setList (kmapList, selected);
  setTitle (title);
  panel->recalc();
  return SDialog::getInput (SDialog::SS_OK_CANCEL);
}

void
SKMapDialog::setFont (const SString& font, double fontSize)
{
  panel->setFont (font, fontSize);
  SDialog::setFont (font, fontSize);
}

void
SKMapDialog::setFontSize (double fontSize)
{
  panel->setFontSize (fontSize);
  SDialog::setFontSize (fontSize);
}

void
SKMapDialog::setBackground (const SColor& bg)
{
  SDialog::setBackground (bg);
  panel->setBackground (bg);
}

void
SKMapDialog::setForeground (const SColor& lrfg, const SColor& rlfg)
{
  SDialog::setForeground (lrfg, rlfg);
  panel->setForeground (lrfg, rlfg);
}

void
SKMapDialog::setTitleForeground (const SColor& fg)
{
  SDialog::setTitleForeground (fg);
  panel->setTitleForeground (fg);
}

void
SKMapDialog::setSliderBackground (const SColor& bg)
{
  panel->setSliderBackground (bg);
}
void
SKMapDialog::setLabelForeground (const SColor& fg)
{
  //SDialog::setLabelForeground (fg);
  panel->setLabelForeground (fg);
}

const SStringVector&
SKMapDialog::getKMapList() const
{
  return panel->getKMapList ();
}

int
SKMapDialog::getSelectedKMap() const
{
  return panel->getSelectedKMap ();
}

/**
 * Set XInputs that are coming with <name>:<locale>
 * pair, where name is the name of the input method and
 * encoding is an optional locale setting.
 */
void
SKMapDialog::setXInputs (const SStringVector& list)
{
  SStringVector xinputs;
  for (unsigned int i=0; i<list.size(); i++)
  {
    SString sx ("x-");
    sx.append (list[i]);
    xinputs.append (sx);
  }
  panel->setXInputs (xinputs);
}
