/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef SKMapDialog_h
#define SKMapDialog_h

#include "swidget/SDialog.h"
#include "gui/SKMapPanel.h"
#include "stoolkit/SStringVector.h"

class SKMapDialog : public SDialog
{
public:
  SKMapDialog (void);
  virtual ~SKMapDialog ();
  bool  getInput (const SString& title, 
        const SStringVector& kmapList, int selected);

  void setFont (const SString& font, double fontSize=0.0);
  void setFontSize (double fontSize);
  void setXInputs (const SStringVector& list);

  virtual void setBackground (const SColor& bg);
  virtual void setForeground (const SColor& lrfg, const SColor& rlfg);
  virtual void setTitleForeground (const SColor& fg);
  virtual void setSliderBackground (const SColor& bg);
  void setLabelForeground (const SColor& fg);

  const SStringVector& getKMapList() const;
  int getSelectedKMap() const;
protected:
  SStringVector xinputs;
  SKMapPanel*   panel;
};

#endif /* SKMapDialog_h */
