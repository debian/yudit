/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef SKMapPanel_h
#define SKMapPanel_h

#include "swidget/STextTable.h"
#include "swidget/SListBox.h"
#include "swidget/SButton.h"
#include "swidget/SLabel.h"
#include "swidget/SPanel.h"

class SKMapPanel : public SPanel, public SListListener, public SButtonListener
{
public:
  SKMapPanel (void);
  virtual ~SKMapPanel ();
  void  setList (const SStringVector& kmapList, int selected);

  void setFont (const SString& font, double fontSize=0.0);
  void setFontSize (double fontSize);

  virtual void setBackground (const SColor& bg);
  virtual void setForeground (const SColor& lrfg, const SColor& rlfg);
  virtual void setTitleForeground (const SColor& fg);
  virtual void setSliderBackground (const SColor& bg);
  void setLabelForeground (const SColor& fg);
  void setXInputs (const SStringVector& list);

  const SStringVector& getKMapList() const;
  int getSelectedKMap() const;
  void recalc();

protected:
  SStringVector allinputs;
  SStringVector xinputs;
  SStringVector kmapList;
  SStringVector sampleList;

  int           selectedKMap;
  SLabel*       titleLabel;
  SLabel*       currentLabel;
  SLabel*       sampleTitleLabel;
  SLabel*       sampleLabel;
  SListBox*     listBox; 
  SButton*      replaceButton;
  STextTable*   kmapTable;
  STextTable*   sample;
  virtual void buttonPressedAccel (void* source, const SAccelerator* acc);
  virtual void itemSelected (void* source, const SAccelerator* acc);
  void getSample();
};

#endif /* SKMapPanel_h */
