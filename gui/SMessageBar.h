/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef SMessageBar_h
#define SMessageBar_h

#include "swidget/SLabel.h"
#include "stoolkit/SProperties.h"
#include "stoolkit/STextData.h"

class SMessageBar  : public SPanel
{
public:
  enum SLabelIndex {
   SS_ROW_TITLE=0, SS_ROW,
   SS_COLUMN_TITLE, SS_COLUMN, 
   SS_FONT_SIZE_TITLE, SS_FONT_SIZE, 
   SS_GLYPH_TITLE, SS_GLYPH_INFO, 
   SS_MAX };

  SMessageBar (const SProperties props);
  virtual ~SMessageBar ();
  virtual void resize(const SDimension& d);

  void setRow (unsigned int row);
  void setColumn (unsigned int column);
  void setFontSize (double d);
  void setGlyphInfo (const SGlyph* g, const SString& encoded);
  double fontSize;

private:
  SLabel* labels[SS_MAX];
  bool    xmanagerCludge;
};

#endif /* SMessageBar_h */
