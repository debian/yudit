/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "gui/SMessageLabel.h"
#include "swidget/SIconFactory.h"
#include "stoolkit/SUtil.h"

SMessageLabel::SMessageLabel (const SProperties props)
{
  xmanagerCludge = true;
  SString font = props["yudit.default.font"];
  SString fontSizeString=props["yudit.default.fontsize"];

  double fontSize=0;
  fontSizeString.append ((char)0);
  sscanf (fontSizeString.array(), "%lf", &fontSize);
  if (fontSize < 2.0) fontSize = 2.0;
  SColor fg = SColor(props["yudit.label.foreground"]);
  SColor tfg = SColor(props["yudit.title.foreground"]);

  SLabel* l;
  l = new SLabel ("",  SIconFactory::getIcon("Inform"));
  l->setFont(font, fontSize);
  l->setForeground (tfg);
  SDimension dh = l->getPreferredSize();
  l->move (SLocation (0, 0));
  l->resize (dh);
  messageTitleLabel = l;

  l = new SLabel ("");
  l->setFont(font, fontSize);
  l->move (SLocation ((int)dh.width, 0));
  l->resize (dh);

  messageLabel = l;
  add (messageTitleLabel);
  add (messageLabel);

  currentMessageType = SS_NONE;
  preferredSize = SDimension (dh.width + 100, dh.height); 
}

SMessageLabel::~SMessageLabel ()
{
}

/**
 * This routine was introduced to make this
 * usable on peecees with windows.
 * Seems like XManager has some bug that they don't
 * fix. On unix you could in fact get rid of this whole  method.
 * The presiously set size got lost. I don't really know why XManager is so
 * popular....
 */
void 
SMessageLabel::resize(const SDimension& d)
{
  SPanel::resize (d);
  /* we only need to do it once. */
  if (!xmanagerCludge)
  {
    return;
  }
  xmanagerCludge = false;
  SDimension dh = messageTitleLabel->getPreferredSize();
  messageTitleLabel->move (SLocation (0, 0));
  messageTitleLabel->resize (dh);

  messageLabel->move (SLocation ((int)dh.width, 0));
  messageLabel->resize (SDimension (d.width, dh.height));
  messageTitleLabel->setIcon (0);
}

void
SMessageLabel::setMessage (const SString& msg, SMessageType info)
{
  messageLabel->setText (msg);
  messageLabel->resize (messageLabel->getPreferredSize());

  if (info==currentMessageType) return;
  currentMessageType = info;
  switch (info)
  {
  case SS_ERR:
    messageTitleLabel->setIcon (SIconFactory::getIcon("Error"));
    //messageTitleLabel->setBackground (SColor("Black"));
    //messageTitleLabel->setLabelBackground (SColor("Red"));
    messageTitleLabel->setForeground (SColor("White"));
    break;
  case SS_WARN:
    messageTitleLabel->setIcon (SIconFactory::getIcon("Caution"));
    //messageTitleLabel->setBackground (SColor("Black"));
    //messageTitleLabel->setLabelBackground (SColor("Yellow"));
    messageTitleLabel->setForeground (SColor("Black"));
    break;
  case SS_INFO:
    messageTitleLabel->setIcon (SIconFactory::getIcon("Inform"));
    messageTitleLabel->setBackground (background);
    messageTitleLabel->setForeground (SColor("Black"));
    break;
  default:
    messageTitleLabel->setBackground (background);
    messageTitleLabel->setForeground (SColor("Black"));
    messageTitleLabel->setIcon (0);
    break;
  }
}
