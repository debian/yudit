/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef SMessagLabel_h
#define SMessagLabel_h

#include "swidget/SPanel.h"
#include "swidget/SLabel.h"

class SMessageLabel  : public SPanel
{
public:
  enum SMessageType {
   SS_ERR=0, SS_WARN=1, SS_INFO=2, SS_NONE=3
  };

  SMessageLabel (const SProperties props);
  virtual ~SMessageLabel ();
  void   setMessage (const SString& msg, SMessageType info=SS_NONE);
  virtual void   resize(const SDimension& d);

protected:
  SMessageType      currentMessageType; 
  SLabel*           messageTitleLabel;
  SLabel*           messageLabel;
  bool  xmanagerCludge;
};

#endif /* SMessagLabel_h */
