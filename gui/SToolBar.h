/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef SToolBar_h
#define SToolBar_h

#include "stoolkit/SProperties.h"
#include "swidget/SPanel.h"
#include "swidget/SButton.h"
#include "swidget/SLabel.h"
#include "stoolkit/SBinVector.h"
#include "stoolkit/SCharClass.h"
#include "stoolkit/SEvent.h"

class SIconUpdater : public SEventTarget
{
public:
  SIconUpdater (SWindow* _w, SButton* label, 
       int* currentEvent, int* displayEvent);
  ~SIconUpdater();
  bool timeout (const SEventSource* s);
  void start ();
  void setEncrypted(bool is);
private:
  bool           isEncrypted;
  SButton*       label;
  int*           currentEvent; 
  int*           displayEvent; 
  STimer*        eventTimer;
  SWindow*       window;
};

/**
 * A caret that redraws itself differently for lr and rl text 
 */
class SToolBarListener
{
public:
  SToolBarListener(void);
  virtual ~SToolBarListener();
  virtual void toolBarButtonPressed (void* src, int button, bool accel)=0;
  virtual void toolBarButtonEnter (void* src, int button);
  virtual void toolBarButtonLeave (void* src, int button);
};

class SToolBar  : public SPanel, public SButtonListener, public SSyntaxListener
{
public:
  enum SButtonIndex {SS_OPEN=0, 
   SS_SAVE, SS_PRINT, SS_PRINT_PREVIEW, SS_FIND, SS_GOTO, 
   SS_UNDO, SS_REDO, SS_DOCUMENT_EMBEDDING, SS_SET_OVERRIDE, SS_SET_EMBEDDING,
   SS_YIELD_EMBEDDING, SS_MAGNIFY_MINUS, SS_MAGNIFY_PLUS, 
   SS_HIGHLIGHTING,
   SS_FONT, SS_INPUT, SS_PARAGRAPH_BREAK, SS_MAX };

  SToolBar (const SProperties props);
  virtual ~SToolBar ();

  SButton* buttons[SS_MAX];
  void setListener (SToolBarListener* listener);
  void setFileName (const SString& fileName, const SString& encoding); 
  void setEncrypted (bool is); 
  void setHighlightName (const SString& _fileType);
  void inputChanged (const SStringVector& _inputs, unsigned int _current);

  virtual void buttonPressedAccel (void* source, const SAccelerator* accel);
  virtual void leaveComponent (void* source);
  virtual void enterComponent (void* source);
  virtual void redraw(SCanvas* w, int x, int y,
     unsigned int width ,unsigned int height);
  virtual void resize(const SDimension & d);
  // SSyntaxListener
  virtual void syntaxChanged (SS_EventType _evnt);

  unsigned int  currentFontSize;
  SStringVector fontsizes;
  unsigned int  currentInput;
  SStringVector inputs;
  unsigned int  currentFont;
  SStringVector fonts;

  enum SFormatIndex { SS_FORMAT_UNIX=0, SS_FORMAT_DOS, 
     SS_FORMAT_MAC, SS_FORMAT_PS };

  void setParagraphSeparator (int index, bool notify);
  void setEmbedding (SS_Embedding index, bool notify);

  SStringVector paragraphBreakStrings;
  unsigned int  currentParagraphSeparator;
  SS_Embedding  currentEmbedding;
  SStringVector paragraphBreaks;
  void          directionChanged (bool islr);
  bool   editorLR;

  void   setModified (bool flag);
  void   setPrinting (bool flag);

  bool   overrideLR;
  bool   embedLR;
  bool   currentLR;

  bool   modified;
  bool   printing;

private:
  SOpaqueLabel*     title;
  SOpaqueLabel*     titleEncoding;
  SLabel*           tooltip;
  SStringVector     displayInputs;
  SIconUpdater*     iconUpdater;
  SString           highlightName;

  void setFont (int index);
  void setFontSize (int index);
  void setInput (int index);
  void setButton (SButtonIndex index, const SString& txt, bool notify);
  void buildDisplayInputs();

  SDimension getMaxSize(SButton* b, const SStringVector&l, const SString& str);
  void setFAccelerator (SButton* b, unsigned int se, bool ctrl);
  int getFAccelerator (const SAccelerator* accel);
  unsigned int preferredWidth;
  SToolBarListener* listener;
  bool  xmanagerCludge;
  int   currentEvent;
  int   displayEvent;
};

#endif /* SToolBar_h */
