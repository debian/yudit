/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef SYudit_h
#define SYudit_h

#include "stoolkit/SStringVector.h"
#include "stoolkit/SProperties.h"
#include "stoolkit/SProperties.h"
#include "stoolkit/sedy/SSedy.h"
#include "swidget/SFrame.h"
#include "swidget/STextEdit.h"
#include "swidget/SSlider.h"
#include "swidget/SFileDialog.h"
#include "swidget/SPasswordDialog.h"
#include "swidget/STextDialog.h"
#include "swidget/SFreeHand.h"

#include "gui/SToolBar.h"
#include "gui/SMessageBar.h"
#include "gui/SMessageLabel.h"
#include "gui/SKMapDialog.h"
#include "gui/SHighlightD.h"
/**
 * A caret that redraws itself differently for lr and rl text 
 */
class SYudit  : public SFrame, 
  public SFrameListener, public SToolBarListener, public STextEditLS, 
public SDropListener, public SFreeHandListener
{
public:
  SYudit (const SStringVector& args,  const SProperties& allprops, const SString& sedyLib);
  virtual ~SYudit ();
  virtual void show ();
protected:
  STimer* deferredLoader;
  virtual bool timeout (const SEventSource* s);
  
  virtual bool close (SPanel* comp);
  virtual void toolBarButtonPressed (void* src, int which, bool accel);
  virtual void toolBarButtonLeave (void* src, int which);
  virtual void toolBarButtonEnter (void* src, int which);

  virtual void textChanged (void *source);
  virtual void textEntered (void *source);
  virtual void focusOutRequest (void *source);
  virtual void focusChanged (void *source, bool in);
  virtual void caretMoved (void *source, unsigned int line, unsigned int col, bool before);
  virtual void freeHandTextChanged (void* source, const SString& lookup);

  STextEdit*  editor;
  STextEdit*  command;
  SSlider*    slider;
  SToolBar*   toolbar;
  SMessageBar* messagebar;
  SMessageLabel* messagelabel;
  SFreeHand* freehand;
  bool freeHandShown;

  SFileDialog*     fileDialog;
  SPasswordDialog*     passwordDialog;
  SHighlightD*     highlightDialog;
  STextDialog*     textDialog;
  SKMapDialog*     kmapDialog;

  STextIndex             caretNow;
  SHashtable<STextIndex> caretIndex;
  virtual bool dropped (void* p, const SString& mimetype, const SString& data);
private:
  enum SStatus { Canceled, Failed, Succeeded };
  SYudit::SStatus decryptText (const SString& fileName, const SString& text, SString* ret);

  bool encrypted;
  bool addMytoolPrefix (SString* executable);
  void showFreeHand(bool is);

  void createFileDialog();
  void createPasswordDialog();
  void createTextDialog();
  void createKMapDialog();
  void createHighlightDialog();
  void caretMoved ();

  SLayout editorLayout;
  SLayout sliderLayout;
  unsigned int freeHandHeight;

  SProperties originalProperties;
  SProperties helpStrings;

  void print (const SStringVector& args, const SString& str);
  void load (const SStringVector& args, const SString& str);
  void save (const SStringVector& args, const SString& str);
  void goLineRow (const SStringVector& args, const SString& str);
  void find (const SStringVector& args, const SString& str);
  void replace (const SStringVector& args, const SString& str);
  bool newFileExists (const SString& filename);
  SString    composeFileNameCommand ( const SString& prefix, 
        const SString& encoding, const SString& fileName);
  SString    quoteFileName(const SString& file);
  SString completeFileName(const SString& file);
  bool    setHighlight (const SString& syntax);
  void    setFileTitle ();

  void       saveProperties ();
  SString    currentFileName;
  SString    currentPassword;
  SString    lastencoding;
  SString    lastprinterOption;
  SString    lastpreviewOption;
  SString    sedyLibrary;
  SString    cryptFileMatcher;
  SSedy      sedy;

  SProperties userProps;
  SString    configFile;
  SStringVector    firstLoad;
  bool isFirstLoad;
  
};

#endif /* SYudit_h */
