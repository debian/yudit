��    a      $  �   ,      8     9     F     V     e     s      y      �     �     �     �  
   �     �     �     �  
   	     	     	     -	     1	  (   :	     c	     y	     	  	   �	     �	     �	     �	     �	     �	     �	     �	  1   �	     !
  
   &
     1
     =
     O
     T
  	   Z
  
   d
     o
     u
     }
     �
     �
     �
     �
     �
     �
     �
     �
  #   �
  "   �
  %        5     G     g     {     �     �  !   �     �     �     �     �           #     D      b  !   �     �     �     �  	   �     �  	   �     �     �               !     *     6  	   G     Q     l     }     �     �     �     �     �  "   �  P        i  "   �  $  �     �     �     �             $   .  )   S     }     �  	   �     �     �  	   �     �     �     �     �            %     !   D     f     r     w  *   �  :   �     �  	   �     �            D        Z     `     u     �     �     �     �     �     �     �     �     �     �     �          #     (  	   4     >  A   A  1   �  E   �     �  2        K     d     s     �  '   �     �     �     �  ?   �  <   %  8   b  2   �  C   �  %        8     D     J     d  
   l     w  !   �     �     �     �     �     �               $     @     Q     h     �     �     �  &   �  0     t   3  '   �  0   �     A       =   $   2       O       #          E   :   7            9       V   [          I   )   ^   Z       1      &                    K   0          ,           P       D      \   .   6          4      R                             -      '   8       N         H   3          `      S   /   (           	      ;           L       *       Q   a      U       M   5   @   J              ?       !   ]      
       W                Y   <       _   T   F      %   "   B   G       X   C   >   +     Line Break: Available KMaps Best File Type Bigger(__Z__) CD to Can not create
specified folder
 Can not go to
specified folder.
 Can not read:  Can not write:  Cancel Candidates Characters: Clear Column: Converters Current KMap: Current KMaps DOS Directed Direction of selected text has been set. Error while writing:  F-Key File File Type File exists.
Save anyway? File exists. Use -yes option. File: Filter: Find(__Q__) Folder Folder: Folders can be created
in current directory only. Font Font Size: Glyph Info: Handwriting Input Home Input Key Input Line Break Line: Look-up MAC Make Folder No Nothing to redo. Nothing to undo. OK Opened:  Output PS Pending printing job.
Exit anyway?
 Pending printing job. Enter quits! Pending printing job. Try -yes option Print Previe__w__ Print Previe__w__ (pending job) Printed %d page(s). Printing... Show Hidden Smaller(__A__) Specified folder
already exists.
 Strokes: Unix Unknown encoding:  Unsaved changes.
Exit anyway?
 Unsaved changes.
Open anyway?
 Unsaved changes. Enter discards! Unsaved changes. Enter quits! Unsaved changes. Try -yes option Will not create
specified folder
 Wrote:  Yes __D__irection Override __G__o To __O__pen __P__rint __P__rint (pending job) __R__edo __S__ave __S__ave (needed) __U__ndo find string go line [column] not found open -yes -e encoding file print -e program save -e encoding file search string not found. unsupported text/uri:  usage:  usage: find text usage: go line [column]  usage: open -e utf-8 -yes filename usage: print [-o file] [-p printer] [-e exec] [-break] [-hsize header-font-size] usage: replace original new usage: save -e utf-8 -yes filename Project-Id-Version: yudit 2.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2001-02-07 12:57+0200
Last-Translator: Vasif İsmailoglu <azerb_linux@hotmail.com>>
Language-Team: Azerbaijani Turkish
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8 bit
 Sətir Qırılması:  Mövcud "KMap"lǝr Ən Uyğun Fayl Növü Ən Böyük(__Z__) Qovluğa keç Bildirilən qovluq
yaradıla bilmir
 Bildirdiyiniz qovluğa
keçə bilmirəm.
 Oxuya bilmirəm:  Yaza bilmirəm:  Ləğv et Namizədlər Xarakterlər: Təmizlə Sütün: Çeviricilər Hazırkı KMap: Hazırkı "KMap"lǝr DOS İstiqamiətli Seçili mǝtnin istiqamiəti quruldu. Yazılma sırasında xəta oldü  F-Düymǝsi Fayl Fayl Növü Fayl mövcud deyil.
Yenə də qeyd edimmi? Fayl onsuz da vardır. -yes seçənəyindən istifadə et. Fayl: Süzgəc: Axtar(__Q__) Qovluq Qovluq: Qovluqlar sadəcə olaraq
hazırkı cərgədə yaradıla bilərlər. Yazı Yazı Böyüklüyü: Glif Haqqında: Əlyazma Girişi Ev Giriş Düymǝ Girişi Sətir Qırması Sətir: Axtar MAC Qovluq yarat Xeyr Geri alınacaq gediş yoxdur. Ediləcək bir vəzifə yoxdur. Oldu Açıldı:  Çıxış PS Gözləyən çap etmə sərəncamı var.
Yenə də çıxım mı?
 Gözləyən çap etmə vazifəsi var. Çıxa bas! Gözləyən çap etmə sərəncamı var. -yes seçənəyini sınayın Çap Etmə Nümayişi(__w__) Çap Etmə Nümayişi(__w__) (sıradakı vəzifə) %d səhifə çap edildı Çap Edilir... Gizliləri Göstər Ən Kiçik(__A__) Bildirdiyiniz qovluq
onsuz da vardır.
 Sətirlər: Unix Naməlum kodlama:  Qeyd edilməmiş dəyişikliklər var.
Yenə də çıxım mı?
 Qeyd edilməmiş dəyişikliklər var.
Yenə də açımmı?
 Qeyd edilməmiş dəyişikliklər var. İmtina etə bas! Qeyd edilməmiş dəyişikliklər var. Çıxa bas! Qeyd edilməmiş dəyişikliklər var. -yes seçənəyini sınayın Bildirilən qovluğu
yaratmayacağam
 Yazıldı:  Bəli Mətn İstiqaməti(__D__) __G__et Aç(__O__) Ça__p__ Et Ça__p__ Et (sıradakı vəzifə) Yenidən Et(__R__) Qeyd Et(__S__) Qeyd Et(__S__) (lazımdır) Geri Al(__U__) kəlməni axtar sətrə get [sütün] tapılmadı open -yer -e kodlama faylı print -e proqram save -e kodlama faylı axtarılan kəlmə tapılmadı. dəstəklənməyən mətn/uri:  istifadə qaydası:  istifadə qaydası: find next istifadə qaydası: go line [sütün]  istifadə qaydası: open -e utf-8 -yes fayl adı istifadə qaydası: print [-o fayl] [-p çapedici] [-e icra] [-fasilə] [-hsize başlıq-yazı-növü-böyüklüyü] istifadə qaydası: əvəz_et əsl yeni istifadə qaydası: save -e utf-8 -yes fayl adı 