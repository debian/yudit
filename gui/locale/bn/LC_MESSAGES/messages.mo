��    l      |  �   �      0	     1	     J	     W	     g	     v	     �	      �	      �	     �	     �	     �	  
   �	     �	     
     
     
      
  
   4
     ?
     Z
     h
     v
     z
  +   �
  (   �
     �
     �
     
       	             9     W     ]     e     q     x  1   �     �  
   �     �     �     �     �            	     
        !     '     /     3     ?     B     S     d     g     p     w  #   z  "   �  %   �     �     �          -     9     A     M  !   \     ~     �     �     �     �      �     �        !   =     _     g     k     �  	   �     �  	   �     �     �     �     �     �     �            	   ,     6     Q     b     x     �     �     �     �  "   �  P   �     N  "   j    �  5   �     �  )   �  5        O  	   e  Y   o  W   �  %   !  (   G     p  (   �     �     �     �  	   �  F   �     ?  9   X  /   �  ,   �  	   �  "   �  b     P     F   �  5        M     V     c  a   �  l   �     R     `     s     �     �  |   �     5     E     e  -   |     �  &   �     �     �          %     ?     M     Z  %   j     �  0   �  i   �     2     F     e     l  U   o  z   �  i   @  <   �  Y   �  9   A     {     �     �     �  T   �     C     P  -   i  k   �  X     r   \  �   �  n   R  T   �  $      	   ;   A   E   =   �      �      �      �   2   �      1!  &   L!  7   s!  M   �!  @   �!     :"     L"     h"  C   �"     �"  >   �"  N   #  0   k#     �#      �#  &   �#  F   �#  m   C$  O   �$  C   %     a   S   N   ]   >          %              )   	   4   =       '   6       G      <   #   *   e                  i           $       B       X   Y      ?      Q      R                 g   c                 .   @   `   O   [               j   2   T      M   h   f      ^       P              +   ,   8         l   V   H   F   5          (               D   K   A   !      C   W          b   ;              I   3   L       k                           -   1   E   Z   
   7             &       0          U      d   /   _   :   J                 "             9   \     Incomplete Last Line...  Line Break: Available KMaps Best File Type Bigger(__Z__) CD to Can not create
specified folder
 Can not go to
specified folder.
 Can not read:  Can not write:  Cancel Candidates Category Characters: Clear Column: Command not found:  Converters Could not save preferences Current KMap: Current KMaps DOS Directed Direction of selected text has been re-set. Direction of selected text has been set. Document __T__ext Embedding Error while writing:  F-Key File File Type File exists.
Save anyway? File exists. Use -yes option. File: Filter: Find(__Q__) Folder Folder: Folders can be created
in current directory only. Font Font Size: Glyph Info: Handwriting Input Highlighting Highlighting(__N__) Home Input Key Input Line Break Line: Look-up MAC Make Folder No Nothing to redo. Nothing to undo. OK Opened:  Output PS Pending printing job.
Exit anyway?
 Pending printing job. Enter quits! Pending printing job. Try -yes option Print Previe__w__ Print Previe__w__ (pending job) Printed %d page(s). Printing... Remark: Show Hidden Smaller(__A__) Specified folder
already exists.
 Strokes: Unix Unknown encoding:  Unsaved changes.
Exit anyway?
 Unsaved changes.
Open anyway?
 Unsaved changes. Enter discards! Unsaved changes. Enter quits! Unsaved changes. Try -yes option Will not create
specified folder
 Wrote:  Yes __D__irection Override __E__mbedding Override __G__o To __O__pen __P__rint __P__rint (pending job) __R__edo __S__ave __S__ave (needed) __U__ndo __Y__ield Embedding find string go line [column] not found open -yes -e encoding file print -e program save -e encoding file search string not found. unsupported text/uri:  usage:  usage: find text usage: go line [column]  usage: open -e utf-8 -yes filename usage: print [-o file] [-p printer] [-e exec] [-break] [-hsize header-font-size] usage: replace original new usage: save -e utf-8 -yes filename Project-Id-Version: Yudit2.6.4
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2002-10-13 18:15+0530
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: utf-8
 অসম্পূর্ণ শেষ লাইন... লাইন ভংগ  উপলব্ধ কী-ম্যাপ শ্রেষ্ঠ ফাইল প্রকার বড় কর [__Z__] যাও উল্লিখিত ফোল্ডার
বানাতে পারছি না
 উল্লিখিত ফোল্ডার-এ
যেতে পারছি না
 পড়তে পারছি না : লিখতে পারছি না:  খারিজ্ কর সম্ভাব্য অক্ষর শ্রেণি অক্ষর: সাফ্ করো কলম এই আদেশ খুঁজে পাওয়া যায় নি:  পরিবর্তক পছন্দ সঞ্চয় করা যায়নি বর্তমান কি-ম্যাপ্ বর্তমান কি-ম্যাপ ডস্ দিশা অনুযায়ী চয়ণিত পাঠের দিশা পুনর্বহাল করা হয়েছে চয়নিত পাঠের দিশা ঠিক করা হয়েছে সম্পুর্ণ পাঠের অনুপোতন [__T__] লেখার সময় ভুল হয়েছে:  F-কি ফাইল ফাইল প্রকার ফাইল আগে থেকেই আছে।
তবুও সঞ্চিত করবো? ফাইল আগে থেকেই আছে, -yes বিকল্প ব্যবহার করুন ফাইল: ছাঁকনী খোঁজো [__Q__] ফোল্ডার ফোল্ডার: কেবল বর্তমান ডিরেক্টরিতে 
ফোল্ডার বাননো যাবে । ফন্ট্ ফন্টের আকার হরফ সূচক হাতে লেখা প্রবেশ  হাইলাইট করা হাইলাইট করা(__N__) ঘর প্রবেশ কি প্রবেশ লাইন অন্ত লাইন: দেখো ম্যাক ফোল্ডার বানাও না আবার করার কিছু নেই বাতিল করার মত কোনো পুর্ববর্তী কার্য নেই ঠিক আছে খোলা হয়েছে:  ফল PS ছাপা বাকি আছে
তবুও বেড়িয়ে যাবে?
 ছাপার কাজ বাকি আছে, এন্টার মারলে বন্ধ হয়ে যাবে ! ছাপার কাজ বাকি আছে, -yes বিকল্প চেষ্টা করুন মুদ্রন প্রাগ্দর্শক [__w__] মুদ্রন প্রাগ্দর্শক(বাকি কার্য){__w__] %d পৃষ্ঠা ছাপা হয়ে গেছে ছাপা চলছে.... মন্তব্য গুপ্ত দেখাও ছোটো করো [__A__] উল্লিখিত ফোল্ডার
আগে থেকেই আছে 
 আঁচড় ইউনিক্স্ অজানা এন্‌কোডিং : পরিবর্তন সঞ্চিত হয়নি!
তবুও বেড়িয়ে যাবে?
 পরিবর্তন সঞ্চিত হয়নি 
তবুও খুলবে?
 পরিবর্তন সঞ্চিত নেই, এন্টার মারলে চলে যাবে ! পরিবর্তন সঞ্চিত হয়নি, এন্টার মারলে বন্ধ হয়ে যাবে ! পরিবর্তন সঞ্চিত নেই, -yes বিকল্প চেষ্টা করুন নির্দিষ্ট ফোল্ডার 
তৈরী করবে না
 লেখা হয়ে গেছে: হাঁ লিখন দিশা পরিবর্তন কর [__D__] অনুপোতন পরিবর্তন কর [__E__] যাও[__G__] খোলো[__O__] ছাপো[__P__] ছাপো(বাকি কার্য) [__P__] আবার কর[__R__] সঞ্চিত রাখো[__S__] সঞ্চিত রাখো(জরুরী)[__S__] পুর্ববর্তী কার্য বাতিল কর [__U__] অনুপোতন পুনর্বহাল কর [__Y__] find শব্দ go লাইন [কলম] পাওয়া যায়নি open -yes -e ফাইল প্রকার ফাইল নাম print -e program save -e ফাইল প্রকার ফাইল নাম যে কথা খুঁজেছি, তা পাওয়া যায়নি লিখন (text/uri) অসমর্থিত ব্যবহার:  ব্যবহার: find text ব্যবহার go line [column] ব্যবহার: open -e utf-8 -yes ফাইলের নাম ব্যবহার: print [-o ফাইল] [-p ছাপক] [-e exec] [-break] [-hsize header-font-size] ব্যবহার: replace মূল শব্দ নতুন শব্দ ব্যবহার: save -e utf-8 -yes ফাইল্ নাম 