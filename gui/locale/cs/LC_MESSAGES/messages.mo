��    h      \  �   �      �     �     �     �     �     	     	      "	      C	     d	     s	     �	  
   �	     �	     �	     �	     �	  
   �	     �	     �	     �	     
     	
  +   
  (   >
     g
     �
     �
     �
  	   �
     �
     �
     �
     �
     �
             1        A  
   F     Q     ]     o     t  	   z  
   �     �     �     �     �     �     �     �     �     �     �     �  #   �  "     %   /     U     g     �     �     �     �  !   �     �     �     �          $      C     d      �  !   �     �     �     �     �  	   �     	  	             4     =     F     X     a     u     �  	   �     �     �     �     �     �               '  "   @  P   c     �  "   �  6  �  "   *     M     `     p     �     �      �  &   �     �            
        &  	   -     7     @     U     b     {     �     �     �  #   �  $   �     �       
   &     1     8  *   D  #   o     �     �     �  	   �  
   �  7   �     �               #     0     6     <     P     b     k     w     {     �  $   �  )   �     �  	   �     �     �  "   �       !   ;     ]  %   r     �     �     �     �     �          	       *   &  +   Q  %   }  &   �  )   �     �  
                #   ?     c     r     �     �     �     �     �     �     �            	   .     8     X     i  !   �     �     �     �     �  $   �  c   $  "   �  $   �     I   Q   S              *                                   Y   L       P   D   	      6              $           ?   .   N   a   [   c   \         E   2          @      Z      %   4   d   /   +      R   )   ,             8               `   ;   A             U   :       =               "          #   K       V   G   b      f                 '   _      W   3       T       &           ^   C      ]   5   1   M   !      9       0   7           (   e      J         >   O   
   X   <      H   B   F   h               g   -     Incomplete Last Line...  Line Break: Available KMaps Best File Type Bigger(__Z__) CD to Can not create
specified folder
 Can not go to
specified folder.
 Can not read:  Can not write:  Cancel Candidates Characters: Clear Column: Command not found:  Converters Could not save preferences Current KMap: Current KMaps DOS Directed Direction of selected text has been re-set. Direction of selected text has been set. Document __T__ext Embedding Error while writing:  F-Key File File Type File exists.
Save anyway? File exists. Use -yes option. File: Filter: Find(__Q__) Folder Folder: Folders can be created
in current directory only. Font Font Size: Glyph Info: Handwriting Input Home Input Key Input Line Break Line: Look-up MAC Make Folder No Nothing to redo. Nothing to undo. OK Opened:  Output PS Pending printing job.
Exit anyway?
 Pending printing job. Enter quits! Pending printing job. Try -yes option Print Previe__w__ Print Previe__w__ (pending job) Printed %d page(s). Printing... Show Hidden Smaller(__A__) Specified folder
already exists.
 Strokes: Unix Unknown encoding:  Unsaved changes.
Exit anyway?
 Unsaved changes.
Open anyway?
 Unsaved changes. Enter discards! Unsaved changes. Enter quits! Unsaved changes. Try -yes option Will not create
specified folder
 Wrote:  Yes __D__irection Override __E__mbedding Override __G__o To __O__pen __P__rint __P__rint (pending job) __R__edo __S__ave __S__ave (needed) __U__ndo __Y__ield Embedding find string go line [column] not found open -yes -e encoding file print -e program save -e encoding file search string not found. unsupported text/uri:  usage:  usage: find text usage: go line [column]  usage: open -e utf-8 -yes filename usage: print [-o file] [-p printer] [-e exec] [-break] [-hsize header-font-size] usage: replace original new usage: save -e utf-8 -yes filename Project-Id-Version: yudit 2.7.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2003-02-03 08:25+0100
Last-Translator: Luboš Staněk <stanekl@atlas.cz>
Language-Team: čeština <cs@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 0.9.6
  Neukončená poslední řádka... Zalomení řádku: Dostupné KMapy Nejvhodnější typ souboru Větší(__Z__) Do adresáře Nelze vytvořit
daný adresář
 Nelze přejít do
daného adresáře.
 Nelze číst:  Nelze zapisovat:  Zrušit Kandidáti Znaky: Vyčistit Sloupec: Příkaz nenalezen:  Převodníky Nelze uložit nastavení Aktuální KMapa: Aktuální KMapy DOS Směrovaný Směr vybraného textu byl obnoven. Směr vybraného textu byl nastaven. Směr __T__extu dokumentu Chyba při zápisu:  F-klávesa Soubor Typ souboru Soubor již existuje.
Chcete jej přepsat? Soubor existuje. Zkuste volbu -yes. Soubor: Filtr: Najít(__Q__) Adresář Adresář: Adresáře lze vytvářet
jen v aktuálním adresáři. Písmo Velikost písma: Info o glyfu: Psaní rukou Domů Vstup Vstup z klávesnice Zalomení řádku Řádek: Vyhledání MAC Vytvořit adresář Ne Neexistuje nic, co by šlo opakovat! Neexistuje nic, co by šlo vrátit zpět! OK Otevřen: Výstup PS Probíhá tisk.
Přesto skončit?
 Probíhá tisk. Enter končí! Probíhá tisk. Zkuste volbu -yes Náhled tisku(__w__) Náhled tisku (probíhá tisk)(__w__) Vytištěno stránek %d. Probíhá tisk... Zobrazit skryté Menší(__A__) Daný adresář
již existuje.
 Čar: Unix Neznámé kódování:  Změny nejsou uloženy.
Přesto skončit?
 Změny nejsou uloženy.
Přesto otevřít?
 Změny nejsou uloženy. Enter ruší! Změny nejsou uloženy. Enter končí! Změny nejsou uloženy. Zkuste volbu -yes Nevytvoří
daný adresář
 Zapsáno:  Ano Předefinování směru(__D__) Předefinování vkládání(__E__) Jít na(__G__) __O__tevřít Tisk(__P__) Tisk (probíhá tisk)(__P__) Opakovat(__R__) Uložit(__S__) Uložit (je třeba)(__S__) Zpět(__U__) Přepnout vkládání(__Y__) find řetězec go line [sloupec] nenalezen open -yes -e soubor kódování print -e program save -e soubor kódování hledaný řetězec nebyl nalezen. nepodporovaný text/uri:  použití:  použití: find text použití: go line [sloupec]  použití: open -e utf-8 -yes soubor použití: print [-o soubor] [-p tiskárna] [-e spustit] [-break] [-hsize velikost-fontu-záhlaví] použití: replace původní nový použití: save -e utf-8 -yes soubor 