��    a      $  �   ,      8     9     F     V     e     s      y      �     �     �     �  
   �     �     �     	     	     	  
   #	     .	     I	     W	     e	  (   n	     �	     �	     �	  	   �	     �	     �	     �	      
     
     
  1   
     M
  
   R
     ]
     i
     {
     �
     �
     �
  	   �
  
   �
     �
     �
     �
     �
     �
     �
     �
     �
       #     "   2  %   U     {     �     �     �     �     �     �  !   �               .     M      l     �      �  !   �     �     �  	   �       	             /     8     A     S     \     h  	   y     �     �     �     �     �     �     �       "   '  P   J     �  "   �  3  �               /     ?  
   O  (   Z  +   �     �     �  	   �  	   �  	   �     �          
       	   *      4     U     c     r  -   {     �     �     �     �  $   �  %   �     "     )     6     =  <   E     �     �     �     �     �     �     �     �     �     �                    $     )     @     _  
   b     m  .   u  +   �  .   �     �  ,        C  	   \  
   f     q     �     �  
   �     �  .   �  ,        .     N  !   m  )   �     �     �     �     �     �  #   �          !     -     E     a     s     �     �     �     �     �     �  
             .  %   J  :   p  '   �  &   �     B       ?   %   4       O      $          F   ;   8            :       V   [           J       N   Z       3      )                ^   K   2                      P   !   E      \      7          L      R                             /      *   9                I   5          `      =   1   +           	      <           '       -       Q   T   ,   U   (   M   6       S               A       "   ]      
       W   a            Y   >       _   0   G      &   #   C   H       X   D   @   .     Line Break: Available KMaps Best File Type Bigger(__Z__) CD to Can not create
specified folder
 Can not go to
specified folder.
 Can not read:  Can not write:  Cancel Candidates Category Characters: Clear Column: Command not found:  Converters Could not save preferences Current KMap: Current KMaps Directed Direction of selected text has been set. Error while writing:  F-Key File File Type File exists.
Save anyway? File exists. Use -yes option. File: Find(__Q__) Folder Folder: Folders can be created
in current directory only. Font Font Size: Glyph Info: Handwriting Input Highlighting Highlighting(__N__) Home Input Key Input Line Break Line: Look-up Make Folder No Nothing to redo. Nothing to undo. OK Opened:  Output Pending printing job.
Exit anyway?
 Pending printing job. Enter quits! Pending printing job. Try -yes option Print Previe__w__ Print Previe__w__ (pending job) Printed %d page(s). Printing... Remark: Show Hidden Smaller(__A__) Specified folder
already exists.
 Strokes: Unknown encoding:  Unsaved changes.
Exit anyway?
 Unsaved changes.
Open anyway?
 Unsaved changes. Enter discards! Unsaved changes. Enter quits! Unsaved changes. Try -yes option Will not create
specified folder
 Wrote:  Yes __G__o To __O__pen __P__rint __P__rint (pending job) __R__edo __S__ave __S__ave (needed) __U__ndo find string go line [column] not found open -yes -e encoding file print -e program save -e encoding file search string not found. unsupported text/uri:  usage:  usage: find text usage: go line [column]  usage: open -e utf-8 -yes filename usage: print [-o file] [-p printer] [-e exec] [-break] [-hsize header-font-size] usage: replace original new usage: save -e utf-8 -yes filename Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2002-02-20 13:00+0100
Last-Translator: Mike Fabian <mfabian@suse.de>
Language-Team: Thomas Wohlfarth <thomas.wohlfarth@gmx.net>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: utf-8
 Zeilenumbruch: Verfügbare KMaps Bester Dateityp Größer(__Z__) Wechsle zu Kann angegebenen
Ordner nicht erstellen
 Kann nicht in angegebenen
Ordner wechseln.
 Kann nicht lesen: Kann nicht schreiben: Abbrechen Vorschlag Kategorie Buchstaben: Löschen Spalte: Befehl nicht gefunden:  Konverter Kann Einstellungen nicht sichern Aktuelle KMap Aktuelle KMaps Richtung Richtung des gewählten Textes wurde gesetzt. Fehler beim Schreiben: F-Taste Datei Dateityp Datei existiert!
Trotzdem speichern? Datei vorhanden. Verwende -yes Option Datei: Finde(__Q__) Ordner Ordner: Ordner können nur im aktuellen
Verzeichnis erstellt werden. Font Font-Größe: Zeichen-Info: Handschifteingabe Hervorhebung Hervorhebung(__N__) Heimatverzeichnis Eingabe Eingabe Zeilenumbruch Zeile: Suchen Erstelle Ordner Nein Nichts zu wiederholen. Nichts rückgängig zu machen. OK Geöffnet: Ausgabe Anstehender Druckauftrag!
Trotzdem verlassen?
 Anstehender Druckauftrag. <Return> beendet! Anstehender Druckauftrag. Versuche -yes Option Drucke Vorschau(__W__) Drucke Vorschau (__W__  anstehender Auftrag) Habe %d Seiten gedruckt. Drucke... Bemerkung: Zeige Versteckte Kleiner (__A__) Angegebener
Ordner existiert.
 Strichzahl Unbekannte Kodierung: Ungesicherte Änderungen!
Trotzdem verlassen?
 Ungesicherte Änderungen!
Trotzdem öffnen?
 Ungesichert. <Return> verwirft! Ungesichert. <Return> beendet! Ungesichert. Versuche -yes Option Werde angegebenen
Ordner nicht erstellen
 Schrieb: Ja __G__ehe zu Öffne(__O__) Drucke(__P__) Drucke (__P__ anstehender  Auftrag) Wiede__r__holen __S__ichere __S__ichere (benötigt) Rückgängig machen (__U__) find Zeichenfolge go Zeile [Spalte] nicht gefunden open -yes -e Kodierung Datei print -e Programm save -e Kodierung Datei Suchtext nicht gefunden. nicht unterstützter Text: Gebrauch:  Gebrauch: find Text Gebrauch: go Zeile [Spalte] Gebrauch: open -e utf8 -yes Dateiname Gebrauch: print [-o Datei] [-p Drucker] [-e exec] [-break] Gebrauch: replace alter_Text neuer_Text Gebrauch: save -e utf-8 -yes Dateiname 