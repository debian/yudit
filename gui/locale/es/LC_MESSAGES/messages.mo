��    h      \  �   �      �     �     �     �     �     	     	      "	      C	     d	     s	     �	  
   �	     �	     �	     �	     �	  
   �	     �	     �	     �	     
     	
  +   
  (   >
     g
     �
     �
     �
  	   �
     �
     �
     �
     �
     �
             1        A  
   F     Q     ]     o     t  	   z  
   �     �     �     �     �     �     �     �     �     �     �     �  #   �  "     %   /     U     g     �     �     �     �  !   �     �     �     �          $      C     d      �  !   �     �     �     �     �  	   �     	  	             4     =     F     X     a     u     �  	   �     �     �     �     �     �               '  "   @  P   c     �  "   �  ,  �           =     N     k     �     �  *   �  +   �     �            
   #     .     :     B     K     a  '   m     �     �     �     �  :   �  7        G     [     p     x     �  )   �  *   �     �     �     �     	       7        R     Y     l     �     �     �     �     �     �     �     �     �     �  $   �  #        9  	   =     G  	   N  E   X  M   �  D   �     1  6   P     �     �     �     �  #   �     �            8   #  -   \  9   �  A   �  9     &   @  	   g     q     u  *   �     �     �     �  #   �          /     B     a     �     �     �     �  %   �             !   6  &   X          �     �  )   �  _   �     ;  *   W     I   Q   S              *                                   Y   L       P   D   	      6              $           ?   .   N   a   [   c   \         E   2          @      Z      %   4   d   /   +      R   )   ,             8               `   ;   A             U   :       =               "          #   K       V   G   b      f                 '   _      W   3       T       &           ^   C      ]   5   1   M   !      9       0   7           (   e      J         >   O   
   X   <      H   B   F   h               g   -     Incomplete Last Line...  Line Break: Available KMaps Best File Type Bigger(__Z__) CD to Can not create
specified folder
 Can not go to
specified folder.
 Can not read:  Can not write:  Cancel Candidates Characters: Clear Column: Command not found:  Converters Could not save preferences Current KMap: Current KMaps DOS Directed Direction of selected text has been re-set. Direction of selected text has been set. Document __T__ext Embedding Error while writing:  F-Key File File Type File exists.
Save anyway? File exists. Use -yes option. File: Filter: Find(__Q__) Folder Folder: Folders can be created
in current directory only. Font Font Size: Glyph Info: Handwriting Input Home Input Key Input Line Break Line: Look-up MAC Make Folder No Nothing to redo. Nothing to undo. OK Opened:  Output PS Pending printing job.
Exit anyway?
 Pending printing job. Enter quits! Pending printing job. Try -yes option Print Previe__w__ Print Previe__w__ (pending job) Printed %d page(s). Printing... Show Hidden Smaller(__A__) Specified folder
already exists.
 Strokes: Unix Unknown encoding:  Unsaved changes.
Exit anyway?
 Unsaved changes.
Open anyway?
 Unsaved changes. Enter discards! Unsaved changes. Enter quits! Unsaved changes. Try -yes option Will not create
specified folder
 Wrote:  Yes __D__irection Override __E__mbedding Override __G__o To __O__pen __P__rint __P__rint (pending job) __R__edo __S__ave __S__ave (needed) __U__ndo __Y__ield Embedding find string go line [column] not found open -yes -e encoding file print -e program save -e encoding file search string not found. unsupported text/uri:  usage:  usage: find text usage: go line [column]  usage: open -e utf-8 -yes filename usage: print [-o file] [-p printer] [-e exec] [-break] [-hsize header-font-size] usage: replace original new usage: save -e utf-8 -yes filename Project-Id-Version: Yudit 2.7
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2002-11-12 23:30GMT+2
Last-Translator: Juan Rafael Fernández García <jrfern@bigfoot.com>
Language-Team: Spanish <es@li.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Última Línea incompleta... Salto de línea: Mapas de teclado disponibles Mejor tipo de fichero Más grande (__Z__) CD a No se puede crear
la carpeta especificada
 No se puede abrir
la carpeta especificada.
 Error de lectura:  No se pudo escribir:  Cancelar Candidatos Caracteres: Limpiar Columna: Orden no encontrada:  Conversores No se pudieron salvar las preferencias  Mapa de teclado actual: Mapas de teclado actuales DOS Dirigido Se ha re-establecido la dirección del texto seleccionado. Se ha establecido la dirección del texto seleccionado. Incrustar __T__exto Error de escritura:  Tecla-F Fichero Tipo de fichero El fichero ya existe.
¿Confirma guardar? El fichero ya existe. Use la opción -yes. Fichero: Filtro: Buscar (__Q__uery) Carpeta Carpeta: Sólo se pueden crear carpetas
en el directorio actual. Fuente Tamaño de Fuente: Información sobre el glifo: Escritura a mano Inicio Entrada Entrada de Teclado Salto de línea Línea: Buscar MAC Crear carpeta No No hay modificaciones que recuperar. No hay modificaciones que deshacer. Sí Abierto:  Salida Sep.Parr. Hay un trabajo de impresión pendiente.
¿Confirma que quiere salir?
 Hay trabajos de impresión pendientes. ¡Si pulsa Intro saldrá del programa! Quedan trabajos de impresión pendientes. Pruebe con la opción -yes Imprimir: Vista Previa (__W__) Imprimir: Vista (__W__) Previa (del trabajo pendiente) Imprimida(s) %d página(s) Imprimiendo... Mostrar oculto Más pequeño (__A__) La carpeta especificada
ya existe.
 Trazos: Unix Codificación desconocida:  No ha salvado los cambios.
¿Confirma que quiere salir?
 No ha salvado los cambios.
¿Confirma abrir?
 No ha salvado los cambios. ¡Si pulsa Intro se perderán! No ha salvado los cambios. ¡Si pulsa Intro saldrá del programa! No se han salvado los cambios. Pruebe con la opción -yes No se creará
la carpeta especificada
 Escrito:  Sí Forzar __D__irección del Texto Forzar dirección de (__E__) incrustación Ir A (__G__o To) Abrir (__O__pen) Im__P__rimir Im__P__rimir (el trabajo pendiente) __R__ecuperar modificación Guardar (__S__ave) Guardar modificaciones (__S__) Deshacer (__U__) modificación Abandonar (__Y__) incrustación buscar cadena de texto ir a línea [columna] no se encuentra open -yes -e archivo_de_codificación print -e programa save -e fichero de codificación no se encontró la cadena buscada text/uri no aceptado por el programa:  uso:  uso: buscar texto uso: go línea [columna]  uso: open -e utf-8 -yes nombre_de_fichero uso: print [-o archivo] [-p impresora][-e exec] [-break] [-hsize tamaño-de-fuente-de-cabecera] uso: replace original nuevo uso: save -e utf-8 -yes _nombre_de_fichero 