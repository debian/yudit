��    h      \  �   �      �     �     �     �     �     	     	      "	      C	     d	     s	     �	  
   �	     �	     �	     �	     �	  
   �	     �	     �	     �	     
     	
  +   
  (   >
     g
     �
     �
     �
  	   �
     �
     �
     �
     �
     �
             1        A  
   F     Q     ]     o     t  	   z  
   �     �     �     �     �     �     �     �     �     �     �     �  #   �  "     %   /     U     g     �     �     �     �  !   �     �     �     �          $      C     d      �  !   �     �     �     �     �  	   �     	  	             4     =     F     X     a     u     �  	   �     �     �     �     �     �               '  "   @  P   c     �  "   �  @  �  #   4     X     f     �     �     �     �  #   �     �          %  
   -     8     @     F     N  	   c  "   m     �     �     �  	   �  5   �  #   �          3     K     W     `  1   o  .   �  	   �  	   �     �  	   �  
   �  8        >     G     U     g     w     �     �     �     �     �     �     �     �     �     �     �     �            -     +   @  '   l     �     �     �     �     �     �          *     2     7  4   P  1   �  =   �  2   �  .   (     W     t     �     �     �     �     �     �     �     �     
  !     	   <     F     [     k     |  !   �     �     �  !   �     �  
             1  )   L  a   v  $   �  )   �     I   Q   S              *                                   Y   L       P   D   	      6              $           ?   .   N   a   [   c   \         E   2          @      Z      %   4   d   /   +      R   )   ,             8               `   ;   A             U   :       =               "          #   K       V   G   b      f                 '   _      W   3       T       &           ^   C      ]   5   1   M   !      9       0   7           (   e      J         >   O   
   X   <      H   B   F   h               g   -     Incomplete Last Line...  Line Break: Available KMaps Best File Type Bigger(__Z__) CD to Can not create
specified folder
 Can not go to
specified folder.
 Can not read:  Can not write:  Cancel Candidates Characters: Clear Column: Command not found:  Converters Could not save preferences Current KMap: Current KMaps DOS Directed Direction of selected text has been re-set. Direction of selected text has been set. Document __T__ext Embedding Error while writing:  F-Key File File Type File exists.
Save anyway? File exists. Use -yes option. File: Filter: Find(__Q__) Folder Folder: Folders can be created
in current directory only. Font Font Size: Glyph Info: Handwriting Input Home Input Key Input Line Break Line: Look-up MAC Make Folder No Nothing to redo. Nothing to undo. OK Opened:  Output PS Pending printing job.
Exit anyway?
 Pending printing job. Enter quits! Pending printing job. Try -yes option Print Previe__w__ Print Previe__w__ (pending job) Printed %d page(s). Printing... Show Hidden Smaller(__A__) Specified folder
already exists.
 Strokes: Unix Unknown encoding:  Unsaved changes.
Exit anyway?
 Unsaved changes.
Open anyway?
 Unsaved changes. Enter discards! Unsaved changes. Enter quits! Unsaved changes. Try -yes option Will not create
specified folder
 Wrote:  Yes __D__irection Override __E__mbedding Override __G__o To __O__pen __P__rint __P__rint (pending job) __R__edo __S__ave __S__ave (needed) __U__ndo __Y__ield Embedding find string go line [column] not found open -yes -e encoding file print -e program save -e encoding file search string not found. unsupported text/uri:  usage:  usage: find text usage: go line [column]  usage: open -e utf-8 -yes filename usage: print [-o file] [-p printer] [-e exec] [-break] [-hsize header-font-size] usage: replace original new usage: save -e utf-8 -yes filename Project-Id-Version: 2.7
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: Miikka-Markus Alhonen <Miikka-Markus.Alhonen@tigatieto.com>
Language-Team: Tigatieto Oy <tigatieto@tigatieto.com>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: utf-8
  Viimeinen rivi epätäydellinen...  Rivinvaihto: Saatavilla olevat KMap:it Paras tiedostotyyppi Suurempi(__Z__) Vaihda hakemistoa En pysty luomaan
hakemistoa
 En pysty siirtymään
hakemistoon.
 En pysty lukemaan:  En pysty kirjoittamaan:  Peruuta Ehdotelmat Merkit: Pyyhi Sarake: Käskyä ei löydy:  Muuntimet En pystynyt tallentamaan asetuksia Nykyinen KMap: Nykyiset KMap:it DOS Suunnattu Valitun tekstin suunta on palautettu alkuperäiseksi. Valitun tekstin suunta on asetettu. Koko dokumen__t__in suunta Virhe kirjoitettaessa:  F-näppäin Tiedosto Tiedostotyyppi Tiedosto on jo olemassa.
Kirjoitetaanko päälle? Tiedosto on jo olemassa. Käytä optiota -yes. Tiedosto: Suodatin: Etsi(__Q__) Hakemisto Hakemisto: Hakemistoja voidaan luoda
vain nykyisessä hakemistossa. Kirjasin Kirjasinkoko: Tietoa merkistä: Käsinkirjoitus Kotihakemisto Syöte Näppäinsyöte Rivinvaihto Rivi: Etsi MAC Luo hakemisto Ei Ei mitään palautettavaa. Ei mitään kumottavaa. OK Avattu:  Tuloste PS Tulostustyö kesken.
Lopetetaanko kuitenkin?
 Tulostustyö kesken. Enterillä lopetetaan! Tulostustyö kesken. Koeta optiota -yes Esikatselu(__W__) Esikatselu(__W__) (työ kesken) Tulostettu %d sivu(a). Tulostetaan... Näytä piilotetut Pienempi(__A__) Hakemisto on jo
olemassa.
 Vetoja: Unix Tuntematon koodaustapa:  Tallentamattomia muutoksia.
Lopetetaanko kuitenkin?
 Tallentamattomia muutoksia.
Avataanko kuitenkin?
 Tallentamattomia muutoksia. Enterillä hylätään muutokset! Tallentamattomia muutoksia. Enterillä lopetetaan! Tallentamattomia muutoksia. Koeta optiota -yes En pysty luomaan
hakemistoa
 Kirjoitettu:  Kyllä Vaih__d__a suuntaa Upota t__e__kstiä Mene riville(__G__) Avaa(__O__) Tulosta(__P__) Tulosta(__P__) (työ kesken) Palauta(__R__) Tallenna(__S__) Tallenna(__S__) (muutoksia tehty) K__u__moa Poista upotus(__Y__) find merkkijono go rivi [sarake] ei löytynyt open -yes -e koodaustapa tiedosto print -e ohjelma save -e koodaustapa tiedosto Haettua merkkijonoa ei löytynyt. ei-tuettu text/uri:  käyttö:  käyttö: find teksti käyttö: go rivi [sarake] käyttö: open -e utf-8 -yes tiedostonimi käyttö: print [-o tiedosto] [-p tulostin] [-e käynnistä] [-break] [-hsize otsikon_fonttikoko] käyttö: replace alkuperäinen uusi käyttö: save -e utf-8 -yes tiedostonimi 