��    `        �         (     )     6     F     U     c      i      �     �     �     �  
   �     �     �     �     �  
   
	     	     #	     1	  (   5	     ^	     t	     z	  	   	     �	     �	     �	     �	     �	     �	     �	  1   �	     
  
   !
     ,
     8
     J
     O
  	   U
  
   _
     j
     p
     x
     |
     �
     �
     �
     �
     �
     �
  #   �
  "   �
  %        -     ?     _     s          �  !   �     �     �     �     �     �           <      Z  !   {     �     �     �  	   �     �  	   �     �     �     �               "     .  	   ?     I     d     u     �     �     �     �     �  "   �  P        a  "   }  -  �     �     �     �          !  /   )  1   Y     �     �     �  	   �     �     �  	   �     �     �               .  E   2     x     �     �     �  4   �  *   �  	   "     ,     5     D     P  H   ^     �     �     �     �     �                    &     .     7     ;     Q     U     f  	   w     �     �  ,   �      �  *   �               8     Q     g     �  '   �     �     �     �  6   �  5     *   H  )   s  3   �  %   �     �                  
   .     9     F     S     _     s     �     �     �     �     �            +   1     ]     {     �      �  (   �  f   �  *   T  #        @       <   $   1       N      #          D   9   6            8       U   Z          H   )   ]   Y              &          O          J   0          ,                   C      [   .   5          3      Q                             -      '   7       M         G   2          _      R   /   (           	      :           K       *       P   `      T       L   4   ?   I              >       !   \      
       V                X   ;       ^   S   E      %   "   A   F       W   B   =   +     Line Break: Available KMaps Best File Type Bigger(__Z__) CD to Can not create
specified folder
 Can not go to
specified folder.
 Can not read:  Can not write:  Cancel Candidates Characters: Clear Column: Command not found:  Converters Current KMap: Current KMaps DOS Direction of selected text has been set. Error while writing:  F-Key File File Type File exists.
Save anyway? File exists. Use -yes option. File: Filter: Find(__Q__) Folder Folder: Folders can be created
in current directory only. Font Font Size: Glyph Info: Handwriting Input Home Input Key Input Line Break Line: Look-up MAC Make Folder No Nothing to redo. Nothing to undo. Opened:  Output PS Pending printing job.
Exit anyway?
 Pending printing job. Enter quits! Pending printing job. Try -yes option Print Previe__w__ Print Previe__w__ (pending job) Printed %d page(s). Printing... Show Hidden Smaller(__A__) Specified folder
already exists.
 Strokes: Unix Unknown encoding:  Unsaved changes.
Exit anyway?
 Unsaved changes.
Open anyway?
 Unsaved changes. Enter discards! Unsaved changes. Enter quits! Unsaved changes. Try -yes option Will not create
specified folder
 Wrote:  Yes __D__irection Override __G__o To __O__pen __P__rint __P__rint (pending job) __R__edo __S__ave __S__ave (needed) __U__ndo find string go line [column] not found open -yes -e encoding file print -e program save -e encoding file search string not found. unsupported text/uri:  usage:  usage: find text usage: go line [column]  usage: open -e utf-8 -yes filename usage: print [-o file] [-p printer] [-e exec] [-break] [-hsize header-font-size] usage: replace original new usage: save -e utf-8 -yes filename Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2001-02-18 20:00
Last-Translator: Olivier Faucheux <olivier.faucheux@etu.enseeiht.fr>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: utf-8
  Retour à la ligne : KMaps disponibles Meilleur type de fichier Plus grand(__Z__) Va dans Impossible de créer
le répertoire spécifié
 Impossible d'entrer
dans le répertoire indiqué
 Ne peut être lu :  Impossible d'écrire :  Annuler Candidats Caractères : R.A.Z. Colonne : Ne peut être lu :  Convertisseurs KMap en service : KMaps en service DOS La direction d'écriture du texte sélectionné a été positionnée. Erreur pendant l'écriture de  Touche de fonction Fichier Type de fichier Un fichier existe déjà.
Sauvegarder malgré tout ? Un fichier existe. Utilisez l'option -yes. Fichier : Filtre : Trouver(__Q__) Répertoire Répertoire : Les répertoires ne peuvent
être crées que dans le répertoire
courant Police Taille de police : Infos sur le symbole : Entrée à la souris Répertoire personnel Entrée Entrée Retour à la ligne Ligne : Chercher MAC Créer un répertoire Non Rien à refaire. Rien à annuler. Ouvert :  Sortie PS Impression en cours.
Quitter malgré tout ?
 Impression en cours. Continuer ? Impression en cours. Essayez l'option -yes Prévisualisation (__w__) Prévisualisation (__w__) %d page(s) imprimée(s). Impression lancée... Affiche fichiers cachés Plus petit(__A__) Le répertoire indiqué
existe déjà.
 Traits: Unix Encodage inconnu :  Changements non enregistrés !
Quitter malgré tout ?
 Changements non enregistrés !
Ouvrir malgré tout ?
 Changements non enregistrés ! Continuer ? Changements non enregistrés. Continuer ? Changements non enregistrés. Essayez l'option -yes Impossible de 
créer le répertoire
 Ecrit :  Oui __D__irection d'écriture Aller à(__G__) __O__uvrir Im__p__rimer Im__p__rimer __R__efaire Enregistrer (__S__) Enregistrement (requis) (__S__) Annuler(__U__) find chaîne_de_caractères go ligne [colonne] aucune correspondance open -yes -e encodage fichier print -e programme save -e encodage fichier la chaîne cherchée n'a pu être trouvée. texte ou uri non supporté :  utilisation :  utilisation : find texte utilisation : go ligne [colonne] Utilisation : open -e utf-8 -yes fichier utilisation : print [-o fichier] [-p imprimante] [-e exécutable][-break] [-hsize taille_police_titre] utilisation : replace original remplaçant utilisation : save -e utf-8 fichier 