��    l      |  �   �      0	     1	     J	     W	     g	     v	     �	      �	      �	     �	     �	     �	  
   �	     �	     
     
     
      
  
   4
     ?
     Z
     h
     v
     z
  +   �
  (   �
     �
     �
     
       	             9     W     ]     e     q     x  1   �     �  
   �     �     �     �     �            	     
        !     '     /     3     ?     B     S     d     g     p     w  #   z  "   �  %   �     �     �          -     9     A     M  !   \     ~     �     �     �     �      �     �        !   =     _     g     k     �  	   �     �  	   �     �     �     �     �     �     �            	   ,     6     Q     b     x     �     �     �     �  "   �  P   �     N  "   j  #  �  (   �     �     �            
   +  .   6  5   e     �     �     �     �  	   �     �     �     �       
      '   +     S     `     p  	   t  *   ~  &   �      �     �  	               7   .  3   f     �  	   �     �  	   �  
   �  B   �               &     3     N     W     g     m     u     �     �     �     �     �     �     �     �     �  
   �            9     .   J  <   y     �  ,   �     �          (  "   /     R  #   c  	   �     �     �  =   �  6   �  -   "  )   P  8   z  '   �     �     �     �            
   *     5     D     b     s     �     �     �     �     �     �     �          )     C     `  	   ~     �     �  "   �  Q   �  &   .  "   U     a   S   N   ]   >          %              )   	   4   =       '   6       G      <   #   *   e                  i           $       B       X   Y      ?      Q      R                 g   c                 .   @   `   O   [               j   2   T      M   h   f      ^       P              +   ,   8         l   V   H   F   5          (               D   K   A   !      C   W          b   ;              I   3   L       k                           -   1   E   Z   
   7             &       0          U      d   /   _   :   J                 "             9   \     Incomplete Last Line...  Line Break: Available KMaps Best File Type Bigger(__Z__) CD to Can not create
specified folder
 Can not go to
specified folder.
 Can not read:  Can not write:  Cancel Candidates Category Characters: Clear Column: Command not found:  Converters Could not save preferences Current KMap: Current KMaps DOS Directed Direction of selected text has been re-set. Direction of selected text has been set. Document __T__ext Embedding Error while writing:  F-Key File File Type File exists.
Save anyway? File exists. Use -yes option. File: Filter: Find(__Q__) Folder Folder: Folders can be created
in current directory only. Font Font Size: Glyph Info: Handwriting Input Highlighting Highlighting(__N__) Home Input Key Input Line Break Line: Look-up MAC Make Folder No Nothing to redo. Nothing to undo. OK Opened:  Output PS Pending printing job.
Exit anyway?
 Pending printing job. Enter quits! Pending printing job. Try -yes option Print Previe__w__ Print Previe__w__ (pending job) Printed %d page(s). Printing... Remark: Show Hidden Smaller(__A__) Specified folder
already exists.
 Strokes: Unix Unknown encoding:  Unsaved changes.
Exit anyway?
 Unsaved changes.
Open anyway?
 Unsaved changes. Enter discards! Unsaved changes. Enter quits! Unsaved changes. Try -yes option Will not create
specified folder
 Wrote:  Yes __D__irection Override __E__mbedding Override __G__o To __O__pen __P__rint __P__rint (pending job) __R__edo __S__ave __S__ave (needed) __U__ndo __Y__ield Embedding find string go line [column] not found open -yes -e encoding file print -e program save -e encoding file search string not found. unsupported text/uri:  usage:  usage: find text usage: go line [column]  usage: open -e utf-8 -yes filename usage: print [-o file] [-p printer] [-e exec] [-break] [-hsize header-font-size] usage: replace original new usage: save -e utf-8 -yes filename Project-Id-Version: yudit 2.8.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2003-12-16 19:55-0500
Last-Translator: Kevin Patrick Scannell <kscanne@gmail.com>
Language-Team: Irish <ga@li.org>
Language: ga
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
  Is neamhiomlán an líne dheireanach...  Scoilt idir línte: KMapanna ar fáil Cineál Comhaid Is Fearr Níos Mó(__Z__) `CD' chuig Ní féidir an fillteán
ainmnithe a chruthú
 Ní féidir a dhul go dtí
an fhillteáin ainmnithe.
 Ní féidir a léamh:  Ní féidir a scríobh:  Cealaigh Iarrthóirí Catagóir Carachtair: Glan Colún: Ní bhfuarthas an t-ordú:  Tiontairí Níorbh fhéidir airíonna a shábháil KMap Reatha: KMapanna Reatha DOS Stiúrtha Athshocraíodh treo don téacs roghnaithe. Socraíodh treo don téacs roghnaithe. Leabú __T__éacs don Cháipéis Earráid agus ag scríobh:  F-Eochair Comhad Cineál Comhaid Tá an comhad ann cheana féin.
Sábháil ar aon chaoi? Tá an comhad ann cheana.  Úsáid an rogha `-yes'. Comhad: Scagaire: Cuardaigh(__Q__) Fillteán Fillteán: Ní féidir fillteáin a chruthú
lasmuigh den chomhadlann reatha. Cló Clómhéid: Eolas Glife: Ionchur Scríbhneoireachta Aibhsiú Aibhsiú(__N__) Baile Ionchur Ionchur Eochrach Scoilt idir línte Líne: Cuardach MAC Cruthaigh Fillteán Ní hea Faic le hathdhéanamh. Faic le cur ar ceal. Ceart Oscailte:  Aschur PS Tá jab priontála ar siúl.
Éirigh amach ar aon chaoi?
 Tá jab priontála ar siúl.  Iontráil quits! Tá jab priontála ar siúl.  Bain triail as an rogha `-yes' Réamhamharc Priontála Réamhamharc Priontála(__w__): jab ar siúl Priontáladh %d leathanach. Ag priontáil... Nóta: Taispeáin comhaid atá i bhfolach Níos Lú(__A__) Tá an chomhadlann
seo ann cheana.
 Stríoca: Unix Ionchódú anaithnid:  Tá athruithe gan sábháil ann.
Éirigh amach ar aon chaoi?
 Tá athruithe gan sábháil ann.
Oscail ar aon chaoi?
 Athruithe gan sábháil.  Iontráil discards! Athruithe gan sábháil. Iontráil quits! Athruithe gan sábháil.  Bain triail as an rogha `-yes' Ní chruthófar an
fillteán ainmnithe
 Scríobhadh:  Is sea Sáraigh Treo(__D__) Sáraigh Leabú(__E__) Téigh __G__o __O__scail __P__riontáil __P__riontáil (jab ar siúl) Athdhéan(__R__) __S__ábháil __S__ábháil (riachtanach) C__U__ir ar neamhní Táirg leabú(__Y__) teaghrán a chuardach go líne [colún] ar iarraidh open -yes -e ionchódú comhad print -e clár save -e ionchódú comhad Ní bhfuarthas an teaghrán. níl an téacs/uri ar fáil:  úsáid:  úsáid: find téacs úsáid: go líne [colún]  úsáid: open -e utf-8 -yes comhad úsáid: print [-o comhad] [-p clódóir] [-e clár] [-break] [-hsize clómhéid] úsáid: replace bunthéacs nuathéacs úsáid: save -e utf-8 -yes comhad 