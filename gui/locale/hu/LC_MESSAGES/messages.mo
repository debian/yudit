��    r      �  �   <      �	     �	     �	     �	     �	     �	     
      

      +
     L
     [
     k
     r
  
   �
     �
     �
     �
     �
     �
  
   �
     �
     �
     �
            +     (   A     j     �     �     �     �  	   �     �     �                         &  1   .     `  
   e     p     |     �     �     �     �  	   �  
   �     �     �     �     �     �                    -     0     9     @     ^     w  	   z  #   �  "   �  %   �     �          #     7     C     K     W  !   f     �     �     �     �     �      �           &  !   G     i     q     u     �  	   �     �  	   �     �     �     �     �     �               %  	   6     @     [     l     �     �     �     �     �  "   �  P        X  "   t    �     �     �     �     �            *   #  %   N     t     �     �     �  	   �  
   �     �     �     �     �       1        H     X     i  	   m  7   w  0   �     �     �       (        7     =  &   I  *   p     �     �     �     �     �  0   �     �            
   %  	   0     :     K     R     Z  
   b     m     r     {          �     �     �     �     �     �     �                @     C  1   K  +   }  E   �     �  &        5     K     ]     j     r      �     �     �     �  %   �  .   �             A  0   b  *   �     �     �     �     �     �               2     O     g     w     �  !   �     �     �     �          $     5  #   M     q     �     �     �  $   �  X   �     I  (   g     ,      _   '       "   k      j      T   Y   ;      f       
       a   .   %       J          	   &   o   *   I   3           S   7   H          1   @   m   #   P   r                  X   \          +   W      =                 e   :   D      C       B   F   Q   p            A   l             ]                 5   E   `           [           ^           )   M                         9   R       (   2       !       0   b   V              4   g   q   d   /       $   >   N   <       Z   -   U   G   L   ?          c   i           O                6          n       h   8   K                    Incomplete Last Line...  Line Break: Available KMaps Best File Type Bigger(__Z__) CD to Can not create
specified folder
 Can not go to
specified folder.
 Can not read:  Can not write:  Cancel Canceled by user. Candidates Category Characters: Clear Column: Command not found:  Converters Could not save preferences Current KMap: Current KMaps DOS Directed Direction of selected text has been re-set. Direction of selected text has been set. Document __T__ext Embedding Error while writing:  F-Key Failed to set new password. File File Type File exists.
Save anyway? File exists. Use -yes option. File: Filter: Find(__Q__) Folder Folder: Folders can be created
in current directory only. Font Font Size: Glyph Info: Handwriting Input Highlighting Highlighting(__N__) Home Input Key Input Line Break Line: Look-up MAC Make Folder New Password is too short. No Nothing to redo. Nothing to undo. OK Opened:  Output Output file is not .sedy file Output sedy file exists. PS Password: Pending printing job.
Exit anyway?
 Pending printing job. Enter quits! Pending printing job. Try -yes option Print Previe__w__ Print Previe__w__ (pending job) Printed %d page(s). Printing... Remark: Show Hidden Smaller(__A__) Specified folder
already exists.
 Strokes: Unix Unknown encoding:  Unsaved changes.
Exit anyway?
 Unsaved changes.
Open anyway?
 Unsaved changes. Enter discards! Unsaved changes. Enter quits! Unsaved changes. Try -yes option Will not create
specified folder
 Wrote:  Yes __D__irection Override __E__mbedding Override __G__o To __O__pen __P__rint __P__rint (pending job) __R__edo __S__ave __S__ave (needed) __U__ndo __Y__ield Embedding find string go line [column] not found open -yes -e encoding file print -e program save -e encoding file search string not found. unsupported text/uri:  usage:  usage: find text usage: go line [column]  usage: open -e utf-8 -yes filename usage: print [-o file] [-p printer] [-e exec] [-break] [-hsize header-font-size] usage: replace original new usage: save -e utf-8 -yes filename Project-Id-Version: 2.0
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2001-01-09 22:51+0900
Last-Translator: Gaspar Sinai <gsinai@yudit.org>
Language-Team: Gaspar Sinai <gsinai@yudit.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Az utolsó sor nem teljes. Sor tördelés: Elérhető kmapok Legjobb fájltípus Nagyobb (__Z__) Mappaváltás Nem tudom a megadott
mappát létrehozni!
 Nem tudok a megadott
mappába menni!
 Nem tudom olvasni: Nem tudom írni: Mégsem Felhasználó által törölve. Jelöltek Kategória Betűk: Törlés Oszlop: Nem találom a parancsot: Átalakítók Nem tudtam elmenteni az egyéni beállításokat. Jelenlegi kmap: Jelenlegi kmapok DOS Írányos A kiválasztott szöveg irányát alaphelyzetbe hoztam. A kiválasztott szöveg iránya megváltoztatva. Szövegbeágyazás (__T__) Íráshiba: F-gomb Nem tudtam az új jelszót beállítani. Fájl Fájltípus Létező fájl!
Tényleg oda mentsük? Létező fájl. A -yes opciót használja! Fájl: Szűrő: Keresés(__Q__) Mappa Mappa: Mappát csak a jelenlegi
helyen tudok csinálni. Betűtípus Betűméret: Glif információ: Kézírás Kiemelés Kiemelés(__N__) Otthon Bevitel Bevitel Tördelés Sor: Keresés MAC Mappakészítés Az új jelszó túl rövid. Nem Nincs mit újracsinálni! Nincs mit visszacsinálni! Rendben Megnyitottam: Kimenet A célfájl nem .sedy fájl. A sedy célfájl már létezik. PS Jelszó Nyomtatás közben vagyunk.
Tényleg kilépünk?
 Nyomtatás közben vagyunk. Enterrel kilép Nyomtatás még nem fejeződött be. Próbálkozzon a -yes opcióval! Nyomtatás előnézet (__w__)  Nyomtatás előnézet(__w__) (foglalt) %d oldalt nyomtattam. Most nyomtatok... Megjegyzés: Rejtett Kisebb (__A__) A megadott mappa
már létezik!
 Vonásszám Unix A kódolás ismeretlen: Nincs elmentve.
Tényleg kilépünk?
 Nincs elmentve!
Tényleg nyissunk új fájlt?
 Nincs elmentve. Enter kitöröl! Nincs elmentve. Enterrel kilép! Nincs elmentve. Próbálkozzon a -yes opcióval! Nem fogom a megadott
mappát létrehozni!
 Írtam: Igen Szövegírány (__D__) Beágyazás (__E__) Pozicinálás (__G__) Megnyitás (__O__) Nyomtatás (__P__) Nyomtatás (__P__) (foglalt) Újracsinálás (__R__) Mentés (__S__) Mentés (__S__) (szükséges) Visszacsinálás (__U__) Beágyazás kikapcsolása (__Y__) find szöveg Használat: go sor [oszlop] Nem találom. open -yes -e kódolás fájl print -e program save -e kódolás fájl Nem találtam a keresett szöveget! Nem támogatott text/uri Használat: Használat: find szöveg Használat: go sor [oszlop] Használat: open -e utf-8 -yes fájl Használat: print [-o fájl] [-p nyomtató] [-e exec] [-break] [-hsize header-font-size] Használat: replace régi új Használat: save -e utf-8 -yes fájlnév 