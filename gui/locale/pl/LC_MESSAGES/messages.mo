��    j      l  �   �      	     	     	     .	     =	     K	      Q	      r	     �	     �	     �	  
   �	     �	     �	     �	     �	     �	  
   �	     
     !
     /
     =
     A
  (   J
     s
     �
     �
     �
  	   �
     �
     �
     �
     �
                  1        M  
   R     ]     i     {     �     �     �  	   �  
   �     �     �     �     �     �     �     �     �                 #     "   9  %   \     �     �     �     �     �     �     �  !   �          "     '     :     Y      x     �      �  !   �     �                 	   4     >  	   G     Q     i     r     {     �     �     �     �  	   �     �     �     �          ,     C     K     \  "   u  P   �     �  "     (  (     Q     _     ~     �  	   �  &   �  (   �               1     8  	   E     O  	   V     `     i  
   ~     �     �     �     �  
   �  "   �          *     B     F  	   K  &   U  &   |     �     �     �     �     �  0   �          
          ,     :     R  	   q     {     �     �     �     �     �     �     �     �     �     �  	   �            .     &   A  '   h     �  *   �     �     �     �     �               7     F     K  1   `  1   �  '   �  &   �  +     &   ?  
   f     q  '   u  "   �     �     �     �  "   �       	        %     ;     J     i          �  "   �     �     �  '   �          9     E     b  )   �  l   �  "     )   ;     e   Q   L   [   <          #              '      2   ;       %   4       E      :   !   (                      g           "       @       V   W      =      O      P                     a                 ,   >   ^   M   Y               h   0   R   c   K   f   d      \       N              )   *   6         j   T   F   D   3          &               B   I   ?         A   U       _   `   9              G   1   J       i                           +   /   C   X   	   5             $       .          S      b   -   ]   8   H      
                        7   Z     Line Break: Available KMaps Best File Type Bigger(__Z__) CD to Can not create
specified folder
 Can not go to
specified folder.
 Can not read:  Can not write:  Cancel Candidates Category Characters: Clear Column: Command not found:  Converters Could not save preferences Current KMap: Current KMaps DOS Directed Direction of selected text has been set. Document __T__ext Embedding Error while writing:  F-Key File File Type File exists.
Save anyway? File exists. Use -yes option. File: Filter: Find(__Q__) Folder Folder: Folders can be created
in current directory only. Font Font Size: Glyph Info: Handwriting Input Highlighting Highlighting(__N__) Home Input Key Input Line Break Line: Look-up MAC Make Folder No Nothing to redo. Nothing to undo. OK Opened:  Output PS Pending printing job.
Exit anyway?
 Pending printing job. Enter quits! Pending printing job. Try -yes option Print Previe__w__ Print Previe__w__ (pending job) Printed %d page(s). Printing... Remark: Show Hidden Smaller(__A__) Specified folder
already exists.
 Strokes: Unix Unknown encoding:  Unsaved changes.
Exit anyway?
 Unsaved changes.
Open anyway?
 Unsaved changes. Enter discards! Unsaved changes. Enter quits! Unsaved changes. Try -yes option Will not create
specified folder
 Wrote:  Yes __D__irection Override __E__mbedding Override __G__o To __O__pen __P__rint __P__rint (pending job) __R__edo __S__ave __S__ave (needed) __U__ndo __Y__ield Embedding find string go line [column] not found open -yes -e encoding file print -e program save -e encoding file search string not found. unsupported text/uri:  usage:  usage: find text usage: go line [column]  usage: open -e utf-8 -yes filename usage: print [-o file] [-p printer] [-e exec] [-break] [-hsize header-font-size] usage: replace original new usage: save -e utf-8 -yes filename Project-Id-Version: Yudit 2.8.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2002-11-19 13:24GMT+2
Last-Translator: Paweł Zawiła-Niedźwiecki <zawel1@gmail.com>
Language-Team: Polish <pl@li.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Koniec linii: Dostępne mapowania klawiatury Automatyczny wybór kodowania Powięks__z__ Wejdź do Nie mogę utworzyć
podanego katalogu
 Nie mogę wejść do
podanego katalogu.
 Nie można odczytać:  Nie można zapisać:  Anuluj Możliwości Kategoria Znaki: Wyczyść Kolumna: Nieznane polecenie:  Konwertery Nie mogę zapisać opcji  Bieżące mapowanie klawiatury: Bieżące mapowania klawiatury DOS Skierowany Kierunek tekstu został ustawiony. Rozmieszczenie tekstu Błąd podczas zapisu:  F-x Plik Kodowanie Plik istnieje.
Czy na pweno nadpisać? Plik istnieje. Spróbuj z opcją -yes. Plik: Filtr: Szukaj (__Q__) Katalog Katalog: Katalogi mogą być tworzone
tylko w bieżącym. Czcionka Wielkość czcionki: Numer znaku: Pismo ręczne Podświetlenie składni Podświetlenie składni(__N__) Początek Wejście Wejście Koniec lini Linia: Szukaj MAC Utwórz katalog Nie Nie ma zmian do powtórzenia. Nie ma zmian do cofnięcia. OK Otwarte:  Wyjście PS Trwa drukowanie.
Czy na pewno chcesz wyjść?
 Zadania drukowania zostaną przerwane! Trwa drukowanie. Spróbuj z opcją -yes Podgląd __W__ydruku Podgląd __W__ydruku (bieżącego zadania) Wydrukowano %d stron Drukowanie... Uwaga: Pokaż ukryte Zmniejsz (__A__) Podany katalog
nie istnieje.
 Naciśnięcia: Unix Nieznane kodowanie:  Niezapisane zmiany.
Czy na pewno chcesz wyjść?
 Niezapisane zmiany.
Czy na pewno otworzyć nowy?
 Niezapisane zmiany zostaną zapomniane! Niezapisane zmiany zostaną porzucone! Nie zapisane zmiany. Spróbuj z opcją -yes Podany katalog
nie zostanie utworzony
 Zapisano:  Tak Zmień globalny kierunek tekstu (__D__) Zmień lokalny ki__e__runek tekstu Idź do (__G__) __O__twórz Drukuj (__P__) Drukuj (__P__) (bieżące zadanie) Powtó__r__z Zapi__s__ Zapi__s__ (potrzebny) Cofnij (__U__) Prz__y__wróć kierunek tekstu znajdź ciąg znaków go linia [kolumna] nie znaleziony open -yes -e kodowanie nazwa_pliku print -e program save -e kodowanie nazwa_pliku nie odnaleziono podanego ciągu znaków nieobsługiwany tekst/adres:  składnia:  skłania: find ciąg_znaków składnia: go linia [kolumna]  składnia: open -e utf-8 -yes nazwa_pliku składnia: print [-o nazwa_pliku] [-p drukarka][-e program] [-break] [-hsize wielkość_czcionki_nagłówka] składnia: replace oryginalny nowy składnia: save -e utf-8 -yes nazwa_pliku 