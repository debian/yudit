��    f      L  �   |      �     �     �     �     �     �      �      	     #	     2	     B	  
   I	     T	     ]	     i	     o	  
   w	     �	     �	     �	     �	  +   �	  (   �	      
     
     
  	   !
     +
     E
     c
     i
     q
     }
     �
  1   �
     �
  
   �
     �
     �
     �
     �
            	     
   "     -     3     ;     ?     K     N     _     p     s     |     �  #   �  "   �  %   �     �          %     9     E     M     Y  !   h     �     �     �     �     �      �     
      (  !   I     k     s     w  	   �     �  	   �     �     �     �     �     �     �     �  	             2     C     Y     r     �     �     �  "   �  P   �     /  "   K  !  n     �     �     �     �     �     �     
     &     6  	   G  	   Q  
   [     f     m     u     ~     �     �     �  	   �  ,   �  ,   �     %     :     B     K  !   Z  (   |  	   �     �     �     �     �  .   �                    1     A     M     `     f     k     }     �     �     �     �     �     �     �     �     �     �     �  3   �  +   -  4   Y     �  $   �     �     �     �     �     	          /     7     <  '   P  &   x  *   �  $   �  /   �       
   >     I     L     ^  	   n     x     �     �  
   �     �     �  	   �     �               )     :     U     s     �     �     �  !   �  _   �     @  !   [         \      ^       	   J   <   a       C   ?      @                      )       (   X   ,   0              I   U         7   /   Z   %   F         Y   >      R   L   =          +   "   ]   c           A      `       5   P   $           S       d   f          1      '   *       _              6       G   9      M   -   b   4                     D   Q                  3   !          ;   K      W          H                        B      #   :   &   V   E   .   
   T   e   2           N   8         O      [         Line Break: Available KMaps Best File Type Bigger(__Z__) CD to Can not create
specified folder
 Can not go to
specified folder.
 Can not read:  Can not write:  Cancel Candidates Category Characters: Clear Column: Converters Current KMap: Current KMaps DOS Directed Direction of selected text has been re-set. Direction of selected text has been set. Error while writing:  F-Key File File Type File exists.
Save anyway? File exists. Use -yes option. File: Filter: Find(__Q__) Folder Folder: Folders can be created
in current directory only. Font Font Size: Glyph Info: Handwriting Input Highlighting Highlighting(__N__) Home Input Key Input Line Break Line: Look-up MAC Make Folder No Nothing to redo. Nothing to undo. OK Opened:  Output PS Pending printing job.
Exit anyway?
 Pending printing job. Enter quits! Pending printing job. Try -yes option Print Previe__w__ Print Previe__w__ (pending job) Printed %d page(s). Printing... Remark: Show Hidden Smaller(__A__) Specified folder
already exists.
 Strokes: Unix Unknown encoding:  Unsaved changes.
Exit anyway?
 Unsaved changes.
Open anyway?
 Unsaved changes. Enter discards! Unsaved changes. Enter quits! Unsaved changes. Try -yes option Will not create
specified folder
 Wrote:  Yes __D__irection Override __G__o To __O__pen __P__rint __P__rint (pending job) __R__edo __S__ave __S__ave (needed) __U__ndo find string go line [column] not found open -yes -e encoding file print -e program save -e encoding file search string not found. unsupported text/uri:  usage:  usage: find text usage: go line [column]  usage: open -e utf-8 -yes filename usage: print [-o file] [-p printer] [-e exec] [-break] [-hsize header-font-size] usage: replace original new usage: save -e utf-8 -yes filename Project-Id-Version: yudit 2.8.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2001-11-11 22:10+0100
Last-Translator: Roman Maurer <roman.maurer@amis.net>
Language-Team: Slovenian <sl@li.org>
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
  Prelom vrstice: Dostopne preslikave tipk Najboljša vrsta datoteke Večje(__Z__) Pojdi v Ni moč ustvariti
podane mape.
 Ni moč iti
v podano mapo.
 Ni moč brati:  Ni moč pisati:  Prekliči Kandidati Kategorija Znaki: Zbriši Stolpec: Pretvorniki Trenutna preslikava tipk: Trenutne preslikave tipk DOS Usmerjeno Nastavljena je bila smer izbranega besedila. Nastavljena je bila smer izbranega besedila. Napaka pri pisanju:  F-tipka Datoteka Vrsta datoteke Datoteka obstaja.
Vseeno shranim? Datoteka obstaja. Uporabite izbiro -yes. Datoteka: Filter: Poišči(__Q__) Mapa Mapa: Mape je moč ustvariti
le v trenutnem imeniku. Pisava Velikost pisave: Podatki o sestavljenem: Vnos s pisanjem Poudarjanje Poudarjanje(__N__) Domov Vhod Vhod s tipkovnice Prelom vrstice Vrstica: Poglej MAC Ustvari mapo Ne Ničesar za ponoviti. Ničesar za razveljaviti. V redu Odprto:  Izhod PS Potekajoče tiskalniško opravilo.
Vseeno končam?
 Poteka tiskalniško opravilo. Enter konča! Poteka tiskalniško opravilo. Poskusite izbiro -yes. Predogled tiskanja(__w__) Predogled tiskanja(__w__) (opravilo) Natisnjenih strani: %d. Tiskanje... Opomba: Prikaži skrite M__a__njše Podana mapa
že obstaja.
 Zamahi: Unix Neznano kodiranje:  Neshranjene spremembe.
Vseeno končam?
 Neshranjene spremembe.
Vseeno odprem?
 Neshranjene spremembe. Enter jih zanemari! Neshranjene spremembe. Enter konča! Neshranjene spremembe. Poskusite z izbiro -yes. Podane mape
ni moč ustvariti
 Zapisano:  Da Smer bese__d__ila Pojdi na(__G__) __O__dpri Tiskaj(__P__) Tiskaj(__P__) (opravilo) Ponovi(__R__) __S__hrani __S__hrani (treba) Razveljavi(__U__) najdi niz pojdi v vrstico [stolpec] niso najdeni open -yes -e encoding file print -e program save -e kodiranje datoteka iskalnega niza ni moč najti. nepodprt text/uri:  raba:  raba: find besedilo raba: go vrstica [stolpec]  raba: open -e utf-8 -yes datoteka raba: print [-o datoteka] [-p tiskalnik] [-e program] [-break] [-hsize velikost-pisave-v-glavi] raba: replace izvirni novi raba: save -e utf-8 -yes datoteka 