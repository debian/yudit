��    b      ,  �   <      H     I     V     f     u     �      �      �     �     �     �  
   �     �     	     	     	  
   *	     5	     C	     Q	     U	  (   ^	     �	     �	     �	  	   �	     �	     �	     �	     �	     �	     
     
  1   
     E
  
   J
     U
     a
     s
     x
  	   ~
  
   �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
  #   �
  "     %   3     Y     k     �     �     �     �  !   �     �     �     �     	     (      G     h      �  !   �     �     �     �  	   �     �  	   �     	     !     *     3     E     N     Z  	   k     u     �     �     �     �     �     �        "     P   <     �  "   �  �   �  
   �     �     �     �     �      �           /     ?     O  	   V     `     p  	   w     �  	   �     �  	   �     �     �  !   �     �                 +     '   K  	   s  	   }     �  	   �     �  1   �     �     �     �       	     	        '     .  	   ;     E     L     P     `     d     }     �     �     �     �  2   �  -   �  0        F  %   [     �     �     �     �  #   �  	   �     �     �  2     >   @  -     -   �  0   �           -     :     >     S     b     q     �     �     �     �     �     �     �  	     '        ?  !   P  !   r     �  
   �     �     �  (   �  `     .   v  (   �     B       >   %   3       P      $          F   ;   8            :       W   \          J   *   _   [       2      '                    L   1          -           Q   !   E      ]   /   7          5      S                             .      (   9       O         I   4          a      T   0   )           	      <           M       +       R   b      V       N   6   A   K               @       "   ^      
       X                Z   =       `   U   G      &   #   C   H       Y   D   ?   ,     Line Break: Available KMaps Best File Type Bigger(__Z__) CD to Can not create
specified folder
 Can not go to
specified folder.
 Can not read:  Can not write:  Cancel Candidates Characters: Clear Column: Command not found:  Converters Current KMap: Current KMaps DOS Directed Direction of selected text has been set. Error while writing:  F-Key File File Type File exists.
Save anyway? File exists. Use -yes option. File: Filter: Find(__Q__) Folder Folder: Folders can be created
in current directory only. Font Font Size: Glyph Info: Handwriting Input Home Input Key Input Line Break Line: Look-up MAC Make Folder No Nothing to redo. Nothing to undo. OK Opened:  Output PS Pending printing job.
Exit anyway?
 Pending printing job. Enter quits! Pending printing job. Try -yes option Print Previe__w__ Print Previe__w__ (pending job) Printed %d page(s). Printing... Show Hidden Smaller(__A__) Specified folder
already exists.
 Strokes: Unix Unknown encoding:  Unsaved changes.
Exit anyway?
 Unsaved changes.
Open anyway?
 Unsaved changes. Enter discards! Unsaved changes. Enter quits! Unsaved changes. Try -yes option Will not create
specified folder
 Wrote:  Yes __D__irection Override __G__o To __O__pen __P__rint __P__rint (pending job) __R__edo __S__ave __S__ave (needed) __U__ndo find string go line [column] not found open -yes -e encoding file print -e program save -e encoding file search string not found. unsupported text/uri:  usage:  usage: find text usage: go line [column]  usage: open -e utf-8 -yes filename usage: print [-o file] [-p printer] [-e exec] [-break] [-hsize header-font-size] usage: replace original new usage: save -e utf-8 -yes filename Project-Id-Version: yudit 2.4.8
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2001-11-23 15:11+0800
Last-Translator: 周金念
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
  换行： 可用的输入法 预设文件类型 放大(__Z__) 转到目录 无法建立
指定的文件夹
 无法转到
指定的文件夹
 无法读取： 无法写入： 取消 选字区 字符编号： 清除 字数： 无法读取： 参照档 当前输入法： 输入法 DOS 笔划方向 选定文字的方向已设定。 写入时发生错误： F- 键 文件 文件类型 文件已存在。
是否确定要替换？ 文件已存在。使用 -yes 选项。 文件： 类别： 查找 (__Q__) 文件夹 文件夹： 文件夹只可以在
当前的目录下建立。 字体 字号大小： 字符编号： 手写输入 根目录 输入法 输入 换行方式 行数： 校对 MAC 建立文件夹 否 没有操作可重复。 没有操作可还原。 确定 已打开： 输出 PS 有待打印的文件。
是否确定要退出？
 有待打印的文件。按输入键退出！ 有待打印的文件。尝试使用 -yes 选项 打印预览 (__W__) 打印预览(待打印文件) (__W__) 已打印 %d 页。 正在打印... 切换隐藏文件 缩小 (__A__) 指定的文件夹
已经存在。
 笔划： Unix 不详的编码： 改变的尚未保存。
是否确定要退出？
 改变的尚未保存。
是否确定打开另一个文件？
 改变的尚未保存。按回车键放弃！ 改变的尚未保存。按回车键退出！ 改变的尚未保存。尝试使用 -yes 选项 不能建立
指定的文件夹
 已写入： 是 文字方向 (__D__) 转到 (__G__) 打开 (__O__) 打印 (__P__) 打印(待打印文件) (__P__) 重复 (__R__) 保存 (__S__) 保存 (已改变的)(__S__) 还原 (__U__) 查找文字串 go 行数 [字数] 找不到 open  -yes -e 编码类别 文件名称 打印 -e 程序 save -e 编码类别 文件名称 找不到要查找的字符串。 不支持的 text/uri： 用法：  用法：find 字符串 用法：go 行数 [字数] 用法：open -e utf-8 -yes 文件名称 用法：print [-o 文件名称] [-p 打印机] [-e 执行] [-break] [-hsize 标题字号大小] 用法：replace 原文字串  新的文字串 用法：save -e utf-8 -yes 文件名称 