2002-12-09
  o Removed some decoder-only maps from Keymap selection
  o updated tests/text/ipa.txt
  o On windows replaced kinput2 with WindowsIM
  o Removed winuser.h - accidental inclusion, sorry
  o added command 'configure'
  o updated doc/ru/FAQ.TXT.in
  o Tried to fix Windows pipe command for printing.
    tested on Win98 and WinXP with ghostgun. 
2002-12-08
  o Fixed some bugs in TTF character lookup
2002-12-07
  o renamed OldHungarian.kmap to HungarianRunes.kmap
  o updated README.TXT
  o Vyacheslav Dikonov <sdiconov@mail.ru> added:
    config/yudit.properties.alt1dark
    config/yudit.properties.alt1light
  o As Vyacheslav Dikonov <sdiconov@mail.ru> suggested:
    mv Ben-Inscript.kmap Bengali-Inscript.kmap
    mv Dev-Inscript.kmap Devanagari-Inscript.kmap
    mv Dev-Phonetic.kmap Devanagari-Phonetic.kmap
    mv Guj-Inscript.kmap Gujarati-Inscript.kmap
    mv Gur-Inscript.kmap Gurmukhi-Inscript.kmap
    mv Kan-Inscript.kmap Kannada-Inscript.kmap
    mv Mal-Inscript.kmap Malayalam-Inscript.kmap
    mv Ori-Inscript.kmap Oriya-Inscript.kmap
    mv Tam-Inscript.kmap Tamil-Inscript.kmap
    mv Tel-Inscript.kmap Telugu-Inscript.kmap

    # Misc renamings
    mv smeTeX.kmap TeX-smeTeX.kmap1
    mv SK.kmap Slovak-Programmer.kmap

    # Vietnamese soup (just make their names start with 'Vietnamese') 
    mv TCVNcombine.kmap Vietnamese-TCVNcombine.kmap
    mv TCVNkey.kmap Vietnamese-TCVNkey.kmap
    mv VietnameseTelex.kmap Vietnamese-Telex1.kmap
    mv VNtelex.kmap Vietnamese-Telex2.kmap

    # Russian Soup (okroshka)
    # Translit hacky maps
    mv Russian-ISO-Translit.kmap Russian-ISO-Latinitsa.kmap

    # this transliteration is very strange and misleading 
    mv Russian.kmap Russian-Translit-Slovene.kmap

    # from a Russian point of view
    mv RU-translit.kmap Russian-Translit.kmap
    mv Russian-javertz.kmap Russian-Translit-German.kmap
    # the character sequence is not "javertz". It is translit for Germans.
    mv RU-javerty.kmap Russian-JAVERTY.kmap
    # Standard keyboards
    mv RU-icuken-extended.kmap Russian-extended.kmap
    mv RU-icuken.kmap Russian.kmap

    # Where is the Ukrainian.kmap? - it is in mys - gaspar
    mv Ukrainian-translit.kmap Ukrainian-Translit.kmap
  o Added doc/ru/FAQ.TXT.in from Vyacheslav Dikonov <sdiconov@mail.ru>
2002-12-06
  o created SAMPA kmap.
  o created experimental tests/text/ipa.html for Hungarian
  o updated 
      IS_AS.mys IS_BN.mys IS_DV.mys IS_GJ.mys IS_KN.mys IS_ML.mys
      IS_OR.mys IS_PJ.mys IS_RM.mys IS_TL.mys IS_TM.mys 
    from Anirban Mitra <mitra_anirban@yahoo.co.in>
  o update doc/HOWTO-windows.txt 
    from Anirban Mitra <mitra_anirban@yahoo.co.in>
  o update ani.ttf in
      http://www.yudit.org/download/fonts
    from Anirban Mitra <mitra_anirban@yahoo.co.in>
2002-12-05
  o made a blind fix for TAB key to return a character on windows in
    swindow/swin32/SWin32.cpp. This needs testing...
  o updated TODO.TXT
2002-12-04
  o Fixed OTF ligating problem in Tamil inttoduced by Korean JAMO support
    Fonts:
      http://groups.yahoo.com/group/tamilinix/files/unicode/fonts
    reported by Thuraiappah Vaseeharan <t_vasee@yahoo.com>
  o findGlyph will not go through all tables unless :unicode is
    precified. It will just try the best table.
  o For root input style do not set any IC values.
2002-12-03
  o Added simple-dark syntax highlight for light background setup.
  o Small fixes to tests/text/ligatures.txt - should this be move to
    a howto document?
  o In getShape I changed getChar to getFirstChar to allow for
    composing shaping for certain characters.
    Mainly TATWEEL, and some Syriac chars, when composing marks 
    apllied, now should shape properly.
2002-12-02
  o Applied setlocale 'C' -> setlocale '' patch 
     from Jean-Marc Lienher <oksid@bluewin.ch>.
     (yudit-2.7.patch4.txt).
  o Added SD_COMBINING_LIGATURE cluster (that will only
    have unicode representation) for ligatures with combining mark.
  o Added addCombiningLigature to SCluster.cpp, this can be used for
    \u0644\u0650\u0623\u064e\u0628\u064a for instance, where
    the marks are in beetween the characters forming the ligature.
2002-12-01
  o Fixed U+540D handwriting data in kanji.hwd
2002-11-30
  o Create 2.7.2 tree from 2.7
  o Applied 
      2.7.patch1 - redrawing protruding glyphs
      2.7.patch2 - embedding/unmebedding selected text in DOS mode
      2.7.patch3 - XCIN over the spot problem fix
  o Added kmaps from Bruno Cauchy Lefebvre <bb.lefebvre@free.fr>
     o Berbere-conversion1.kmap 
     o Berbere-etendu.kmap
     o Berbere-standard.kmap
  o Added HOWTO-berber.txt from Bruno Cauchy Lefebvre <bb.lefebvre@free.fr>
  o Added kmaps from Vyacheslav Dikonov <sdiconov@mail.ru>
     o RU-icuken-extended.kmap
     o RU-icuken.kmap
  o Added kmaps from Hamed Seyed-allaei <allaei@sissa.it>
     o Farsi.kmap
  o mytool/kmap/ cleanup
    mv DE-RU.kmap Russian-javertz.kmap
    rm KOI8_R_KEY.kmap 
    mv SR-Cyrillic.kmap Serbian.kmap
    mv CS-dead.kmap Czech-deadkeys.kmap
  o mytool/mys cleanup
    mv Ukr-extended.mys Ukrainian-extended.mys
  o removed "decode bad map zero matched" error message (Farsi.kmap does it).
  o added Russian menu translations from Vyacheslav Dikonov <sdiconov@mail.ru>
-------------------------------------------------------------------------------
Older changes are in history/CHANGELOG-2.7.TXT
