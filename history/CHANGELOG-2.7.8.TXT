2005-03-22
 o 19:30 JST beta period ended.

2005-03-21
 o added filename to title bar.
 o set application icon.

2005-03-09
 o added 
      doc/it/FAQ.TXT.in contributed by Marco Ciampa <ciampix@libero.it>

2005-02-23
 o Applied patch from Marcus Meissner <meissner@suse.de>
   for gcc4. (His other patch for InputMethod.cpp fixes already fixed items)

2004-12-17
 o applied yudit-2.7.6-patch2.txt from John Lane john at turbolinux.co.jp.
   this fixes InputMethod.cpp last arg 0 (int to char*) on 64 bit intel.

2004-08-21
 o  uniprint and uniconv help message to be printed on stdout.

2004-08-02
 o Added 
      mytool/kmap/GreekAncient.kmap
      doc/HOWTO-greekancient.txt
   from Yves Codet <ycodet@club-internet.fr>

2004-07-09
 o Added some Bangla TODO items from Dr Anirban Mitra.
 o Added Telugu FAQ into doc/te/FAQ.TXT.in from krsnadasakaviraju@yahoo.com.
 o Modified French kmap adding
       NO-BREAK SPACE
       RIGHT SINGLE QUOTATION MARK (APOSTROPHE)
   from Yves Codet <ycodet@club-internet.fr>
 o Modified Klingon.kmap adding
       KLINGON LETTERS N+GH
   for easy typing by
       "David L. Yeung" <dlyeung@hopper.math.uwaterloo.ca>
2004-03-10
 o Fixed Malayalam-Inscript.kmap:

   "0x3d=0x0D43", // MALAYALAM VOWEL SIGN VOCALIC R

 o Added Sanskrit-Translit.kmap from Horia Dumitrescu <pandumi@wanadoo.fr>

2004-01-25
 o Added SpanishPrefix.kmap from
   Jesús Marín <jesus@aplsoftware.com>

2004-01-04
 o Added Irish (gui/local/ga) from Kevin Patrick Scannell <scannell@slu.edu>

2003-12-09
 o Updated Tibetan-Wylie.kmap from Gregory Mokhin <mokhin@bog.msu.ru>
 o Added termination patches for
        mytool/kmap/Berbere-etendu.kmap
        mytool/kmap/Bulgarian.kmap
        mytool/kmap/Devanagari-Phonetic.kmap
        mytool/kmap/GrandLatin.kmap
        mytool/kmap/Gujarati-Inscript.kmap
        mytool/kmap/Oriya-Inscript.kmap
        mytool/kmap/Russian-Translit-German.kmap
        mytool/kmap/Russian-Translit-Slovene.kmap
        mytool/kmap/Telugu-Inscript.kmap
   from eisvogel <eisvogel.flieg@gmx.net>
2003-11-23
 o Added ArabTeX.kmap from Alberto Corbi Bellot <alberto_corbi@terra.es>
2003-11-05
 o added CS-qwerty keymap
   from Michal Èihaø <michal@cihar.com>
2003-10-04
 o applied kmap patch yudit-2.7.6.patch1.txt
2003-09-20
 o Added new menu translations:
   mr  Marathi               Swapnil Hajare<dreamil1000a@yahoo.com>
   gu  Gujarati              Vibha Sinojia <vibhavachhani@hotmail.com>
 o Added jodakshar.hwd marathi.hwd roman.hwd handwrinting recognition files
   from Swapnil Hajare<dreamil1000a@yahoo.com>.
 All these have been received from:
   http://www.indictrans.org/
-------------------------------------------------------------------------------
Older changes are in history/CHANGELOG-2.7.6.TXT
