/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
 
#ifndef ProcessFile_h
#define ProcessFile_h

#include "stoolkit/SBMap.h"

#define MAX_COMMENT_SIZE	2048
#define MAX_NMAPS 		256

class SBMapItemVector
{
public:
	SBMapItemVector();
	// For kmaps
	SBMapItemVector(int _size, const char** _names, int _reverse);

	~SBMapItemVector();

	int addKLine (int index, const char* line, const unsigned char* comment, int commentSize, int reverse);

	int		count;
	SBMapItem* 	maps[MAX_NMAPS];
};


SBMapItemVector* processUNI (const char* filename, SInputStream& file, int nocomment, int hilo, int keysize, int from, int to, int reverse, char* comment, char* name, int* type);

SBMapItemVector* processMYS (const char* filename, SInputStream& file, int nocomment, int reverse, char* comment, char*name, int* type);

SBMapItemVector* processKMap (const char* filename, SInputStream& file, int nocomment, int reverse, char* comment, char*name, int* type);


#endif /* ProcessFile_h */
