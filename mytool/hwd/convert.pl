#!/usr/bin/perl
# A converter to convert these include files to 
# something more readable.
# Gaspar Sinai <gsinai@yudit.org>
# Tokyo 2001-10-01
# uniconv was fixed from yudit-2.4.8.beta7 for shift-jis


die "usage: $0 gb or sjis < filename" if ($#ARGV < 0 ||
  ($ARGV[0] ne "gb" && $ARGV[0] ne "sjis"));
$isgb = ($ARGV[0] eq "gb") ? 1 : 0;
shift;

$convopt = ($isgb==1) ? "gb-2312-x11" : "shift-jis";

$converter = "| uniconv -decode $convopt ";
die "can not execute $converter" unless (open (UNICONV, $converter));
($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst)
  = localtime(time);
$datenow = sprintf ("%04d-%02d-%02d %02d:%02d:%02d", $year+1900, $mon+1, $mday,
  $hour, $min, $sec);

print UNICONV <<EOD;
#HWD 1.0  DO NOT REMOVE THIS LINE
# Machine Generated, but editable file.
# Memory Mappable Handwriting Data
# Converted from $ARGV[0] by $0
#
# NOT RECOMMENDED FORMAT FOR HAND-MADE DATA
# Gaspar Sinai <gsinai\@yudit.org>
# $datenow
#
#   stroke      character   
#   count         utf-8         Stroke Desc. 
#   <---->[space]<---->[space]<----------------->[\\n]
#   2Bytes       N bytes      Variable Bytecount
#
# Strokes Desc
# * This is from JStroke. (TDR) is Todd's JavaDict format.
# * One stroke is a capital letter that might 
#   be followed by a small letter, in case of curves.
# * Capital and small letters have the same meaning: angle.
# * Strokes are in order packed one after the other
#   
#   A or a TDR='1' CLK=07:30 DEG=225
#   B or b TDR='2' CLK=06:00 DEG=180
#   C or c TDR='3' CLK=04:30 DEG=135
#   D or d TDR='4' CLK=09:00 DEG=270 
#   F or e TDR='6' CLK=03:00 DEG=090 
#   G or f TDR='7' CLK=10:30 DEG=315 
#   H or h TDR='8' CLK=12:00 DEG=360 
#   I or i TDR='9' CLK=01:30 DEG=045 
#   J or j TDR='x' down   06:00 then 07:30 
#   K or k TDR='y' down   06:00 then 04:30 
#   L or l TDR='c' down   06:00 then 03:00 
#   M or m TDR='b' across 03:00 then 06:00 
#
#   Notes:
#       TDR is the JavaDict format. 
#       JavaDict does not pack, it has spaces.  
#
#   After the last stroke there can be an extra filter.  
#   Filters use window coordinate system so the upper 
#   left corcer is the origo. Filter signals are
#   calculated by substrating one location-or-length
#   from another.  Filters start at '|'. Generally 
#       <nane-1><strike-1><-sign><nane-2><strike-2>
#   If a filter is followed by '!' that makes the filter 
#
#   stronger.
#   Filter names:
#
#   x1 stroke 0 x-start 
#   y2 stroke 1 y-start
#   i1 stroke 0 x-end 
#   j1 stroke 0 y-end 
#   a1 stroke 0 x-middle 
#   b1 stroke 0 y-middle 
#   l3 stroke 2 length 
#   - 
#   ' ' new filter follows
#   '|' new filter set starts
#
EOD

$line = 0;
while (<>) {
  $line++;
  if (!/^\s*\"/) {
    next;
  }
  $data = eval $_;
  if (!defined $data) {
    die "Could not parse line $line: $!";
  }
  $first = substr($data,0,1);
  my ($c1, $c2) = unpack ("CC", substr($data,1,2));
  $rest = substr($data,3,-1);
  $decoded = "";
  if ($isgb==1){
    $decoded = "\033\$(A" . chr($c1&0x7f) . chr($c2&0x7f) . "\033(B";
  } else {
    $decoded = chr($c1) . chr($c2);
  }
  $fstr = sprintf ("%02d", ord($first)-ord('A')+1);
  $str = $fstr . " " . $decoded  . " " . $rest . "\n";

  print UNICONV $str;
}
close (UNICONV);

exit (0);
