#!/usr/bin/perl -w

# This script generates the kmap file for "3-set-keyboard-like"  
# transliteration-based Korean input(I call it Hangul_R3.kmap)
# Transliteration/Romanization rule is my own  and has nothing to
# with any standard for transliteration of Korean with Roman alphabets. 
# A couple of transliteration standards are available
# at http://asadal.cs.pusan.ac.kr/hangeul/
# and it's trivial to modify this script to use one of those standards.

# It convers only pre-composed Hangul syllables beginning at  UAC00.
# Some enhancement is necessary to support standalone Jamos and 
# other characters for full Korean support
# It doesn't deal with composing Hangul syllables
# using conjoining Hangul Jamos  [U1100-U11FF].

@init = ( "G",  "Gg", "N", "D","Dd","L","M", "B","Bb",
          "S", "Ss", "", "J", "Jj", "Ch", "K","T",
          "P", "H");

@vowel = ( "a", "ae", "ya", "yae","eo","e","yeo","ye",
           "o","oa", "oae", "oi","yo","u","ueo","ue","ui",
           "yu", "eu", "eui", "i");

@vowel2 = ( "A", "Ae", "Ya", "Yae","Eo","E","Yeo","Ye",
           "O","Oa", "Oae", "Oi","Yo","U","Ueo","Ue","Ui",
           "Yu", "Eu", "Eui", "I");

@final = ( "g", "gg", "gs", "n","nj", "nh", "d", 
           "l", "lg", "lm", "lb","ls","lt","lp","lh",
           "m", "b","bs", "s", "ss", "ng", "j", "ch", "k",
           "t", "p", "h" );

$start= 0xac00;

for ($i=0; $i < 19; $i++) {
	$ic = $init[$i];
	for ($m=0; $m < 21; $m++) {
		for ($f=0; $f < 28; $f++) {
			$mv = $vowel[$m];
			$fc = ($f == 0 ? '' : $final[$f-1]) ; 
			if ($ic eq "")
			{
				$mv = $vowel2[$m];
			}
			printf ("%04X -> %s%s%s\n", ($i * 21 +$m) * 28 + $f + $start, &quote ($ic), &quote ($mv), &quote ($fc));;
		}
	}
}

sub
quote
{
	local ($str) = $_[0];
	$all = "";
	for ($s=0;$s<length($str); $s++)
	{
		$all = $all . "'" . substr ($str, $s, 1) . " ";
	}
	return $all;
}
