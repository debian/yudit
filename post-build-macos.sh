#!/bin/bash
#
# Experiemnting with background image.
out="test.dmg"
# Picture is in .background
backgroundPictureName="mac-installer-512x320.png"
src="target/Applications"
title="Yudit"
applicationName="Yudit"
#size="40000"
# kilobytes
exact_size=$(du -ks $src | cut -f 1)
size=$(echo "$exact_size + 40000" | bc)
finalDMGName="$(basename $(echo target/*.dmg))"

echo "$exact_size $size"

if [ ! -f "target/$finalDMGName" ]
then
  echo "This program should be run after build-for-mac.sh"   
  exit 1
fi

echo "$oldname"
echo "creating rw disk"
#
# https://stackoverflow.com/questions/96882/how-do-i-create-a-nice-looking-dmg-for-mac-os-x-using-command-line-tools
#
#


# 1. Make sure that "Enable access for assistive devices" 
# is checked in System Preferences>>Universal Access. 
# It is required for the AppleScript to work. 
# You may have to reboot after this change 
# (it doesn't work otherwise on Mac OS X Server 10.4).

#
# 2. Create a R/W DMG. It must be larger than the result will be. 
# In this example, the bash variable "size" contains the size in 
# Kb and the contents of the folder in the "src" 
# bash variable will be copied into the DMG:
#
rm -f pack.temp.dmg

hdiutil create -srcfolder "${src}" -volname "${title}" -fs HFS+ \
      -fsargs "-c c=64,a=16,e=16" -format UDRW -size ${size}k pack.temp.dmg

echo "Make sure Yudit is unmounted / detached and press enter:"
read response

# 3. Mount the disk image, and store the device name 
# (you might want to use sleep for a few seconds after this operation):
echo "mounting rw disk"
device=$(hdiutil attach -readwrite -noverify -noautoopen "pack.temp.dmg" | \
         egrep '^/dev/' | sed 1q | awk '{print $1}')
#
# 4. Store the background picture (in PNG format) in a folder 
# called ".background" in the DMG, and store its name in the 
# "backgroundPictureName" variable.
#
echo "mounted $device"

# press enter
#
# 5. Use AppleScript to set the visual styles (name of 
# .app must be in bash variable "applicationName", 
# use variables for the other properties as needed):
# 


echo "First iteration"
echo '
   tell application "Finder"
     tell disk "'${title}'"
           open
           set theViewOptions to the icon view options of container window
           set current view of container window to icon view
           set toolbar visible of container window to false
           set statusbar visible of container window to false
           set the bounds of container window to {400, 100, 912, 420}
           set theViewOptions to the icon view options of container window
           set arrangement of theViewOptions to not arranged
           set icon size of theViewOptions to 64
           set background picture of theViewOptions to file ".background:'${backgroundPictureName}'"
           update without registering applications
           delay 5
           close
     end tell
   end tell
' | osascript

echo "Second iteration"
sleep 3

echo '
   tell application "Finder"
     tell disk "'${title}'"
           open
           set theViewOptions to the icon view options of container window
           set current view of container window to icon view
           set toolbar visible of container window to false
           set statusbar visible of container window to false
           set the bounds of container window to {400, 100, 912, 420}
           set arrangement of theViewOptions to not arranged
           set icon size of theViewOptions to 72
           set background picture of theViewOptions to file ".background:'${backgroundPictureName}'"
           update without registering applications
           delay 5
     end tell
   end tell
' | osascript

# removed close after delay.
echo "Update: Set Sizes and positions manually and press enter. "
read text

chmod -Rf go-w /Volumes/"${title}"
sync
sync
hdiutil detach ${device}
hdiutil convert "pack.temp.dmg" -format UDZO -imagekey zlib-level=9 -o "${finalDMGName}"

mv ${finalDMGName} target/
rm -f pack.temp.dmg 
echo "Replaced target/${finalDMGName}"
