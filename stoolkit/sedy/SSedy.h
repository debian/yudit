/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023 Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef SSedy_h
#define SSedy_h

#include <stoolkit/SString.h>
#include <stoolkit/SStringVector.h>

// Encryption is loaded as an external dll because
// of export restrictions.
extern "C"
{
    typedef struct _SSedyFunctions {
        void* (*sedy_create) (char* password, char* cypher);
        void (*sedy_destroy) (void* sedy);
        char* (*sedy_get_result) (void* sedy);
        unsigned long (*sedy_get_result_size) (void* sedy);
        int (*sedy_encrypt)(void* sedy, char * buffer, unsigned long buffer_size);
        int (*sedy_decrypt)(void* sedy, char * buffer, unsigned long buffer_size);
        // Get last cypher.
        char* (*sedy_get_cypher)(void* sedy);
    } SSedyFunctions;
}

class SSedy {
public:
    SSedy ();
    ~SSedy ();
    SString encrypt (const SString& text, const SString& passWord);
    SString decrypt (const SString& text, const SString& passWord);
    static SString loadLibrary (const SStringVector& path);
    static SString getLocation ();
    SString getLastError();
    SString getLastCypher();
private:
    void prepareLastError();
    SString lastError;
};
#endif /* SSedy_h */
