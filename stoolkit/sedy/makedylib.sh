#!/bin/sh
gcc -c -Wall -Werror -fpic example.c
gcc -shared -o libsedy.dylib example.o
rm example.o
