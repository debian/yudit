#!/bin/sh
gcc -c -Wall -Werror -fpic example.c
gcc -shared -o libsedy.so example.o
rm example.o
