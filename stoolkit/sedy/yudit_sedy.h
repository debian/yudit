/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023 Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

// Interface file.

#ifndef _yudit_sedy_h
#define _yudit_sedy_h

#ifdef _WIN32
#define SEDY_EXPORT __declspec(dllexport)
#define SEDY_EXTERN 
#else
#define SEDY_EXPORT
#define SEDY_EXTERN extern
#endif

#define SEDY_NO_ERROR 0
#define SEDY_UNSOPPORTED_CYPHER 1
#define SEDY_BAD_PASSWORD 2
#define SEDY_OUT_OF_MEMORY 3

#ifdef __cplusplus
extern "C" {
#endif

// If cypher is null, it will be guessed from decrypted data.
// return the context.
SEDY_EXPORT SEDY_EXTERN void* sedy_create (char* password, char* cypher);
SEDY_EXPORT SEDY_EXTERN void sedy_destroy (void* sedy);
SEDY_EXPORT SEDY_EXTERN char* sedy_get_result (void* sedy);
SEDY_EXPORT SEDY_EXTERN unsigned long sedy_get_result_size (void* sedy);
SEDY_EXPORT SEDY_EXTERN char* sedy_get_cypher (void* sedy);
SEDY_EXPORT SEDY_EXTERN int sedy_encrypt (void* sedy, char* buffer, unsigned long buffer_size);
SEDY_EXPORT SEDY_EXTERN int sedy_decrypt (void* sedy, char* buffer, unsigned long buffer_size);

#ifdef __cplusplus
} // extern c
#endif

#endif /* _yudit_sedy_h */
