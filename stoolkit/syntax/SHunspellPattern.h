/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
 
#ifndef SHunspellPattern_h
#define SHunspellPattern_h

#include "stoolkit/syntax/SPattern.h"
#include "stoolkit/SString.h"
#include "stoolkit/SStringVector.h"
#include "stoolkit/SEncoder.h"

extern "C"
{
typedef struct _HunspellFunctions_
{
  void* (*create)(const char * affpath, const char * dpath);
  int (*spell)(void* hunspell, const char *word);
  int (*destroy)(void* hunspell);
  char* (*get_dic_encoding)(void* hunspell);
} HunspellFunctions;
}

/**
 * A regular expression pattern.
 * @author Gaspar Sinai
 * @version 2007-11-27
 */
class SHunspellPattern : public SPattern
{
public:
  SHunspellPattern (const SString& name, const SStringVector& path);
  virtual ~SHunspellPattern ();
  // This method updates matchBegin and matchEnd (exclusive)
  // variables in case of a match, in case of no match, this
  // will not be touched. Action will contain that action string.
  virtual bool   checkMatch ();
  static SString getFolderFor (const SString& name, const SStringVector& path);
  static SString getMissingFile (const SString& name, const SStringVector& path);
  static SString getLibraryLocation ();
  static SString loadLibrary (const SStringVector& path);
private:
  bool isSeparator (SS_UCS4 chr) const;
  bool isNumber (SS_UCS4 chr) const;
  bool isOther (SS_UCS4 chr) const;
  bool isJapanese (SS_UCS4 chr) const;
  SString  name;
  SString  dicFile;
  SString  affFile;
  void*             hunspell;
  HunspellFunctions functions;
  SEncoder*         entryEncoder;
};

#endif /* SHunspellPattern_h */
