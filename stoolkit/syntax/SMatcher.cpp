/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
 
#include "stoolkit/syntax/SMatcher.h"

SMatcher::SMatcher (SPattern& pat, SMatcherIterator& it) 
  : pattern(pat), iterator(it)
{
  eod = false;
  start = 0;
  pattern.clear ();
}

// When a string macthes, it does not return immediately, until
// it reads again.
// if unitWork is true SD_MATCH_AGAIN can be returned after
// one character is read or one unit work is performed.
unsigned int
SMatcher::find (bool unitWork)
{
  int c;
  if (pattern.checkMatch ())
  {
    return pattern.matchBegin;
  }
  if (eod)
  {
    return SD_MATCH_EOD;
  }
  while ((c = iterator.getNextCharacter ()) != -1)
  {
    // our next is current + 1
    pattern.append ((SS_UCS4)c);
    if (pattern.checkMatch ())
    {
      return pattern.matchBegin;
    }
    if (unitWork) return SD_MATCH_AGAIN;
  }
  if (!eod)
  {
    eod = true;
    c = SD_MATCH_EOD;
    pattern.append ((SS_UCS4)c);
  }
  if (pattern.checkMatch ())
  {
    return pattern.matchBegin;
  }
  pattern.action = pattern.ACT_NONE;
  pattern.matchBegin = SD_MATCH_EOD;
  return SD_MATCH_EOD;
}

void
SMatcher::applyActions (SMatcherAction& _action)
{
  if (pattern.matchBegin == SD_MATCH_EOD)
  {
    // EOD is counted that is why upper limit is next-1
    _action.applyAction (pattern.ACT_NONE, pattern.matchEnd, pattern.next-1);
    return;
  }
  _action.applyAction (pattern.ACT_NONE, start, pattern.matchBegin);
  _action.applyAction (pattern.action, pattern.matchBegin, pattern.matchEnd);
  start = pattern.matchEnd;
}
