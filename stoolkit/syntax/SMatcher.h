/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
 
#ifndef SMatcher_h
#define SMatcher_h

#include "stoolkit/syntax/SMatcherIterator.h"
#include "stoolkit/syntax/SPattern.h"

class SMatcherAction
{
public:
  SMatcherAction (void) {}  
  virtual ~SMatcherAction () {}  
  /* 
   * from is the staring character index, till is the ending character index
   */
  virtual void applyAction (const SString& name, 
    unsigned int from, unsigned int till) = 0;
};


/**
 * A regular expression matcher.
 * @author Gaspar Sinai
 * @version 2007-11-27
 */
class SMatcher 
{
public:
  SMatcher (SPattern& pattern, SMatcherIterator& it);
  // This will return SD_MATCH_EOD on end

  // This class is not yet ready, it just provides some 
  // sample implementation.
  // if unitWork is true SD_MATCH_AGAIN can be returned after
  // one character is read or one unit work is performed.
  unsigned int find (bool unitWork=false);
  /* actions will be applied in order */
  void applyActions (SMatcherAction& action);
private:
  SPattern& pattern;
  SMatcherIterator& iterator;
  unsigned int   start;
  bool           eod;
};

#endif /* SMatcher_h */
