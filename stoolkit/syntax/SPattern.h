/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
 
#ifndef SPattern_h
#define SPattern_h

#include "stoolkit/STypes.h"
#include "stoolkit/SString.h"
#include "stoolkit/syntax/SMatcherIterator.h"

/**
 * A regular expression pattern.
 * @author Gaspar Sinai
 * @version 2007-11-27
 */
class SPattern
{
public:
  SPattern (void);
  virtual ~SPattern ();
  bool isValid () const { return valid; }

  // This method updates matchBegin and matchEnd (exclusive)
  // variables in case of a match, in case of no match, this
  // will not be touched. Action will contain that action string.
  virtual bool   checkMatch ();
  unsigned int   matchBegin;
  unsigned int   matchEnd;
  SString        action;

  // Actions
  SString        ACT_NONE;
  SString        ACT_ERROR;
  SString        ACT_NUMBER;
  SString        ACT_KEYWORD;
  SString        ACT_VARIABLE;
  SString        ACT_DEFINE;
  SString        ACT_CONTROL;
  SString        ACT_OTHER;

  // Append a character and update the next to show how 
  // many characters we received so far.
  void           append (SS_UCS4 in);
  unsigned int   next;

  void           clear ();

protected:
  SV_UCS4        current;
  bool           checkMatchTest ();
  bool           singleMatchTest (unsigned int leaveSize);
  bool           valid;
};

#endif /* SPattern_h */
