// SVGOpen autogenerated by svg2cpp
#include "SVGBase.h"

class SVGOpen : public SVGBase {
public:
     SVGOpen() : SVGBase("SVGOpen") {};
    ~SVGOpen() {};
    virtual double getWidth();
    virtual double getHeight();
    virtual void renderInternal();
};

double
SVGOpen::getWidth () {
    return 20;
}

double
SVGOpen::getHeight () {
    return 20;
}

void
SVGOpen::renderInternal () {
    newpath(0);
    moveto(1.81286556890915, 16.0526321906341);
    lineto(1.8421039950269, 4.88304090970208);
    lineto(3.30409234973519, 3.88888906736659);
    lineto(8.18713310825616, 3.91814261159504);
    lineto(10.2339166687848, 6.11112510586221);
    lineto(18.3040922930423, 6.16961707620841);
    lineto(18.2456116612791, 16.0818978293511);
    closepath ();
    color("#c7ba2b");
    fill();
    color("#000000");
    stroke(1.5000000472441);
    newpath(1);
    moveto(8.70121010082552, 13.2259136134146);
    lineto(8.72778018040253, 8.25911383493272);
    lineto(5.51396880358957, 8.20599257341709);
    lineto(10.162043658647, 2.52206124478933);
    lineto(14.9429197777298, 8.23255320417491);
    lineto(11.9415915572155, 8.31223320668451);
    lineto(11.9415904233572, 13.2259121016035);
    closepath ();
    color("#0c8202");
    fill();
    color("#000000");
    stroke(1.00000001889764);
}
