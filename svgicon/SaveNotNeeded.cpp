// SVGSaveNotNeeded autogenerated by svg2cpp
#include "SVGBase.h"

class SVGSaveNotNeeded : public SVGBase {
public:
     SVGSaveNotNeeded() : SVGBase("SVGSaveNotNeeded") {};
    ~SVGSaveNotNeeded() {};
    virtual double getWidth();
    virtual double getHeight();
    virtual void renderInternal();
};

double
SVGSaveNotNeeded::getWidth () {
    return 20;
}

double
SVGSaveNotNeeded::getHeight () {
    return 20;
}

void
SVGSaveNotNeeded::renderInternal () {
    newpath(0);
    moveto(2.3001309, 9.98536);
    curveto(2.3001309,5.69992, 5.7561309,2.24392, 10.041571, 2.24392);
    curveto(14.327011,2.24392, 17.783011,5.69992, 17.783011, 9.98536);
    curveto(17.783011,14.2708, 14.327011,17.7268, 10.041571, 17.7268);
    curveto(5.7561309,17.7268, 2.3001309,14.2708, 2.3001309, 9.98536);
    closepath ();
    color("#5aa02c");
    fill();
    color("#000000");
    stroke(1.5);
}
