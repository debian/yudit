#!/usr/bin/perl
# Hand made icons 
# Gaspar Sinai <gaspar yudit org>
# 2020-05-25

# Original left middle is 1 pixel larger!
$line= "M 2,17 h 15 v -5 h -8 v -6 h 4 v -5 h -11 z";
# 1. modification proportional
$line= "M 2,19 h 16 v -6 h -8 v -6 h 4 v -6 h -12 z";
# 1. modification longer

# EmbedLeft
$line="M 2,19 h 16 v -6 h -9 v -6 h 5 v -6 h -12 z";
print "Background EmbedLeft: " . "d=\"$line\"" . $/;

$line = "M 1.25,19.75 h 17.5 v -7.5 h -9 v -4.5 h 5 v -7.5 h -13.5 z";
$line .= " M 2.75,18.25 v -4.5 h 14.5 v 4.5 z";
$line .= " M 2.75,12.25 v -4.5 h 5.5 v 4.5 z";
$line .= " M 2.75,6.25 v -4.5 h 10.5 v 4.5 z";
print "Foreground EmbedLeft: " . "d=\"$line\"" . $/;

# EmbedNone
$line="M 2,19 h 14 v -6 h 2 v -6 h -4 v -6 h -12 v 6 h 7 v 6 h -7 z";
print "Shade EmbedNone: " . "d=\"$line\"" . $/;

$line = "M 1.25,19.75 h 15.5 v -6 h 2 v -7.5  h -4 v -6 h -13.5 v 7.5 h 7 v 4.5 h -7 z";
$line .= " M 2.75,18.25 v -4.5 h 12.5 v 4.5 z";
$line .= " M 9.75,12.25 v -4.5 h 7.5 v 4.5 z";
$line .= " M 2.75,6.25 v -4.5 h 10.5 v 4.5 z";

print "Background EmbedNone" . "d=\"$line\"" . $/;

$line="M 18,19 h -16 v -6 h 9 v -6 h -5 v -6 h 12 z";
print "Background EmbedRight " . "d=\"$line\"" . $/;

$line = "M 18.75,19.75 h -17.5 v -7.5 h 9 v -4.5 h -5 v -7.5 h 13.5 z";
$line .= " M 17.25,18.25 v -4.5 h -14.5 v 4.5 z";
$line .= " M 17.25,12.25 v -4.5 h -5.5 v 4.5 z";
$line .= " M 17.25,6.25 v -4.5 h -10.5 v 4.5 z";
print "Foreground EmbedRight " . "d=\"$line\"" . $/;

$SD_MOVE_TO = "M";
$SD_LINE_TO = "L";
$SD_CLOSE_PATH = "Z";
$SD_CURVE_TO = "C";

$fb_crlf_width = 721; 
# from SFontFB.cpp
@fb_crlf = (
 [$SD_MOVE_TO,  [700, 300]],
 [$SD_LINE_TO,  [700, 0]],
 [$SD_LINE_TO,  [300, 0]],
 [$SD_LINE_TO,  [300, -100]],
 [$SD_LINE_TO,  [50, 25]],
 [$SD_LINE_TO,  [300, 150]],
 [$SD_LINE_TO,  [300, 50]],
 [$SD_LINE_TO,  [450, 50]],
 [$SD_LINE_TO,  [450, 300]],
 [$SD_LINE_TO,  [500, 300]],
 [$SD_LINE_TO,  [500, 50]],
 [$SD_LINE_TO,  [650, 50]],
 [$SD_LINE_TO,  [650, 300]],
 [$SD_CLOSE_PATH, []],
);

$line = "";
$fb_crlf_scale = 18.0 / ($fb_crlf_width + 100);
$moveY = -(100 * $fb_crlf_scale) +1;
$moveX = 1;

$line = &print_path (\@fb_crlf, $fb_crlf_scale, $moveX, $moveY);
print "LBDOS " . "d=\"$line\"" . $/;

$fb_ps_width = 721; 
# from SFontFB.cpp
@fb_ps = (
 [$SD_MOVE_TO,  [700, 700]],
 [$SD_LINE_TO,  [700, 0]],
 [$SD_LINE_TO,  [300, 0]],
 [$SD_LINE_TO,  [300, -100]],
 [$SD_LINE_TO,  [50, 25]],
 [$SD_LINE_TO,  [300, 150]],
 [$SD_LINE_TO,  [300, 50]],
 [$SD_LINE_TO,  [650, 50]],
 [$SD_LINE_TO,  [650, 700]],
 [$SD_CLOSE_PATH, []]
);

$line = "";
$fb_ps_scale = 18.0 / ($fb_ps_width + 100);
$moveY = -(100 * $fb_ps_scale) +1;
$moveX = 1;

$line = &print_path (\@fb_ps, $fb_ps_scale, $moveX, $moveY);
print "LBPS " . "d=\"$line\"" . $/;


$fb_ls_width = 721; 

@fb_ls = (
 [$SD_MOVE_TO,  [700, 300]],
 [$SD_LINE_TO,  [700, 0]],
 [$SD_LINE_TO,  [100, 0]],
 [$SD_LINE_TO,  [100, 50]],
 [$SD_LINE_TO,  [300, 50]],
 [$SD_LINE_TO,  [650, 50]],
 [$SD_LINE_TO,  [650, 300]],
 [$SD_CLOSE_PATH, []],
);

$line = "";
$fb_ls_scale = 18.0 / ($fb_ls_width + 100);
$moveY = -(100 * $fb_ls_scale) +1;
$moveX = 1;

$line = &print_path (\@fb_ls, $fb_ls_scale, $moveX, $moveY);
print "LBLS " . "d=\"$line\"" . $/;


$fb_cr_width = 721; 

@fb_cr = (
 [$SD_MOVE_TO,  [500, 300]],
 [$SD_LINE_TO,  [500, 0]],
 [$SD_LINE_TO,  [300, 0]],
 [$SD_LINE_TO,  [300, -100]],
 [$SD_LINE_TO,  [50, 25]],
 [$SD_LINE_TO,  [300, 150]],
 [$SD_LINE_TO,  [300, 50]],
 [$SD_LINE_TO,  [450, 50]],
 [$SD_LINE_TO,  [450, 300]],
 [$SD_CLOSE_PATH, []],
);

$line = "";
$fb_cr_scale = 18.0 / ($fb_cr_width + 100);
$moveY = -(100 * $fb_cr_scale) +1;
$moveX = 1;

$line = &print_path (\@fb_cr, $fb_cr_scale, $moveX, $moveY);
print "LBMAC " . "d=\"$line\"" . $/;

$fb_lf_width = 721; 

@fb_lf = (
 [$SD_MOVE_TO,  [700, 300]],
 [$SD_LINE_TO,  [700, 0]],
 [$SD_LINE_TO,  [300, 0]],
 [$SD_LINE_TO,  [300, -100]],
 [$SD_LINE_TO,  [50, 25]],
 [$SD_LINE_TO,  [300, 150]],
 [$SD_LINE_TO,  [300, 50]],
 [$SD_LINE_TO,  [650, 50]],
 [$SD_LINE_TO,  [650, 300]],
 [$SD_CLOSE_PATH, []]
);

$line = "";
$fb_lf_scale = 18.0 / ($fb_lf_width + 100);
$moveY = -(100 * $fb_lf_scale) +1;
$moveX = 1;

$line = &print_path (\@fb_lf, $fb_lf_scale, $moveX, $moveY);
print "LBUNIX " . "d=\"$line\"" . $/;

#---------------------------------------------------------------------
# From SCaret.cpp
#---------------------------------------------------------------------
$fb_size = 20; 
# SGC
$fb_caret_width = 30 * 1.732; 
$fb_caret_height = 30; 
$lw = $fb_caret_width / 10;

# Our lw distorted.
#$lw = 2.0 * $lw;

# CHANGE THIS
#$lw = $fb_caret_width / 6;
# move it to beginning
# we are positionint so that 20x20 is fine.
#$middleX = $fb_caret_width; # $fb_caret_width/2.0;
#$middleY =  $fb_caret_height/2.0;
$middleX =  20.0/2 - 14.6880 /2;
$middleY =  20.0 /2;
#$middleX = $x + $fb_caret_width/2.0; 
#$middleY = $y + $fb_caret_height/2.0;
$dx = $fb_caret_width / 3.0; 
$dy = $fb_caret_height / 3.0; 

@fb_caret = (
 [$SD_MOVE_TO,  [$middleX, $middleY - $dy +1]],
 [$SD_LINE_TO,  [$middleX + $dx -1, $middleY]],
 [$SD_LINE_TO,  [$middleX, $middleY + $dy -1]],
 [$SD_LINE_TO,  [$middleX, $middleY + $dy -1 - $lw * 0.57736 ]],
 [$SD_LINE_TO,  [$middleX + $dx -1 - $lw, $middleY]],
 [$SD_LINE_TO,  [$middleX, $middleY - $dy +1 + $lw * 0.57736]],
 [$SD_CLOSE_PATH, []]
);

$line = "";
$fb_caret_scale = 18.0 / ($fb_size);
$moveY = 1;
$moveX = 1;

$line = &print_path (\@fb_caret, $fb_caret_scale, $moveX, $moveY);
print "CaretLR " . "d=\"$line\"" . $/;

@fb_caret = (
 [$SD_MOVE_TO,  [$middleX, $middleY - $dy +1]],
 [$SD_LINE_TO,  [$middleX + $dx -1, $middleY]],
 [$SD_LINE_TO,  [$middleX, $middleY + $dy -1]],
 [$SD_CLOSE_PATH, []],

 #[$SD_MOVE_TO,  [$middleX +$lw/1.5, $middleY - $dy +1 + 1.5 *$lw]],
 #[$SD_LINE_TO,  [$middleX +$lw/1.5, $middleY + $dy -1 - 1.5 *$lw]],
 [$SD_MOVE_TO,  [$middleX +$lw/2.0, $middleY - $dy +1 + 0.866*$lw]],
 [$SD_LINE_TO,  [$middleX +$lw/2.0, $middleY + $dy -1 - 0.866*$lw]],
 [$SD_LINE_TO,  [$middleX + $dx -1 -$lw, $middleY]],
 [$SD_CLOSE_PATH, []]
);

$line = &print_path (\@fb_caret, $fb_caret_scale, $moveX, $moveY);
print "CaretLRE " . "d=\"$line\"" . $/;

@fb_caret = (
 [$SD_MOVE_TO,  [$middleX, $middleY - $dy +1]],
 [$SD_LINE_TO,  [$middleX + $dx -1, $middleY]],
 [$SD_LINE_TO,  [$middleX, $middleY + $dy -1]],
 [$SD_CLOSE_PATH, []],
);

$line = &print_path (\@fb_caret, $fb_caret_scale, $moveX, $moveY);
print "CaretLRO " . "d=\"$line\"" . $/;

#--------------------------------
$fb_size = 20; 
# SGC
# move it to beginning
#$x = -5 + 5; # $fb_caret_width/2.0;
#$x = 0; #  ($fb_caret_width - 20); # $fb_caret_width/2.0;
#$y = 0; 
#$middleX = $x + $fb_caret_width/2.0; 
#$middleY = $y + $fb_caret_height/2.0;
$middleX =  20.0/2 + 14.6880 /2;
$middleY =  20.0 /2;
$moveX = 1;
$moveY = 1;
$dx = $fb_caret_width / 3.0; 
$dy = $fb_caret_height / 3.0; 

@fb_caret = (
 [$SD_MOVE_TO,  [$middleX, $middleY - $dy +1]],
 [$SD_LINE_TO,  [$middleX - $dx +1, $middleY]],
 [$SD_LINE_TO,  [$middleX, $middleY + $dy -1]],
 [$SD_LINE_TO,  [$middleX, $middleY + $dy -1 - $lw * 0.57736]],
 [$SD_LINE_TO,  [$middleX - $dx +1 + $lw, $middleY]],
 [$SD_LINE_TO,  [$middleX, $middleY - $dy +1 + $lw * 0.57736]],
 [$SD_CLOSE_PATH, []]
);

$line = &print_path (\@fb_caret, $fb_caret_scale, $moveX, $moveY);
print "CaretRL " . "d=\"$line\"" . $/;

@fb_caret = (
 [$SD_MOVE_TO,  [$middleX, $middleY - $dy +1]],
 [$SD_LINE_TO,  [$middleX - $dx +1, $middleY]],
 [$SD_LINE_TO,  [$middleX, $middleY + $dy -1]],
 [$SD_CLOSE_PATH, []],

 [$SD_MOVE_TO,  [$middleX -$lw/2.0, $middleY - $dy +1 + 0.866 *$lw]],
 [$SD_LINE_TO,  [$middleX -$lw/2.0, $middleY + $dy -1 - 0.866 *$lw]],
 [$SD_LINE_TO,  [$middleX - $dx +1 +$lw, $middleY]],
 [$SD_CLOSE_PATH, []]
);

$line = &print_path (\@fb_caret, $fb_caret_scale, $moveX, $moveY);
print "CaretRLE " . "d=\"$line\"" . $/;

@fb_caret = (
 [$SD_MOVE_TO,  [$middleX, $middleY - $dy +1]],
 [$SD_LINE_TO,  [$middleX - $dx +1, $middleY]],
 [$SD_LINE_TO,  [$middleX, $middleY + $dy -1]],
 [$SD_CLOSE_PATH, []],
);

$line = &print_path (\@fb_caret, $fb_caret_scale, $moveX, $moveY);
print "CaretRLO " . "d=\"$line\"" . $/;

$fb_scale = 20/1400;
$moveX = 5;
$moveY = 0;

@fb_guide_f = (
  [$SD_MOVE_TO, [85, 0]],
  [$SD_LINE_TO, [85, 450]],
  [$SD_LINE_TO, [7, 450]],
  [$SD_LINE_TO, [7, 518]],
  [$SD_LINE_TO, [85, 518]],
  [$SD_LINE_TO, [85, 573]],
  [$SD_CURVE_TO, [85, 618, 88, 638, 99, 660]],
  [$SD_CURVE_TO, [100, 661, 100, 663, 102, 666]],
  [$SD_CURVE_TO, [124, 706, 166, 727, 228, 727]],
  [$SD_CURVE_TO, [236, 727, 243, 727, 251, 727]],
  [$SD_CURVE_TO, [269, 725, 288, 724, 310, 718]],
  [$SD_LINE_TO, [297, 642]],
  [$SD_CURVE_TO, [282, 646, 266, 646, 252, 646]],
  [$SD_CURVE_TO, [249, 646, 247, 646, 245, 646]],
  [$SD_CURVE_TO, [190, 646, 172, 626, 172, 565]],
  [$SD_LINE_TO, [172, 518]],
  [$SD_LINE_TO, [273, 518]],
  [$SD_LINE_TO, [273, 450]],
  [$SD_LINE_TO, [172, 450]],
  [$SD_LINE_TO, [172, 0]],
  [$SD_LINE_TO, [85, 0]],
  [$SD_CLOSE_PATH, []]
);
# From SFontFB.cpp
@fb_guide_a = (
  [$SD_MOVE_TO, [406, 64]],
  [$SD_CURVE_TO, [391, 50, 376, 39, 363, 31]],
  [$SD_CURVE_TO, [316, 0, 269, -11, 215, -11]],
  [$SD_CURVE_TO, [107, -11, 38, 45, 38, 135]],
  [$SD_CURVE_TO, [38, 207, 83, 261, 162, 283]],
  [$SD_CURVE_TO, [167, 284, 172, 285, 176, 287]],
  [$SD_CURVE_TO, [184, 288, 190, 289, 201, 292]],
  [$SD_CURVE_TO, [210, 293, 223, 295, 241, 297]],
  [$SD_CURVE_TO, [274, 301, 304, 305, 330, 312]],
  [$SD_CURVE_TO, [358, 317, 380, 324, 398, 327]],
  [$SD_CURVE_TO, [399, 335, 399, 340, 399, 343]],
  [$SD_CURVE_TO, [399, 347, 399, 349, 399, 351]],
  [$SD_CURVE_TO, [399, 425, 360, 456, 274, 456]],
  [$SD_CURVE_TO, [191, 456, 157, 431, 139, 358]],
  [$SD_LINE_TO, [53, 370]],
  [$SD_CURVE_TO, [55, 394, 60, 408, 68, 422]],
  [$SD_CURVE_TO, [70, 426, 73, 430, 76, 436]],
  [$SD_CURVE_TO, [112, 496, 182, 529, 285, 529]],
  [$SD_CURVE_TO, [387, 529, 453, 499, 475, 442]],
  [$SD_CURVE_TO, [476, 436, 478, 431, 480, 426]],
  [$SD_CURVE_TO, [485, 406, 487, 385, 487, 333]],
  [$SD_LINE_TO, [487, 217]],
  [$SD_CURVE_TO, [487, 111, 487, 85, 493, 60]],
  [$SD_CURVE_TO, [493, 57, 493, 53, 495, 51]],
  [$SD_CURVE_TO, [497, 39, 500, 32, 504, 24]],
  [$SD_CURVE_TO, [506, 16, 509, 9, 515, 0]],
  [$SD_LINE_TO, [423, 0]],
  [$SD_CURVE_TO, [420, 2, 418, 7, 417, 13]],
  [$SD_CURVE_TO, [411, 27, 408, 41, 406, 63]],
  [$SD_CLOSE_PATH, []],
  [$SD_MOVE_TO, [398, 260]],
  [$SD_CURVE_TO, [392, 257, 387, 254, 381, 253]],
  [$SD_CURVE_TO, [350, 242, 305, 232, 255, 226]],
  [$SD_CURVE_TO, [161, 214, 131, 190, 131, 138]],
  [$SD_CURVE_TO, [131, 87, 172, 56, 236, 56]],
  [$SD_CURVE_TO, [286, 56, 330, 73, 360, 104]],
  [$SD_CURVE_TO, [361, 105, 363, 107, 365, 109]],
  [$SD_CURVE_TO, [390, 138, 398, 167, 398, 226]],
  [$SD_LINE_TO, [398, 260]],
  [$SD_CLOSE_PATH, []]
);
@fb_guide_A = (
  [$SD_MOVE_TO, [-1, 0]],
  [$SD_LINE_TO, [273, 715]],
  [$SD_LINE_TO, [375, 715]],
  [$SD_LINE_TO, [668, 0]],
  [$SD_LINE_TO, [560, 0]],
  [$SD_LINE_TO, [477, 216]],
  [$SD_LINE_TO, [178, 216]],
  [$SD_LINE_TO, [99, 0]],
  [$SD_LINE_TO, [-1, 0]],
  [$SD_CLOSE_PATH, []],
  [$SD_MOVE_TO, [205, 294]],
  [$SD_LINE_TO, [447, 294]],
  [$SD_LINE_TO, [373, 492]],
  [$SD_LINE_TO, [322, 640]],
  [$SD_LINE_TO, [283, 504]],
  [$SD_LINE_TO, [205, 294]],
  [$SD_CLOSE_PATH,[]],
);


$line = &print_path (\@fb_guide_f, $fb_scale, $moveX, $moveY);
#print "LetterSmallF " . "d=\"$line\"" . $/;
$line = &print_path (\@fb_guide_a, $fb_scale, $moveX, $moveY);
#print "LetterSmallA" . "d=\"$line\"" . $/;
$line = &print_path (\@fb_guide_A, $fb_scale, $moveX, $moveY);
print "LetterCapitalA " . "d=\"$line\"" . $/;


exit 0;

sub print_path {
    my ($yuditpath, $scale, $moveX, $moveY) = @_;
    my $line = "";
    
    for(@{$yuditpath}) {
        if ($_->[0] eq "$SD_MOVE_TO") {
            $x = $_->[1]->[0] * $scale + $moveX;
            $y = 18 - $_->[1]->[1] * $scale + $moveY;
            $line .= sprintf ("M %0.4f,%0.4f ", $x, $y);
        }
        if ($_->[0] eq "$SD_LINE_TO") {
            $x = $_->[1]->[0] * $scale + $moveX;
            $y = 18 - $_->[1]->[1] * $scale + $moveY;
            $line .= sprintf ("L %0.4f,%0.4f ", $x, $y);
        }
        if ($_->[0] eq "$SD_CURVE_TO") {
            $x0 = $_->[1]->[0] * $scale + $moveX;
            $y0 = 18 - $_->[1]->[1] * $scale + $moveY;

            $x1 = $_->[1]->[2] * $scale + $moveX;
            $y1 = 18 - $_->[1]->[3] * $scale + $moveY;

            $x = $_->[1]->[4] * $scale + $moveX;
            $y = 18 - $_->[1]->[5] * $scale + $moveY;

            $line .= sprintf ("C %0.4f,%0.4f %0.4f,%0.4f %0.4f,%0.4f ", 
                $x0, $y0, $x1, $y1, $x, $y);
        }
        if ($_->[0] eq "$SD_CLOSE_PATH") {
            $line .= "Z ";
        }
    }
    return $line;
} 


