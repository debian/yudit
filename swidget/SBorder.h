/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef SBorder_h
#define SBorder_h

#include "swidget/SPanel.h"

class SBorder : public SComponent
{
public:
  enum SStyle { ETCHED, IN, OUT, SOLID, SOLID_VISIBLE};
  SBorder (SStyle style=IN);
  virtual ~SBorder ();

  void setStyle (SStyle style);
  const SDimension& getBorderSize() const;
  void changeStyle (SStyle newStyle);

  virtual void redraw (SCanvas *canvas, int x, int y, 
      unsigned int width, unsigned int height);

  virtual void resize (const SDimension& size);
  virtual void move (const SLocation& loc);
protected:
  SDimension borderSize;
  SStyle	style;
  int borderScale;
};

#endif /* SBorder_h */
