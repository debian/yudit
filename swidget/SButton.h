/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef SButton_h
#define SButton_h

#include "swidget/SOpaqueLabel.h"
#include "swidget/SPanel.h"
#include "swidget/SBorder.h"
#include "swindow/SAccelerator.h"

class SButtonListener
{
public:
  SButtonListener(void);
  virtual ~SButtonListener();
  virtual void enterComponent (void* source);
  virtual void leaveComponent (void* source);
  virtual void buttonPressedAccel (void* source, const SAccelerator* acc)=0;
};

class SButton : public SPanel, public SAcceleratorListener
{
public:
  SButton (const SString& string);
  SButton (const SString& string, SIcon* icon);
  SButton (SIcon* icon, const SString& accel);
  virtual ~SButton ();

  virtual void redraw(SCanvas* w, int x, int y,
     unsigned int width ,unsigned int height);

  virtual void enterWindow (SWindow* w);
  virtual void leaveWindow (SWindow* w);

  void addAccelerator (SWindowListener::SKey k, bool ctrl, 
          bool shift, bool meta);

  bool addAccelerator (const SString& str);

  virtual void resize (const SDimension& size);
  virtual void move (const SLocation& loc);
  virtual void buttonPressed (SWindow * w, int button, int x, int y);
  virtual void buttonReleased (SWindow * w, int button, int x, int y);
  virtual void buttonDragged (SWindow * w, int button, int x, int y);
  virtual void acceleratorPressed (const SAccelerator& a);
  virtual void acceleratorReleased (const SAccelerator& a);

  void setFont (const SString& font, double fontSize=0.0);
  void setFontSize (double fontSize);

  void setForeground (const SColor& fg);
  void setForeground (const SColor& lrfg, const SColor& rlfg);
  virtual void setBackground (const SColor& bg);
  void setButtonBackground (const SColor& bg);

  const SColor& getBackground ();
  const SColor& getForeground (bool lr);
  void clip (bool on);
  void setIcon (SIcon* icon);
  void setText (const SString& text);
  void setAlignment (SAlignment _alignment);

  void setButtonListener (SButtonListener* l);

private:
  void         applyAccelerator();
  void         processLabelText ();
  void         changed();
  bool         pressed;
  bool         inside;
  int          button;
  SOpaqueLabel label;
  SBorder      border;
  SColor       back;
  SButtonListener* listener;
protected:
  virtual void recalcSize ();
};

#endif /* SButton_h */
