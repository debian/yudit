/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef SCaret_h
#define SCaret_h

#include "swidget/SComponent.h"
#include "stoolkit/SEvent.h"
#include "stoolkit/SCharClass.h"
#include "stoolkit/SEmbedState.h"
#include "stoolkit/SCursorIndex.h"

/**
 * A caret that redraws itself differently for lr and rl text 
 */
class SCaret : public SComponent, public SEventTarget
{
public:
  SCaret (void);
  virtual ~SCaret ();

  virtual void redraw (SCanvas* canvas, int x, int y, 
       unsigned int width, unsigned int height);
  void redraw (SCanvas* canvas);

  void setDirection (SS_DR_Dir direction);
  SS_DR_Dir getDirection() const;
  bool isLR() const;

  void setGlyphIndex (unsigned int position);
  unsigned int  getGlyphIndex () const;

  virtual void setBackground (const SColor& bg);
  void setForeground (const SColor& lrfg, const SColor& rlfg);

  void redraw ();
  void on (bool ison=true);
  bool isOn () const;
  void animate (bool _animate);
  bool isAnimating() const;

  STimer*  timer;
  bool timeout (const SEventSource* s);

  virtual void resize (const SDimension& size);
  virtual void move (const SLocation& loc);
  virtual void move (const SCursorIndex& index);

  inline const SEmbedState& getEmbedState() const;
  inline void setEmbedState(const SEmbedState& ns);

  inline const SCursorIndex& getCursorIndex () const;

protected:
  bool         isSaneSize();
  SPen         lrpen;
  SPen         rlpen;

  SS_DR_Dir    direction;

  bool         showCaret;
  bool         skipBlink;
  bool         state;
  SEmbedState  embedState;
  SCursorIndex cursorIndex;
};

/**
 * return the explicit embedding state of this glyph
 */
const SEmbedState&
SCaret::getEmbedState() const
{
  return embedState;
}

void
SCaret::setEmbedState(const SEmbedState& ns)
{
  embedState = ns;
}

const SCursorIndex&
SCaret::getCursorIndex() const
{
  return cursorIndex;
}
#endif /* SCaret_h */
