/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "SComponent.h"

SWindowInterface::SWindowInterface(void)
{
}

SWindowInterface::~SWindowInterface()
{
}

SWindow*
SWindowInterface::getComponentWindow ()
{
  return 0;
}

/**
 * This is the interface for the slidable objects.
 * these object can be object that have a viewport or
 * the slider itself.
 */
SSlidable::SSlidable(void)
{
}

SSlidable::SSlidable(const SSlidable & s)
{
  value = s.value;
  step = s.step;
  page = s.page;
  max = s.max;
}

SSlidable::~SSlidable()
{
}

SSlidable
SSlidable::operator=(const SSlidable & s)
{
  value = s.value;
  step = s.step;
  page = s.page;
  max = s.max;
  return *this;
}

bool
SSlidable::operator == (const SSlidable & s) const
{
  return (value == s.value && step == s.step && page == s.page && max == s.max);
}

/**
 * this is lsitening to changes in the slider.
 */
SSliderListener::SSliderListener ()
{
}

/**
 * this is lsitening to changes in the slider.
 */
SSliderListener::~SSliderListener ()
{
}

SComponent::SComponent (void) : location (0,0), size (0,0), 
    preferredSize(10,10), 
    background (1.0,1.0,1.0,1.0)
{
  windowInterface = 0;
}
SComponent::~SComponent ()
{
}

/**
 * Set the window interface of the window.
 * The component does not have its own window. If it wants to
 * redraw itself, is should use this interface to draw.
 * Please note that components should redraw themselfves on a canvas.
 * With this interface they can draw themselves on a window.
 */
SWindowInterface*
SComponent::setWindowInterface(SWindowInterface* wi)
{
  SWindowInterface *i = windowInterface;
  windowInterface = wi; 
  return i;
}

/**
 * Return the underlying window using this interface
 */
SWindow*
SComponent::getWindow()
{
  if (windowInterface == 0)
  {
    return 0;
  }
  return windowInterface->getComponentWindow();
}

const SLocation&
SComponent::getLocation ()
{
  return location;
}

const SDimension&
SComponent::getSize ()
{
  return size;
}

const SDimension&
SComponent::getPreferredSize ()
{
  return preferredSize;
}

void
SComponent::redraw (SCanvas* canvas, int x, int y, unsigned int width, unsigned int height)
{
}

void
SComponent::setBackground (const SColor& bg)
{
  this->background = bg;
}

/**
 * This is just a palceholder. We not necessarily have 
 * foreground.
 */
void
SComponent::setForeground (const SColor& lrfg, const SColor& rlfg)
{
}

/**
 * Resize the component
 * @param d is the new size
 */
void 
SComponent::resize(const SDimension& d)
{
  size = d;
}

//void gdb_break (void) {
//    fprintf (stderr, "Moved to zerus\n");
//}

/**
 * Move the component
 * @param l is the new location
 */
void 
SComponent::move(const SLocation& l)
{
// if (l.x == 0 && l.y == 0) {
//    gdb_break();
//}


  location = l;
}

/**
 * SWindowListener
 */
void
SComponent::keyPressed (SWindow * w, SWindowListener::SKey key, const SString& s, bool ctrl, bool shift, bool meta)
{
}

void
SComponent::keyReleased (SWindow * w, SWindowListener::SKey key, const SString& s, bool ctrl, bool shift, bool meta)
{
}

void
SComponent::buttonPressed (SWindow * w, int button, int x, int y)
{
}

void
SComponent::buttonReleased (SWindow * w, int button, int x, int y)
{
}

void
SComponent::buttonDragged (SWindow * w, int button, int x, int y)
{
}
/**
 * If component has slidable, this should return true.
 * @param l is the listener that will get notified about slider changes.
 */
SSlidable*
SComponent::setSliderListener (SSliderListener* l)
{
  return 0;
}

void
SComponent::valueChanged (SSlidable* slidable, SSlideType type)
{
}
