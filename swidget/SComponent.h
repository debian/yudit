/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef SComponent_h
#define SComponent_h

#include "stoolkit/SLocation.h"
#include "stoolkit/SDimension.h"

#include <swindow/SCanvas.h>
#include <swindow/SColor.h>
#include <swindow/SWindow.h>

class SWindowInterface 
{
public:
  SWindowInterface(void);
  virtual ~SWindowInterface();
  virtual SWindow* getComponentWindow ();
};

class SSlidable
{
public:
  SSlidable(void);
  SSlidable(const SSlidable & s);
  ~SSlidable();
  SSlidable operator=(const SSlidable & s);
  bool operator == (const SSlidable & s) const;
  SLocation value;
  SDimension step;
  SDimension page;
  SDimension max;
};

class SSliderListener
{
public:
  enum SSlideType { SS_VERTICAL, SS_HORIZONTAL, SS_BOTH };
  SSliderListener ();
  virtual ~SSliderListener ();
  virtual void valueChanged (SSlidable* slidable, SSlideType type) = 0; 
};

class SComponent : public SSliderListener
{
public:
  SComponent (void);
  virtual ~SComponent ();
  const SLocation& getLocation();
  const SDimension& getSize();
  const SDimension& getPreferredSize();

  virtual SWindowInterface* setWindowInterface (SWindowInterface* wi);
  SWindow* getWindow();

  virtual void resize (const SDimension& size);
  virtual void move (const SLocation& loc);

  virtual void setBackground (const SColor& bg);
  virtual void setForeground (const SColor& lrfg, const SColor& rlfg);
  virtual void redraw (SCanvas* canvas, int x, int y, unsigned int width, unsigned int height);
  virtual void keyPressed (SWindow * w, SWindowListener::SKey key, const SString& s,
          bool ctrl, bool shift, bool meta);
  virtual void keyReleased (SWindow * w, SWindowListener::SKey key, const SString& s,
          bool ctrl, bool shift, bool meta);
  virtual void buttonPressed (SWindow * w, int button, int x, int y);
  virtual void buttonReleased (SWindow * w, int button, int x, int y);
  virtual void buttonDragged (SWindow * w, int button, int x, int y);

  virtual SSlidable* setSliderListener (SSliderListener* l);
  virtual void valueChanged (SSlidable* slidable, SSlideType type); 

protected:
  SWindowInterface* windowInterface;
  SLocation  location;
  SDimension size;
  SDimension preferredSize;
  SColor     background;
};

#endif /* SComponent_h */
