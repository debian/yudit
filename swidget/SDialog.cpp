/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "swidget/SDialog.h"
#include "swidget/SIconFactory.h"
#include "stoolkit/SUtil.h"

SDialog::SDialog (SPanel* panel)
{
  forceLayout (SLayout (SDimension (2000,3000)));
  //This is a top level guy. no layout will be set!
  // But just in case....
  setFrameListener (this);
  isCancel = true;
  type = SS_NONE;

  yesButton = new SButton(translate ("Yes"));
  yesButton->setIcon (SIconFactory::getIcon("Yes"));

  noButton = new SButton (translate ("No"));
  noButton->setIcon (SIconFactory::getIcon("Cancel"));

  yesButton->setButtonListener (this);
  noButton->setButtonListener (this);

  add (yesButton);
  add (noButton);
  add (dialogPanel = panel);
//  recalc();
  firstTimeShow = true;
}

SDialog::~SDialog ()
{
}

bool
SDialog::getInput (SType  messageType)
{
  type = messageType;
  switch (type)
  {
  case SS_ERR: 
    noButton->setText (translate ("No"));
    yesButton->setText (translate ("OK"));
    break;
  case SS_WARN:
    noButton->setText (translate ("No"));
    yesButton->setText (translate ("OK"));
    break;
  case SS_INFO:
    noButton->setText (translate ("No"));
    yesButton->setText (translate ("OK"));
    break;
  case SS_QUESTION:
    noButton->setText (translate ("No"));
    yesButton->setText (translate ("Yes"));
    break;
  case SS_OK_CANCEL:
    noButton->setText (translate ("Cancel"));
    yesButton->setText (translate ("OK"));
    break;
  default:
    break;
  }

  recalc();
  isCancel = true;
  // there is a bug in X11: does not call back when resized.
  //resized(window, getLocation().x+1, getLocation().y+1, getPreferredSize().width+1, getPreferredSize().height+1);
  show();
  if (firstTimeShow) {
    resize(preferredSize);
    firstTimeShow = false;
  }
  center ();
  wait();
  hide();
  return !isCancel;
}

/**
 * recalculate geometry
 */
void
SDialog::recalc ()
{
  SDimension dl = dialogPanel->getPreferredSize();
  SDimension db = yesButton->getPreferredSize();
  SDimension dn = noButton->getPreferredSize();


//fprintf (stderr, "dialogPanel->getPreferredSize %u,%d\n",
//    dl.width, dl.height);

  if (type == SS_QUESTION || type == SS_OK_CANCEL)
  {
    if (dn.width > db.width) db.width = dn.width;
    if (dn.width < db.width) dn.width = db.width;
  }

  //fprintf (stderr, "dl.width = %u dl.height = %u \n", dl.width, dl.height);
  //dl.width = dl.width + 20;
  //dl.height = dl.height + 20;

  unsigned int MGN = (unsigned int) (SAwt::getScale() * 5.0);
  int OFFSCR = (int) (SAwt::getScale() * 100.0);

  if (db.width + dn.width < 4*MGN)
  {
    db.width = dn.width = 2*MGN;
  }

  if (db.width + 2*MGN > dl.width)
  {
    dl.width = db.width + 2*MGN; 
  }

  preferredSize = SDimension (
           dl.width + 2*MGN,
           dl.height + db.height + 3*MGN);


  dialogPanel->setLayout (
    SLayout (
       SLocation (MGN,MGN),
       SLocation (dl.width+MGN, dl.height+MGN),
       SLocation (0,0),
       SLocation (100,100)
    )
  );

  

  if (type != SS_QUESTION && type != SS_OK_CANCEL)
  {
    noButton->setLayout(
      SLayout (
        SLocation (-OFFSCR*2, -OFFSCR*2),
        SLocation (-100, -OFFSCR)
      )
    );
    //noButton->hide();
    yesButton->setLayout (
      SLayout (
        SLocation (MGN + ((int)dl.width)/2 - (int)db.width/2-1, 
             (int)dl.height + 2*MGN),
        SLocation (MGN + ((int)dl.width)/2 + (int)db.width/2+1, 
             (int)dl.height + 2*MGN + (int) db.height),
        SLocation (50,100),
        SLocation (50,100)
      )
    );
  }
  else
  {
    //noButton->show();
    yesButton->setLayout (
      SLayout (
        SLocation (MGN, 
                 (int)dl.height + 2*MGN),
        SLocation (MGN + (int) db.width, 
                  (int)dl.height + 2*MGN + (int) db.height),
        SLocation (50,100),
        SLocation (50,100)
      )
    );
    noButton->setLayout (
      SLayout (
        SLocation (MGN + ((int)dl.width) - ((int)dn.width), 
             (int)dl.height + 2*MGN),
        SLocation (MGN + ((int)dl.width), 
             (int)dl.height + 2*MGN + (int) db.height),
        SLocation (50,100),
        SLocation (50,100)
      )
    );
  }

  /* pretend we have this layout */
  SLayout goodLayout = layout;
  forceLayout (SLayout (preferredSize));
  setLayout (goodLayout);

  //preferredSize = SDimension (preferredSize.width + 2, 
  //  preferredSize.height + 2); 
  //resize (preferredSize);
  setMinimumSize(preferredSize);
}

void
SDialog::buttonPressedAccel (void* source, const SAccelerator* acc)
{
  isCancel = false;
  if (source == noButton)
  {
     isCancel =true;
  }
  if (source == yesButton)
  {
     isCancel =false;
  }
  hide ();
}
/**
 * from frameListener
 */
bool
SDialog::close (SPanel* comp)
{
  isCancel =true;
  hide();
  return false;
}
void
SDialog::setBackground (const SColor& bg)
{
  SFrame::setBackground (bg);
}

void 
SDialog::setTitleForeground (const SColor& fg)
{
  yesButton->setForeground (fg);
  noButton->setForeground (fg);
}

void
SDialog::setFont (const SString& font, double fontSize)
{
  yesButton->setFont (font, fontSize);
  noButton->setFont (font, fontSize);
  //recalc();
}
void
SDialog::setFontSize (double fontSize)
{
  yesButton->setFontSize (fontSize);
  noButton->setFontSize (fontSize);
  //recalc();
}

/**
 * Hide the dialog if this is not a question dialog.
 */
void
SDialog::keyPressed (SWindow * w, SKey key, const SString& s,
          bool ctrl, bool shift, bool meta)
{
  if (type == SS_QUESTION || type == SS_OK_CANCEL) return;
  if (key == Key_Enter || key == Key_Return) 
  {
     hide ();
  }
}
