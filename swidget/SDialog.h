/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef SDialog_h
#define SDialog_h

#include "swidget/SFrame.h"
#include "swidget/SButton.h"
#include "swidget/SLabel.h"

class SDialog : public SFrame, public SButtonListener,
  public SFrameListener
{
public:
  enum SType { SS_ERR=0, SS_WARN=1, SS_INFO=2, SS_QUESTION, SS_OK_CANCEL, SS_NONE=3 };
  SDialog (SPanel* dialogPanel);
  virtual ~SDialog ();
  bool  getInput (SType  messageType);
  virtual void setBackground (const SColor& bg);
  virtual void setTitleForeground (const SColor& fg);
  virtual void setFont (const SString& font, double fontSize=0.0);
  virtual void setFontSize (double fontSize);
protected:
  virtual void keyPressed (SWindow * w, SKey key, const SString& s,
          bool ctrl, bool shift, bool meta);
  virtual void buttonPressedAccel (void* source, const SAccelerator* acc);
  virtual bool close (SPanel* comp);
  SType    type;
  SPanel*  dialogPanel;
  void     recalc ();
  bool     isCancel;
  SButton* yesButton;
  SButton* noButton;
  bool     firstTimeShow;
};

#endif /* SDialog_h */
