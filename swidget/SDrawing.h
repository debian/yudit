/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef SDrawing_h
#define SDrawing_h

#include "swidget/SBorder.h"
#include "swindow/SColor.h"
#include "stoolkit/SVector.h"
#include "stoolkit/SLocation.h"
#include "stoolkit/SLineCurve.h"

class SDrawingListener
{
public:
  SDrawingListener(void);
  virtual ~SDrawingListener();
  virtual void clicked (void* source, int button)=0;
  virtual void strokeChanged (void* source, unsigned int newcount)=0;
};

class SDrawing : public SPanel
{
public:
  SDrawing (void);
  virtual ~SDrawing ();
  void  setDrawingListener (SDrawingListener* listener);
  virtual void setBackground (const SColor& bg);
  virtual void setTextBackground (const SColor& bg);
  void setForeground (const SColor& fg, const SColor& fgrecent);

  virtual void redraw (SCanvas *canvas, int x, int y, 
     unsigned int width, unsigned int height);

  virtual void buttonPressed (SWindow * w, int button, int x, int y);
  virtual void buttonReleased (SWindow * w, int button, int x, int y);
  virtual void buttonDragged (SWindow * w, int button, int x, int y);

  virtual void resize (const SDimension& size);
  virtual void move (const SLocation& loc);
  void clear();

  /* TODO: add getDrawing */
  const SDimension& getPreferredSize();
  const SLineCurves& getDrawing();
  void undo ();

protected:
  SDrawingListener   *listener;
  SLineCurve	     lastLines;
  SLineCurves        allLines;
  SColor            fg;
  SColor            lfg;
  void clip (bool on);
  void redrawClear (bool isclear);
  void redrawContours (SWindow* canvas, const SColor& fg, const SLineCurve& lines);
  void redrawInternal (SCanvas* canvas, const SColor& fg, const SLineCurve& lines);

  SBorder            border;
};

#endif /* SDrawing_h */
