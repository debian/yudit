/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef SEditor_h
#define SEditor_h

#include "swidget/SUndo.h"
#include "swidget/SEditorIF.h"
#include "swindow/SWindow.h"
#include "stoolkit/STextIndex.h"
#include "stoolkit/SCursorIndex.h"
#include "stoolkit/SStringVector.h"


/**
 * This is the class that maps actions into editing actions.
 */
class SEditor 
{
public:
  SEditor(void);
  SEditor(const SString& paragBreak);
  SEditor (const SEditor& in);
  virtual ~SEditor ();
  SEditor operator=(const SEditor& in);
  virtual void setInterface(SEditorIF* editorIF);
  virtual void keyPressed (SWindowListener::SKey key, const SString& s,
            bool ctrl, bool shift, bool meta);
  virtual void keyReleased (SWindowListener::SKey key, const SString& s,
            bool ctrl, bool shift, bool meta);
  virtual void buttonPressed (int button, const SCursorIndex& index);
  virtual void buttonReleased (int button, const SCursorIndex& index);
  virtual void buttonDragged (int button, const SCursorIndex& index);
  virtual void multiClicked (int button, const SCursorIndex& index, int count);

  virtual void setInputMethods (const SStringVector& in);
  virtual void setFonts (const SStringVector& in);
  virtual void setVI (bool vimode=true);
  virtual bool undo();
  virtual bool redo();
  virtual void clearUndo();
  virtual void clear ();
  virtual void setParagraphSeparator (const SString& paragBreak);
  virtual void insertText (const SString& text);
  virtual SString getParagraphSeparator () const;
protected:
  void          pasteText (const SCursorIndex& index, bool isPrimary);
  void          add (const SUndo& undo);
  bool          vimode;
  bool          cleared;
private:
  SString       paragBreak;
  void          fKey(int num, bool ctrl);
  void          setup();
  SStringVector inputMethods;
  SStringVector fonts;
  SEditorIF*    editorIF;
  SV_Undo	    undoBuffer;
  unsigned int  undoIndex;
};

#endif /* SEditor_h */
