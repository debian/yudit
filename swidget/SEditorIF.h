/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNES FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef SEditorIF_h
#define SEditorIF_h

#include "stoolkit/SString.h"
#include "stoolkit/STextIndex.h"
#include "stoolkit/SCursorIndex.h"
#include "stoolkit/STextData.h"
#include "swindow/SWindow.h"

#define SS_DEFAULT_IM  "straight"
#define SS_KINPUT2_IM  "kinput2"
#define SS_WINDOWS_IM  "WindowsIM"
#define SS_DEFAULT_FONT  "default"

class SEditorIF 
{
public:
  SEditorIF(void);
  virtual ~SEditorIF ();

  virtual void setFont (const SString& font)=0;
  virtual void setInputMethod (const SString& im)=0;

  virtual void insertClipboardText (bool isPrimary)=0;
  virtual void insertText (const SString& str, bool embed=true)=0;
  virtual void insertDirtyText (const SString& str, bool embed=true)=0;
  virtual void insertPreEditText (const SString& utf8t, SPreEditor::SStyle style)=0;

  virtual SString eraseText (const STextIndex& till)=0;

  virtual void pageUp ()=0;
  virtual void pageDown ()=0;

  virtual const SCursorIndex& getCursorIndex () const=0;
  virtual void setCursorIndex (const SCursorIndex& index)=0;

  virtual void caretUp ()=0;
  virtual void caretDown ()=0;
  virtual void caretLeft ()=0;
  virtual void caretRight ()=0;

  virtual void startSelect ()=0;
  virtual void endSelect ()=0;
  virtual void clipSelect (bool isPrimary)=0;

  virtual void selectText (const SCursorIndex& till)=0; 
  virtual void selectWord ()=0;
  virtual void selectLine ()=0;
  virtual void deselectText ()=0;

  virtual SString eraseSelectedText ()=0;
  virtual STextIndex getSelectedIndex (bool hi)=0;
  virtual STextIndex getIndexAfterLineBreak ()=0;

  virtual SString getText (const STextIndex& from, const STextIndex& till)=0;

  virtual SString backspace ()=0;
  virtual SString erase ()=0;

  virtual bool addComposing(SS_UCS4 c) = 0;
  virtual SS_UCS4 removeComposing() = 0;

  virtual void setDirection (SS_DR_Dir dir)=0;
  virtual SS_DR_Dir getDirection () const=0;

  virtual void focusOut ()=0;
  virtual void setEditable (bool editable)=0;
  virtual bool isEditable () const =0;
  virtual bool isSelecting () const =0;
  virtual void clear () = 0;
  virtual bool clearState() = 0;

  virtual void setDocumentEmbedding (SS_Embedding e) = 0;
  virtual SS_Embedding getDocumentEmbedding () const = 0;
};

#endif /* SEditorIF_h */
