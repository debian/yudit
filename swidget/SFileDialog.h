/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef SFileDialog_h
#define SFileDialog_h

#include "swidget/SFrame.h"
#include "swidget/SBorder.h"
#include "swidget/STextList.h"
#include "swidget/STextEdit.h"
#include "swidget/SButton.h"
#include "swidget/SListBox.h"
#include "swidget/STextEdit.h"
#include "swidget/STextDialog.h"
#include "swindow/SImage.h"
#include <stoolkit/SIO.h> 

class SFileDialog : public SFrame, 
  public SListListener, public SButtonListener, public STextEditLS,
  public SFrameListener
{
public:
  SFileDialog (void);
  virtual ~SFileDialog ();
  bool  getInput(const SString& title, bool shouldExist, bool warnexist);

  void setFont (const SString& font, double fontSize=0.0);
  void setFontSize (double fontSize);

  virtual void setBackground (const SColor& bg);
  virtual void setForeground (const SColor& lrfg, const SColor& rlfg);
  virtual void setTitleForeground (const SColor& fg);
  virtual void setSliderBackground (const SColor& bg);
  virtual void setApplicationImage (const SImage& image);

  void setLabelForeground (const SColor& fg);

  void setFileName (const SString& filename);
  bool setFileType (const SString& fileType);
  void setFileTypes (const SStringVector& fileTypes);
  const SStringVector& getFileTypes() const;


  SString getFileName ();
  SString getUnixFileName ();
  SString getFileType ();

  protected:
  virtual void buttonPressedAccel (void* source, const SAccelerator* acc);
  virtual void itemSelected (void* source, const SAccelerator* acc);
  virtual void textEntered (void *source);
  virtual void textChanged (void *source);
  virtual void focusChanged (void *source, bool in);
  virtual bool close (SPanel* comp);
  virtual bool gainedKeyboardFocus (SWindow* w);

  private:
  bool     isCancel;
  bool     isFocused;
  void     fileTypeChanged (const SString& filetype);
  void     filterChanged();

  /* filetypes */
  SStringVector fileTypeVector;
  SString    fileFilter;
  
  void     rereadDir();
  void     recalc ();
  void     setHidden();

  STextDialog* textDialog;

  bool     showHidden;

  SListBox*  folderListBox;
  SListBox*  fileListBox;
  SListBox*  fileTypeListBox;

  SButton*   fileTypeButton;

  STextEdit*   filterTextEdit;
  STextEdit*   fileTextEdit;

  SLabel*    folderTitleLabel;
  SLabel*    folderNameLabel;
  SLabel*    filterTitleLabel;


  SLabel*    fileTitleLabel;
  SLabel*    fileTypeLabel;
  SString    fileTypeText;
 
  SButton*   hiddenButton;
  SButton*   yesButton;
  SButton*   cdButton;
  SButton*   createButton;
  SButton*   cancelButton;
  SButton*   homeButton;


  SDir currentDirectory;
};

#endif /* SFileDialog_h */
