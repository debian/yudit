/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "SFrame.h"

SFrame::SFrame (void)
{
  // if we dont do this there is a funny bug when
  // the window is mapped and it does not get focus. 
  // It does not happen all the time.
  setThisDoubleBuffered (false);
  frameListener = 0;
}

SFrame::~SFrame ()
{
} 

/**
 * This is SWindowListener implementation
 */
void
SFrame::redraw(SCanvas* w, int x, int y, unsigned int width ,unsigned int height)
{
  SPanel::redraw(w, x, y, width , height);
}
/**
 * This is SWindowListener implementation
 */
bool
SFrame::windowClose (SWindow* w)
{
  if (frameListener == 0)
  {
    return true;
  }
  bool ok = frameListener->close (this);
  if (!ok) return false;
  SPanel::windowClose (w);
  return true;
}

void
SFrame::setApplicationImage (const SImage& image)
{
  getComponentWindow()->setApplicationImage (image);
}

/**
 * Add a frame listener to handle frame events
 * @param l is the new frameListener
 * @return the previous frame listener
 */
SFrameListener*
SFrame::setFrameListener (SFrameListener *l)
{
  SFrameListener* o = frameListener;
  frameListener = l;
  return o;
}

/**
 * remove a frame listener to handle frame events
 * @param l is the new frameListener
 * @return the previous frame listener
 */
SFrameListener*
SFrame::removeFrameListener ()
{
  SFrameListener* o = frameListener;
  frameListener = 0;
  return o;
}

