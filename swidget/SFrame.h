/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef SFrame_h
#define SFrame_h

#include "swidget/SPanel.h"
#include "swindow/SImage.h"

class SFrameListener
{
public:
  SFrameListener(void) {};
  virtual ~SFrameListener() {};
  virtual bool close (SPanel* frame) { return true; };
};

class SFrame : public SPanel
{
public:
  SFrame (void);
  ~SFrame ();
  SFrameListener* setFrameListener (SFrameListener *l);
  SFrameListener* removeFrameListener ();
  virtual void redraw(SCanvas* w, int x, int y,
     unsigned int width ,unsigned int height);
  virtual bool windowClose (SWindow* w);
  virtual void setApplicationImage (const SImage& image);
protected:
  SFrameListener* frameListener;
};

#endif /* SFrame_h */
