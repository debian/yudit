/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef SFreeHand_h
#define SFreeHand_h

#include "swidget/SDrawing.h"

#include "swidget/SButton.h"
#include "swidget/SLabel.h"
#include "swidget/SListBox.h"
#include "stoolkit/SString.h"
#include "stoolkit/SHWConverter.h"

class SFreeHandListener
{
public:
  SFreeHandListener(void);
  virtual ~SFreeHandListener();
  virtual void freeHandTextChanged (void* source, const SString& lookup)=0;
};

class SFreeHand : public SPanel, public SListListener, 
  public SButtonListener, public SDrawingListener
{
public:
  SFreeHand (void);
  virtual ~SFreeHand ();
  void  setFreeHandListener (SFreeHandListener* listener);

  virtual void setFont (const SString& font, double fontSize);
  virtual void setFontSize (double fontSize);
  virtual void setButtonFont (const SString& font, double fontSize);

  virtual void setBackground (const SColor& bg);
  virtual void setDrawingBackground (const SColor& bg);
  virtual void setSliderBackground (const SColor& bg);

  void setForeground (const SColor&  lrfg, const SColor& rlfg);
  void setLabelForeground (const SColor& lrfg, const SColor& rlfg);
  void setDrawingForeground (const SColor& fg, const SColor& fgrecent);


  virtual void redraw (SCanvas *canvas, int x, int y, 
     unsigned int width, unsigned int height);

  virtual void buttonPressedAccel (void* source, const SAccelerator* acc);
  virtual void itemSelected (void* source, const SAccelerator* acc);
  virtual void resize (const SDimension& _size);
  virtual void clicked (void* source, int button);
  virtual void strokeChanged(void* src, unsigned int newsize);
  const SString&    getLookupText();

  void              setConverter (const SString& name);
  const SString&    getConverter () const;
  bool              isOK () const;

protected:
  void convertOne();
  void initConverters();
  bool              needConversion;
  SHWConverter      converter;
  void              recalc ();
  SString	    lookupText;
  SStringVector	    allConverters;
  SFreeHandListener   *listener;
  bool              isDirected;

  SButton*          lookupButton;
  SButton*          clearButton;
  SButton*          directedButton;
  SLabel*           strokesLabel;
  SLabel*           strokesCount;
  SLabel*           titleLabel;
  SListBox*         candidates;
  SListBox*         converters;
  SDrawing*         drawing;
};

#endif /* SFreeHand_h */
