/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef SIcon_h
#define SIcon_h

#include "swidget/SComponent.h"
#include "stoolkit/SEvent.h"

/**
 * A caret that redraws itself differently for lr and rl text 
 */
class SIcon : public SComponent
{
public:
  SIcon (const SImage& image);
  SIcon (const SString& image);
  virtual ~SIcon ();

  virtual void redraw (SCanvas* canvas, int x, int y, 
       unsigned int width, unsigned int height);
  
  virtual void setBackground (const SColor& bg);
  void redraw (SCanvas* canvas);

  static void put (const SString& name, const SImage& image);
  static void remove (const SString& name);
  static bool exists (const SString& name);

  const SImage& getImage () const;

protected:
  SColor background;
  SImage image;
};

#endif /* SIcon_h */
