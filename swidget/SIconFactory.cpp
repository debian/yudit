/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "swidget/SIconFactory.h"
#include "stoolkit/SBinHashtable.h"

#include "icons/SS_Caution.xpm"
#include "icons/SS_Inform.xpm"
#include "icons/SS_Open.xpm"
#include "icons/SS_Print.xpm"
#include "icons/SS_Redo.xpm"
#include "icons/SS_Save.xpm"
#include "icons/SS_SaveNotNeeded.xpm"
#include "icons/SS_Undo.xpm"
#include "icons/SS_MagnifyPlus.xpm"
#include "icons/SS_MagnifyMinus.xpm"
#include "icons/SS_GreenRight.xpm"
#include "icons/SS_GreenLeft.xpm"
#include "icons/SS_Right.xpm"
#include "icons/SS_Font.xpm"
#include "icons/SS_Help.xpm"
#include "icons/SS_Document.xpm"
#include "icons/SS_DocumentCX.xpm"
#include "icons/SS_DocumentXX.xpm"
#include "icons/SS_DocumentXM.xpm"
#include "icons/SS_Preview.xpm"
#include "icons/SS_Error.xpm"
#include "icons/SS_Goto.xpm"
#include "icons/SS_Find.xpm"
#include "icons/SS_Yes.xpm"
#include "icons/SS_Cancel.xpm"
#include "icons/SS_Folder.xpm"
#include "icons/SS_CreateFolder.xpm"
#include "icons/SS_CircleOff.xpm"
#include "icons/SS_CircleOn.xpm"
#include "icons/SS_Best.xpm"
#include "icons/SS_Home.xpm"

#include "icons/SS_LBDOS.xpm"
#include "icons/SS_LBLS.xpm"
#include "icons/SS_LBMAC.xpm"
#include "icons/SS_LBPS.xpm"
#include "icons/SS_LBUNIX.xpm"
#include "icons/SS_Clear.xpm"

#include "icons/SS_EmbedLeft.xpm"
#include "icons/SS_EmbedRight.xpm"
#include "icons/SS_EmbedNone.xpm"

#include "icons/SS_RLO_XPM.xpm"
#include "icons/SS_LRO_XPM.xpm"

#include "icons/SS_RLE_XPM.xpm"
#include "icons/SS_LRE_XPM.xpm"

#include "icons/SS_RL_XPM.xpm"
#include "icons/SS_LR_XPM.xpm"
#include "icons/SS_Yudit_XPM.xpm"
#include "icons/SS_SedyGreen.xpm"
#include "icons/SS_SedyRed.xpm"

#include "svgicon/Open.cpp"
#include "svgicon/CreateFolder.cpp"
#include "svgicon/Folder.cpp"
#include "svgicon/Save.cpp"
#include "svgicon/SaveNotNeeded.cpp"
#include "svgicon/Document.cpp"
#include "svgicon/Find.cpp"
#include "svgicon/Help.cpp"
#include "svgicon/Preview.cpp"
#include "svgicon/Print.cpp"
#include "svgicon/EmbedLeft.cpp"
#include "svgicon/EmbedNone.cpp"
#include "svgicon/EmbedRight.cpp"
#include "svgicon/LBDOS.cpp"
#include "svgicon/LBUNIX.cpp"
#include "svgicon/LBMAC.cpp"
#include "svgicon/LBLS.cpp"
#include "svgicon/LBPS.cpp"
#include "svgicon/CaretLR.cpp"
#include "svgicon/CaretLRE.cpp"
#include "svgicon/CaretLRO.cpp"
#include "svgicon/CaretRL.cpp"
#include "svgicon/CaretRLE.cpp"
#include "svgicon/CaretRLO.cpp"
#include "svgicon/Font.cpp"
#include "svgicon/Right.cpp"
#include "svgicon/Undo.cpp"
#include "svgicon/Redo.cpp"
#include "svgicon/MagnifyPlus.cpp"
#include "svgicon/MagnifyMinus.cpp"
#include "svgicon/Goto.cpp"
#include "svgicon/CheckStarted.cpp"
#include "svgicon/CheckFinished.cpp"
#include "svgicon/Best.cpp"
#include "svgicon/BestEmpty.cpp"
#include "svgicon/Home.cpp"
#include "svgicon/CircleOn.cpp"
#include "svgicon/CircleOff.cpp"
#include "svgicon/Yes.cpp"
#include "svgicon/Cancel.cpp"
#include "svgicon/Error.cpp"
#include "svgicon/Caution.cpp"
#include "svgicon/Inform.cpp"
#include "svgicon/Clear.cpp"
#include "svgicon/SedyGreen.cpp"
#include "svgicon/SedyRed.cpp"
#include "svgicon/GreenLeft.cpp"
#include "svgicon/GreenRight.cpp"

struct _icon_map {
  const char*  name;
  const char** icon;
  SVGBase* svg;
} icon_map[] = {
  { "Caution", SS_Caution, new SVGCaution() },
  { "Inform", SS_Inform, new SVGInform() },
  { "Open", SS_Open, new SVGOpen() },
  { "Print", SS_Print, new SVGPrint()},
  { "Redo", SS_Redo, new SVGRedo()},    
  { "Save", SS_Save, new SVGSave()},
  { "SaveNotNeeded", SS_SaveNotNeeded, new SVGSaveNotNeeded()},
  { "Undo", SS_Undo, new SVGUndo()},
  { "MagnifyPlus", SS_MagnifyPlus, new SVGMagnifyPlus()},
  { "MagnifyMinus", SS_MagnifyMinus, new SVGMagnifyMinus()},
  { "GreenRight", SS_GreenRight, new SVGGreenRight()},
  { "GreenLeft", SS_GreenLeft, new SVGGreenLeft()},
  { "Right", SS_Right, new SVGRight()},
  { "Font", SS_Font, new SVGFont()},
  { "Help", SS_Help, new SVGHelp()},
  { "Document", SS_Document, new SVGDocument()},
// Replaced by SVGCheckStarted SVGCheckFinished
  { "DocumentXM", SS_DocumentXM, 0},
  { "DocumentCX", SS_DocumentCX, 0},
  { "DocumentXX", SS_DocumentXX, 0},
// To indicate spell checking progress.
  { "CheckStarted", SS_DocumentXX, new SVGCheckStarted()},
  { "CheckFinished", SS_Document, new SVGCheckFinished()},
  { "Preview", SS_Preview, new SVGPreview()},
  { "Error", SS_Error, new SVGError()},
  { "Goto", SS_Goto, new SVGGoto},
  { "Find", SS_Find, new SVGFind()},
  { "Yes", SS_Yes, new SVGYes()},
  { "Cancel", SS_Cancel, new SVGCancel},
  { "Clear", SS_Clear, new SVGClear()},
  { "Folder", SS_Folder, new SVGFolder },
  { "CreateFolder", SS_CreateFolder, new SVGCreateFolder()},
  { "CircleOff", SS_CircleOff, new SVGCircleOff},
  { "CircleOn", SS_CircleOn, new SVGCircleOn()},
  { "Best", SS_Best, new SVGBest()},
  { "BestEmpty", SS_Best, new SVGBestEmpty()},
  { "Home", SS_Home, new SVGHome()},

  { "DOS", SS_LB_DOS_XPM, new SVGLBDOS()},
  { "LS", SS_LB_LS_XPM, new SVGLBLS()},
  { "PS", SS_LB_PS_XPM,  new SVGLBPS()},
  { "MAC", SS_LB_MAC_XPM,  new SVGLBMAC()},
  { "UNIX", SS_LB_UNIX_XPM, new SVGLBUNIX()},

  { "LRO", SS_LRO_XPM, new SVGCaretLRO()},
  { "RLO", SS_RLO_XPM, new SVGCaretRLO()},
  { "LRE", SS_LRE_XPM, new SVGCaretLRE()},
  { "RLE", SS_RLE_XPM, new SVGCaretRLE()},
  { "LR", SS_LR_XPM, new SVGCaretLR()},
  { "RL", SS_RL_XPM, new SVGCaretRL()},

  { "EmbedLeft", SS_EmbedLeft, new SVGEmbedLeft()},
  { "EmbedRight", SS_EmbedRight, new SVGEmbedRight()},
  { "EmbedNone", SS_EmbedNone, new SVGEmbedNone()},
  { "SedyGreen", SS_SedyGreen, new SVGSedyGreen()},
  { "SedyRed", SS_SedyRed, new SVGSedyRed()},

  { "Yudit", SS_Yudit_XPM, 0},
  {  0, 0, 0 }
};


SIconFactory::SIconFactory (void)
{
}

SIconFactory::~SIconFactory ()
{
}

SStringVector 
SIconFactory::getIconList() {
  SStringVector ret;
  unsigned int i=0;
  while (icon_map[i].name) {
     ret.append (SString(icon_map[i].name)); 
     i++;
  }
  return SStringVector(ret);
}

SBinHashtable<SIcon*> iconCache;

SIcon*
SIconFactory::getIcon (const SString& name)
{
  SIcon* ret = iconCache.get(name);
  if (ret) return new SIcon(*ret);
  if (SIcon::exists(name))
  {
    return new SIcon (name);
  }
  unsigned int i=0;
  while (icon_map[i].name)
  {
    if (name == SString (icon_map[i].name)) break;
    i++;
  }
  if (icon_map[i].name==0)
  {
    fprintf (stderr, "SIconFactory::getIcon does not know how to load %*.*s\n",
        SSARGS(name));
    return 0;
  }
  SImage im((const char* const*)icon_map[i].icon, icon_map[i].svg);
  ret = new SIcon (im);
  iconCache.put (name, ret);
  return new SIcon(*ret);
}
