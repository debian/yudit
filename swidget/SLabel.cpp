/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "swidget/SLabel.h"

SLabel::SLabel (const SString& string)
  : label (string), border (SBorder::SOLID)
{
  recalcSize();
}

SLabel::SLabel (const SString& string, SIcon* icon)
   : label (string, icon), border (SBorder::SOLID)
{
  recalcSize();
}

SLabel::~SLabel ()
{
}

void
SLabel::redraw(SCanvas* w, int x, int y,
     unsigned int width ,unsigned int height)
{
  label.redraw (w, x, y, width, height);
  border.redraw (w, x, y, width, height);
}

void
SLabel::resize (const SDimension& _size)
{
  SPanel::resize (_size);
  border.resize (_size);
  SDimension ds = getSize() - (border.getBorderSize() * 2);
  SLocation dl = SLocation (
     border.getBorderSize().width,
     border.getBorderSize().height);
  label.move (dl);
//fprintf (stderr, "labelsize=%u %u\n", _size.width, size.height);
  label.resize (ds);
}
/**
 * FIXME: we don't need this method
 */
void
SLabel::resized (SWindow* w, int x, int y, 
    unsigned int width, unsigned int height)
{
  SPanel::resized (w, x, y, width, height);
  //SDimension d(width, height);
  //border.resize (d);
  //label.resize (d - border.getBorderSize() * 2);
}

void
SLabel::recalcSize()
{
  SDimension db = border.getBorderSize();
  SDimension dl = label.getPreferredSize();
  preferredSize = (db *2 ) + dl;
  //preferredSize.height += 2;
//fprintf (stderr, "%u,%u\n", preferredSize.width, preferredSize.height);
  //label.move (SLocation (db.width, db.height));
}

void
SLabel::move (const SLocation& loc)
{
  SPanel::move (loc);
}

void
SLabel::setFont (const SString& font, double fontSize)
{
  label.setFont (font, fontSize);
  recalcSize();
  window->redraw (true, 0, 0, getSize().width, getSize().height);
}

void
SLabel::setFontSize (double fontSize)
{
  label.setFontSize (fontSize);
  recalcSize();
  window->redraw (true, 0, 0, getSize().width, getSize().height);
}

void
SLabel::setForeground (const SColor& fg)
{
  label.setForeground (fg);
}

void
SLabel::setForeground (const SColor& lrfg, const SColor& rlfg)
{
  label.setForeground (lrfg, rlfg);
  window->redraw (true, 0, 0, getSize().width, getSize().height);
}
void
SLabel::setBackground (const SColor& bg)
{
  SPanel::setBackground (bg);
  label.setBackground (bg);
  border.setBackground (bg);
  window->redraw (true, 0, 0, getSize().width, getSize().height);
}

void
SLabel::setLabelBackground (const SColor& bg)
{
  SPanel::setBackground (bg);
  label.setBackground (bg);
  window->redraw (true, 0, 0, getSize().width, getSize().height);
}

const SColor&
SLabel::getBackground ()
{
  return label.getBackground  ();
}

const SColor&
SLabel::getForeground (bool lr)
{
  return label.getForeground (lr);
}
void
SLabel::setText (const SString& text)
{
  label.setText (text);
  recalcSize();
  window->redraw (true, 0, 0, getSize().width, getSize().height);
}
void
SLabel::setIcon (SIcon* icon)
{
  label.setIcon (icon);
  recalcSize();
  window->redraw (true, 0, 0, getSize().width, getSize().height);
}

void
SLabel::setAlignment (SAlignment _alignment)
{
  label.setAlignment (_alignment);
  window->redraw (true, 0, 0, getSize().width, getSize().height);
} 
void
SLabel::changeStyle (SBorder::SStyle newStyle)
{
  border.changeStyle (newStyle);
} 
