/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef SLabel_h
#define SLabel_h

#include "swidget/SOpaqueLabel.h"
#include "swidget/SPanel.h"
#include "swidget/SBorder.h"

class SLabel : public SPanel
{
public:
  SLabel (const SString& string);
  SLabel (const SString& string, SIcon* icon);
  virtual ~SLabel ();

  virtual void redraw(SCanvas* w, int x, int y,
     unsigned int width ,unsigned int height);

  virtual void resize (const SDimension& size);
  virtual void resized (SWindow* w, int x, int y, 
        unsigned int width, unsigned int height);
  virtual void move (const SLocation& loc);

  void setFont (const SString& font, double fontSize=0.0);
  void setFontSize (double fontSize);

  void setForeground (const SColor& fg);
  void setForeground (const SColor& lrfg, const SColor& rlfg);
  virtual void setBackground (const SColor& bg);
  void setLabelBackground (const SColor& bg);

  const SColor& getBackground ();
  const SColor& getForeground (bool lr);
  void setText (const SString& text);
  void setIcon (SIcon* icon);
  void setAlignment (SAlignment _alignment);
  void changeStyle (SBorder::SStyle newStyle);

private:
  void         recalcSize();
  SOpaqueLabel label;
  SBorder      border;

};

#endif /* SLabel_h */
