/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef SLayout_h
#define SLayout_h

#include "stoolkit/SLocation.h"
#include "stoolkit/SDimension.h"

class SLayout
{
public:
  SLayout (void);
  SLayout (const SDimension& d);
  SLayout (const SLocation& lc, const SLocation rc,
       const  SLocation& dlc, const SLocation drc);
  SLayout (const SLocation& lc, const SLocation rc);
  SLayout (const SLayout& l);
  SLayout operator = (const SLayout& l);
  SLayout operator += (const SLayout l);
  bool operator == (const SLayout l) const;
  bool operator != (const SLayout l) const;
  ~SLayout ();
  bool isEmpty() const;

  SDimension getDimension () const;
  SLocation getLocation () const;

  void setLayout (const SLayout& old, const SLayout& nl);
  SLocation getLocation (const SLayout& parent, const SDimension& d) const;
  SDimension getDimension (const SLayout& parent, const SDimension& d) const;
  void print();

private:
  SLocation lc;
  SLocation rc; 
  SLocation dlc;
  SLocation drc; 
};

#endif /* SLayout_h */
