/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "swidget/SListBox.h"
#include "swindow/SAwt.h"

static int getSliderWidth () {
//    return 20 * SAwt::getRasterScale();
    return (int) (SAwt::getScale() * 20.0);
}

SListBox::SListBox (const SString& title) : border(SBorder::ETCHED) 
{
 SPanel::forceLayout (SLayout (SDimension (1000,1000)));
 topLabel = new SLabel (title);
// topLabel->changeStyle (SBorder::SOLID_VISIBLE);
 topLabel->setAlignment (SD_Center);
 textList = new STextList();
 slider = new SSlider();
 add (topLabel);
 add (textList);
 add (slider);
 recalc ();
}

void 
SListBox::resize(const SDimension& d)
{
// fprintf (stderr, "XXX ListBox Panel %u,%u\n", d.width, d.height);
  border.resize(d);
  SPanel::resize (d);
}

void
SListBox::redraw (SCanvas *canvas, int x, int y, 
  unsigned int width, unsigned int height)
{
  border.redraw(canvas, x, y, width, height);
}

SListBox::~SListBox ()
{
}

void
SListBox::setText (const SStringVector& v)
{
  textList->setText (v);
}
void
SListBox::setListListener (SListListener* l)
{
  textList->setListListener (l);
}

/**
 * recalculate the layout
 */
void
SListBox::recalc()
{
  const SDimension& bd = border.getBorderSize();

  int lh = (int) topLabel->getPreferredSize().height;
  if (lh < 4) lh = 4;
  int dheight = lh * 2;
  int dwidth = (int) topLabel->getPreferredSize().width + 2 * (int) bd.width;

  if (dheight  < 20)dheight = 20 ;
  if (dwidth < 30) dwidth = 30;
  preferredSize = SDimension (dwidth, dheight);

  topLabel->setLayout (
    SLayout (
      SLocation (bd.width,bd.height),
      SLocation (dwidth-(int)bd.width, lh+(int)bd.height),
      SLocation (0, 0),
      SLocation (100, 0)
    )
  );

  int tw = (int)dwidth - getSliderWidth() - (int) bd.width;

  textList->setLayout (
    SLayout (
      SLocation (bd.width, lh+bd.height),
      SLocation (tw, (int)dheight - (int)bd.height),
      SLocation (0, 0),
      SLocation (100, 100)
    )
  );  
  slider->setLayout (
    SLayout (
      SLocation (tw, lh+(int)bd.height),
      SLocation (tw+getSliderWidth(), (int)dheight - (int)bd.height),
      SLocation (100, 0),
      SLocation (100, 100)
    )
  );

  /* save current */
  SLayout goodlayout = layout;

  /* pretend we have this layout */
  forceLayout (preferredSize);

  /* accept old layout */
  setLayout (goodlayout);
}

void
SListBox::setBackground (const SColor& bg)
{
  SPanel::setBackground (bg);
  border.setBackground (bg);
}

bool
SListBox::selectText (const SString& s)
{
  return textList->selectText (s);
}

bool
SListBox::selectItem (int item)
{
  return textList->selectItem (item);
}

void
SListBox::setLabelForeground (const SColor& fg)
{
  topLabel->setForeground (fg);
}

void
SListBox::setSliderBackground (const SColor& bg)
{
  slider->setSliderBackground (bg);
}

void
SListBox::setFont (const SString& font, double fontSize)
{
  topLabel->setFont (font, fontSize);
  textList->setFont (font, fontSize);
  recalc();
}
void
SListBox::setFontSize (double fontSize)
{
  topLabel->setFontSize (fontSize);
  textList->setFontSize (fontSize);
  recalc();
}
