/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "swidget/SOpaqueLabel.h"
#include "swindow/SAwt.h"

#define SS_ORIG_MGN 2

SOpaqueLabel::SOpaqueLabel (const SString& string) : textView (string)
{
  alignment = SD_Left;
  textView.setMultiline (false);
  textView.setLineEndMark (false);
  textView.textData.fireEvent();
  processLabelText();
  icon = 0;
  recalcSize();
}

SOpaqueLabel::SOpaqueLabel (const SString& string, SIcon* _icon) :
   textView (string)
{
  alignment = SD_Left;
  textView.setMultiline (false);
  textView.setLineEndMark (true);
  textView.textData.fireEvent();
  processLabelText();
  icon = _icon;
  recalcSize();
}

SOpaqueLabel::~SOpaqueLabel ()
{
  if (icon) delete icon;
}

void
SOpaqueLabel::redraw(SCanvas* w, int x, int y,
     unsigned int width ,unsigned int height)
{
//fprintf (stderr, "Icon %d,%d, %ux%u\n", x, y, width, height);
  if (icon) {
        icon->redraw (w, x, y, width, height);
  }
  textView.redraw (w, x, y, width, height);
}

void
SOpaqueLabel::resize (const SDimension& size)
{
  SComponent::resize(size);
  recalcSize();
}

void
SOpaqueLabel::move (const SLocation& loc)
{
  SComponent::move (loc);
  recalcSize ();
}

void
SOpaqueLabel::recalcSize()
{
  SDimension di;
  if (icon)
  {
     di = icon->getPreferredSize();
  }
  SDimension dt = textView.getPreferredSize();
  SString s = textView.textData.getText();
  unsigned int mp = (s.size()==0) ? 2: SAwt::getRasterScale() +2;
  //mp = mp * SAwt::getRasterScale();
  int MGN = SS_ORIG_MGN * SAwt::getRasterScale();
  SDimension w = SDimension (di.width + dt.width + mp * MGN , 
        (di.height > dt.height) ? di.height : dt.height
  );
  preferredSize = w;
  SDimension _size = getSize();

  /* centering */
  int dw = ((int)_size.width-(int)w.width - getLocation().x * 2);
  int dh = ((int)_size.height-(int)w.height - getLocation().y * 2);


  SLocation nl = getLocation ();
  if (alignment == SD_Center)
  {
//fprintf (stderr,"center: dw=%d nl.x=%d size.width=%u w.width=%u new nl.x=%d\n", 
 //   dw, nl.x, _size.width, w.width, nl.x + dw/2);
     nl.x = nl.x + dw/2 + getLocation ().x;
     nl.y = nl.y + dh/2 + getLocation ().y;
  }
  else if (alignment == SD_Right)
  {
     nl.x = nl.x + dw;
     nl.y = nl.y + dh;
  }
  //SDimension dd ((dw<=0)?2:dw, (dh<=0)?2:dh);
  //SDimension nd (_size.width-dd/2); 

  int h = w.height;
  if (icon)
  {
     icon->move (SLocation (nl.x + MGN, nl.y + (h-(int)di.height)/2));
  }
  textView.move (SLocation (nl.x + (int)di.width +2*MGN, 
       nl.y + (h-(int)dt.height)));

  if (icon) icon->resize (di);
  textView.resize (dt);
}

void
SOpaqueLabel::setFontSize (double fontSize)
{
  textView.setFontSize (fontSize);
  recalcSize();
}

void
SOpaqueLabel::setFont (const SString& font, double fontSize)
{
  textView.setFont(font, fontSize);
  recalcSize();
}

void
SOpaqueLabel::setForeground (const SColor& fg)
{
  textView.setForeground (fg, fg);
}

void
SOpaqueLabel::setForeground (const SColor& lrfg, const SColor& rlfg)
{
  textView.setForeground (lrfg, rlfg);
}

void
SOpaqueLabel::setBackground (const SColor& bg)
{
  textView.setBackground (bg);
  if (icon) icon->setBackground (bg);
}


const SDimension&
SOpaqueLabel::getPreferredSize ()
{
  recalcSize();
  return SComponent::getPreferredSize();
}

const SColor&
SOpaqueLabel::getBackground ()
{
  return textView.getBackground();
}

const SColor&
SOpaqueLabel::getForeground (bool lr)
{
  return textView.getForeground(lr);
}

void
SOpaqueLabel::setIcon (SIcon* _icon)
{
  if (icon) delete icon;
  icon = _icon;
// SGC
  if (icon) icon->setBackground (getBackground());
  recalcSize();
}

void
SOpaqueLabel::setText (const SString& text)
{
  textView.setText (text);
  processLabelText();
  recalcSize();
}

void
SOpaqueLabel::processLabelText()
{
  /* underline __keys__ */
  textView.textData.move(STextIndex(0,0));
  STextIndex st = textView.textData.find ("__");
  if (st == STextIndex(0,0)) return;
  textView.textData.move(st);
  STextIndex et = textView.textData.find ("__");
  if (et == STextIndex(0,0))
  {
    return;
  }
  textView.textData.underline (st);

  textView.textData.move(STextIndex(0,0));
  st = textView.textData.find ("__");
  textView.textData.remove (st);
  textView.textData.fireEvent ();
  st = textView.textData.find ("__");
  textView.textData.remove (st);
  textView.textData.fireEvent ();
}

void
SOpaqueLabel::setAlignment (SAlignment _alignment)
{
  alignment = _alignment;
  recalcSize();
}
