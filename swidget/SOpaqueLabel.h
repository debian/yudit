/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef SOpaqueLabel_h
#define SOpaqueLabel_h

#include "swidget/SComponent.h"
#include "swidget/STextView.h"
#include "swidget/SIcon.h"
#include "stoolkit/STypes.h"

class SOpaqueLabel : public SComponent
{
public:
  SOpaqueLabel (const SString& string);
  SOpaqueLabel (const SString& string, SIcon* icon);
  virtual ~SOpaqueLabel ();

  virtual void redraw(SCanvas* w, int x, int y,
     unsigned int width ,unsigned int height);

  virtual void resize (const SDimension& size);
  virtual void move (const SLocation& loc);

  void setFont (const SString& font, double fontSize=0.0);
  void setFontSize (double fontSize);

  void setForeground (const SColor& fg);
  void setForeground (const SColor& lrfg, const SColor& rlfg);
  virtual void setBackground (const SColor& bg);
  virtual const SDimension& getPreferredSize ();

  const SColor& getBackground ();
  const SColor& getForeground (bool lr);
  void setIcon (SIcon* icon);
  void setText (const SString& text);
  void setAlignment (SAlignment alignment);

  STextView textView;

private:
  SAlignment     alignment;
  void           processLabelText ();
  virtual void   recalcSize ();
  SIcon*         icon;

};

#endif /* SOpaqueLabel_h */
