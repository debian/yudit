/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef SPanel_h
#define SPanel_h

#include "swidget/SComponent.h"
#include "swidget/SLayout.h"

#include <swindow/SWindow.h>
#include <swindow/SAwt.h>
#include "stoolkit/SEvent.h"

class SDropListener
{
public:
  SDropListener(void);
  virtual ~SDropListener();
  virtual bool dropped (void* p, const SString& mimetype, const SString& data)=0;
};
// public SWindowListener
class SPanel : public SComponent, public SWindowInterface, 
  public SEventTarget, public SWindowListener
{
public:
  SPanel (void);
  virtual ~SPanel ();
  static void setAllDoubleBuffered (bool yes);
  void setThisDoubleBuffered (bool yes);
  bool    isShown();
  virtual void show ();
  virtual void hide ();
  void add (SComponent* comp);
  void add (SPanel* comp);
  virtual SWindow* getComponentWindow ();
  virtual void resize (const SDimension& size);
  virtual void move (const SLocation& loc);
  virtual void setBackground (const SColor& bg);

  void setTitle (const SString& title);
  virtual void setDropListener (SDropListener*ls, const SStringVector& mimes);
  virtual void setModal (SPanel* parent=0, bool decorated=true);
  virtual void wait ();
  void setMinimumSize (const SDimension& minimumSize);
  void center ();

  /* 
   * setLayout is purposefully not virtual. If your widget 
   * is properly designed, you don't need to override this.
   */
  void setLayout (const SLayout& layout);
  const SLayout& getLayout () const;

protected:
  virtual void forceLayout (const SLayout& layout);
  SLayout layout;

  SPanel* modalParent;
  STimer*  resizeTimer;

  virtual void redraw(SCanvas* w, int x, int y,
     unsigned int width ,unsigned int height);
  virtual bool windowClose (SWindow* w);
  virtual void keyPressed (SWindow * w, SWindowListener::SKey key, const SString& s,
          bool ctrl, bool shift, bool meta);
  virtual void keyReleased (SWindow * w, SWindowListener::SKey key, const SString& s,
          bool ctrl, bool shift, bool meta);
  virtual void buttonPressed (SWindow * w, int button, int x, int y);
  virtual void buttonReleased (SWindow * w, int button, int x, int y);
  virtual void buttonDragged (SWindow * w, int button, int x, int y);
  virtual void lostKeyboardFocus (SWindow* w);
  virtual bool gainedKeyboardFocus (SWindow* w);
  virtual void lostClipSelection (SWindow* w);
  virtual void resized (SWindow* w, int x, int y, 
        unsigned int width, unsigned int height);

  virtual void valueChanged (SSlidable* slidable, SSlideType type);
  virtual bool drop (SWindow* w, const SString& mimetype, const SString& data);
  // Override.
  virtual void enterWindow (SWindow* w);
  virtual void leaveWindow (SWindow* w);

  SDropListener* dropListener;
  SWindow*       window;

  SBinVector<SComponent*>  slidableComponents;
  SBinVector<SSlidable*>   slidables;

  SBinVector<SComponent*>  children;
  SBinVector<SPanel*>      containers;

  void cleanup();

  static SAwt*  awt;
  static int    count;

  void _resized (); 
  /* for resized */
  bool timeout (const SEventSource* s);
  bool isLayoutOK() const;

  bool hasParent;
  bool layoutOK;
};

#endif /* SPanel_h */
