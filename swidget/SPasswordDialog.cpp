/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "swidget/SPasswordDialog.h"
#include "swidget/SIcon.h"
#include "swidget/SIconFactory.h"
#include "stoolkit/SUtil.h"
#include "stoolkit/SEncoder.h"


SPasswordDialog::SPasswordDialog (void)
{
  setFrameListener (this);
  isCancel = true;

  yesButton = new SButton (translate ("OK"), SIconFactory::getIcon("Yes"));
  yesButton->setAlignment (SD_Center);
  yesButton->setButtonListener (this);

  cancelButton = new SButton (translate ("Cancel"), SIconFactory::getIcon("Cancel"));
  cancelButton->setAlignment (SD_Center);
  cancelButton->setButtonListener (this);
  add (yesButton);
  add (cancelButton);

  passwordLabel = new SLabel (translate ("Password:"));
  add (passwordLabel);
  fileNameLabel = new SLabel ("", SIconFactory::getIcon("SedyRed")); 
  add (fileNameLabel);

  passwordTextEdit = new STextEdit("");
  passwordTextEdit->setMultiline(false);
  passwordTextEdit->setLineEndMark(false);
  passwordTextEdit->addTextEditLS(this);
  passwordTextEdit->setHideText(true);

  add (passwordTextEdit);

  isFocused = false;
  recalc();
}

void
SPasswordDialog::setApplicationImage (const SImage& image)
{
  getComponentWindow()->setApplicationImage (image);
}

SPasswordDialog::~SPasswordDialog ()
{
}

void
SPasswordDialog::setFileName (const SString& fileName)
{
    fileNameLabel->setText(fileName);
    recalc();
}
/**
 * recalculate the geometry
 * it is based upon 300x200 layout.
 * preferredSize will be calculated here.
 */
void
SPasswordDialog::recalc ()
{
  unsigned int bw = 1;
  if (yesButton->getPreferredSize().width > bw )
  {
     bw = yesButton->getPreferredSize().width;
  }
  if (cancelButton->getPreferredSize().width > bw )
  {
     bw = cancelButton->getPreferredSize().width;
  }
  unsigned int bh= cancelButton->getPreferredSize().height;

  unsigned int MGN = (unsigned int) (SAwt::getScale() * 5.0);
  unsigned int WD = (unsigned int) (SAwt::getScale() * 300);
  unsigned int HT = (unsigned int) (SAwt::getScale() * 200);

  int prefWidth = MGN + bw + MGN + bw + MGN;
  int prefHeight = MGN + bh + MGN;

  int gap = (WD-((int)bw * 4 + 2*MGN))/3;
  int currentx= MGN;

  yesButton->setLayout (
    SLayout (
      SLocation (currentx, HT-MGN-bh),
      SLocation (currentx+(int)bw, HT-MGN),
      SLocation (0, 100),
      SLocation (0, 100)
    )
  );
  currentx = currentx + (int) bw + gap;
  cancelButton->setLayout (
    SLayout (
      SLocation (WD-MGN-(int)bw, HT-MGN-bh),
      SLocation (WD-MGN, HT-MGN),
      SLocation (100, 100),
      SLocation (100, 100)
    )
  );
  SDimension fnd = fileNameLabel->getPreferredSize();
  int fndY = fnd.height + MGN;
  SDimension pd = passwordLabel->getPreferredSize();
  SDimension pte = passwordTextEdit->getPreferredSize();

  int pheight = pd.height > pte.height ? pd.height : pte.height;
  int pwidth = pd.width * 2;
  fileNameLabel->setLayout (
    SLayout (
      SLocation (MGN, MGN),
      SLocation (WD-MGN, MGN + fnd.height),
      SLocation (0,0),
      SLocation (100,0)
    )
  );

  passwordLabel->setLayout (
    SLayout (
      SLocation (MGN, MGN+fndY),
      SLocation (MGN+pd.width, MGN + pheight + fndY),
      SLocation (0,0),
      SLocation (0, 0)
    )
  );

  passwordTextEdit->setLayout (
    SLayout (
      SLocation (MGN+pd.width+MGN, MGN+fndY),
      SLocation (WD-MGN, MGN + pheight+fndY),
      SLocation (0, 0),
      SLocation (100, 0)
    )
  );
  pwidth = MGN+pwidth+MGN+pwidth+MGN;
  if (pwidth > prefWidth)
  {
    prefWidth = pwidth;
  }
  prefHeight = MGN + pheight + prefHeight + MGN + fndY;

  preferredSize = SDimension (prefWidth,  prefHeight);

  /* This was out layout... */
  forceLayout (SLayout (SDimension (WD,HT)));

  /* But this one is the one that works */
  setLayout (SLayout (preferredSize));

  setMinimumSize (preferredSize);

  resize (SDimension (preferredSize.width, preferredSize.height));

}

SString
SPasswordDialog::getPassword ()
{
  return passwordTextEdit->getText();
    
}

bool
SPasswordDialog::getInput (const SString& titleString) 
{
  setTitle (titleString);
  SString saved = passwordTextEdit->getText();
  isCancel = true;
  isFocused = false;
  passwordTextEdit->clear();
  center();
  show();
  passwordTextEdit->setFocus();
  wait();
  hide();
  if (isCancel) {
     passwordTextEdit->setText(saved);
  }
  return !isCancel;
}

/**
 * buttons call this
 */
void
SPasswordDialog::buttonPressedAccel (void* source, const SAccelerator* acc)
{
  if (source == cancelButton)
  {
    isCancel = true;
    hide();
    return;
  }
  if (source == yesButton)
  {
    isCancel = false;
    hide();
    return;
  }
}


/**
 * STextEditLS
 */
void
SPasswordDialog::textEntered (void *source)
{
  if (source == passwordTextEdit)
  {
    isCancel = false;
    hide();
  }
}

bool
SPasswordDialog::close (SPanel* comp)
{
  isCancel =true;
  hide();
  return false;
}

void
SPasswordDialog::setTitleForeground (const SColor& fg)
{
  yesButton->setForeground (fg);
  cancelButton->setForeground (fg);
}

void 
SPasswordDialog::setLabelForeground (const SColor& fg)
{
  passwordLabel->setForeground (fg);
  fileNameLabel->setForeground (fg);
}

void
SPasswordDialog::setBackground (const SColor& bg)
{
  SFrame::setBackground (bg);
  passwordTextEdit->setTextBackground (SColor("white"));
}

void
SPasswordDialog::setForeground (const SColor& lrfg, const SColor& rlfg)
{
  SFrame::setForeground (lrfg, rlfg);
  setLabelForeground (lrfg);
  setTitleForeground (lrfg);
}

void
SPasswordDialog::setFont (const SString& font, double fontSize)
{
  passwordLabel->setFont (font, fontSize);
  fileNameLabel->setFont (font, fontSize);
  passwordTextEdit->setFont (font, fontSize);
  yesButton->setFont (font, fontSize);
  cancelButton->setFont (font, fontSize);
  recalc();
}

void
SPasswordDialog::setFontSize (double fontSize)
{
  passwordLabel->setFontSize (fontSize);
  fileNameLabel->setFontSize (fontSize);
  passwordTextEdit->setFontSize (fontSize);
  yesButton->setFontSize (fontSize);
  cancelButton->setFontSize (fontSize);
  recalc();
}
bool 
SPasswordDialog::gainedKeyboardFocus (SWindow* w)
{
  if (isFocused) return true;
  isFocused = true;
  passwordTextEdit->setFocus();
  return true;
}
