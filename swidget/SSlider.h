/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef SSlider_h
#define SSlider_h

#include "swidget/SPanel.h"
#include "swidget/SFrame.h"
#include "swidget/SBorder.h"

class SSliderComponent : public SComponent
{
public:
  SSliderComponent (void);
  virtual ~SSliderComponent();
  virtual void redraw(SCanvas* w, int x, int y,
      unsigned int width ,unsigned int height);
  virtual void redrawFull(SWindow* w);
  virtual void setBackground (const SColor& bg);
private:
  SColor  lighter;
  SColor  darker;
};

class SSlider : public SPanel
{
public:
  SSlider (void);
  ~SSlider ();

  /**
   * These methods are not the preferred way to interact with a slider.
   * the preferred way is the automatic interaction.
   */
  void set (int value, unsigned int step, unsigned int page, 
       unsigned int max, bool vertical=false);

  void slide (int value, bool vertical=false);

  void set (const SLocation& value, const SDimension& step, 
       const SDimension &page, const SDimension& max);

  void slide (const SLocation& value);

  SLocation getValue() const;

  virtual void redraw(SCanvas* w, int x, int y,
     unsigned int width ,unsigned int height);
  virtual void resize (const SDimension& d);
  virtual void buttonPressed (SWindow * w, int button, int x, int y);
  virtual void buttonReleased (SWindow * w, int button, int x, int y);
  virtual void buttonDragged (SWindow * w, int button, int x, int y);
  virtual void setBackground (const SColor& bg);
  virtual void setSliderBackground (const SColor& bg);

  /* This is from SSliderListener */
  virtual void valueChanged (SSlidable* slidable, SSlideType type);

  /* This is from component */
  virtual SSlidable* setSliderListener (SSliderListener* l);

private:
  SColor     sliderBackground;
  void       recalculateGeometry();
  void       setSliderGeometry (SLocation l, SDimension d);
  void       clip (bool on);
  void       slide(bool notify);
  void       page (int count);

  SSlidable        slidable;
  SSliderListener* listener;

  SLocation        dragPoint;
  bool             dragOK;

  /* subcomponents */
  SBorder          border;
  SSliderComponent slider;
};

#endif /* SSlider_h */
