/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "swidget/STextDialog.h"
#include "swidget/SIconFactory.h"
#include "stoolkit/SUtil.h"

STextDialog::STextDialog (void) : SDialog (new SLabel("LA\nbel"))
{
  label = (SLabel*) dialogPanel;
  label->setAlignment (SD_Center);
}

STextDialog::~STextDialog ()
{
}

/**
 * @return true if ok or yes was presses.
 */
bool
STextDialog::getInput(const SString& titleString, 
  const SString& message, SType  messageType)
{
  type = messageType;
  setTitle (titleString);
  switch (type)
  {
  case SS_ERR: 
    label->setIcon (SIconFactory::getIcon("Error"));
    break;
  case SS_WARN:
    label->setIcon (SIconFactory::getIcon("Caution"));
    break;
  case SS_INFO:
    label->setIcon (SIconFactory::getIcon("Inform"));
    break;
  case SS_QUESTION:
    label->setIcon (SIconFactory::getIcon("Help"));
    break;
  default:
    label->setIcon (0);
  }
  label->setText (message);
  firstTimeShow = true;
  //recalc();
  bool input = SDialog::getInput (messageType);
  return input;
}

void
STextDialog::setBackground (const SColor& bg)
{
  SDialog::setBackground (bg);
  //label->setBackground (SColor("black"));
  //label->setLabelBackground (bg);
  label->setBackground (bg);
}

void
STextDialog::setLabelBackground (const SColor& bg)
{
  label->setLabelBackground (bg);
}

void
STextDialog::setLabelForeground (const SColor& fg)
{
  label->setForeground (fg);
}
void
STextDialog::setFont (const SString& font, double fontSize)
{
  label->setFont (font, fontSize);
  SDialog::setFont (font, fontSize);
}
void
STextDialog::setFontSize (double fontSize)
{
  label->setFontSize (fontSize);
  SDialog::setFontSize (fontSize);
}
