/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef STextDialog_h
#define STextDialog_h

#include "swidget/SDialog.h"
#include "swidget/SLabel.h"

class STextDialog : public SDialog
{
public:
  STextDialog (void);
  virtual ~STextDialog ();
  bool  getInput (const SString& title, const SString& message, 
      SType  messageType);
  virtual void setBackground (const SColor& bg);
  virtual void setLabelBackground (const SColor& bg);
  virtual void setLabelForeground (const SColor& fg);
  void setFont (const SString& font, double fontSize=0.0);
  void setFontSize (double fontSize);
protected:
  SLabel* label;
};

#endif /* STextDialog_h */
