/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "swidget/STextEditLS.h"

/**
 * A generic text editor listener.
 */
STextEditLS::STextEditLS(void)
{
}

STextEditLS::~STextEditLS()
{
}

void
STextEditLS::textChanged (void *source)
{
}
void
STextEditLS::textEntered (void *source)
{
}
void
STextEditLS::focusOutRequest (void *source)
{
}

void
STextEditLS::scrolledHorizontal (void *source, int value)
{
}

void
STextEditLS::scrolledVertical (void *source, int value)
{
}

void
STextEditLS::inputMethodChanged (void *source, const SString& im)
{
}

void
STextEditLS::fontChanged (void *source, const SString& font)
{
}
void
STextEditLS::focusChanged (void *source, bool in)
{
}

void
STextEditLS::caretMoved (void *source, unsigned int line, unsigned int col, bool before)
{
}
