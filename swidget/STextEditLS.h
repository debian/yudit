/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef STextEditLS_h
#define STextEditLS_h

#include "stoolkit/SString.h"

/**
 * A generic text editor listener.
 */
class STextEditLS 
{
public:
  STextEditLS(void);
  virtual ~STextEditLS();
  virtual void inputMethodChanged (void *source, const SString& im);
  virtual void fontChanged (void *source, const SString& font);
  virtual void textChanged (void *source);
  virtual void textEntered (void *source);
  virtual void focusOutRequest (void *source);
  virtual void focusChanged (void *source, bool in);
  virtual void caretMoved (void *source, unsigned int line, unsigned int col, 
      bool before);
  virtual void scrolledHorizontal (void *source, int value);
  virtual void scrolledVertical (void *source, int value);
};

#endif /* STextEditLS_h */
