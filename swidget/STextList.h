/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef STextList_h
#define STextList_h

#include "swidget/SOpaqueLabel.h"

#include "swidget/SComponent.h"
#include "swidget/SBorder.h"

#include "stoolkit/SStringVector.h"
#include "stoolkit/STypes.h"

class SListListener
{
public:
  SListListener(void);
  virtual ~SListListener();
  virtual void itemSelected (void* source, const SAccelerator* acc)=0;
  virtual void itemHighlighted (void* source, int item);
};
/**
 * Show the components in a list-manner.
 */
class STextList : public SPanel
{
public:
  STextList (void);
  ~STextList ();

  void setText (const SStringVector& v);
  void clear ();

  virtual void setFont (const SString& font, double fontSize);
  virtual void setFontSize (double size);
  virtual void setBackground (const SColor& bg);
  virtual void setTextBackground (const SColor& bg);
  virtual void setSelectedBackground (const SColor& bg);
  virtual void setForeground (const SColor& lrfg, const SColor& rlfg);
  virtual void setSelectedForeground (const SColor& fg);

  virtual void redraw (SCanvas *canvas, int x, int y, 
     unsigned int width, unsigned int height);

  virtual void keyPressed (SWindow * w, SKey key, const SString& s,
          bool ctrl, bool shift, bool meta);
  virtual void keyReleased (SWindow * w, SKey key, const SString& s,
          bool ctrl, bool shift, bool meta);
  virtual void buttonPressed (SWindow * w, int button, int x, int y);
  virtual void buttonReleased (SWindow * w, int button, int x, int y);
  virtual void buttonDragged (SWindow * w, int button, int x, int y);

  virtual void resize (const SDimension& size);
  virtual void move (const SLocation& loc);

  /* This is from component */
  virtual SSlidable* setSliderListener (SSliderListener* l);

  /* This is from SSliderListener */
  virtual void valueChanged (SSlidable* slidable, SSlideType type);
  void setListListener (SListListener* l);
  bool selectItem (int line);
  bool selectText (const SString& text);
  int getLastSelected ();
  SString getLastSelectedText ();
  
private:
  SListListener*   listListener;
  void             makeSelectedVisible();
  void             slideDown();
  void             slideUp();
  void             recalc (int current);
  void             selectLocation (int y);
  int              indexForLocation (int y, bool correct=true);
  int              locationForIndex (unsigned int index);

  void             redraw (SCanvas* canvas, unsigned int index, int x, unsigned int width);
  SOpaqueLabel     label;
  SOpaqueLabel     selectedLabel;
  int              lastSelected;
  SDimension       viewableSize;
  void             clip (bool on);
  SSliderListener* sliderListener;
  SSlidable        slidable;
  SBorder          border;
  SStringVector    vector;
  SBinVector<bool> selectedVector;
};

#endif /* STextList_h */
