/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "swidget/STextTable.h"

STextTable::STextTable (const SStringVector& titles) : border(SBorder::ETCHED)
{
  SPanel::forceLayout (SLayout(SDimension(2000,2000)));
  listener = 0;
  lastSelected = -1;

  SLabel *l = 0;
  STextList *tl = 0;
  for (unsigned int i=0; i<titles.size(); i++)
  { 
    l  = new  SLabel (titles[i]);
    tl = new STextList();
    topLabels.append (l);
    textLists.append (tl);
    add (l);
    add (tl);
    tl->setListListener (this);
//    l->changeStyle (SBorder::SOLID_VISIBLE);
    l->setAlignment (SD_Center);
  }
  slider = new SSlider();
  add (slider);
  recalc();
}

STextTable::~STextTable ()
{
}
void
STextTable::setText (const SStringTable& t)
{
  table = t;
  for (unsigned int i=0; i<textLists.size(); i++)
  {
     textLists[i]->setText (table[i]);
  }
}

bool
STextTable::selectText (const SString& s, unsigned int column)
{
  SStringVector sl = table [column];
  for (unsigned int i=0; i<sl.size(); i++)
  {
     if (s == sl[i])
     {
       return selectItem ((int) i);
     }
  }
  return false;
}

bool
STextTable::selectItem (int item)
{
  bool ret = true;
  for (unsigned int i=0; i<textLists.size(); i++)
  {
    ret = ret && textLists[i]->selectItem (item);
  }
  return ret;
}

void
STextTable::setListListener (SListListener* l)
{
  listener = l;
}

void
STextTable::setBackground (const SColor& bg)
{
  SPanel::setBackground (bg);
  border.setBackground (bg);
}

void
STextTable::setLabelForeground (const SColor& fg)
{
  for (unsigned int i=0; i<topLabels.size(); i++)
  {
     topLabels[i]->setForeground (fg);
  }
}

void
STextTable::setSliderBackground (const SColor& bg)
{
  slider->setSliderBackground (bg);
}

void
STextTable::setFont (const SString& font, double fontSize)
{
  for (unsigned int i=0; i<topLabels.size(); i++)
  {
    topLabels[i]->setFont (font, fontSize);
    textLists[i]->setFont (font, fontSize);
  }
  recalc ();
}
void
STextTable::setFontSize (double fontSize)
{
  for (unsigned int i=0; i<topLabels.size(); i++)
  {
    topLabels[i]->setFontSize (fontSize);
    textLists[i]->setFontSize (fontSize);
  }
  recalc ();
}
int
STextTable::getLastSelected ()
{
  return lastSelected;
}
void
STextTable::itemSelected (void* source, const SAccelerator* acc)
{
  STextList* listSource = (STextList*) source;
  lastSelected = listSource->getLastSelected();
  for (unsigned int i=0; i<topLabels.size(); i++)
  {
    if (textLists[i] == listSource) continue;
    textLists[i]->selectItem (lastSelected);
  }
  if (listener) listener->itemSelected (this, acc);
}

void
STextTable::itemHighlighted (void* source, int item)
{
  STextList* listSource = (STextList*) source;
  for (unsigned int i=0; i<topLabels.size(); i++)
  {
    if (textLists[i] == listSource) continue;
    textLists[i]->selectItem (item);
  }
}

void
STextTable::resize(const SDimension& d)
{
  border.resize(d);
  SPanel::resize (d);
}

void
STextTable::redraw (SCanvas *canvas, int x, int y, 
     unsigned int width, unsigned int height)
{
  //SPanel::redraw (canvas, x, y, width, height);
  border.redraw(canvas, x, y, width, height);
}

/**
 * recalculate the Layout of each component
 * preferredSize should be calculated before calling.
 */
void
STextTable::recalc()
{
  unsigned int w = 20;
  unsigned int i;
  for (i=0; i<topLabels.size(); i++)
  { 
    w += topLabels[i]->getPreferredSize().width;
  }
  unsigned int h = topLabels[0]->getPreferredSize().height;
  preferredSize = SDimension (w + 4, h * 2 + 4);
  SDimension d = preferredSize;

  //unsigned int sliderWidth = 20;
  unsigned int sliderWidth = (int) (SAwt::getScale() * 20.0);

  unsigned int lh = topLabels[0]->getPreferredSize().height;
  SDimension bd = border.getBorderSize();
  
  unsigned int fullWidth = 0;
  for (i=0; i<topLabels.size(); i++)
  {
    fullWidth += topLabels[i]->getPreferredSize().width;
  }
  SDimension estate;
  estate  = d - (bd * 2);
  if (estate.width > sliderWidth) estate.width -= sliderWidth;

  unsigned int widthCount = 0;
  unsigned int current = 0;
  for (i=0; i<topLabels.size(); i++)
  {
    current = topLabels[i]->getPreferredSize().width;
    current = (current * estate.width)/fullWidth;
    if (current == 0) current = 2;
    topLabels[i]->setLayout (
      SLayout (
        SLocation (widthCount + bd.width, bd.height),
        SLocation (widthCount + current + bd.width, bd.height + lh),
        SLocation (widthCount * 100 / estate.width, 0),
        SLocation ((widthCount + current) * 100 / estate.width, 0)
      )
    );
    textLists[i]->setLayout (
      SLayout (
        SLocation (widthCount + bd.width, bd.height + lh),
        //SLocation (widthCount + current + bd.width, d.height + bd.height),
        SLocation (widthCount + current + bd.width, d.height - bd.height),
        SLocation (widthCount * 100 / estate.width, 0),
        SLocation ((widthCount + current) * 100 / estate.width, 100)
      )
    );
    widthCount += current;
  }
  slider->setLayout (
    SLayout (
      SLocation (estate.width + bd.width, lh+(int)bd.height),
      SLocation (d.width-(int)bd.width, d.height - bd.height),
      SLocation (100, 0),
      SLocation (100, 100)
    )
  );

  /* save current */
  SLayout goodlayout = layout;

  /* pretend we have this layout */
  forceLayout (preferredSize);

  /* accept old layout */
  setLayout (goodlayout);
}
