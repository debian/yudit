/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef STextTable_h
#define STextTable_h

#include "swidget/STextList.h"
#include "swidget/SSlider.h"
#include "swidget/SBorder.h"
#include "swidget/SLabel.h"
#include "stoolkit/STypes.h"

class STextTable : public SPanel, public SListListener
{
public:
  STextTable (const SStringVector& titles);
  virtual ~STextTable ();
  void setText (const SStringTable& t);

  bool selectText (const SString& s, unsigned int column=0);
  bool selectItem (int item);
  void setListListener (SListListener* l);

  virtual void setBackground (const SColor& bg);
  virtual void setLabelForeground (const SColor& fg);
  virtual void setSliderBackground (const SColor& bg);

  void setFont (const SString& font, double fontSize=0.0);
  void setFontSize (double fontSize);
  /* needed because of border */
  virtual void resize(const SDimension& d);
  int  getLastSelected ();
protected:
  SStringTable  table;
  SListListener* listener;
  int lastSelected;
  void itemHighlighted (void* source, int item);

  virtual void itemSelected (void* source, const SAccelerator* acc);
  virtual void redraw (SCanvas *canvas, int x, int y, 
     unsigned int width, unsigned int height);

  SBinVector<STextList*> textLists;
  SBinVector<SLabel*>    topLabels;
  void recalc();

  SSlider*               slider;
  SBorder                border;
};

#endif /* STextTable_h */
