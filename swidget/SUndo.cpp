/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNES FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "swidget/SUndo.h"

/**
 * Create an undo object.
 * @param t is one of 
 * <ul>
 * <li> SS_Insert </li>
 * <li> SS_Erase </li>
 * <li> SS_EraseSelect </li>
 * </ul>
 * @param dir is the direction 
 * @param s is the string involved.
 * @param b is the textindex before the action.
 * @param a is the textindex after the action.
 * @param b is the textindex 'before' after the action.
 */
SUndo::SUndo (SType t, SS_DR_Dir dir, SS_Embedding emb, const SString& s, 
     const SCursorIndex& b, const SCursorIndex& a, const SCursorIndex& ba)
{
  type = t;
  direction = dir;
  embedding = emb;
  string = s;
  before = b;
  after = a;
  beforeAfter = ba;
}

/**
 * Copy
 */
SUndo::SUndo (const SUndo& undo)
{
  direction = undo.direction;
  embedding = undo.embedding;
  type = undo.type;
  string = undo.string;
  before = undo.before;
  after = undo.after;
  beforeAfter = undo.beforeAfter;
}

SUndo::~SUndo()
{
}

SUndo
SUndo::operator = (const SUndo& undo)
{
  direction = undo.direction;
  embedding = undo.embedding;
  type = undo.type;
  string = undo.string;
  before = undo.before;
  after = undo.after;
  beforeAfter = undo.beforeAfter;
  return *this;
}
/**
 * This is from object
 */
SObject*
SUndo::clone () const
{
  return new SUndo(*this);
}
