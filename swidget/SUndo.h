/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNES FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef SUndo_h
#define SUndo_h

#include "stoolkit/SObject.h"
#include "stoolkit/SVector.h"
#include "stoolkit/SString.h"
#include "stoolkit/SCursorIndex.h"
#include "stoolkit/STextIndex.h"
#include "stoolkit/SCharClass.h"

class SUndo : public SObject
{
public:
  enum SType { SS_Insert, SS_Erase, SS_EraseSelect, SS_EraseOne,
     SS_BackSpaceOne, SS_ComposeOne };
  SUndo (SType type, SS_DR_Dir dir, SS_Embedding embedding, const SString& str, 
       const SCursorIndex& before, const SCursorIndex& after,
       const SCursorIndex& beforeAfter);
  SUndo (const SUndo& undo);
  virtual ~SUndo();
  SUndo operator = (const SUndo& undo);
  SObject* clone () const;
  SType	     type;
  SS_DR_Dir  direction;
  SS_Embedding embedding;
  SString    string;

  SCursorIndex after;
  SCursorIndex beforeAfter;
  SCursorIndex before;
};

typedef SVector<SUndo> SV_Undo;

#endif /* SUndo_h */
