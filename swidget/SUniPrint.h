/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef SUniPrint_h
#define SUniPrint_h

#include "stoolkit/SString.h"
#include "swindow/SPrinter.h"
#include "swidget/STextView.h"

/**
 * This is a stoolkit printer.
 */
class SUniPrint 
{
public:
  SUniPrint (const SPrinter& printer, 
           const SString& headerFontName,  double headerFontSize,
           const SString& bodyFontName,  double bodyFontSize);
  ~SUniPrint();
  bool print (const SString& header, const SString& buffer, bool evt=false);
  unsigned int getPageCount () const;
  void setDocumentEmbedding (SS_Embedding e);
  void setLineEndMark (bool brk); 
  void setWordWrap (bool brk); 
  bool hasNative ();
private:
  unsigned int pageHeight;
  unsigned int pageCount;
  SPrinter  printer;
  STextView body;
  STextView headerLeft;
  STextView headerCenter;
  STextView headerRight;
  bool      printheader;
};
#endif /* SUniPrint_h */
