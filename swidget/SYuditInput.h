/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNES FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef SYuditInput_h
#define SYuditInput_h

#include "swidget/SEditor.h"
#include "swindow/SWindow.h"

#include "stoolkit/SCursorIndex.h"
#include "stoolkit/STextData.h"
#include "stoolkit/SEncoder.h"

class SYuditInput 
{
public:
  SYuditInput (const SString& name, SEditorIF* eif, SEditor* editor);
  ~SYuditInput ();

  void keyPressed (SWindowListener::SKey key, const SString& s,
            bool ctrl, bool shift, bool meta);
  bool clear(bool tosend=true);
  SString encode (const SV_UCS4& v) const;
  bool isOK();
private:
  void       almostClear();
  unsigned int preEditSize;
  SEncoder     encoder;
  SCursorIndex startIndex;
  SCursorIndex endIndex;
  SS_DR_Dir    startDirection; 
  
  SEditorIF  *eif;
  SEditor    *editor;
};

#endif /* SYuditInput_h */
