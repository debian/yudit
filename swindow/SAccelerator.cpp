/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
 
#include "swindow/SAccelerator.h"
#include "stoolkit/STypes.h" 

SAccelerator::SAccelerator (void)
{
  key = 0;
  ctrl = false;
  shift = false;
  meta = false;
  string = SString("empty");
}

SAccelerator::SAccelerator (const SString& str)
{
  key = 0;
  ctrl = false;
  shift = false;
  meta = false;
  int ikey;
  int ictrl;
  int ishift;
  int imeta;
  SString a = str;
  a.append ((char)0);
  if (sscanf (a.array(), "%d %d %d %d", &ikey, &ictrl, &ishift, &imeta) != 4)
  {
    fprintf (stderr, "bad accelerator:%s\n", a.array());
  }
  else
  {
    key = ikey;
    ctrl = (bool) ictrl;
    shift = (bool) ishift;
    meta = (bool) imeta;
  }
  char as[64];
  snprintf (as, 64, "%d %d %d %d", (int) key, (int) ctrl, (int) shift, (int) meta);
  string = SString(as);
}

SAccelerator::SAccelerator (int _key, bool _ctrl, bool _shift, bool _meta)
{
  key = _key;
  ctrl = _ctrl;
  shift = _shift;
  meta = _meta;
  char as[64];
  snprintf (as, 64, "%d %d %d %d", (int) key, (int) ctrl, (int) shift, (int) meta);
  string = SString(as);
}

SAccelerator::SAccelerator(const SAccelerator& a)
{
  string = a.string;
  key = a.key;
  ctrl = a.ctrl;
  shift = a.shift;
  meta = a.meta;
}

SAccelerator
SAccelerator::operator = (const SAccelerator& a)
{
  string = a.string;
  key = a.key;
  ctrl = a.ctrl;
  shift = a.shift;
  meta = a.meta;
  return *this;
}

SAccelerator::~SAccelerator ()
{
}

SObject*
SAccelerator::clone() const
{
  return new SAccelerator (*this);
}

const SString&
SAccelerator::toString() const
{
  return string;
}

SAcceleratorListener::SAcceleratorListener()
{
}

SAcceleratorListener::~SAcceleratorListener()
{
}
