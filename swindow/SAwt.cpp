/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
 

#include "stoolkit/SExcept.h"
#include "swindow/SAwt.h"
#include "swindow/SRasterizer.h"
#include "swindow/SFontImpl.h"

#ifdef USE_OSX
# include "sosx/SOSX.h"
# define SS_GUI_FOUND OSX
#else
# ifdef USE_WINAPI
#  include "swin32/SWin32.h"
#  define SS_GUI_FOUND WIN32
#  else
#  if USE_X11
#   include "sx11/SX11Impl.h"
#   include "sx11/SX11Window.h"
#   define SS_GUI_FOUND X11
#  endif
# endif
#endif

#ifndef SS_GUI_FOUND
# if WITH_GUI
#  error "No GUI development libraries have been found. You can install libx11-dev or './configure --without-gui' in case X11 development libraries are missing, and you do not need a GUI."
# endif
#endif


/**
 * @author: Gaspar Sinai <gaspar@yudit.org>
 * @version: 2000-04-23
 * This is the abstract widget toolkit
 */

/**
 * This class should be implemented for a window toolkit implementation
 */
SAwtImpl::SAwtImpl()
{
}
SAwtImpl::~SAwtImpl()
{
}

SWindow*
SAwtImpl::getWindow (SWindowListener* l, const SString& name)
{
  return 0;
}

SFontNative*
SAwtImpl::getFont (const SString& enc)
{
  return 0;
}

/**
 * Set the encoder for locale conversions.
 */
void
SAwtImpl::setEncoding (const SString& encoder)
{
}

SAwt::SAwt (bool cacheOn, unsigned int cacheSize)
{
  if (!delegate)
  {
    SRasterizer::setCacheSize (cacheSize);
    SRasterizer::setCacheOn (cacheOn);

#ifdef USE_OSX
    SOSXImpl* i = new SOSXImpl();
    delegate =  (i->isOK()) ? i : 0;
    SOSXWindow::setPixmapCacheSize (cacheSize);
    SOSXWindow::setPixmapCacheOn (cacheOn);
#else
# ifdef USE_WINAPI
    SW32Impl* i = new SW32Impl();
    delegate =  (i->isOK()) ? i : 0;
    SW32Window::setPixmapCacheSize (cacheSize);
    SW32Window::setPixmapCacheOn (cacheOn);
# else
#  if USE_X11
    SX11Impl* i = new SX11Impl();
    delegate =  (i->isOK()) ? i : 0;
    SX11Window::setPixmapCacheSize (cacheSize);
    SX11Window::setPixmapCacheOn (cacheOn);
#  else
    delegate = 0;
#  endif
# endif
#endif
  }
}
// Destroys the implementation.
SAwt::~SAwt ()
{
  if (delegate) delete delegate;
}

void
SAwt::setImpl (SAwtImpl* impl)
{
  delegate = impl;
}

SWindow*
SAwt::getWindow (SWindowListener* l, const SString& name)
{
  if (delegate==0) return 0;
  SWindow* ret = delegate->getWindow(l, name);
  return ret;
}

SFontNative*
SAwt::getFont (const SString& enc)
{
  if (delegate==0) return 0;
  return delegate->getFont(enc);
}

void
SAwt::setEncoding (const SString& encoder)
{
  if (delegate==0) return;
  delegate->setEncoding(encoder);
}

bool
SAwt::implemented()
{
  return (delegate!=0);
}

bool
SAwt::hasGUI()
{
  bool ret = false;
#ifdef USE_OSX
  ret = true;
#else
# ifdef USE_WINAPI
  ret = true;
# else
#  if USE_X11
  ret = true;
#  endif
# endif
#endif
  return ret;
}

void
SAwt::setScale (double _scale)
{
   scale = _scale;
}
double
SAwt::getScale () 
{
    return scale;
}
int
SAwt::getRasterScale () 
{
    return (int) (scale + 0.5);
}
SAwtImpl*
SAwt::getDelegate () 
{
    return delegate;
}

SAwtImpl* SAwt::delegate = 0;
double SAwt::scale = 1.0;
