/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
 
#include "swindow/SFontFB.h"
#include "stoolkit/STypes.h" 
#include <stdio.h>
/**
 * The following is from freeware 'dixon.zip' font at 
 * http://www.brockmeiers.de/fonts/
 */

#ifdef __GNUC__
#pragma GCC diagnostic ignored "-Wnarrowing"
#endif 

#define SD_BOXR 10
#define SD_BWIDTH (fb_width*SD_BOXR/400)

//static short fb_ascent=728;
//static short fb_descent=210;
static short fb_doffset=210;
static short fb_aoffset=728;

/* we will draw with fb_aoffset ascender but pretend it is 0 */
static short fb_ascent=fb_aoffset+fb_doffset;
static short fb_descent=0;

//static double fb_scale=0.001 * (728.0/ (728.0+210.0));
static double fb_scale= 1.0/( + double(fb_ascent) * (1.0 + double (SD_BOXR) / 50));

//static double fb_scale=0.001;
static double fb_height = 728.0+210.0;

//static double fb_scale=0.001;

//static short fb_gap=150;
static short fb_gap=0;
static short fb_width=721;

#define SD_BEGIN_GLYPH 32000
#define SD_MOVE_TO 32001
#define SD_LINE_TO 32002
#define SD_CURVE_TO 32003
#define SD_CLOSE_PATH 32004
#define SD_END_GLYPH 32005

/**
 * The first number after the SD_BEGIN_GLYPH is the width 
 * don't worry we just use fb_width to scale.
 */
short fb_box[] =
{
SD_BEGIN_GLYPH, 721, /*  width as in U+0030 '0'*/ 
 SD_MOVE_TO, -fb_width*SD_BOXR/200 - SD_BWIDTH, -100 - SD_BWIDTH,
  SD_LINE_TO, 719 + fb_width*SD_BOXR/200 + SD_BWIDTH, -100 - SD_BWIDTH,
  SD_LINE_TO, 719 + fb_width*SD_BOXR/200 + SD_BWIDTH, 726 + SD_BWIDTH,
  SD_LINE_TO, -fb_width*SD_BOXR/200 - SD_BWIDTH, 726 + SD_BWIDTH,
 SD_CLOSE_PATH,
 /* reversed rotation because ghsotscript did not like it. */
 SD_MOVE_TO, -fb_width*SD_BOXR/200 + 2, -100,
  SD_LINE_TO, -fb_width*SD_BOXR/200 + 2, 726,
  SD_LINE_TO, 719 + fb_width*SD_BOXR/200 , 726,
  SD_LINE_TO, 719 + fb_width*SD_BOXR/200 , -100,
 SD_CLOSE_PATH,
SD_END_GLYPH
};

/* linefeed */
short fb_lf[] =
{
SD_BEGIN_GLYPH, 721, /*  width */ 
 SD_MOVE_TO,  700, 300,
 SD_LINE_TO,  700, 0,
 SD_LINE_TO,  300, 0,
 SD_LINE_TO,  300, -100,
 SD_LINE_TO,  50, 25,
 SD_LINE_TO,  300, 150,
 SD_LINE_TO,  300, 50,
 SD_LINE_TO,  650, 50,
 SD_LINE_TO,  650, 300,
 SD_CLOSE_PATH,
SD_END_GLYPH
};

/* line separator */
short fb_ls[] =
{
SD_BEGIN_GLYPH, 721, /*  width */ 
 SD_MOVE_TO,  700, 300,
 SD_LINE_TO,  700, 0,
 SD_LINE_TO,  100, 0,
 SD_LINE_TO,  100, 50,
 SD_LINE_TO,  300, 50,
 SD_LINE_TO,  650, 50,
 SD_LINE_TO,  650, 300,
 SD_CLOSE_PATH,
SD_END_GLYPH
};

/* form feed */
short fb_ff[] =
{
SD_BEGIN_GLYPH, 721, /*  width */ 
 SD_MOVE_TO,  700, 300,
 SD_LINE_TO,  700, 0,
 SD_LINE_TO,  100, 0,
 SD_LINE_TO,  100, 300,
 SD_CLOSE_PATH,
SD_END_GLYPH
};

/* crlf */
short fb_crlf[] =
{
SD_BEGIN_GLYPH, 721, /*  width */ 
 SD_MOVE_TO,  700, 300,
 SD_LINE_TO,  700, 0,
 SD_LINE_TO,  300, 0,
 SD_LINE_TO,  300, -100,
 SD_LINE_TO,  50, 25,
 SD_LINE_TO,  300, 150,
 SD_LINE_TO,  300, 50,
 SD_LINE_TO,  450, 50,
 SD_LINE_TO,  450, 300,
 SD_LINE_TO,  500, 300,
 SD_LINE_TO,  500, 50,
 SD_LINE_TO,  650, 50,
 SD_LINE_TO,  650, 300,
 SD_CLOSE_PATH,
SD_END_GLYPH
};

short fb_cr[] =
{
SD_BEGIN_GLYPH, 721, /*  width */ 
 SD_MOVE_TO,  500, 300,
 SD_LINE_TO,  500, 0,
 SD_LINE_TO,  300, 0,
 SD_LINE_TO,  300, -100,
 SD_LINE_TO,  50, 25,
 SD_LINE_TO,  300, 150,
 SD_LINE_TO,  300, 50,
 SD_LINE_TO,  450, 50,
 SD_LINE_TO,  450, 300,
 SD_CLOSE_PATH,
SD_END_GLYPH
};


/* Paragraph Separator */
short fb_ps[] =
{
SD_BEGIN_GLYPH, 721, /*  width */ 
 SD_MOVE_TO,  700, 700,
 SD_LINE_TO,  700, 0,
 SD_LINE_TO,  300, 0,
 SD_LINE_TO,  300, -100,
 SD_LINE_TO,  50, 25,
 SD_LINE_TO,  300, 150,
 SD_LINE_TO,  300, 50,
 SD_LINE_TO,  650, 50,
 SD_LINE_TO,  650, 700,
 SD_CLOSE_PATH,
SD_END_GLYPH
};

/* LRM */
short fb_lrm[] =
{
SD_BEGIN_GLYPH, 721, /*  width */ 
 SD_MOVE_TO,  720-500, 300,
 SD_LINE_TO,  720-300, 300,
 SD_LINE_TO,  720-300, 200,
 SD_LINE_TO,  720-50, 325,
 SD_LINE_TO,  720-300, 450,
 SD_LINE_TO,  720-300, 350,
 SD_LINE_TO,  720-500, 350,
 SD_CLOSE_PATH,
SD_END_GLYPH
};

/* RLM */
short fb_rlm[] =
{
SD_BEGIN_GLYPH, 721, /*  width */ 
 SD_MOVE_TO,  500, 300,
 SD_LINE_TO,  300, 300,
 SD_LINE_TO,  300, 200,
 SD_LINE_TO,  50, 325,
 SD_LINE_TO,  300, 450,
 SD_LINE_TO,  300, 350,
 SD_LINE_TO,  500, 350,
 SD_CLOSE_PATH,
SD_END_GLYPH
};

/* ZWNJ */
short fb_zwnj[] =
{
SD_BEGIN_GLYPH, 721, /*  width */ 
 SD_MOVE_TO,  550, 325,
 SD_LINE_TO,  400, 200,
 SD_LINE_TO,  400, 300,

 SD_LINE_TO,  250, 300,
 SD_LINE_TO,  250, 200,
 SD_LINE_TO,  100, 325,
 SD_LINE_TO,  250, 450,
 SD_LINE_TO,  250, 350,

 SD_LINE_TO,  400, 350,
 SD_LINE_TO,  400, 450,
 SD_CLOSE_PATH,
SD_END_GLYPH
};

/* ZWJ */
short fb_zwj[] =
{
SD_BEGIN_GLYPH, 721, /*  width */ 
 SD_MOVE_TO,  550, 200,
 SD_LINE_TO,  400, 300,

 SD_LINE_TO,  250, 300,
 SD_LINE_TO,  100, 200,

 SD_LINE_TO,  100, 450,
 SD_LINE_TO,  250, 350,

 SD_LINE_TO,  400, 350,
 SD_LINE_TO,  550, 450,
 SD_CLOSE_PATH,
SD_END_GLYPH
};

/**
 * The first number after the SD_BEGIN_GLYPH is the width 
 */
short fb_box2x[] =
{
SD_BEGIN_GLYPH, 1442, /*  U+0030 '0'*/ 
 SD_MOVE_TO, -fb_width*SD_BOXR/200 - SD_BWIDTH, -100 - SD_BWIDTH,
  SD_LINE_TO, 719*2 + fb_width*SD_BOXR/200 + SD_BWIDTH, -100 - SD_BWIDTH,
  SD_LINE_TO, 719*2 + fb_width*SD_BOXR/200 + SD_BWIDTH, 726 + SD_BWIDTH,
  SD_LINE_TO, -fb_width*SD_BOXR/200 - SD_BWIDTH, 726 + SD_BWIDTH,
 SD_CLOSE_PATH,
 SD_MOVE_TO, -fb_width*SD_BOXR/200 + 2, -100,
  SD_LINE_TO, -fb_width*SD_BOXR/200 + 2, 726,
  SD_LINE_TO, 719*2 + fb_width*SD_BOXR/200, 726,
  SD_LINE_TO, 719*2 + fb_width*SD_BOXR/200, -100,
 SD_CLOSE_PATH,
SD_END_GLYPH
};

short fb_0[] =
{
SD_BEGIN_GLYPH, 555, /*  U+0030 '0'*/ 
 SD_MOVE_TO, 43, 355,
  SD_CURVE_TO, 43, 595, 122, 717, 277, 717,
  SD_CURVE_TO, 431, 717, 510, 594, 510, 352,
  SD_CURVE_TO, 510, 109, 431, -12, 277, -12,
  SD_CURVE_TO, 183, -12, 113, 35, 77, 125,
  SD_CURVE_TO, 54, 183, 43, 255, 43, 354,
 SD_CLOSE_PATH,
 SD_MOVE_TO, 133, 353,
  SD_CURVE_TO, 133, 263, 141, 194, 159, 148,
  SD_CURVE_TO, 177, 93, 223, 59, 277, 59,
  SD_CURVE_TO, 329, 59, 372, 93, 394, 148,
  SD_CURVE_TO, 411, 194, 420, 264, 420, 352,
  SD_CURVE_TO, 420, 440, 411, 507, 394, 555,
  SD_CURVE_TO, 375, 611, 329, 645, 276, 645,
  SD_CURVE_TO, 178, 645, 133, 553, 133, 352,
 SD_CLOSE_PATH,
SD_END_GLYPH
};

short fb_1[] =
{
SD_BEGIN_GLYPH, 555, /* U+0031 '1'*/
 SD_MOVE_TO, 286, 0,
  SD_LINE_TO, 286, 560,
  SD_CURVE_TO, 282, 556, 277, 553, 275, 549,
  SD_CURVE_TO, 268, 545, 262, 539, 256, 535,
  SD_CURVE_TO, 236, 519, 213, 502, 177, 484,
  SD_CURVE_TO, 158, 474, 147, 468, 135, 463,
  SD_CURVE_TO, 132, 461, 129, 460, 125, 459,
  SD_CURVE_TO, 121, 456, 115, 455, 110, 453,
  SD_LINE_TO, 110, 538,
  SD_CURVE_TO, 194, 576, 266, 636, 305, 695,
  SD_CURVE_TO, 309, 701, 313, 710, 317, 717,
  SD_LINE_TO, 374, 718,
  SD_LINE_TO, 374, 0,
  SD_LINE_TO, 286, 0,
 SD_CLOSE_PATH,
SD_END_GLYPH
};

short fb_2[] =
{
SD_BEGIN_GLYPH, 555, /* U+0032 '2' */
 SD_MOVE_TO, 28, 0,
  SD_CURVE_TO, 32, 25, 34, 41, 41, 59,
  SD_CURVE_TO, 43, 67, 48, 74, 52, 84,
  SD_CURVE_TO, 78, 138, 123, 192, 197, 250,
  SD_LINE_TO, 231, 278,
  SD_CURVE_TO, 364, 382, 413, 451, 413, 521,
  SD_CURVE_TO, 413, 591, 356, 645, 278, 645,
  SD_CURVE_TO, 192, 645, 138, 593, 138, 507,
  SD_CURVE_TO, 138, 505, 138, 503, 138, 501,
  SD_LINE_TO, 48, 511,
  SD_CURVE_TO, 60, 643, 141, 717, 280, 717,
  SD_CURVE_TO, 412, 717, 503, 636, 503, 519,
  SD_CURVE_TO, 503, 497, 499, 478, 494, 457,
  SD_CURVE_TO, 474, 394, 421, 331, 327, 251,
  SD_LINE_TO, 255, 191,
  SD_CURVE_TO, 208, 151, 183, 126, 165, 101,
  SD_CURVE_TO, 161, 95, 157, 89, 153, 83,
  SD_LINE_TO, 504, 84,
  SD_LINE_TO, 504, 0,
  SD_LINE_TO, 28, 0,
 SD_CLOSE_PATH,
SD_END_GLYPH
};

short fb_3[] =
{
SD_BEGIN_GLYPH, 555, /* U+0033 '3' */
 SD_MOVE_TO, 43, 189,
  SD_LINE_TO, 132, 200,
  SD_CURVE_TO, 133, 177, 136, 166, 142, 154,
  SD_CURVE_TO, 143, 148, 147, 142, 150, 136,
  SD_CURVE_TO, 171, 85, 218, 59, 272, 59,
  SD_CURVE_TO, 354, 59, 419, 124, 419, 208,
  SD_CURVE_TO, 419, 289, 360, 347, 279, 347,
  SD_CURVE_TO, 275, 347, 271, 347, 269, 347,
  SD_CURVE_TO, 253, 347, 238, 346, 217, 338,
  SD_LINE_TO, 227, 415,
  SD_CURVE_TO, 231, 413, 233, 414, 236, 414,
  SD_CURVE_TO, 237, 414, 239, 414, 241, 414,
  SD_CURVE_TO, 327, 414, 387, 463, 387, 533,
  SD_CURVE_TO, 387, 598, 338, 645, 270, 645,
  SD_CURVE_TO, 199, 645, 153, 598, 140, 516,
  SD_LINE_TO, 52, 533,
  SD_CURVE_TO, 74, 650, 152, 717, 267, 717,
  SD_CURVE_TO, 385, 717, 477, 637, 477, 531,
  SD_CURVE_TO, 477, 465, 441, 416, 379, 386,
  SD_CURVE_TO, 401, 382, 415, 377, 429, 369,
  SD_CURVE_TO, 433, 366, 438, 364, 442, 360,
  SD_CURVE_TO, 486, 328, 512, 272, 512, 210,
  SD_CURVE_TO, 512, 83, 408, -12, 272, -12,
  SD_CURVE_TO, 146, -12, 60, 68, 43, 188,
 SD_CLOSE_PATH,
SD_END_GLYPH
};

short fb_4[] =
{
SD_BEGIN_GLYPH, 555, /* U+0034 '4' */
 SD_MOVE_TO, 325, 0,
  SD_LINE_TO, 325, 171,
  SD_LINE_TO, 14, 171,
  SD_LINE_TO, 14, 252,
  SD_LINE_TO, 341, 715,
  SD_LINE_TO, 413, 715,
  SD_LINE_TO, 413, 252,
  SD_LINE_TO, 510, 252,
  SD_LINE_TO, 510, 171,
  SD_LINE_TO, 413, 171,
  SD_LINE_TO, 413, 0,
  SD_LINE_TO, 325, 0,
 SD_CLOSE_PATH,
  SD_MOVE_TO, 325, 252,
  SD_LINE_TO, 325, 574,
  SD_LINE_TO, 101, 252,
  SD_LINE_TO, 325, 252,
 SD_CLOSE_PATH,
SD_END_GLYPH
};

short fb_5[] =
{
SD_BEGIN_GLYPH, 555, /* U+0035 '5' */
 SD_MOVE_TO, 43, 187,
  SD_LINE_TO, 135, 195,
  SD_CURVE_TO, 149, 107, 198, 58, 273, 58,
  SD_CURVE_TO, 361, 58, 425, 131, 425, 233,
  SD_CURVE_TO, 425, 331, 364, 396, 272, 396,
  SD_CURVE_TO, 217, 396, 169, 368, 141, 326,
  SD_LINE_TO, 59, 338,
  SD_LINE_TO, 128, 706,
  SD_LINE_TO, 484, 706,
  SD_LINE_TO, 484, 622,
  SD_LINE_TO, 198, 622,
  SD_LINE_TO, 160, 429,
  SD_CURVE_TO, 190, 450, 225, 464, 260, 470,
  SD_CURVE_TO, 272, 472, 283, 473, 295, 473,
  SD_CURVE_TO, 423, 473, 518, 375, 518, 241,
  SD_CURVE_TO, 518, 94, 415, -12, 273, -12,
  SD_CURVE_TO, 143, -12, 55, 64, 43, 186,
 SD_CLOSE_PATH,
SD_END_GLYPH
};

short fb_6[] =
{
SD_BEGIN_GLYPH, 555, /* U+0036 '6' */
 SD_MOVE_TO, 127, 369,
  SD_CURVE_TO, 132, 375, 136, 381, 140, 387,
  SD_CURVE_TO, 145, 393, 149, 398, 156, 404,
  SD_CURVE_TO, 160, 408, 163, 412, 169, 416,
  SD_CURVE_TO, 202, 444, 246, 460, 289, 463,
  SD_CURVE_TO, 293, 463, 296, 463, 300, 463,
  SD_CURVE_TO, 421, 463, 512, 364, 512, 231,
  SD_CURVE_TO, 512, 88, 418, -12, 288, -12,
  SD_CURVE_TO, 196, -12, 115, 36, 78, 114,
  SD_CURVE_TO, 51, 171, 39, 238, 39, 333,
  SD_CURVE_TO, 39, 585, 128, 717, 296, 717,
  SD_CURVE_TO, 409, 717, 485, 651, 499, 539,
  SD_LINE_TO, 412, 533,
  SD_CURVE_TO, 402, 605, 361, 645, 295, 645,
  SD_CURVE_TO, 231, 645, 182, 610, 157, 546,
  SD_CURVE_TO, 146, 518, 137, 482, 132, 448,
  SD_CURVE_TO, 128, 424, 126, 402, 126, 381,
  SD_CURVE_TO, 126, 379, 125, 377, 127, 375,
  SD_CURVE_TO, 127, 373, 127, 370, 127, 368,
 SD_CLOSE_PATH,
 SD_MOVE_TO, 140, 233,
  SD_CURVE_TO, 140, 134, 204, 59, 286, 59,
  SD_CURVE_TO, 364, 59, 422, 129, 422, 225,
  SD_CURVE_TO, 422, 319, 365, 385, 283, 385,
  SD_CURVE_TO, 201, 385, 140, 320, 140, 232,
 SD_CLOSE_PATH,
SD_END_GLYPH
};

short fb_7[] =
{
SD_BEGIN_GLYPH, 555, /* U+0037 '7' */
 SD_MOVE_TO, 149, 0,
  SD_CURVE_TO, 150, 70, 167, 161, 197, 251,
  SD_CURVE_TO, 206, 281, 217, 312, 231, 343,
  SD_CURVE_TO, 249, 387, 267, 425, 290, 463,
  SD_CURVE_TO, 304, 489, 322, 516, 342, 546,
  SD_CURVE_TO, 354, 564, 364, 576, 372, 587,
  SD_CURVE_TO, 373, 589, 374, 590, 376, 592,
  SD_CURVE_TO, 382, 601, 390, 609, 400, 621,
  SD_LINE_TO, 49, 622,
  SD_LINE_TO, 49, 706,
  SD_LINE_TO, 513, 706,
  SD_LINE_TO, 513, 638,
  SD_CURVE_TO, 422, 546, 356, 442, 310, 314,
  SD_CURVE_TO, 299, 284, 290, 255, 281, 225,
  SD_CURVE_TO, 258, 145, 244, 65, 240, 0,
  SD_LINE_TO, 149, 0,
 SD_CLOSE_PATH,
SD_END_GLYPH
};

short fb_8[] =
{
SD_BEGIN_GLYPH, 555, /* U+0038 '8' */
 SD_MOVE_TO, 178, 388,
  SD_CURVE_TO, 156, 392, 145, 399, 133, 407,
  SD_CURVE_TO, 130, 409, 127, 412, 123, 415,
  SD_CURVE_TO, 89, 440, 71, 485, 71, 532,
  SD_CURVE_TO, 71, 641, 155, 717, 277, 717,
  SD_CURVE_TO, 397, 717, 485, 639, 485, 530,
  SD_CURVE_TO, 485, 460, 448, 415, 379, 387,
  SD_CURVE_TO, 404, 380, 418, 373, 432, 364,
  SD_CURVE_TO, 437, 360, 440, 357, 446, 353,
  SD_CURVE_TO, 488, 319, 514, 264, 514, 203,
  SD_CURVE_TO, 514, 77, 415, -12, 278, -12,
  SD_CURVE_TO, 140, -12, 42, 78, 42, 205,
  SD_CURVE_TO, 42, 267, 66, 320, 108, 354,
  SD_CURVE_TO, 113, 358, 117, 361, 123, 365,
  SD_CURVE_TO, 137, 374, 151, 381, 178, 387,
 SD_CLOSE_PATH,
 SD_MOVE_TO, 132, 205,
  SD_CURVE_TO, 132, 121, 195, 59, 279, 59,
  SD_CURVE_TO, 363, 59, 424, 119, 424, 202,
  SD_CURVE_TO, 424, 287, 361, 349, 276, 349,
  SD_CURVE_TO, 194, 349, 132, 286, 132, 204,
 SD_CLOSE_PATH,
  SD_MOVE_TO, 161, 534,
  SD_CURVE_TO, 161, 467, 211, 421, 278, 421,
  SD_CURVE_TO, 344, 421, 394, 468, 394, 531,
  SD_CURVE_TO, 394, 596, 343, 645, 277, 645,
  SD_CURVE_TO, 211, 645, 161, 597, 161, 533,
 SD_CLOSE_PATH,
SD_END_GLYPH
};

short fb_9[] =
{
SD_BEGIN_GLYPH, 555, /* U+0039 '9' */
 SD_MOVE_TO, 56, 165,
  SD_LINE_TO, 141, 173,
  SD_CURVE_TO, 155, 94, 192, 58, 260, 58,
  SD_CURVE_TO, 314, 58, 360, 86, 386, 136,
  SD_CURVE_TO, 406, 174, 420, 227, 424, 279,
  SD_CURVE_TO, 425, 292, 426, 304, 426, 318,
  SD_CURVE_TO, 426, 322, 426, 328, 426, 336,
  SD_CURVE_TO, 388, 278, 324, 241, 255, 241,
  SD_CURVE_TO, 132, 241, 43, 340, 43, 476,
  SD_CURVE_TO, 43, 617, 136, 717, 268, 717,
  SD_CURVE_TO, 340, 717, 404, 685, 448, 631,
  SD_CURVE_TO, 494, 573, 514, 497, 514, 371,
  SD_CURVE_TO, 514, 245, 498, 169, 460, 101,
  SD_CURVE_TO, 418, 26, 346, -12, 258, -12,
  SD_CURVE_TO, 144, -12, 70, 50, 56, 164,
 SD_CLOSE_PATH,
 SD_MOVE_TO, 133, 479,
  SD_CURVE_TO, 133, 383, 190, 319, 273, 319,
  SD_CURVE_TO, 355, 319, 416, 383, 416, 473,
  SD_CURVE_TO, 416, 569, 352, 645, 271, 645,
  SD_CURVE_TO, 191, 645, 133, 575, 133, 478,
 SD_CLOSE_PATH,
SD_END_GLYPH
};

short fb_A[] =
{
SD_BEGIN_GLYPH, 666, /* U+0041 'A' */
 SD_MOVE_TO, -1, 0,
  SD_LINE_TO, 273, 715,
  SD_LINE_TO, 375, 715,
  SD_LINE_TO, 668, 0,
  SD_LINE_TO, 560, 0,
  SD_LINE_TO, 477, 216,
  SD_LINE_TO, 178, 216,
  SD_LINE_TO, 99, 0,
  SD_LINE_TO, -1, 0,
 SD_CLOSE_PATH,
  SD_MOVE_TO, 205, 294,
  SD_LINE_TO, 447, 294,
  SD_LINE_TO, 373, 492,
  SD_LINE_TO, 322, 640,
  SD_LINE_TO, 283, 504,
  SD_LINE_TO, 205, 294,
 SD_CLOSE_PATH,
SD_END_GLYPH
};

short fb_B[] =
{
SD_BEGIN_GLYPH, 666, /* U+0042 'B' */
 SD_MOVE_TO, 73, 0,
  SD_LINE_TO, 73, 715,
  SD_LINE_TO, 342, 715,
  SD_CURVE_TO, 442, 713, 499, 693, 541, 639,
  SD_CURVE_TO, 559, 613, 574, 582, 578, 550,
  SD_CURVE_TO, 578, 543, 579, 537, 579, 531,
  SD_CURVE_TO, 579, 465, 544, 407, 479, 379,
  SD_CURVE_TO, 565, 353, 613, 291, 613, 205,
  SD_CURVE_TO, 613, 123, 569, 52, 501, 23,
  SD_CURVE_TO, 497, 21, 492, 19, 488, 18,
  SD_CURVE_TO, 449, 3, 411, 0, 345, 0,
  SD_LINE_TO, 73, 0,
 SD_CLOSE_PATH,
 SD_MOVE_TO, 168, 84,
  SD_LINE_TO, 346, 84,
  SD_CURVE_TO, 392, 82, 412, 84, 432, 91,
  SD_CURVE_TO, 438, 92, 447, 96, 455, 100,
  SD_CURVE_TO, 493, 117, 516, 158, 516, 205,
  SD_CURVE_TO, 516, 255, 489, 296, 447, 314,
  SD_CURVE_TO, 440, 316, 434, 318, 428, 321,
  SD_CURVE_TO, 406, 327, 383, 329, 333, 329,
  SD_LINE_TO, 168, 330,
  SD_LINE_TO, 168, 84,
 SD_CLOSE_PATH,
 SD_MOVE_TO, 168, 414,
  SD_LINE_TO, 323, 414,
  SD_CURVE_TO, 372, 412, 393, 414, 413, 421,
  SD_CURVE_TO, 419, 423, 424, 425, 431, 428,
  SD_CURVE_TO, 465, 442, 486, 477, 486, 519,
  SD_CURVE_TO, 486, 568, 460, 607, 422, 619,
  SD_CURVE_TO, 396, 625, 365, 630, 311, 630,
  SD_LINE_TO, 168, 631,
  SD_LINE_TO, 168, 414,
 SD_CLOSE_PATH,
SD_END_GLYPH
};

short fb_C[] =
{
SD_BEGIN_GLYPH, 721, /* U+0043 'C' */
 SD_MOVE_TO, 589, 250,
  SD_LINE_TO, 683, 226,
  SD_CURVE_TO, 644, 72, 537, -12, 387, -12,
  SD_CURVE_TO, 243, -12, 146, 43, 93, 159,
  SD_CURVE_TO, 65, 221, 51, 290, 51, 362,
  SD_CURVE_TO, 51, 583, 183, 727, 389, 727,
  SD_CURVE_TO, 531, 727, 632, 652, 671, 518,
  SD_LINE_TO, 578, 497,
  SD_CURVE_TO, 542, 598, 485, 645, 387, 645,
  SD_CURVE_TO, 236, 645, 148, 540, 148, 362,
  SD_CURVE_TO, 148, 175, 233, 67, 379, 67,
  SD_CURVE_TO, 489, 67, 566, 131, 589, 249,
 SD_CLOSE_PATH,
SD_END_GLYPH
};

short fb_D[] =
{
SD_BEGIN_GLYPH, 721, /* U+0044 'D' */
 SD_MOVE_TO, 78, 0,
  SD_LINE_TO, 78, 715,
  SD_LINE_TO, 325, 715,
  SD_CURVE_TO, 399, 713, 434, 711, 469, 701,
  SD_CURVE_TO, 481, 697, 491, 692, 505, 686,
  SD_CURVE_TO, 533, 674, 561, 653, 583, 627,
  SD_CURVE_TO, 640, 559, 670, 472, 670, 362,
  SD_CURVE_TO, 670, 206, 614, 85, 517, 35,
  SD_CURVE_TO, 511, 32, 505, 29, 499, 27,
  SD_CURVE_TO, 455, 7, 410, 0, 336, 0,
  SD_LINE_TO, 78, 0,
 SD_CLOSE_PATH,
  SD_MOVE_TO, 173, 84,
  SD_LINE_TO, 325, 84,
  SD_CURVE_TO, 395, 82, 435, 90, 469, 110,
  SD_CURVE_TO, 535, 150, 572, 236, 572, 362,
  SD_CURVE_TO, 572, 447, 557, 503, 523, 553,
  SD_CURVE_TO, 508, 575, 485, 595, 463, 607,
  SD_CURVE_TO, 456, 609, 450, 613, 445, 615,
  SD_CURVE_TO, 417, 627, 388, 630, 323, 630,
  SD_LINE_TO, 173, 631,
  SD_LINE_TO, 173, 84,
 SD_CLOSE_PATH,
SD_END_GLYPH
};

short fb_E[] =
{
SD_BEGIN_GLYPH, 666, /* U+0045 'E' */
 SD_MOVE_TO, 79, 0,
  SD_LINE_TO, 79, 715,
  SD_LINE_TO, 596, 715,
  SD_LINE_TO, 596, 631,
  SD_LINE_TO, 174, 631,
  SD_LINE_TO, 174, 412,
  SD_LINE_TO, 569, 412,
  SD_LINE_TO, 569, 328,
  SD_LINE_TO, 174, 328,
  SD_LINE_TO, 174, 84,
  SD_LINE_TO, 613, 84,
  SD_LINE_TO, 613, 0,
  SD_LINE_TO, 79, 0,
 SD_CLOSE_PATH,
SD_END_GLYPH
};

short fb_F[] =
{
SD_BEGIN_GLYPH, 610, /* U+0046 'F' */
 SD_MOVE_TO, 80, 0,
  SD_LINE_TO, 80, 715,
  SD_LINE_TO, 563, 715,
  SD_LINE_TO, 563, 631,
  SD_LINE_TO, 175, 631,
  SD_LINE_TO, 175, 409,
  SD_LINE_TO, 510, 409,
  SD_LINE_TO, 510, 325,
  SD_LINE_TO, 175, 325,
  SD_LINE_TO, 175, 0,
  SD_LINE_TO, 80, 0,
 SD_CLOSE_PATH,
SD_END_GLYPH
};

short fb_a[] =
{
SD_BEGIN_GLYPH, 555, /* U+0061 'a' */
 SD_MOVE_TO, 406, 64,
  SD_CURVE_TO, 391, 50, 376, 39, 363, 31,
  SD_CURVE_TO, 316, 0, 269, -11, 215, -11,
  SD_CURVE_TO, 107, -11, 38, 45, 38, 135,
  SD_CURVE_TO, 38, 207, 83, 261, 162, 283,
  SD_CURVE_TO, 167, 284, 172, 285, 176, 287,
  SD_CURVE_TO, 184, 288, 190, 289, 201, 292,
  SD_CURVE_TO, 210, 293, 223, 295, 241, 297,
  SD_CURVE_TO, 274, 301, 304, 305, 330, 312,
  SD_CURVE_TO, 358, 317, 380, 324, 398, 327,
  SD_CURVE_TO, 399, 335, 399, 340, 399, 343,
  SD_CURVE_TO, 399, 347, 399, 349, 399, 351,
  SD_CURVE_TO, 399, 425, 360, 456, 274, 456,
  SD_CURVE_TO, 191, 456, 157, 431, 139, 358,
  SD_LINE_TO, 53, 370,
  SD_CURVE_TO, 55, 394, 60, 408, 68, 422,
  SD_CURVE_TO, 70, 426, 73, 430, 76, 436,
  SD_CURVE_TO, 112, 496, 182, 529, 285, 529,
  SD_CURVE_TO, 387, 529, 453, 499, 475, 442,
  SD_CURVE_TO, 476, 436, 478, 431, 480, 426,
  SD_CURVE_TO, 485, 406, 487, 385, 487, 333,
  SD_LINE_TO, 487, 217,
  SD_CURVE_TO, 487, 111, 487, 85, 493, 60,
  SD_CURVE_TO, 493, 57, 493, 53, 495, 51,
  SD_CURVE_TO, 497, 39, 500, 32, 504, 24,
  SD_CURVE_TO, 506, 16, 509, 9, 515, 0,
  SD_LINE_TO, 423, 0,
  SD_CURVE_TO, 420, 2, 418, 7, 417, 13,
  SD_CURVE_TO, 411, 27, 408, 41, 406, 63,
 SD_CLOSE_PATH,
  SD_MOVE_TO, 398, 260,
  SD_CURVE_TO, 392, 257, 387, 254, 381, 253,
  SD_CURVE_TO, 350, 242, 305, 232, 255, 226,
  SD_CURVE_TO, 161, 214, 131, 190, 131, 138,
  SD_CURVE_TO, 131, 87, 172, 56, 236, 56,
  SD_CURVE_TO, 286, 56, 330, 73, 360, 104,
  SD_CURVE_TO, 361, 105, 363, 107, 365, 109,
  SD_CURVE_TO, 390, 138, 398, 167, 398, 226,
  SD_LINE_TO, 398, 260,
 SD_CLOSE_PATH,
SD_END_GLYPH
};

short fb_b[] =
{
SD_BEGIN_GLYPH, 555, /* U+0062 'b' */
 SD_MOVE_TO, 148, 0,
  SD_LINE_TO, 67, 0,
  SD_LINE_TO, 67, 715,
  SD_LINE_TO, 155, 715,
  SD_LINE_TO, 155, 460,
  SD_CURVE_TO, 193, 502, 238, 529, 297, 529,
  SD_CURVE_TO, 429, 529, 516, 423, 516, 266,
  SD_CURVE_TO, 516, 100, 425, -11, 292, -11,
  SD_CURVE_TO, 232, -11, 182, 13, 148, 64,
  SD_LINE_TO, 148, 0,
 SD_CLOSE_PATH,
  SD_MOVE_TO, 147, 263,
  SD_CURVE_TO, 147, 256, 147, 252, 147, 248,
  SD_CURVE_TO, 147, 130, 199, 59, 285, 59,
  SD_CURVE_TO, 371, 59, 426, 136, 426, 258,
  SD_CURVE_TO, 426, 380, 374, 456, 289, 456,
  SD_CURVE_TO, 203, 456, 152, 384, 147, 262,
 SD_CLOSE_PATH,
SD_END_GLYPH
};

short fb_c[] =
{
SD_BEGIN_GLYPH, 499, /* U+0063 'c' */
 SD_MOVE_TO, 404, 189,
  SD_LINE_TO, 490, 178,
  SD_CURVE_TO, 470, 59, 390, -11, 274, -11,
  SD_CURVE_TO, 127, -11, 38, 88, 38, 256,
  SD_CURVE_TO, 38, 426, 127, 529, 275, 529,
  SD_CURVE_TO, 349, 529, 414, 498, 450, 448,
  SD_CURVE_TO, 454, 441, 458, 436, 461, 430,
  SD_CURVE_TO, 471, 412, 478, 395, 482, 365,
  SD_LINE_TO, 396, 352,
  SD_CURVE_TO, 378, 418, 340, 456, 278, 456,
  SD_CURVE_TO, 182, 456, 129, 384, 129, 258,
  SD_CURVE_TO, 129, 130, 179, 59, 273, 59,
  SD_CURVE_TO, 323, 59, 368, 86, 388, 128,
  SD_CURVE_TO, 390, 133, 393, 139, 395, 143,
  SD_CURVE_TO, 400, 155, 404, 165, 404, 188,
 SD_CLOSE_PATH,
SD_END_GLYPH
};

short fb_d[] =
{
SD_BEGIN_GLYPH, 555, /* U+0064 'd' */
 SD_MOVE_TO, 404, 0,
  SD_LINE_TO, 404, 65,
  SD_CURVE_TO, 370, 13, 321, -11, 259, -11,
  SD_CURVE_TO, 128, -11, 36, 99, 36, 257,
  SD_CURVE_TO, 36, 423, 121, 529, 255, 529,
  SD_CURVE_TO, 313, 529, 364, 503, 398, 457,
  SD_LINE_TO, 398, 715,
  SD_LINE_TO, 486, 715,
  SD_LINE_TO, 486, 0,
  SD_LINE_TO, 404, 0,
 SD_CLOSE_PATH,
  SD_MOVE_TO, 126, 258,
  SD_CURVE_TO, 126, 136, 181, 59, 267, 59,
  SD_CURVE_TO, 353, 59, 405, 132, 405, 250,
  SD_CURVE_TO, 405, 379, 352, 456, 263, 456,
  SD_CURVE_TO, 177, 456, 126, 382, 126, 257,
 SD_CLOSE_PATH,
SD_END_GLYPH
};

short fb_e[] =
{
SD_BEGIN_GLYPH, 555, /* U+0065 'e' */
 SD_MOVE_TO, 422, 166,
  SD_LINE_TO, 513, 155,
  SD_CURVE_TO, 483, 45, 403, -11, 285, -11,
  SD_CURVE_TO, 131, -11, 38, 87, 38, 253,
  SD_CURVE_TO, 38, 422, 132, 529, 280, 529,
  SD_CURVE_TO, 424, 529, 516, 423, 516, 259,
  SD_CURVE_TO, 516, 253, 516, 245, 516, 235,
  SD_LINE_TO, 129, 236,
  SD_CURVE_TO, 135, 125, 194, 59, 285, 59,
  SD_CURVE_TO, 353, 59, 400, 93, 422, 165,
 SD_CLOSE_PATH,
  SD_MOVE_TO, 134, 309,
  SD_LINE_TO, 423, 309,
  SD_CURVE_TO, 423, 309, 423, 310, 423, 312,
  SD_CURVE_TO, 423, 313, 423, 314, 423, 316,
  SD_CURVE_TO, 423, 334, 420, 345, 416, 356,
  SD_CURVE_TO, 414, 361, 412, 366, 410, 373,
  SD_CURVE_TO, 391, 423, 338, 456, 281, 456,
  SD_CURVE_TO, 199, 456, 139, 396, 134, 308,
 SD_CLOSE_PATH,
SD_END_GLYPH
};

short fb_f[] =
{
SD_BEGIN_GLYPH, 277, /* U+0066 'f' */
 SD_MOVE_TO, 85, 0,
  SD_LINE_TO, 85, 450,
  SD_LINE_TO, 7, 450,
  SD_LINE_TO, 7, 518,
  SD_LINE_TO, 85, 518,
  SD_LINE_TO, 85, 573,
  SD_CURVE_TO, 85, 618, 88, 638, 99, 660,
  SD_CURVE_TO, 100, 661, 100, 663, 102, 666,
  SD_CURVE_TO, 124, 706, 166, 727, 228, 727,
  SD_CURVE_TO, 236, 727, 243, 727, 251, 727,
  SD_CURVE_TO, 269, 725, 288, 724, 310, 718,
  SD_LINE_TO, 297, 642,
  SD_CURVE_TO, 282, 646, 266, 646, 252, 646,
  SD_CURVE_TO, 249, 646, 247, 646, 245, 646,
  SD_CURVE_TO, 190, 646, 172, 626, 172, 565,
  SD_LINE_TO, 172, 518,
  SD_LINE_TO, 273, 518,
  SD_LINE_TO, 273, 450,
  SD_LINE_TO, 172, 450,
  SD_LINE_TO, 172, 0,
  SD_LINE_TO, 85, 0,
 SD_CLOSE_PATH,
SD_END_GLYPH
};

#define SD_BOX 16
#define SD_BOX2 17
short* fb_all[] =
{
 fb_0, fb_1, fb_2, fb_3, fb_4,
 fb_5, fb_6, fb_7, fb_8, fb_9,
 fb_a, fb_b, fb_c, fb_d, fb_e, fb_f,
 fb_box, fb_box2x
};

/**
 * @author: Gaspar Sinai <gaspar@yudit.org>
 * @version: 2000-04-23
 * Many parts of this file are originally written by Andrew Weeks.
 */

SFontFB::SFontFB (void)
{
  tabSize = 2;
}

SFontFB::~SFontFB ()
{
}

void
SFontFB::signDraw (SCanvas* canvas, const SPen& pen, 
     const SS_Matrix2D& matrix, SIGN sign, SS_UCS4 g)
{
  /* it fails in ps printing - this fix did not help 
   * refer to SPostscript.cpp SPostscript::newpath  */
  if (sign == TAB) return;

  SS_Matrix2D r;
  SS_Matrix2D f = matrix;
  f.translate (f.x0 * double (fb_width) * double (SD_BOXR) / 100.0, 0);

  if (sign == CTRL) {
    draw (canvas, pen, matrix, g);
    return;
  }

  char a[64];
 
  snprintf (a, 64, "sign=%d\n", (int) sign);
  SString key (a);
  key.append ((long)(matrix.x0*16000.0));
  key.append ((long)(matrix.y1*16000.0));
  SColor fg = pen.getForeground();
  key.append ("fg:");
  key.append ((char)fg.red);
  key.append ((char)fg.green);
  key.append ((char)fg.blue);
  key.append ((char)fg.alpha);
//fprintf (stderr, "key=%*.*s\n", SSARGS(key));
  if (canvas->beginImage (matrix.t0, matrix.t1, key, pen.getBackground()))
  {
    canvas->endImage ();
    return;
  }
  canvas->newpath ();
  switch (sign)
  {
  case CR:
    drawOne (canvas, f, fb_cr);
    break;
  case LF:
    drawOne (canvas, f, fb_lf);
    break;
  case CRLF:
  case LFCR:
    drawOne (canvas, f, fb_crlf);
    break;
  case LS:
    drawOne (canvas, f, fb_ls);
    break;
  case FF:
    drawOne (canvas, f, fb_ff);
    break;
  case PS:
    drawOne (canvas, f, fb_ps);
    break;
  case LRM:
    drawOne (canvas, f, fb_lrm);
    break;
  case RLM:
    drawOne (canvas, f, fb_rlm);
    break;
  case FB_ZWNJ:
    drawOne (canvas, f, fb_zwnj);
    break;
  case FB_ZWJ:
    drawOne (canvas, f, fb_zwj);
    break;
  case TAB:
    break;
  case CTRL:
    break;
  }
  canvas->fill (pen);
  canvas->endImage ();
  return;
}
/**
 * Draw the non-existent glyph...
 */
void
SFontFB::draw (SCanvas* canvas, const SPen& pen, 
  const SS_Matrix2D& matrix, SS_UCS4 g)
{
  SS_Matrix2D r;
  char a[64];
  /* FIXME: you should in fact use an instance counter. */
  snprintf (a, 64, "%lx ", (unsigned long) this);
  SString key (a);
  snprintf (a, 64, "%ul;", g);
  key.append (a);
  /* Get the scale factor */
  char wh[64];
  SS_Matrix2D mtrx = canvas->getCurrentMatrix() * matrix;
  snprintf (wh, 64, "scale=%g,%g;", mtrx.x0, mtrx.y1);
  key.append (wh);
  SColor fg = pen.getForeground();
  key.append ("fg:");
  key.append ((char)fg.red);
  key.append ((char)fg.green);
  key.append ((char)fg.blue);
  key.append ((char)fg.alpha);
  if (canvas->beginImage (matrix.t0, matrix.t1, key, pen.getBackground()))
  {
    canvas->endImage ();
    return;
  }

  canvas->newpath();
  if (g<=0xff)
  {
    //r.translate (0, (ascent (r) + descent(r) + gap (r))/0.7);
    SS_Matrix2D f = matrix;
    f.translate (f.x0 * double (fb_width) * double (SD_BOXR) / 100.0, 0);
    drawOne (canvas, f, fb_box);

    r.scale (0.5, 0.9);
    r.translate (r.x0 * double (fb_width) * double (SD_BOXR) / 100.0, 2.0 * (double) SD_BOXR);

    //r.translate (0, (r.y1 * (double)fb_doffset + (double) SD_BOXR));
    SS_Matrix2D m =  f * r;
    drawOne (canvas, m, fb_all[g>>4]);
    m.translate (m.x0 * double (fb_width), 0);
    drawOne (canvas, m, fb_all[g&0x0f]);
  }
  else if (g<=0xffff)
  {
    //r.translate (0, (ascent (r) + descent(r) + gap (r))/0.7);
    SS_Matrix2D f = matrix;
    f.translate (f.x0 * double (fb_width) * double (SD_BOXR) / 100.0, 0);
    drawOne (canvas, f, fb_box);
    r.scale (0.5, 0.4);
    r.translate (r.x0 * double (fb_width) * double (SD_BOXR) / 100.0, 0);

    r.translate (0, (r.y1 * (double)fb_doffset + 2.0 * (double) SD_BOXR));
    SS_Matrix2D s = r;
    r.translate (0, (r.y1 * fb_height));

    SS_Matrix2D m =  f * r;
    drawOne (canvas, m, fb_all[(g>>12)&0x0f]);
    m.translate (m.x0 * double (fb_width), 0);
    drawOne (canvas, m, fb_all[(g>>8)&0x0f]);

    m =  f * s;
    //m.translate (0, (m.y1 * double (fb_descent)));
    drawOne (canvas, m, fb_all[(g>>4)&0x0f]);
    m.translate (m.x0 * double (fb_width), 0);
    drawOne (canvas, m, fb_all[(g)&0x0f]);
    
  }
  else
  {
    //r.translate (0, (ascent (r) + descent(r) + gap (r))/0.7);
    SS_Matrix2D f = matrix;
    f.translate (f.x0 * double (fb_width) * double (SD_BOXR) / 100.0, 0);
    drawOne (canvas, f, fb_box2x);
    r.scale (0.5, 0.4);
    r.translate (r.x0 * double (fb_width) * double (SD_BOXR) / 100.0, 0);

    r.translate (0, (r.y1 * (double)fb_doffset + 2.0 * (double) SD_BOXR));
    SS_Matrix2D s = r;
    r.translate (0, r.y1 * fb_height);

    SS_Matrix2D m =  f * r;
    drawOne (canvas, m, fb_all[(g>>28)&0x0f]);
    m.translate (m.x0 * double (fb_width), 0);
    drawOne (canvas, m, fb_all[(g>>24)&0x0f]);
    m.translate (m.x0 * double (fb_width), 0);
    drawOne (canvas, m, fb_all[(g>>20)&0x0f]);
    m.translate (m.x0 * double (fb_width), 0);
    drawOne (canvas, m, fb_all[(g>>16)&0x0f]);

    m =  f * s;
    //m.translate (0, (m.y1 * double (fb_descent)));
    drawOne (canvas, m, fb_all[(g>>12)&0x0f]);
    m.translate (m.x0 * double (fb_width), 0);
    drawOne (canvas, m, fb_all[(g>>8)&0x0f]);
    m.translate (m.x0 * double (fb_width), 0);
    drawOne (canvas, m, fb_all[(g>>4)&0x0f]);
    m.translate (m.x0 * double (fb_width), 0);
    drawOne (canvas, m, fb_all[(g)&0x0f]);
    
  }
  canvas->fill (pen);
  canvas->endImage ();
  return;
}

/**
 * Draw a glyph between 0..15
 */
void
SFontFB::drawOne (SCanvas* canvas, const SS_Matrix2D& m, short* guide)
{
  unsigned int i=0;
  //SGC short wid;
  double x, y;
  double x0, y0;
  double x1, y1;
  double x2, y2;
  while (guide[i] != SD_END_GLYPH)
  {
    switch (guide[i])
    {
    case  SD_BEGIN_GLYPH:
       //SGC wid = guide[i+1];
       i=i+2;
       break;
    case  SD_MOVE_TO:
       x = m.x0 * double (guide[i+1]) + m.y0 * double (guide[i+2]+fb_doffset) + m.t0;
       y = m.x1 * double (guide[i+1]) + m.y1 * double (guide[i+2]+fb_doffset) + m.t1;
       canvas->moveto (x, y);
       i = i + 3;
       break;

    case  SD_LINE_TO:
       x = m.x0 * double (guide[i+1]) + m.y0 * double (guide[i+2]+fb_doffset) + m.t0;
       y = m.x1 * double (guide[i+1]) + m.y1 * double (guide[i+2]+fb_doffset) + m.t1;
       canvas->lineto (x, y);
       i = i + 3;
       break;
    case  SD_CURVE_TO:
       x0 = m.x0 * double (guide[i+1]) + m.y0 * double (guide[i+2]+fb_doffset) + m.t0;
       y0 = m.x1 * double (guide[i+1]) + m.y1 * double (guide[i+2]+fb_doffset) + m.t1;
       x1 = m.x0 * double (guide[i+3]) + m.y0 * double (guide[i+4]+fb_doffset) + m.t0;
       y1 = m.x1 * double (guide[i+3]) + m.y1 * double (guide[i+4]+fb_doffset) + m.t1;
       x2 = m.x0 * double (guide[i+5]) + m.y0 * double (guide[i+6]+fb_doffset) + m.t0;
       y2 = m.x1 * double (guide[i+5]) + m.y1 * double (guide[i+6]+fb_doffset) + m.t1;
       canvas->curveto (x0, y0, x1, y1, x2, y2);
       i = i + 7;
       break;
    case  SD_CLOSE_PATH:
       canvas->closepath ();
       i = i + 1;
       break;
    case  SD_END_GLYPH:
       break;
    }
  }
}

/**
 * USE tab sizes. 
 */
double
SFontFB::signWidth (const SS_Matrix2D& matrix, SIGN s)
{
  double ws =  (s==TAB) ? 1.0 * tabSize : 1.0;
  double r =  matrix.x0  * 
      (ws * double (fb_width)  + double (fb_width) *  double (SD_BOXR) / 25);
  return (r<0.0) ? -r : r;
}

double
SFontFB::width (const SS_Matrix2D& matrix, SS_UCS4 g)
{
  double ws =  (g > 0xffff) ? 2.0  : 1.0;
  double r =  matrix.x0  * 
      (ws * double (fb_width)  + double (fb_width) *  double (SD_BOXR) / 25);
  return (r<0.0) ? -r : r;
}

double
SFontFB::width (const SS_Matrix2D& matrix) const
{
  double r =  matrix.x0 * double (fb_width)* (1.0 + double (SD_BOXR) / 50);
  return (r<0.0) ? -r : r;
}

double
SFontFB::ascent (const SS_Matrix2D& matrix) const
{
  double r =  matrix.y1 * double (fb_ascent) * (1.0 + double (SD_BOXR) / 50);
  return (r<0.0) ? -r : r;
}

double
SFontFB::descent (const SS_Matrix2D& matrix) const
{
  double r =  matrix.y1 * double (fb_descent) * (1.0 + double (SD_BOXR) / 50);
  return (r<0.0) ? -r : r;
}

double
SFontFB::gap (const SS_Matrix2D& matrix) const
{
  double r =  matrix.y1 * double (fb_gap);
  return (r<0.0) ? -r : r;
}

/* mulltiply this with size you want to get the matrix */
double
SFontFB::scale () const
{
  return fb_scale;
}
