/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
 
#include "swindow/SFontNative.h"
#include <stdio.h>

/**
 * @author: Gaspar Sinai <gaspar@yudit.org>
 * @version: 2000-06-03
 * This is a specific native font, with a certain size
 */
SFontNative::SFontNative (void)
{
}

SFontNative::~SFontNative ()
{
} 

bool
SFontNative::draw (const SString& xlfd, SCanvas* canvas, const SPen& pen, 
    const SS_Matrix2D& matrix, SS_UCS4 g)
{
  return 0;
}

bool
SFontNative::width (const SString& xlfd, SS_UCS4 g, double* width_)
{
  if (width_) *width_=0;
  return false;
}

double
SFontNative::width (const SString& xlfd)
{
  return 0.0;
}

double
SFontNative::ascent (const SString& xlfd)
{
  return 0.0;
}

double
SFontNative::descent (const SString& xlfd)
{
  return 0.0;
}

double
SFontNative::gap (const SString& xlfd)
{
  return 0.0;
}
