/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
 
#include "swindow/SPrinter.h"
#include "swindow/SPostscript.h"

/**
 * @author: Gaspar Sinai <gaspar@yudit.org>
 * @version: 2000-04-23
 * This is a postscript renderer for yudit.
 */

SPrinter::SPrinter (const SWriter& w, SType t, SMedia m, SOrientation o) 
{
  delegate = 0;
  type = t;
  switch (type)
  {
  case POSTSCRIPT:
    delegate = new SPostscript (w, (SPostscript::SMedia) m, 
      (SPostscript::SOrientation) o);
  case PCL:
  case WIN32:
    break;
  }
}

SPrinter::SPrinter (const SPrinter& printer)
{
  delegate = 0;
  type = printer.type;
  switch (type)
  {
  case POSTSCRIPT:
    delegate = new SPostscript (*((SPostscript*) (printer.delegate))); 
    break;
  case PCL:
  case WIN32:
    break;
  }
}

SPrinter::~SPrinter()
{
  if (delegate) delete delegate;
}

unsigned int
SPrinter::getWidth() const
{
  switch (type)
  {
  case POSTSCRIPT:
    return ((SPostscript*) delegate)->getWidth();
  case PCL:
  case WIN32:
    break;
  }
  return 0;
}

unsigned int
SPrinter::getHeight() const
{
  switch (type)
  {
  case POSTSCRIPT:
    return ((SPostscript*) delegate)->getHeight();
  case PCL:
  case WIN32:
    break;
  }
  return 0;
}

/**
 * return the margin stripped X corner.
 */
int
SPrinter::getX() const
{
  switch (type)
  {
  case POSTSCRIPT:
    return ((SPostscript*) delegate)->getX();
  case PCL:
  case WIN32:
    break;
  }
  return 0;
}

/**
 * return the margin stripped Y corner.
 */
int
SPrinter::getY() const
{
  switch (type)
  {
  case POSTSCRIPT:
    return ((SPostscript*) delegate)->getY();
  case PCL:
  case WIN32:
    break;
  }
  return 0;
}

/**
 * Print out the postscript prolog
 * This will by default turn on caching.
 */
bool
SPrinter::open (bool background)
{
  if (delegate) switch (type)
  {
  case POSTSCRIPT:
    return ((SPostscript*) delegate)->open(background);
  case PCL:
  case WIN32:
    break;
  }
  return false;
}

void 
SPrinter::newPage()
{
  if (delegate) switch (type)
  {
  case POSTSCRIPT:
    ((SPostscript*) delegate)->newPage();
    break;
  case PCL:
  case WIN32:
    break;
  }
  return;
}

bool
SPrinter::cacheOn (bool on)
{
  if (delegate) switch (type)
  {
  case POSTSCRIPT:
    return ((SPostscript*) delegate)->cacheOn (on);
  case PCL:
  case WIN32:
    break;
  }
  return false;
}

/**
 * Print out the ending lines.
 */
bool
SPrinter::close ()
{
  if (delegate) switch (type)
  {
  case POSTSCRIPT:
    return ((SPostscript*) delegate)->close();
    break;
  case PCL:
  case WIN32:
    break;
  }
  return false;
}

/*
 * The  Printing instructions follow:
 */
bool
SPrinter::beginImage (double x, double y, const SString& id, const SColor& background)
{
  if (delegate) return delegate->beginImage (x, y, id, background);
  return false;
}
void
SPrinter::endImage ()
{
  if (delegate) delegate->endImage ();
}

void
SPrinter::newpath ()
{
  if (delegate) delegate->newpath ();
}

void
SPrinter::stroke (const SPen& pen)
{
  if (delegate) delegate->stroke (pen);
}

void
SPrinter::fill (const SPen& pen)
{
  if (delegate) delegate->fill (pen);
}

void
SPrinter::moveto (double _x, double _y)
{
  if (delegate) delegate->moveto (_x, _y);
}

void
SPrinter::lineto (double _x, double _y)
{
  if (delegate) delegate->lineto (_x, _y);
}

void
SPrinter::curveto (double _x0, double _y0, double _x1, double _y1, double _x2, double _y2)
{
  if (delegate) delegate->curveto (_x0, _y0, _x1, _y1, _x2, _y2);
}

void
SPrinter::closepath()
{
  if (delegate) delegate->closepath ();
}


void
SPrinter::pushmatrix()
{
  if (delegate) delegate->pushmatrix ();
}

void
SPrinter::popmatrix()
{
  if (delegate) delegate->popmatrix ();
}

void
SPrinter::scale (double x, double y)
{
  if (delegate) switch (type)
  {
  case POSTSCRIPT:
    ((SPostscript*)delegate)->scale (x, y);
    break;
  case PCL:
  case WIN32:
    break;
  }
}

void
SPrinter::translate (double x, double y)
{
  if (delegate) delegate->translate (x, y);
}

void
SPrinter::rotate (double angle)
{
  if (delegate) delegate->rotate (angle);
}

void
SPrinter::bitfont (const SPen& pen, double x, double y, 
       void* native, char* data, unsigned int len)
{
  if (delegate) delegate->bitfont (pen, x, y, native, data, len);
}

/**
 * Fill a solid rectangle
 * @param x is the upper left corner
 * @param y is the upper top corner
 * @param width is the width of the region to fill
 * @param height is the height of the region to fill
 */
void
SPrinter::bitfill (const SColor& bg, int x, int y, 
 unsigned int width, unsigned int height)
{
}

/**
 * Draw a solid line.
 * @param x is the starting x point
 * @param y is the starting y point
 * @param x is the ending non-exclusive  x point
 * @param y is the ending non-exclusive  y point
 */
void
SPrinter::bitline (const SColor& fg, int x, int y, int tox, int toy)
{
}

/**
 * Draw a solid line.
 * @param x is the x point
 * @param y is the y point
 */
void
SPrinter::bitpoint (const SColor& fg, int x, int y)
{
  if (delegate) delegate->bitpoint (SColor("black"), x, y);
}

void
SPrinter::bitpoints (const SColor& fg, const int* x, const int* y, 
         unsigned int _size)
{
  if (delegate) delegate->bitpoints (SColor("black"), x, y, _size);
}

void
SPrinter::fill ()
{
  if (delegate) delegate->fill (SPen(SColor("black")));
}

void
SPrinter::stroke ()
{
  if (delegate) delegate->stroke (SPen(SColor("black")));
}

SString
SPrinter::getCreationDate () const
{
  if (delegate) switch (type)
  {
  case POSTSCRIPT:
    return ((SPostscript*)delegate)->getCreationDate ();
  case PCL:
  case WIN32:
    break;
  }
  return SString();
}

/**
 * return true if printed output used native fonts.
 */
bool
SPrinter::hasNative() const
{
  if (delegate) switch (type)
  {
  case POSTSCRIPT:
    return ((SPostscript*)delegate)->hasNative ();
  case PCL:
  case WIN32:
    break;
  }
  return false;
}

SS_Matrix2D
SPrinter::getCurrentMatrix() const
{
  if (delegate) {
    return delegate->getCurrentMatrix();
  }
  return SS_Matrix2D();;
}
void
SPrinter::putImage (int x, int y, const SImage& image)
{
  if (delegate) {
    delegate->putImage(x, y, image);
  }
}
void
SPrinter::setBackground(const SColor &color)
{
  if (delegate) {
     delegate->setBackground (color);
  }
}

