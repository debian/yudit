/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
 

#include "swindow/SWindow.h"

/**
 * make an ID for a button from string
 */
int
getButtonID (const SString& string)
{
  if (string.size()==0) return -1;
  switch (string[0])
  {
    case 'A': return SWindowListener::Key_A;
    case 'a': return SWindowListener::Key_a;
    case 'B': return SWindowListener::Key_B;
    case 'b': return SWindowListener::Key_b;
    case 'C': return SWindowListener::Key_C;
    case 'c': return SWindowListener::Key_c;
    case 'D': return SWindowListener::Key_D;
    case 'd': return SWindowListener::Key_d;
    case 'E': return SWindowListener::Key_E;
    case 'e': return SWindowListener::Key_e;
    case 'F': return SWindowListener::Key_F;
    case 'f': return SWindowListener::Key_f;
    case 'G': return SWindowListener::Key_G;
    case 'g': return SWindowListener::Key_g;
    case 'H': return SWindowListener::Key_H;
    case 'h': return SWindowListener::Key_h;
    case 'I': return SWindowListener::Key_I;
    case 'i': return SWindowListener::Key_i;
    case 'J': return SWindowListener::Key_J;
    case 'j': return SWindowListener::Key_j;
    case 'K': return SWindowListener::Key_K;
    case 'k': return SWindowListener::Key_k;
    case 'L': return SWindowListener::Key_L;
    case 'l': return SWindowListener::Key_l;
    case 'M': return SWindowListener::Key_M;
    case 'm': return SWindowListener::Key_m;
    case 'N': return SWindowListener::Key_N;
    case 'n': return SWindowListener::Key_n;
    case 'O': return SWindowListener::Key_O;
    case 'o': return SWindowListener::Key_o;
    case 'P': return SWindowListener::Key_P;
    case 'p': return SWindowListener::Key_p;
    case 'Q': return SWindowListener::Key_Q;
    case 'q': return SWindowListener::Key_q;
    case 'R': return SWindowListener::Key_R;
    case 'r': return SWindowListener::Key_r;
    case 'S': return SWindowListener::Key_S;
    case 's': return SWindowListener::Key_s;
    case 'T': return SWindowListener::Key_T;
    case 't': return SWindowListener::Key_t;
    case 'U': return SWindowListener::Key_U;
    case 'u': return SWindowListener::Key_u;
    case 'V': return SWindowListener::Key_V;
    case 'v': return SWindowListener::Key_v;
    case 'W': return SWindowListener::Key_W;
    case 'w': return SWindowListener::Key_w;
    case 'X': return SWindowListener::Key_X;
    case 'x': return SWindowListener::Key_x;
    case 'Y': return SWindowListener::Key_Y;
    case 'y': return SWindowListener::Key_y;
    case 'Z': return SWindowListener::Key_Z;
    case 'z': return SWindowListener::Key_z;
    case '/': return SWindowListener::Key_slash;	    
    case '.': return SWindowListener::Key_period;	    
    case ',': return SWindowListener::Key_comma;	    
    case ';': return SWindowListener::Key_semicolon;	    
    case ':': return SWindowListener::Key_colon;	    
    case '_': return SWindowListener::Key_underscore;	    
    case '-': return SWindowListener::Key_hash;	    
    case '?': return SWindowListener::Key_questionmark;	    
    case '1': return SWindowListener::Key_1;	    
    case '2': return SWindowListener::Key_2;	    
    case '3': return SWindowListener::Key_3;	    
    case '4': return SWindowListener::Key_4;	    
    case '5': return SWindowListener::Key_5;	    
    case '6': return SWindowListener::Key_6;	    
    case '7': return SWindowListener::Key_7;	    
    case '8': return SWindowListener::Key_8;	    
    case '9': return SWindowListener::Key_9;	    
    case '0': return SWindowListener::Key_0;	    
  }
  return -1;
}

