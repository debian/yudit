/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
 
#ifndef SEventOSX_h
#define SEventOSX_h

#include <stoolkit/SEvent.h>
#include <stoolkit/SEventBSD.h>

class SClientOSX : public SClient 
{
public:
 SClientOSX (SEventSource* s, SEventTarget* t, ClientType type);
  ~SClientOSX(); 
 void* cocoaData;
private:
 void* cocoaTimer;
};

typedef SBinHashtable<SClientOSX*> SClientOSXHashtable;
typedef SBinVector<SClientOSX*> SClientOSXVector;

class SEventOSX : public SEventHandlerImpl
{
public:
  SEventOSX(void);
  virtual ~SEventOSX();
  /* One source can have only one target! Same target
   * can be used for more source */
  virtual void addJob (SJob* s, SEventTarget* t);
  virtual void addServer (SServerStream* s, SEventTarget* t);
  virtual void addTimer (STimer* s, SEventTarget* t);
  virtual void addInput (SInputStream* s, SEventTarget* t);

  virtual void addOutput (SOutputStream* s, SEventTarget* t, const SString& m);

  virtual void remove (SEventTarget* t);
  virtual void remove (SEventSource* s);

  // Send async write back to SEventSource
  virtual void start();
  virtual void exit();
  virtual bool next();

  bool fireTimer (SClientOSX* s);

private:
  int process (SClientOSX* s);
  void remove (SClientOSX* s);
    /* using pointer */
  SClientOSXHashtable      sourceHashtable;
  SClientOSXHashtable      targetHashtable;
  /* using id */
  SClientOSXHashtable      clientHashtable;
};

#endif /* SEventOSX_h */
