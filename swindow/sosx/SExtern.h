/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
 
#ifndef SExtern_h
#define SExtern_h

#include <swindow/SColor.h>
#include <swindow/SPen.h>
#include <swindow/SImage.h>
#include <swindow/SWindow.h>
#include <swindow/SAccelerator.h>
#include <swindow/sosx/SEventOSX.h>

extern bool cocoaInit ();
extern void cocoaRun ();
extern void * cocoaCreateView (const char* name);
extern void cocoaSetCPPWindow (void* view, void* cppWindow);

extern void cocoaShow (void* view);
extern void cocoaHide (void* view);
extern void cocoaSetParent (void* view, void*parent);
extern void cocoaSetSize (void* view, unsigned int width, unsigned int height);
extern void cocoaSetPosition (void* view, int x, int y);
extern void cocoaCenter (void* view, void* parent);

extern void cocoaSetTitle (void* view, const char* name);
extern void cocoaSetBackground (void* view, unsigned char red, unsigned char green, unsigned char blue, unsigned char alpha);
extern void cocoaBitline (void* view, const SColor& fg, int x, int y, int tox, int toy);
extern void cocoaBitpoint (void* view, const SColor& fg, int x, int y);
extern void cocoaBitpoints (void* view, const SColor& fg, const int* x, const int* y, unsigned int size);
// return true if cached and only fill will be used.
extern bool cocoaBeginImage (void* view, double _x, double _y, const char* _id);
extern void cocoaNewPath (void* view);
extern void cocoaEndImage (void* view);
extern void cocoaMoveTo (void* view, double _x, double _y);
extern void cocoaLineTo (void* view, double _x, double _y);
extern void cocoaFill (void* view, const SPen& pen);
extern void cocoaStroke (void* view, const SPen& pen);
extern void cocoaClosePath (void* view);
extern void cocoaCurveTo (void* view, double _x0, double _y0, double _x1, 
  double _y1, double _x2, double _y2);
extern void* cocoaCreateImage (void* view, const SImage& im);
extern void cocoaDrawImage (void* view, int _x, int _y, void* im);
extern void cocoaSetClippingRectangle (void* view, int x, int y, unsigned int width, unsigned int height);
extern void cocoaRemoveClippingRectangle (void* view);
extern void cocoaBitfill (void* view, const SColor& bg, int x, int y, unsigned int width, unsigned int height);
extern void cocoaNeedsDisplay (void* view);
extern void cocoaSetModal (void* view, void* parent, bool decorated);
extern void cocoaWait (void* view);
extern void cocoaCountWindows (void* view, int count);
extern void cocoaExitApplication ();
// Returns cocoaData.
extern void* cocoaAddTimer (SEventOSX* eventHandler, SClientOSX* client, float repeatTimeout);
extern void cocoaRemoveTimer (void* cocoaData);
extern void cocoaGetKeyboardFocus (void* view);
extern const char* cocoaGetClipboard ();
extern void cocoaPutClipboard(void * view, const char* utf8);
extern void cocoaRegisterDroppable (void * view, char** droppable);
extern void cocoaAddAccelerator (void* view, const SAccelerator& a, SAcceleratorListener* l);
extern void cocoaRemoveAccelerator (void* view, const SAccelerator& a, SAcceleratorListener* l);
extern void cocoaStartMacKeyboard (void* view, SPreEditor* preEditor);
extern void cocoaStopMacKeyboard (void* view);
extern bool cocoaCommitMacKeyboard (void* view);
extern void cocoaSetMinimumSize (void* view, unsigned int width, unsigned int height);


#endif /* SExtern_h */
