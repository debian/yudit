/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#import <objc/objc-runtime.h>
#import <AppKit/Appkit.h>
#import <Foundation/Foundation.h>
#import <swindow/sosx/SKeyboard.h>
// Code from https://forums.macrumors.com/threads/nsevent-keycode-list.780577/
/* 
 *  Summary:
 *    Virtual keycodes
 *  
 *  Discussion:
 *    These constants are the virtual keycodes defined originally in
 *    Inside Mac Volume V, pg. V-191. They identify physical keys on a
 *    keyboard. Those constants with "ANSI" in the name are labeled
 *    according to the key position on an ANSI-standard US keyboard.
 *    For example, kVK_ANSI_A indicates the virtual keycode for the key
 *    with the letter 'A' in the US keyboard layout. Other keyboard
 *    layouts may have the 'A' key label on a different physical key;
 *    in this case, pressing 'A' will generate a different virtual
 *    keycode.
 */
enum {
  kVK_ANSI_A                    = 0x00,
  kVK_ANSI_S                    = 0x01,
  kVK_ANSI_D                    = 0x02,
  kVK_ANSI_F                    = 0x03,
  kVK_ANSI_H                    = 0x04,
  kVK_ANSI_G                    = 0x05,
  kVK_ANSI_Z                    = 0x06,
  kVK_ANSI_X                    = 0x07,
  kVK_ANSI_C                    = 0x08,
  kVK_ANSI_V                    = 0x09,
  kVK_ANSI_B                    = 0x0B,
  kVK_ANSI_Q                    = 0x0C,
  kVK_ANSI_W                    = 0x0D,
  kVK_ANSI_E                    = 0x0E,
  kVK_ANSI_R                    = 0x0F,
  kVK_ANSI_Y                    = 0x10,
  kVK_ANSI_T                    = 0x11,
  kVK_ANSI_1                    = 0x12,
  kVK_ANSI_2                    = 0x13,
  kVK_ANSI_3                    = 0x14,
  kVK_ANSI_4                    = 0x15,
  kVK_ANSI_6                    = 0x16,
  kVK_ANSI_5                    = 0x17,
  kVK_ANSI_Equal                = 0x18,
  kVK_ANSI_9                    = 0x19,
  kVK_ANSI_7                    = 0x1A,
  kVK_ANSI_Minus                = 0x1B,
  kVK_ANSI_8                    = 0x1C,
  kVK_ANSI_0                    = 0x1D,
  kVK_ANSI_RightBracket         = 0x1E,
  kVK_ANSI_O                    = 0x1F,
  kVK_ANSI_U                    = 0x20,
  kVK_ANSI_LeftBracket          = 0x21,
  kVK_ANSI_I                    = 0x22,
  kVK_ANSI_P                    = 0x23,
  kVK_ANSI_L                    = 0x25,
  kVK_ANSI_J                    = 0x26,
  kVK_ANSI_Quote                = 0x27,
  kVK_ANSI_K                    = 0x28,
  kVK_ANSI_Semicolon            = 0x29,
  kVK_ANSI_Backslash            = 0x2A,
  kVK_ANSI_Comma                = 0x2B,
  kVK_ANSI_Slash                = 0x2C,
  kVK_ANSI_N                    = 0x2D,
  kVK_ANSI_M                    = 0x2E,
  kVK_ANSI_Period               = 0x2F,
  kVK_ANSI_Grave                = 0x32,
  kVK_ANSI_KeypadDecimal        = 0x41,
  kVK_ANSI_KeypadMultiply       = 0x43,
  kVK_ANSI_KeypadPlus           = 0x45,
  kVK_ANSI_KeypadClear          = 0x47,
  kVK_ANSI_KeypadDivide         = 0x4B,
  kVK_ANSI_KeypadEnter          = 0x4C,
  kVK_ANSI_KeypadMinus          = 0x4E,
  kVK_ANSI_KeypadEquals         = 0x51,
  kVK_ANSI_Keypad0              = 0x52,
  kVK_ANSI_Keypad1              = 0x53,
  kVK_ANSI_Keypad2              = 0x54,
  kVK_ANSI_Keypad3              = 0x55,
  kVK_ANSI_Keypad4              = 0x56,
  kVK_ANSI_Keypad5              = 0x57,
  kVK_ANSI_Keypad6              = 0x58,
  kVK_ANSI_Keypad7              = 0x59,
  kVK_ANSI_Keypad8              = 0x5B,
  kVK_ANSI_Keypad9              = 0x5C
};

/* keycodes for keys that are independent of keyboard layout*/
enum {
  kVK_Return                    = 0x24,
  kVK_Tab                       = 0x30,
  kVK_Space                     = 0x31,
  kVK_Delete                    = 0x33,
  kVK_Escape                    = 0x35,
  kVK_Command                   = 0x37,
  kVK_Shift                     = 0x38,
  kVK_CapsLock                  = 0x39,
  kVK_Option                    = 0x3A,
  kVK_Control                   = 0x3B,
  kVK_RightShift                = 0x3C,
  kVK_RightOption               = 0x3D,
  kVK_RightControl              = 0x3E,
  kVK_Function                  = 0x3F,
  kVK_F17                       = 0x40,
  kVK_VolumeUp                  = 0x48,
  kVK_VolumeDown                = 0x49,
  kVK_Mute                      = 0x4A,
  kVK_F18                       = 0x4F,
  kVK_F19                       = 0x50,
  kVK_F20                       = 0x5A,
  kVK_F5                        = 0x60,
  kVK_F6                        = 0x61,
  kVK_F7                        = 0x62,
  kVK_F3                        = 0x63,
  kVK_F8                        = 0x64,
  kVK_F9                        = 0x65,
  kVK_F11                       = 0x67,
  kVK_F13                       = 0x69,
  kVK_F16                       = 0x6A,
  kVK_F14                       = 0x6B,
  kVK_F10                       = 0x6D,
  kVK_F12                       = 0x6F,
  kVK_F15                       = 0x71,
  kVK_Help                      = 0x72,
  kVK_Home                      = 0x73,
  kVK_PageUp                    = 0x74,
  kVK_ForwardDelete             = 0x75,
  kVK_F4                        = 0x76,
  kVK_End                       = 0x77,
  kVK_F2                        = 0x78,
  kVK_PageDown                  = 0x79,
  kVK_F1                        = 0x7A,
  kVK_LeftArrow                 = 0x7B,
  kVK_RightArrow                = 0x7C,
  kVK_DownArrow                 = 0x7D,
  kVK_UpArrow                   = 0x7E
};

/* ISO keyboards only*/
enum {
  kVK_ISO_Section               = 0x0A
};

/* JIS keyboards only*/
enum {
  kVK_JIS_Yen                   = 0x5D,
  kVK_JIS_Underscore            = 0x5E,
  kVK_JIS_KeypadComma           = 0x5F,
  kVK_JIS_Eisu                  = 0x66,
  kVK_JIS_Kana                  = 0x68
};

static const int keyCodeMap[] = {
    
    [kVK_RightControl] =SWindowListener::Key_Control_R,
    [kVK_Control] =SWindowListener::Key_Control_L,
    //Key_Alt_L, 
    //Key_Alt_R,
    [kVK_Command] =SWindowListener::Key_Meta_L,
    //Key_Meta_R,
    [kVK_Shift] =SWindowListener::Key_Shift_L,
    [kVK_RightShift] =SWindowListener::Key_Shift_R,

    [kVK_Tab] =SWindowListener::Key_Tab,
    [kVK_Space] =SWindowListener::Key_Space,

    [kVK_LeftArrow] =SWindowListener::Key_Left,
    [kVK_RightArrow] =SWindowListener::Key_Right,
    [kVK_UpArrow] =SWindowListener::Key_Up,
    [kVK_DownArrow] =SWindowListener::Key_Down,

    // Key_Prior,
    [kVK_End] =SWindowListener::Key_End,
    [kVK_Return] =SWindowListener::Key_Return,
    // Key_Next,
    [kVK_ANSI_KeypadEnter] =SWindowListener::Key_Enter,
    // Key_Enter,
    [kVK_Home] =SWindowListener::Key_Home,
    [kVK_Delete] =SWindowListener::Key_BackSpace,
    [kVK_ForwardDelete] =SWindowListener::Key_Delete,
    [kVK_ANSI_KeypadClear] =SWindowListener::Key_Clear,
    [kVK_Escape] =SWindowListener::Key_Escape,
    //Key_Send,

    [kVK_F1] =SWindowListener::Key_F1,
    [kVK_F2] =SWindowListener::Key_F2,
    [kVK_F3] =SWindowListener::Key_F3,
    [kVK_F4] =SWindowListener::Key_F4,
    [kVK_F5] =SWindowListener::Key_F5,
    [kVK_F6] =SWindowListener::Key_F6,
    [kVK_F7] =SWindowListener::Key_F7,
    [kVK_F8] =SWindowListener::Key_F8,
    [kVK_F9] =SWindowListener::Key_F9,
    [kVK_F10] =SWindowListener::Key_F10,
    [kVK_F11] =SWindowListener::Key_F11,
    [kVK_F12] =SWindowListener::Key_F12,

    //Key_a,
    [kVK_ANSI_A] =SWindowListener::Key_A,
    // Key_b,
    [kVK_ANSI_B] =SWindowListener::Key_B,
    // Key_c,
    [kVK_ANSI_C] =SWindowListener::Key_C,
    // Key_d,
    [kVK_ANSI_D] =SWindowListener::Key_D,
    // Key_e,
    [kVK_ANSI_E] =SWindowListener::Key_E,
    // Key_f,
    [kVK_ANSI_F] =SWindowListener::Key_F,
    // Key_g,
    [kVK_ANSI_G] =SWindowListener::Key_G,
    // Key_h,
    [kVK_ANSI_H] =SWindowListener::Key_H,
    // Key_i,
    [kVK_ANSI_I] =SWindowListener::Key_I,
    // Key_j,
    [kVK_ANSI_J] =SWindowListener::Key_J,
    // Key_k,
    [kVK_ANSI_K] =SWindowListener::Key_K,
    // Key_l,
    [kVK_ANSI_L] =SWindowListener::Key_L,
    // Key_m,
    [kVK_ANSI_M] =SWindowListener::Key_M,
    // Key_n,
    [kVK_ANSI_N] =SWindowListener::Key_N,
    // Key_o,
    [kVK_ANSI_O] =SWindowListener::Key_O,
    // Key_p,
    [kVK_ANSI_P] =SWindowListener::Key_P,
    // Key_q,
    [kVK_ANSI_Q] =SWindowListener::Key_Q,
    // Key_r,
    [kVK_ANSI_R] =SWindowListener::Key_R,
    // Key_s,
    [kVK_ANSI_S] =SWindowListener::Key_S,
    // Key_t,
    [kVK_ANSI_T] =SWindowListener::Key_T,
    // Key_u,
    [kVK_ANSI_U] =SWindowListener::Key_U,
    // Key_x,
    [kVK_ANSI_X] =SWindowListener::Key_X,
    // Key_y,
    [kVK_ANSI_Y] =SWindowListener::Key_Y,
    // Key_v,
    [kVK_ANSI_V] =SWindowListener::Key_V,
    // Key_w,
    [kVK_ANSI_W] =SWindowListener::Key_W,
    // Key_z,
    [kVK_ANSI_Z] =SWindowListener::Key_Z,

    [kVK_ANSI_Slash] =SWindowListener::Key_slash,
    [kVK_ANSI_Period] =SWindowListener::Key_period,
    [kVK_ANSI_Comma] =SWindowListener::Key_comma,
    [kVK_ANSI_Semicolon] =SWindowListener::Key_semicolon,
    // Key_colon,
    [kVK_JIS_Underscore] =SWindowListener::Key_underscore,
    // Key_hash,
    // Key_questionmark,

    [kVK_ANSI_1] =SWindowListener::Key_1,
    [kVK_ANSI_Keypad1] =SWindowListener::Key_1,
    [kVK_ANSI_2] =SWindowListener::Key_2,
    [kVK_ANSI_Keypad2] =SWindowListener::Key_2,
    [kVK_ANSI_3] =SWindowListener::Key_3,
    [kVK_ANSI_Keypad3] =SWindowListener::Key_3,
    [kVK_ANSI_4] =SWindowListener::Key_4,
    [kVK_ANSI_Keypad4] =SWindowListener::Key_4,
    [kVK_ANSI_5] =SWindowListener::Key_5,
    [kVK_ANSI_Keypad5] =SWindowListener::Key_5,
    [kVK_ANSI_6] =SWindowListener::Key_6,
    [kVK_ANSI_Keypad6] =SWindowListener::Key_6,
    [kVK_ANSI_7] =SWindowListener::Key_7,
    [kVK_ANSI_Keypad7] =SWindowListener::Key_7,
    [kVK_ANSI_8] =SWindowListener::Key_8,
    [kVK_ANSI_Keypad8] =SWindowListener::Key_8,
    [kVK_ANSI_9] =SWindowListener::Key_9,
    [kVK_ANSI_Keypad9] =SWindowListener::Key_9,
    [kVK_ANSI_0] =SWindowListener::Key_0,
    [kVK_ANSI_Keypad0] =SWindowListener::Key_0,
};

SWindowListener::SKey convertKeyCode (UInt16 macCode) {
    int length = (sizeof keyCodeMap) / (sizeof keyCodeMap[0]);
    if (length <= macCode) {
        return SWindowListener::Key_Undefined;
    }
    return (SWindowListener::SKey) keyCodeMap[macCode];
}
