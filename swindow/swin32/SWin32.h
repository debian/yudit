/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
 
#ifndef SW32_h
#define SW32_h

#include "stoolkit/SExcept.h"
#include "swindow/SAwt.h"
#include "swindow/SWindow.h"
#include "stoolkit/SString.h"
#include "stoolkit/SEncoder.h"
#include "stoolkit/SBinHashtable.h"

/**
 * @author: Gaspar Sinai <gaspar@yudit.org>
 * @version: 2000-04-23
 * This is the abstract widget toolkit
 */


/**
 * This class should be implemented for a window toolkit implementation
 */
class SW32Impl : public SAwtImpl
{
public:
  SW32Impl();
  virtual ~SW32Impl();
  virtual SWindow* getWindow (SWindowListener* l, const SString& name);
  virtual bool isOK();
  virtual void setEncoding(const SString& str);
  SEncoder encoder;
};

class SW32Window : public SWindow
{
public:
  SW32Window(const SString& n, SW32Impl* i, long _id);
  virtual ~SW32Window();
  virtual bool isShown () { return shown; }
  virtual bool isModal () { return modalFlag; }
  virtual void setModal (SWindow*w, bool decorated);

  /* windowing */
  virtual void show ();
  virtual void hide ();
  virtual void wait ();
  virtual void center (SWindow* window);

  virtual void setParent (SWindow* w, int x, int y);
  void redraw (bool clear, int x, int y, unsigned int width, unsigned int height);
  virtual void resize (unsigned int width, unsigned int height);
  virtual void setMinimumSize (unsigned int _width, unsigned int _height);

  virtual void move (int x, int y);
  virtual void setTitle (const SString& title);

  virtual void setBackground (const SColor &color);
  virtual void clear (int x, int y, unsigned int width, unsigned int height);

  virtual void copy (int x, int y, unsigned int width, unsigned int height, int tox, int toy);

  /* clip */
  virtual void setClippingArea (int x, int y, 
      unsigned int width, unsigned int height);
  virtual void removeClippingArea ();


  virtual SString  getClipUTF8(bool isPrimary=true);
  virtual void putClipUTF8(const SString& utf8, bool isPrimary=true);

  virtual void getKeyboardFocus();
  virtual void addAccelerator (const SAccelerator& a, 
      SAcceleratorListener* l);
  virtual void removeAccelerator (const SAccelerator& a,
      SAcceleratorListener* l);
  /* x11,mac */
  virtual void setDroppable (const SStringVector& targets) {}

  virtual bool startInputMethod (const SString& name, const SProperties& prop, SPreEditor* preEditor);
  virtual void stopInputMethod ();
  virtual void setInputMethodProperties (const SProperties& prop);
  virtual SString getInputMethod ();

  /*---------------- from canvas --------------*/
  virtual bool beginImage (double x, double y, const SString& id, const SColor& background);
  virtual void newpath ();
  virtual void endImage ();
  virtual void fill (const SPen& pen);
  virtual void stroke (const SPen& pen);

  virtual void moveto (double x, double y);
  virtual void lineto (double x, double y);
  virtual void curveto (double x0, double y0, 
      double x1, double y1, double x3, double y3);
  virtual void closepath();
  virtual void pushmatrix();
  virtual void popmatrix();
  virtual void scale (double x, double y);
  virtual void translate (double x, double y);
  virtual void rotate (double angle);
  virtual bool isVisible ();

  /* SCanvas virtual. */
  virtual bool cacheOn (bool on=true);
  virtual SS_Matrix2D  getCurrentMatrix() const;

  virtual void putImage (int x, int y, const SImage& image);
  virtual void bitfill (const SColor& bg, int x, int y, 
        unsigned int width, unsigned int height);
  virtual void bitline (const SColor& fg, int x, int y, int tox, int toy);
  virtual void bitpoint (const SColor& fg, int x, int y);
  virtual void bitpoints (const SColor& fg, const int* x, const int* y, 
         unsigned int size);

  static void setPixmapCacheSize(unsigned int _size);
  static void setPixmapCacheOn (bool _on);
   /* helper */
  void repaintBackground(int left, int top, int right, int bottom);
  long getID () { return id; }

  bool clipChained;
  long clipChain;

  /* only for top level windows */
  long currentFocusWindow;
  long modalID;
  long parentID;
  /* only top level ones will have it. */
  SBinHashtable<long>   acceleratorTable;
  SBinHashtable <SAcceleratorListener*> accelerators;

  /*--- get an integer id for the underlying window ---*/
  virtual unsigned long getWindowID() const;
  // default off
  virtual void setDoubleBuffer (bool isOn);
  virtual bool isDoubleBufferEnabled () const;

  bool dbufferOn; // double buffer
  void* dbuffer;
  long id;
  void* cdc;
  SColor background;
  int currentScroll;

private:
  bool dcin();
  void dcout(bool wasin);

  void*   clipRegion;
  SRasterizer*     engine;
  SPen   pen;
  SW32Impl* impl;
  SString name;
  SString imname;

protected:
  /* From canvas. */
  bool isCacheOn;
  /* common */
  bool    shown;
  bool    modalFlag;

  int     xpos;
  int     ypos;
  unsigned int xwidth;
  unsigned int yheight;

public:
  /* mac input */
  virtual bool commitInputMethod() {return false;}
  /* native x11 font */
  virtual void bitfont (const SPen& pen, double x, double y, void* native, char* data, unsigned int len) {}
  /* x11 only */
  virtual void setApplicationImage (const SImage& im) {};
  /* return true if input method has a status area. */
  virtual bool hasStatusArea () { return false; }

  void setSize (unsigned int _width, unsigned int _height) {
    xwidth =  _width;
    yheight = _height;
  }
  void setPosition (int _posx, int _posy) {
    xpos = _posx;
    ypos = _posy;
  }
  virtual unsigned int getWidth() const {
    return xwidth;
  }
  virtual unsigned int getHeight() const {
    return yheight;
  }
  virtual int getPositionX() const {
    return xpos;
  }
  virtual int getPositionY() const {
    return ypos;
  }


};

#endif /* SW32_h */
