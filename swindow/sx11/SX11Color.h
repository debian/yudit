/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
 
#ifndef SX11Color_h
#define SX11Color_h

#include "swindow/SColor.h"
#include "swindow/sx11/SX11Impl.h"

/**
 * @author: Gaspar Sinai <gaspar@yudit.org>
 * @version: 2000-04-23
 * This is the abstract widget toolkit
 */


class SX11Color : public SColor
{
public:
  SX11Color (SX11Impl* impl, SS_WORD32 value);
  SX11Color (SX11Impl* impl, double red, double green, double blue, double alpha);
  SX11Color (SX11Impl* impl, unsigned char red, unsigned char green, unsigned char blue, unsigned char alpha);
  SX11Color (const SX11Color& color);
  virtual void blend  (const SColor& color);

  SX11Color (SX11Impl* impl,const SColor& color);
  SX11Color operator= (const SX11Color& color);
  virtual ~SX11Color();
  SS_WORD32 getValue() const;
  SPixel    getPixelValue() const;
protected:
  SS_WORD32 allocate(SX11Impl* impl);
  SPixel    pixel;
  SX11Impl* impl;
};

#endif /* SX11Color_h */
