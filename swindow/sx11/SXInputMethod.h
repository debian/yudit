/** 
 *  Yudit Unicode Editor Source File
 *
 *  GNU Copyright (C) 1997-2023  Gaspar Sinai <gaspar@yudit.org>  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, version 2,
 *  dated June 1991. See file COPYYING for details.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
 
#ifndef SXInputMethod_h
#define SXInputMethod_h

#include "stoolkit/SString.h"
#include "stoolkit/SBinVector.h"
#include "stoolkit/SProperties.h"
#include "swindow/sx11/SX11Impl.h"
#include "swindow/sx11/SXEventHandler.h"

class SXInputMethod : public SXEventHandler
{
public:
  SXInputMethod (SX11Impl* impl, long id, long eventmask); 
  virtual ~SXInputMethod();

  bool start (const SString& name, const SProperties& props);
  void stop ();
  void setProperties ( const SProperties& props);
  const SString& getName();
  bool hasStatusArea ();

  /* SXEventHandler */
  virtual bool  handleEvent (const XEvent& event);
  bool isAsciiInput ();
  bool isKInput ();

  XIC getIC ();
  const SString& getICEncoding () const;


private:
  SString     icEncoding;
  long        eventMask;
  bool        isAscii;
  XIC         xic;
  XIM         xim;
  bool        statusAreaOK;

  bool        createIC (const SString& str, const SProperties& props);

  Atom        toAtom (const SString& str);
  Window      getOwner (Atom atom);

  bool        sendEvent (const SString& type, Window w,
                long p0 ,long p1=0, long p2=0, long p3=0, long p4=0);

  bool        setKinputAttributes (const SProperties& properties);
  bool        handlePropertyNotify (const XEvent& event);

  SString     name;
  SString      inputStyle;
  Atom        kproperty; /* do I need this ? */
  Window      conversionOwner;
  SX11Impl*   impl;
  long        id;
  long        clientWindow;
};

/**------------- THIS IS KINPUT2 ------------------**/

#define ATOM_KINPUT2 "_JAPANESE_CONVERSION"

#define CONV_ATTR(code,len) ((unsigned long)((code)<<16)+(len))
#define UPPER16U(data)      (((data)>>16)&0xffff)
#define UPPER16S(data)      ((short)(((data)>>16)&0xffff))
#define LOWER16U(data)      ((data)&0xffff)
#define LOWER16S(data)      ((short)((data)&0xffff))

#define CODE_OF_ATTR(head)  UPPER16U(head)
#define LENGTH_OF_ATTR(head)    LOWER16U(head)

#define CONVPROF_PROTOCOL_VERSION   1
#define CONVPROF_SUPPORTED_STYLES   2


/* 128-255: can be specified only at startup time */
#define CONVATTR_INPUT_STYLE        128
#define CONVATTR_EVENT_CAPTURE_METHOD   129
#define CONVATTR_USE_EXTENSION      255

/* argument for CONVATTR_INPUT_STYLE and CONVPROP_SUPPORTED_STYLES */
#define CONVARG_ROOTWINDOW      1L
#define CONVARG_OFFTHESPOT      2L
#define CONVARG_OVERTHESPOT     4L

/* argument for CONVATTR_EVENT_CAPTURE_METHOD */
#define CONVARG_NONE            0L
#define CONVARG_CREATE_INPUTONLY    1L
#define CONVARG_SELECT_FOCUS_WINDOW 2L

/* 0-127: can be specified at any time (startup and during conversion) */
#define CONVATTR_NONE           0
#define CONVATTR_INDIRECT       1
#define CONVATTR_FOCUS_WINDOW       2
#define CONVATTR_SPOT_LOCATION      3
#define CONVATTR_CLIENT_AREA        4
#define CONVATTR_STATUS_AREA        5
#define CONVATTR_COLORMAP       6
#define CONVATTR_COLOR          7
#define CONVATTR_BACKGROUND_PIXMAP  8
#define CONVATTR_LINE_SPACING       9
#define CONVATTR_FONT_ATOMS     10
#define CONVATTR_CURSOR         11

/**------------- THIS WAS KINPUT2 ------------------**/

#endif /* SXInputMethod_h */
