#!/usr/bin/perl
# Create a cpp file form ps.


$state = 0;
while (<>) {
    chop;
    if (/Glyph1/) {
        $state = 1;
        print ("engine->beginImage(0, 0, SString());\n");
        next;
    }
    next if ($state == 0);
    if (/bind def/) {
        last;
    }
    if (/newpath/) {
        if ($state > 1) {
            die "newpath in state $state\n";
        }
        $state = 2;
        print ("engine->newpath();\n");
    }
    next unless ($state);
    if (/(\S+) (\S+) (lineto|moveto)/) {
        print "engine->$3 ($1,$2);\n"; 
        next;
    }
    if (/(\S+) (\S+) (\S+) (\S+) (\S+) (\S+) (curveto)/) {
        print "engine->$7 ($1,$2,$3,$4,$5,$6);\n"; 
        next;
    }
    if (/closepath/) {
        print "engine->closepath();\n"; 
        next;
    }
    if (/fill/) {
        print "engine->fill(x, y, width, height, pen);\n"; 
        next;
    }
}
print "SImage* im = engine->endImage();\n"; 
print "delete engine;\n"; 
print "return im;\n"; 
