# Yudit properties
#
# A property file for yudit. If a particular property is 
# both present in 
#
#    /usr/share/yudit/yudit.properties 
#
# and in
#
#    ~/.yudit/yudit.properties
#
# the latter will be used.
#
# Format:
#   property=value
#   property=listelement1,listelement2
#   '\' is treated as a line joiner if it stands at the end of line.
#
# Caution 
#   Entries should not contain any white space.
#   Lines that start with '#' are treated as comments of the
#     line below.
#   Comments are merged too.
#   The following properties will always have system comments:
#
#    00HEADER
#    yudit.default.language
#
00HEADER=

#
# This is where the .my files are
# Yudit looks for data in the following order:
# 1. in ~/.yudit/data
# 2. this comma,separated,path, like /my/dir1,/my/dir2
# 3. /usr/share/yudit/data
yudit.datapath=

#
# This is where the .ttf (True Type font) files are
# Yudit looks for fonts in the following order:
# 1. in ~/.yudit/fonts
# 2. this comma,separated,path, like /my/dir1,/my/dir2
# 3. /usr/share/yudit/fonts
# Please use /somedir/** to extend search to subdirectories (till level5)
yudit.fontpath=~/.local/share/fonts/**,~/Library/Fonts/**,/usr/share/fonts/**,/System/Library/Fonts/**

#
# This is where the .syn (Syntax Highlighting) files are
# Yudit looks for syntax in the following order:
# 1. in ~/.yudit/syntax
# 2. this comma,separated,path, like /my/dir1,/my/dir2
# 3. /usr/share/yudit/syntax
yudit.syntaxpath=/usr/share/hunspell

# The overall background.

# The backrgound of the GUI.
yudit.background=gray50

# GUI scaling factor between 1.0..3.0
yudit.scale=1.0

# The overall foreground (text labels, e.t.c.)
yudit.label.foreground=moccasin

# The overall foreground (labels that have titles)
yudit.title.foreground=white

# The overall font, fontsize
yudit.default.font=default
yudit.default.fontsize=16

#
# The size of the window
#
yudit.default.geometry=765x510

# This is the handwriting (freehand) input. you need 'kanji.hwd' for this.
yudit.freehand.converter=kanji

#
# Put your language here. currently I have 
# am  Amharic/Ethiopia
# ar  Arabic/   
# az  Azerbaijani Turkish
# bn  Bengali        
# bg  Bulgaria/Bulgarian  
# de  Germany/German     
# cs  Czechia/Czech     
# en  US/English       
# el  Greece/Greek    
# es  Spain/Spanish  
# fi  Finland/Finnish 
# fr  France/French       
# ga  Irish
# gu  Gujarati
# hi  Hindi              
# hu  Hungary/Hungarian 
# ja  Japan/Japanese   
# ko  Korea/Korean    
# mn  Mongolia/Mongolian   
# mr  Marathi
# pa  Punjabi
# pl  Polish/Poland       
# ru  Russian/Russia     
# sl  Slovenia/Slovenian 
# sr  Yugoslavia/Serbian
# ta  Tamil            
# uk  Ukrainian       
# ur  Urdu/Pakistan   
# vi  Vietnamese/Vietnam  
# yi  Israel/Yiddish     
# zh  Chinese/HongKong  
# zh_CN Chinese (Simplified)
#
# If you leave it empty English is used.
# If you specify yudit.default.language=default, then LANG environment 
#    variable is used. Be careful - you may not have the font!
#
yudit.default.language=

#
# Things will be put on X11 clipboard with this encoding.
# This should be a compound text encoding.
#
yudit.default.clipboard.encoding=iso-2022-x11

#
# The following always get updated and written to
# ~/.yudit/yudit.properties
#
yudit.default.filetype=utf-8
yudit.default.printer.options=

#
# From version 2.5 -e will be passed to mytool -pipecmd
# Which will create a tmp file and pass it to gv.
# when gv exits the temporary file is deleted 
# On windows only preview/printing is available for now.
#
# yudit.default.preview.command=gv
# Linux
#yudit.default.preview.command=okular
# Mac
# 2022-12-29 Mac preview does not support posscript any more. 
# Use skim.
yudit.default.preview.command=evince

# The Editor background.
#
yudit.editor.background=black

#
# Syntax highlighter / spelling checker colors
#
# There is no 
#
#   yudit.editor.syntax.none.foreground
#
# For NONE and ERROR yudit.editor.left.foreground or
# yudit.editor.right.foreground is used, For ERROR
# it is underlined with yudit.editor.syntax.error.foreground
#
yudit.editor.syntax.error.foreground=red
yudit.editor.syntax.number.foreground=orange
yudit.editor.syntax.string.foreground=magenta
yudit.editor.syntax.comment.foreground=lightgray
yudit.editor.syntax.token.foreground=yellow
yudit.editor.syntax.variable.foreground=red
yudit.editor.syntax.define.foreground=cyan
yudit.editor.syntax.control.foreground=CornflowerBlue

#
# Left and right indicate text directions
#
yudit.editor.left.foreground=green
yudit.editor.right.foreground=yellow

#yudit.editor.left.foreground=black
#yudit.editor.right.foreground=blue

yudit.editor.caret.left.foreground=red
yudit.editor.caret.right.foreground=red


#
# This one is to show end of line.
#
yudit.editor.showbreak=true

#
# use true to turn word wrapping on
#
yudit.editor.wordwrap=false

#
# possible values for syntax hightighting:
#   none - no highlighting
#   simple - numbers: orange, delimiter: blue
#
yudit.editor.syntax=simple

#
# The command area
#
yudit.command.background=black
yudit.command.left.foreground=green
yudit.command.right.foreground=yellow
yudit.command.caret.left.foreground=red
yudit.command.caret.right.foreground=red


#
# Slider color
#
yudit.slider.background=#497986

#
# The fontsizes to choose from.
#
yudit.editor.fontsizes=10,12,13,14,15,16,18,20,24,28,32,38,48

#
# The fonts. All of these should be defined in yudit.font.<fontname>
#
#yudit.editor.fonts=default,TrueType,Misc,Serif,SansSerif,Box,Full,Indic-F,Indic-M
yudit.editor.fonts=default,TrueType,Serif,SansSerif,Terminal

#
# You can specify "..,None,.." here too.
# As for x-utf-8:
# en_GB makes sure you will have the iso8859-1 Compose locale.
# For other compose rules, please specify the ones you need:
# like hu_HU. Please note that only x-utf-8 can have many
# different locales.
#
# Note: On Macintosh kinput2, which appears as Kinput, is the
# native keyboard method.
#
# On 16.04 Ubuntu x-fcitx is the fcitx keyboard input.
#
# fcitx only supports preedit-under-status-under input style
#
# On Mac F11 is blocked
yudit.editor.inputs=straight,unicode,x-ibus:en_US.utf-8,Hungarian,freehand,Hangul,\
Emoji,Mnemonic,Esperanto,OldHungarian,Kana,HungarianRunes

#
# kinput2:ja_JP, for instance will appear as x-kinput2 to save room
# but it will be used as x-kinput2-ja_JP
#
# IBUS: http://code.google.com/p/ibus/
# SCIM: http://www.scim-im.org/
# interxim: http://www.oksid.ch/interxim/index.html
# 
# Note: On Macintosh kinput2, which appears as Kinput, is the
# native keyboard method.
#
# On Ubuntu fcitx only supports preedit-under-status-under input style.
#
yudit.editor.xinputs=fcitx:en_US.utf-8,ibus:en_US.utf-8,SCIM:en_US.utf-8,interxim,utf-8:en_GB,utf-8:hu_HU,kinput2:ja_JP.eucJP,xcin:zh_TW.big5,Ami:ko_KR,xcin-zh_CN.GB2312:zh_CN.GB2312

#
# The x input style for the editor window.
# Available styles: 
# preedit-root-status-root
# preedit-over-status-over
# preedit-over-status-under
# preedit-under-status-under
#
# On Ubuntu fcitx only supports preedit-under-status-under input style.
yudit.editor.xinputs.style=preedit-under-status-under

yudit.editor.fontsize=18
yudit.editor.font=default
yudit.editor.input=straight

yudit.fonts=default,TrueType,Bitmap,Japanese,Courier,Times,Helvetica,Lucida,Misc

#
# The font definitions used by yudit.editor.fonts and yudit.fonts
# Don't mess with 'default' font unless you have to. It is internally defined.
#
# - For convention always have one yudit.font.TrueType
# - Fonts additionally can have an encoding specified, the
#    ':' character is used as a separator. 
#    The following font hard-coded encoders will act as range selectors:
#
#     indic: U+0900..U+0FFF
#     deva:  U+0900..U+097F
#     beng:  U+0980..U+09FF
#     guru:  U+0A00..U+0A7F
#     gujr:  U+0A80..U+0AFF
#     orya:  U+0B00..U+0B7F
#     taml:  U+0B80..U+0BFF
#     telu:  U+0C00..U+0C7F
#     knda:  U+0C80..U+0CFF
#     mlym:  U+0D00..U+0D7F
#     sinh:  U+0D80..U+0DFF
#     thai:  U+0E00..U+0E7F
#     lao:   U+0E80..U+0EFF
#     tibt:  U+0F00..U+0FFF
#     jamo:  U+1100..U+11FF
#
# - Fonts, in addition to encoding can have attributes specified.
#    currently used attributes:
#
#     RL - Strictly Right-To-Left font
#     LR - Strictly Left-To-Right font
#
#    If not specified it is used as LR or RL and for RL mirrored glyphs 
#    it will get mirrored by software.
#
yudit.font.TrueType=Arial.ttf,ZenKakuGothicNew-Regular.ttf,gulim.ttf,ogulim.ttf:mslvt,raghu.ttf:deva,tsckanna.ttf:tscii,dc-font.ttf:mlym,MalOtf.ttf:mlym,code2000.ttf,code2001.ttf:unicode:RL,cyberbit.ttf,OldHungarian_Full.ttf:unicode:RL,yudit.ttf,TwitterColorEmoji-SVGinOT.ttf:emoji,EmojiOne_Color.otf:emoji

#
# For convention always have one yudit.font.Bitmap
#
yudit.font.Bitmap=\
-gnu-unifont-medium-r-normal--16-160-75-75-c-80-iso10646-1,*-iso8859-1,\
-sony-fixed-medium-r-normal--*-230-75-75-c-120-iso8859-1

#
# Direct rendering if you have unifont.hex.
#
yudit.font.Unifont=unifont.hex,*-iso8859-1

yudit.font.Japanese=\
*-jisx0208.1983-0,\
*-jisx0201.1976-0,\
*-jisx0212.1990-0,\
-adobe-helvetica-medium-r-normal--*-*-*-*-*-*-iso8859-1

#
# JIS X 0213 Font
# http://www.mars.sphere.ne.jp/imamura/jisx0213.html
#
yudit.font.JP2000X=\
-gnu-unifont-medium-r-normal--16-160-75-75-p-80-iso10646-1,\
*-jisx0213.2000-1,*-jisx0213.2000-2,\
*-jisx0201.1976-0,\
-adobe-helvetica-medium-r-normal--*-*-*-*-*-*-iso8859-1

#
# Serif.ttf is in fact a paid font DFMinchoPPro5-W5.ttf
#
yudit.font.Serif=Serif.ttf,times.ttf,TakaoPMincho.ttf,ipamp.ttf,cyberbit.ttf,FreeSerif.ttf,raghu.ttf:deva,OldHungarian_Full.ttf:unicode:RL,yudit.ttf,EmojiOne_Color.otf:emoji,TwitterColorEmoji-SVGinOT.ttf:emoji

#
# SansSerif.ttf is in fact a paid font  DFGothicPPro5-W5.ttf
#
yudit.font.SansSerif=SansSerif.ttf,Arialuni.ttf,TakaoPGothic.ttf,ipagp.ttf,FreeSans.ttf,raghu.ttf:deva,NotoSansOldHungarian-Regular.ttf,OldHungarian_Full.ttf:unicode:RL,yudit.ttf,EmojiOne_Color.otf:emoji,TwitterColorEmoji-SVGinOT.ttf:emoji

#
# This is a package for full coverage of testpage
# MuktiNarrow.ttf:
# http://savannah.nongnu.org/download/freebangfont/MuktiNarrow-0.94.tar.bz2
#
yudit.font.Full=hln.ttf,MuktiNarrow.ttf:beng,ani.ttf:beng,raghu.ttf:deva,dc-font.ttf:mlym,latha.ttf:taml,tunga.ttf:knda,tibt.ttf:tibt,TCRCYoutsoUnicode.ttf:tibt,raavi.ttf:guru,shruti.ttf:gujr,yudit.ttf,cyberbit.ttf,code2001.ttf,code2000.ttf,extendedWatanabeMincho.ttf:shift-jis-3,OldHungarian_Full.ttf:unicode:RL

#
# A fairly good set of free or almost free fonts. 
# As you might have noticed I used the selectors so that
# they won't mix up with each other. OpenType script name
# can be used for this purpose.
#
yudit.font.Indic-F=raghu.ttf:deva,bangla.ttf:beng,ani.ttf:beng,sampige.ttf:knda,pothana2000.ttf:telu,TCRCYoutsoUnicode.ttf:tibt,MalOtf.ttf:mlym,code2000.ttf,OldHungarian_Full.ttf:unicode:RL,yudit.ttf

#
# A set of fonts that you usually can find on MS systems.
#
yudit.font.Indic-M=mangal.ttf:deva,vrinda.ttf:beng,tunga.ttf:knda,latha.ttf:taml,gautami.ttf:telu,raavi.ttf:guru,shruti.ttf:gujr,tibt.ttf:tibt,Arialuni.ttf,OldHungarian_Full.ttf:unicode:RL,yudit.ttf

#
# Get Courier, Helvetice and Times  from Markus Kuhn's Unicode FAQ.
# Fallback same with iso9959-1.
#
yudit.font.Helvetica=\
-adobe-helvetica-medium-r-normal--*-*-75-75-p-*-iso10646-1,\
-adobe-helvetica-*-iso8859-1

yudit.font.Times=\
-adobe-times-medium-r-normal--*-*-75-75-p-*-iso10646-1,\
-adobe-times-*-iso8859-1

# Some experiment with bold
yudit.font.Courier=\
-adobe-courier-bold-r-normal--*-*-75-75-m-*-iso10646-1,\
-adobe-courier-*-iso8859-1

# Some experiment with bold
yudit.font.Lucida=\
-*-lucidabright-demibold-r-normal--*-*-75-75-p-*-iso10646-1,\
-*-lucidabright-*-r-normal-*-iso8859-1

# http://www.cl.cam.ac.uk/~mgk25/download/ucs-fonts.tar.gz

yudit.font.Bitmap=-gnu-unifont-medium-r-normal--16-160-75-75-c-80-iso10646-1,*-iso8859-1,-sony-fixed-medium-r-normal--*-230-75-75-c-120-iso8859-1

# These size 18 fonts are in ucs-fonts.tar.gz.
#yudit.font.Misc=-misc-fixed-medium-r-normal--18-120-100-100-c-90-iso10646-1,\
#-misc-fixed-medium-r-normal--18-120-100-100-c-90-iso8859-2,\
#-misc-fixed-medium-r-normal-ja-18-120-100-100-c-180-iso10646-1

#
# These size 18,13 fonts are in ucs-fonts.tar.gz - help yourself
# wget http://www.cl.cam.ac.uk/~mgk25/download/ucs-fonts.tar.gz 
# wget http://www.cl.cam.ac.uk/~mgk25/download/ucs-fonts-asian.tar.gz
# mkdir ucs-fonts; cd ucs-fonts
# tar xfz ../ucs-fonts.tar.gz 
# tar xfz ../ucs-fonts-asian.tar.gz 
# xset fp+ `pwd`
#
yudit.font.Misc=-misc-fixed-medium-r-*--18-*-*-*-c-*-iso10646-1,\
-misc-fixed-medium-r-*-ja-18-*-*-*-c-*-iso10646-1,\
-misc-fixed-medium-r-*--18-*-*-*-c-*-iso10646p2-1,\
-misc-fixed-medium-r-*-ja-18-*-*-*-c-*-iso10646p2-1,\
-misc-fixed-medium-r-*--18-*-*-*-c-*-iso10646p2-2,\
-misc-fixed-medium-r-*-ja-18-*-*-*-c-*-iso10646p2-2,\
yudit.ttf


# http://www.cl.cam.ac.uk/~mgk25/download/ucs-fonts.tar.gz
yudit.font.MiscBold=\
-misc-fixed-bold-r-normal--*-*-75-75-*-*-iso10646-1,\
-misc-fixed-bold-r-normal--*-*-75-75-*-*-iso8859-1\


#
# Get a full list from uniconv -h. Only internal encoding are added,
# The rest is appended from this list.
#
yudit.editor.filetypes=Devanagari,Bengali,Tamil,Hungarian,OldHungarian,HungarianRunes,Hangul,Bulgarian,Russian,Russian-ISO-Translit,SGML,Yiddish,Troff,Chinese-CJ,DE-RU,GreekBible,Kana,Mnemonic,RU-translit,IS_AS,IS_BN,IS_DV,IS_GJ,IS_KN,IS_ML,IS_OR,IS_PJ,IS_RM,IS_TL,IS_TM
yudit.command.font=default
yudit.command.fontsize=18

#
# This is a bitmap font, good for box drawing
# This one has descent and can not be mixed in :
# -misc-fixed-medium-r-normal-ja-18-120-100-100-c-180-iso10646-1
#
yudit.font.Box=unifont.hex,-misc-fixed-medium-r-*--18-120-100-100-c-90-iso10646-1
# A Font Set for Programmers. JetBrains fixes the Yen sign in ipag.
yudit.font.Terminal=JetBrainsMono-Medium.ttf,TakaoGothic.ttf,ipag.ttf,unifont.ttf,EmojiOne_Color.otf:emoji,OldHungarian_Full.ttf:unicode:RL,yudit.ttf
